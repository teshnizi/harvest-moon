**Harvest Moon**

A simulation game in which you could farm crops, mine stones, take care of animals, trade your goods and do many other amazing things!

**Some Scenes of the game:**

![](maps/img1.png)

![](maps/img2.png)

![](maps/House.png)
