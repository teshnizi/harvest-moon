
import javafx.scene.shape.LineTo;

import java.io.IOException;
import java.util.*;

/**
 * 
 */
public class Village extends Environment {

    private Gym gym;

    private Cafe cafe;

    private Laboratory laboratory;

    private Ranch ranch;

    private WorkShop workshop;

    private Clinic clinic;

    private CityHall cityHall;

    private Store huckster;

    public void setCityHall(CityHall cityHall) {
        this.cityHall = cityHall;
    }

    /**
     * Default constructor
     *
     * @param name
     */
    public Village(String name, Barn barn) {
        super(name);
        ArrayList<Exercise> exercises = new ArrayList<>();
        exercises.add(Producer.runningExercise());
        exercises.add(Producer.yoga());
        exercises.add(Producer.bodybuildering());
        gym = new Gym("Gym",exercises);
        cafe = new Cafe("Cafe",null);
        laboratory = new Laboratory("Laboratory", null);
        ranch = new Ranch("Ranch",barn);
        workshop = new WorkShop("WorkShop");
        clinic = new Clinic("Clinic");
        huckster = new Store("Huckster");
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }
    public void setLaboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
    }
    public void setGym(Gym gym) {
        this.gym = gym;
    }

    public Gym getGym() {
        return gym;
    }

    @Override
    public void nextDay(Season season){
        clinic.nextDay(season);
        ranch.nextDay(season);
        try {
            cafe.nextDay(season);
        } catch (IOException e) {
            e.printStackTrace();
        }
        huckster.nextDay(season);
    }

    @Override
    public Thing findObject(String objectName) {
        if (objectName.equals("Gym"))
            return gym;
        if (objectName.equals("Cafe"))
            return cafe;
        if (objectName.equals("Laboratory"))
            return laboratory;
        if (objectName.equals("Ranch"))
            return ranch;
        if (objectName.equals("WorkShop"))
            return workshop;
        if(objectName.equals("Clinic"))
            return clinic;
        if(objectName.equals("CityHall"))
            return cityHall;
        if(Person.age % 7 == 1)
            if(objectName.equals("Huckster"))
                return huckster;
        return null;
    }

    public Clinic getClinic() {
        return clinic;
    }

    @Override
    public ArrayList<String> listOfObjects() {
        ArrayList<String> s = new ArrayList<>();
        s.add("Clinic");
        s.add("Ranch");
        s.add("Cafe");
        s.add("Laboratory");
        s.add("Gym");
        s.add("WorkShop");
        s.add("CityHall");
        if (Person.age % 7 == 1)
            s.add("Huckster");
        return s;
    }


    @Override
    public  ArrayList<String> listOfPlaces(){
        ArrayList<String> ret = new ArrayList<>();

        for(JA neighbour : neighbours)
            ret.add(neighbour.getName());
        for (Place place : places)
            ret.add(place.getName());
        return ret;
    }
    

    @Override
    public JA findJA(String name) {
        for (JA x : neighbours) {
            if (x.getName().equals(name))
                return x;
        }

        for(Place place : places){
            if(place.getName().equals(name))
                return place;
        }
        return null;
    }

    public Ranch getRanch() {
        return ranch;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public WorkShop getWorkshop() {
        return workshop;
    }

    public Laboratory getLaboratory() {
        return laboratory;
    }
}