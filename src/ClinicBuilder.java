import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by pc on 7/8/2017.
 */
public class ClinicBuilder extends Application {
    static int in = 0;
    Stage mystage;
    public ClinicBuilder(Stage stage){
        this.mystage  =stage;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group mainGroup = new Group();
        Group group = new Group();
        mainGroup.getChildren().addAll(group);
        Scene scene = new Scene(mainGroup,620,700,new ImagePattern(new Image("images\\white-texture.jpg")));
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                primaryStage.close();
                mystage.show();
            }
        });
        Text text = new Text("Clinic");
        text.setX(250);
        text.setY(50);
        text.setFont(new Font(40));

        Text text2 = new Text();
        text2.setX(150);
        text2.setY(500);
        text2.setFont(Main.comicFont);

        Text text3 = new Text();
        text3.setLayoutX(150);
        text3.setLayoutY(560);
        text3.setFont(Main.comicFont);

        Rectangle rectangle1 = new Rectangle(10,10,30,30);
        rectangle1.setFill(new ImagePattern(new Image("images\\backButton.jpg")));
        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle1.setCursor(Cursor.HAND);
            }
        });
        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().clear();
               mainGroup.getChildren().addAll(group);
            }
        });

        Button button = new Button("Heal me!");
        button.setLayoutX(200);
        button.setLayoutY(150);
        button.setFont(Main.comicFont);
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setCursor(Cursor.HAND);
                if (Main.Eisenhower.getHealth().getCurrentValue() < Main.Eisenhower.getHealth().getMaxValue())
                    text2.setText("Your health level is " + Main.Eisenhower.getHealth().getCurrentValue() + " \nIf you want to healed you must pay " +(int)((Main.Eisenhower.getHealth().getCurrentValue()/Main.Eisenhower.getHealth().getMaxValue()) * 100.0) + "$");
                else
                    text2.setText("You are healthy");
            }
        });
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            Person person = Main.Eisenhower;
            @Override
            public void handle(MouseEvent event) {
                if (person.getMoney() < 100){
                    text3.setText("Not Enough Money!");
                }
                else {
                    person.getHealth().changeCurrentValue(person.getHealth().getMaxValue()  - person.getHealth().getCurrentValue() );
                    person.takeMoney((int)((Main.Eisenhower.getHealth().getCurrentValue()/Main.Eisenhower.getHealth().getMaxValue()) * 100.0));
                    text3.setText("Healing done! your money is " + person.getMoney() + "$");
                }
            }
        });
        button.setPrefSize(200,20);

        Button button1 = new Button("Check items");
        button1.setLayoutX(200);
        button1.setLayoutY(200);
        button1.setFont(Main.comicFont);
        button1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button1.setCursor(Cursor.HAND);
            }
        });
        button1.setPrefSize(200,20);
        button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().remove(group);
                Group group1 = new Group();
                insideshop(group1,scene);
                mainGroup.getChildren().add(rectangle1);
                mainGroup.getChildren().add(group1);
                rectangle1.setVisible(true);
            }
        });

        Button button2 = new Button("Buy item");
        button2.setLayoutX(200);
        button2.setLayoutY(250);
        button2.setFont(Main.comicFont);
        button2.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button2.setCursor(Cursor.HAND);
            }
        });
        button2.setPrefSize(200,20);
        button2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().remove(group);
                Group group1 = new Group();
                getfromShop(group1);
                mainGroup.getChildren().add(rectangle1);
                mainGroup.getChildren().add(group1);
                rectangle1.setVisible(true);
            }
        });

        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                text2.setText(" ");
            }
        });

        group.getChildren().addAll(text3,text ,text2,button ,button1 ,button2);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void insideshop(Group group , Scene scene){
        Text text1 = new Text("Items available in Clinic :");
        text1.setX(190);
        text1.setY(50);
        text1.setFont(new Font(25));

        Text text2 = new Text();
        text2.setX(100);
        text2.setY(500);
        text2.setFont(Main.comicFont);

        Rectangle rectangle = new Rectangle(40,40);
        rectangle.setFill(new ImagePattern(new Image("images\\Medicine.png")));
        group.getChildren().addAll(rectangle);
        rectangle.setX(200);
        rectangle.setY(50);
        rectangle.setVisible(false);
        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setVisible(false);
            }
        });

        Clinic clinic = Main.village.getClinic();
        int num = 0;
        for (ItemStack itemStack : clinic.storage.getItemStacks()){

            if (itemStack.getItems().size() == 0)
                continue;

            Button button = new Button(itemStack.getItems().get(0).getName() + " " + itemStack.getItems().size()+ " X");
            button.setLayoutX(200);
            button.setLayoutY((num%12)*40 + 100);
            button.setPrefSize(200,30);
            button.setFont(Main.comicFont);
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                    rectangle.setX(button.getLayoutX() - 50);
                    rectangle.setY(button.getLayoutY() - 5);
                    rectangle.setVisible(true);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int price = itemStack.getItems().get(0).getPrice();
                    if(clinic.isOnAuction) price = price*3/4;
                    text2.setText("1st item Price: $" + (50.0 - (double)itemStack.getItems().size())/20.0 * (double)price + " \n(total Price may be more expensive due to lack of items)");
                }
            });
            group.getChildren().addAll(button);
            num++;
        }
        group.getChildren().addAll(text1,text2);
    }
    public void getfromShop(Group group){
        Person person = Main.Eisenhower;
        Clinic clinic = Main.village.getClinic();
        int num = 0;

        Text text1 = new Text();
        text1.setX(200);
        text1.setY(450);
        group.getChildren().addAll(text1);
        text1.setFont(Main.comicFont);

        ArrayList<AbstractMap.SimpleEntry<Text,String>> map = new ArrayList<>();

        Button buy = new Button("Buy");
        buy.setLayoutX(200);
        buy.setLayoutY(620);
        buy.setFont(new Font(25));
        group.getChildren().add(buy);
        buy.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setCursor(Cursor.HAND);
            }
        });
        buy.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int total = 0;
                for (AbstractMap.SimpleEntry<Text,String> x : map){
                    int num2 = Integer.parseInt(x.getKey().getText());
                    String [] s = x.getValue().split("&");
                    int size = Integer.parseInt(s[0]);
                    int price = Integer.parseInt(s[1]);
                    ItemStack itemStackt = null;
                    for (ItemStack itemStack : clinic.storage.getItemStacks()){
                        if (itemStack.getItems().size() == 0)
                            continue;
                        if (itemStack.getItems().get(0).getName().equals(s[2])){
                            itemStackt = itemStack;
                            break;
                        }
                    }
                    if (num2 != 0) {
                        if (Main.online)
                        {
                            try {
                                DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                                dout.writeUTF(sender(itemStackt.getItems().get(0).getName(),num2,-1));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        person.getBackPack().putInItemStack(itemStackt, num2);
                    }
                    total +=(int)calculate(size,num2,price);
                }
                person.takeMoney((int)total);
                group.getChildren().clear();
                getfromShop(group);
            }
        });


        for (ItemStack itemStack : clinic.storage.getItemStacks()){

            if (itemStack.getItems().size() == 0)
                continue;

            int price = itemStack.getItems().get(0).getPrice();
            if (clinic.isOnAuction) price = price*3/4;

            Button button = new Button(itemStack.getItems().get(0).getName() + " " + itemStack.getItems().size()+ " X");
            button.setLayoutX(150);
            button.setLayoutY((num%12)*100 + 80);
            button.setPrefSize(200,30);
            button.setFont(Main.comicFont);



            Text text = new Text("0");
            text.setX(406);
            text.setY((num%12)*100 + 100);
            map.add(new AbstractMap.SimpleEntry<Text, String>(text,clinic.storage.getItemStacks().size() + "&" + price + "&" + itemStack.getItems().get(0).getName()));

            int max = Math.min(itemStack.getItems().size(),person.getMoney()/itemStack.getItems().get(0).getPrice());

            Rectangle up = new Rectangle(400,(num%12)*100 + 60,20,20);
            up.setFill(new ImagePattern(new Image("images\\Up.png")));
            up.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    up.setCursor(Cursor.HAND);
                }
            });
            up.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int num1 = Integer.parseInt(text.getText());
                    if (num1<max) {
                        num1++;
                        text.setText(String.valueOf(num1));
                        int total = 0;
                        for (AbstractMap.SimpleEntry<Text,String> x : map){
                            int num2 = Integer.parseInt(x.getKey().getText());
                            String [] s = x.getValue().split("&");
                            int size = Integer.parseInt(s[0]);
                            int price = Integer.parseInt(s[1]);
                            total +=(int)calculate(size,num2,price);
                        }
                        if (total > person.getMoney())
                        {
                            num1-- ;
                            text.setText(String.valueOf(num1));
                        }
                        else {
                            text1.setText(" ");
                            text1.setText("Total price : " +String.valueOf(total) + "$");
                        }
                        text.setText(String.valueOf(num1));
                    }
                }
            });
            Rectangle down = new Rectangle(400,(num%12)*100 + 105,20,20);
            down.setFill(new ImagePattern(new Image("images\\Down.jpg")));
            down.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    down.setCursor(Cursor.HAND);
                }
            });
            down.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int num1 = Integer.parseInt(text.getText());
                    if (num1>0) {
                        num1--;
                        text.setText(String.valueOf(num1));
                        int total = 0;
                        for (AbstractMap.SimpleEntry<Text,String> x : map){
                            int num2 = Integer.parseInt(x.getKey().getText());
                            String [] s = x.getValue().split(" ");
                            int size = Integer.parseInt(s[0]);
                            int price = Integer.parseInt(s[1]);
                            total +=(int)calculate(size,num2,price);
                        }
                        text1.setText(" ");
                        text1.setText("Total price : " +String.valueOf(total) + "$");
                    }
                }
            });
            group.getChildren().addAll(button,text,up,down);
            num++;
        }
    }
    public double calculate(int size , int num ,int price){
        int totalNum = size;
        double totalPrice = 0;
        for (int i = totalNum ; i > totalNum - num ; i--)
            totalPrice += (50.0 - (double)i)/20.0 * (double)price; //  0 <= i <= 40 take a look at class Store
        return totalPrice;
    }
    public String sender(String itemname, int num , int type)
    {
        String s = null;
        if (type == 1)
           s=Protocol.add ;
        if (type == -1)
            s = Protocol.sub;
        String ans =  Getter.socketname + Protocol.protocol + Protocol.clinic + Protocol.protocol+itemname + Protocol.protocol + num + Protocol.protocol + s ;
        return ans;
    }
}
