import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/15/2017.
 */
public class AnimalMenu extends Menu {
    Animal animal;
    public AnimalMenu(Animal animal) {
        this.animal = animal;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("This is : " + animal.getName());
        s.add("1.Status");
        s.add("2.Feed " + animal.getName());
        s.add("3.Heal " + animal.getName());
        s.add("4.Get outcome");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        try {

            if (x == 1) {
                System.out.println(animal.toString());
                return null;
            }
            if (x == 2) {

                System.out.println("Choose a " + animal.getFoodName());
                ItemStack itemStack = person.getBackPack().getItemStackFromPlayer();
                if(! itemStack.getItems().get(0).getName().equals(animal.getFoodName())){
                    System.out.println("You should choose " + animal.getFoodName());
                    return null;
                }
                Item food = itemStack.getItems().get(0);
                if(person.getEnergy().getCurrentValue() <= 2){
                    System.out.println("Not enough energy!");
                    return null;
                }
                person.getEnergy().changeCurrentValue(-2);
                animal.Feed();
                person.getBackPack().takeItem(food.getName(),1);
                System.out.println("Animal Fed!");
            }

            if(x == 3){
                System.out.println("Choose an animal Medicine:");
                ItemStack itemStack = person.getBackPack().getItemStackFromPlayer();
                if(! (itemStack.getItems().get(0) instanceof AnimalMedicine)){
                    System.out.println("You should choose an animal medicine!");
                    return null;
                }
                person.getBackPack().takeItem("AnimalMedicine",1);
                animal.heal();
                animal.isSeek = false;
                System.out.println(animal.getName() + " healed!");
                return null;
            }

            if(x == 4){
                int spaceNeeded = 0;
                for(ItemStack itemStack : animal.getOutcome()){
                    spaceNeeded += itemStack.getItems().get(0).getSize();
                }
                if(person.getBackPack().getRemainingCapacity() < spaceNeeded){
                    System.out.println("Not enough place!");
                    return null;
                }
                if(animal.getOutcome() == null || animal.getOutcome().size() == 0){
                    System.out.println("No outcome available!");
                    return null;
                }
                if(animal.getToolName() != null) {
                    System.out.println("Choose " + animal.getToolName() + " : ");
                    ItemStack it = person.getBackPack().getItemStackFromPlayer();
                    if (!(it.getItems().get(0) instanceof Tool)) {
                        System.out.println("You should choose " + animal.getToolName());
                        return null;
                    }
                    Tool tool = (Tool) it.getItems().get(0);
                    if (!tool.getName().equals(animal.getToolName())) {
                        System.out.println("You should choose " + animal.getToolName());
                        return null;
                    }
                    if (tool.health <= 0) {
                        System.out.println("this tool is broken!");
                        return null;
                    }
                    if (!tool.canUseTool()) {
                        System.out.println("You are too tired!");
                        return null;
                    }
                    tool.abilityChange.affect();
                }
                if (animal.getToolName() == null){
                    if (person.getEnergy().getCurrentValue() < 2){
                        System.out.println("You are too tired!");
                        return null;
                    }
                }
                for (ItemStack itemStack : animal.getOutcome()) {
                    System.out.println(itemStack.getItems().get(0).getName());
                    person.getBackPack().putInItemStack(itemStack,itemStack.getItems().size());
                }
                System.out.println("Collected!");
            }
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
