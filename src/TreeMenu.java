import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Tigerous215 on 5/13/2017.
 */
public class TreeMenu extends Menu {

    public TreeMenu(Tree tree) {
        this.tree = tree;
    }

    Tree tree;

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("This is a tree: " + tree.getName());
        s.add("1.Status");
        s.add("2.Water");
        s.add("3.Take Fruits");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        Storage backPack = person.getBackPack();
        if( x == 1 ){
            System.out.println(tree.toString());
        }
        if( x == 2 ){
            try {
                System.out.println("Choose A watering Can:");
                ItemStack tmp = backPack.getItemStackFromPlayer();
                WateringCan wateringCan = (WateringCan) tmp.getItems().get(0);
                if (wateringCan.getHealth() == 0) {
                    System.out.println("Watering can is broken!");
                    return null;
                }
                if (wateringCan.getWaterLevel() >= 1 && wateringCan.canUseTool()) {
                    wateringCan.water();
                    tree.water();
                    System.out.println("Watering Done!!");
                } else {
                    System.out.println("Watering Can is empty!");
                }
            } catch (Exception e) {
                System.out.println("You should choose a watering can!");
            }
        }
        if( x == 3 ){
            if(tree.age < tree.matureAge){
                System.out.println("This tree is too young to take fruits from");
                return null;
            }
            try{
                ItemStack outcome = tree.getOutCome();
                System.out.println(outcome.toString());
                backPack.putInItemStack(outcome, outcome.getItems().size());
            }
            catch (Exception e){
                System.out.println("BadCommand!");
            }
        }
        return null;
    }
}
