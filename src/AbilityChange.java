
import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class AbilityChange implements Serializable {

    /**
     * Default constructor
     */

    public AbilityChange(Ability ability, int maxValueChange, int currentValueChange, int refillRateChange, int consumptionRateChange) {
        this.currentValueChange = currentValueChange;
        this.maxValueChange = maxValueChange;
        this.refillRateChange = refillRateChange;
        this.ability = ability;
        this.consumptionRateChange = consumptionRateChange;
    }

    public AbilityChange(Ability ability, int maxValueChange, int currentValueChange, int refillRateChange) {
        this.currentValueChange = currentValueChange;
        this.maxValueChange = maxValueChange;
        this.refillRateChange = refillRateChange;
        this.ability = ability;
    }

    Ability ability;
    public Ability getAbility() {
        return ability;
    }


    private double maxValueChange;

    private double currentValueChange;

    private double refillRateChange;

    private double consumptionRateChange;

    public double getCurrentValueChange() {
        return currentValueChange;
    }

    public double getMaxValueChange() {
        return maxValueChange;
    }

    public double getRefillRateChange() {
        return refillRateChange;
    }

    public boolean canAffect(){
        if(ability.getCurrentValue() + currentValueChange >= 0)
            return true;
        return false;
    }
    public void affect(){
        ability.changeCurrentValue(currentValueChange);
        ability.changeMaxValue(maxValueChange);
        ability.changeRefillRate(refillRateChange);
    }
    @Override
    public String toString(){
        String s = ability.getName() + "  CVC: " + currentValueChange + " MVC : " + maxValueChange + " RRC:" + refillRateChange;
        return s;
    }
}