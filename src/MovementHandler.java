import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Pair;

import java.util.HashSet;

/**
 * Created by tigerous215 on 7/6/2017.
 */
public class MovementHandler {

    static void move(int dir, HashSet hashSet, ImageView character){
        Main.Eisenhower.getStamina().changeCurrentValue(-1);
        if (Main.Eisenhower.getStamina().getCurrentValue() <= 0)
            Main.Eisenhower.canMove = false;

        Pair<Integer, Integer> next = pixelToTile(character.getX(),character.getY());
        switch (dir){
            case 0:
                next = new Pair<>(next.getKey()+1, next.getValue()  );
                break;
            case 1:
                next = new Pair<>(next.getKey()  , next.getValue()-1);
                break;
            case 2:
                next = new Pair<>(next.getKey()-1, next.getValue()  );
                break;
            case 3:
                next = new Pair<>(next.getKey()  , next.getValue()+1);
                break;
        }
        //System.out.println(hashSet.size());
        if (!hashSet.contains(next))
            return;
        //System.out.println(next.getKey() + " " + next.getValue());
        next = tileToPixel(next.getKey(), next.getValue());
        character.setX(next.getKey());
        character.setY(next.getValue());
    }
    public static Pair<Integer, Integer> tileToPixel(int x, int y){
        return new Pair<>(x*32, y*32 - 14);
    }
    public static Pair<Integer, Integer> pixelToTile(double x, double y){
        return new Pair<>((int)x/32, (int)(y+14)/32);
    }
}
