
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Machine extends Instrument implements Serializable {

    /**
     * Default constructor
     */
    public Machine(String name, int size, ArrayList<ItemStack> inputItems, ArrayList<ItemStack> outputItems, int price, String status) {
        super(name, size, price);
        this.inputItems = inputItems;
        this.outPutItems = outputItems;
        this.status = status;
    }

    private ArrayList<ItemStack> inputItems;
    private ArrayList<ItemStack> outPutItems;

    public ArrayList<ItemStack> getInputItems() {
        return inputItems;
    }

    public ArrayList<ItemStack> getOutPutItems() {
        return outPutItems;
    }

    public int outputSize(){
        int x = 0;
        for(ItemStack itemStack : outPutItems){
            x += itemStack.getItems().get(0).getSize();
        }
        return x;
    }

    String status;

    @Override
    public String status(){
        return status;
    }

}