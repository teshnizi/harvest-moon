
import java.util.*;

/**
 * 
 */
public class Recipe extends Item {

    /**
     * Default constructor
     */
    public Recipe(String name) {
        super(name,1);
    }


    private ArrayList<ItemStack> tools = new ArrayList<>();

    private ArrayList<ItemStack> ingredients = new ArrayList<>();

    public ArrayList<ItemStack> getTools() {
        return tools;
    }

    public ArrayList<ItemStack> getIngredients() {
        return ingredients;
    }

    Meal meal;
    @Override
    public String toString(){
        String s = "Recipe of " + getName() + "\nTools needed for cooking this Food:\n";
        for (ItemStack tool: tools)
            s = s + tool.getItems().get(0).getName() + "\n";
        s = s+"Ingredients needed for cooking this food:\n";
        for(ItemStack food : ingredients)
            s = s + food.getItems().get(0).getName() + " x" + food.getItems().size()+"\n";
        return s;
    }

    public void Cook(Person person, ArrayList<ItemStack> toolShelf) {
        for (ItemStack toolStack : tools){
            boolean hasItem = false;
            for(ItemStack itemStack : toolShelf){
                if(itemStack.getItems().get(0).getName().equals(toolStack.getItems().get(0).getName())){
                    Tool tool = (Tool)itemStack.getItems().get(0);
                    System.out.println(tool.getName());
                    hasItem = true;
                    if(tool.abilityChange != null && !tool.canUseTool()){
                        System.out.println("You are too tired!");
                        return;
                    }
                    if (tool.getHealth() <= 0 ){
                        System.out.println(tool.getName() + " is Broken!");
                        return;
                    }
                }
            }
            if(!hasItem){
                System.out.println(toolStack.getItems().get(0).getName() + " not found in shelf!");
                return;
            }
        }
        for (ItemStack ingredient : ingredients){
            System.out.println("Choose " +  ingredient.getItems().get(0).getName() + " x" + ingredient.getItems().size());
            ItemStack ing = person.getBackPack().getItemStackFromPlayer();
            if(!ing.getItems().get(0).getName().equals(ingredient.getItems().get(0).getName())){
                System.out.println("You should choose " + ingredient.getItems().get(0).getName() + " !");
                return;
            }
            if(ing.getItems().size() < ingredient.getItems().size()){
                System.out.println("Not enough ingredients!");
                return;
            }
        }
        for (ItemStack ingredient: ingredients){
            person.getBackPack().takeItemStack(ingredient);
        }

        for (ItemStack toolStack : tools) {
            for (ItemStack itemStack : toolShelf) {
                if (itemStack.getItems().get(0).getName().equals(toolStack.getItems().get(0).getName())) {
                    Tool tool = (Tool) itemStack.getItems().get(0);
                    tool.health--;
                }
            }
        }
        person.getBackPack().putInItemStack(new ItemStack(new Meal(meal.getName(), meal.getSize(), meal.getPrice(), meal.getEffect())), 1);
        System.out.println("Cooking done!");
    }
}