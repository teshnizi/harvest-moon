import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pc on 5/14/2017.
 */
public class WoodMenu extends Menu {
    WoodType woodType;
    public WoodMenu(WoodType woodType){
        this.woodType = woodType;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("1.Collect Branch");
        s.add("2.Collect Old Lumber");
        s.add("3.Collect Pine Lumber");
        s.add("4.Collect Oak Lumber");
        return s;
    }
    public ItemStack getwood(int i){
        int num = 0;
        for (ItemStack itemStack : woodType.getWoods()){
            if (num == i)
                return itemStack;
            num ++;
        }
        return null;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        ArrayList<Item> items = new ArrayList<>();
        ArrayList<Tool> axis  = new ArrayList<>();
        if (x == 1){
            Storage storage =person.getBackPack();
            int num =(int) (Math.min(5 ,getwood(0).getItems().size() ) * getwood(0).getItems().size()/50.0);
            if (num > 0) {
                if (storage.putInItemStack(getwood(0), num) == 1) {
                    System.out.println(num + " Branch where collected. \nEnergy usage : 30");
                    person.getEnergy().changeCurrentValue(-30);
                }
            }
            else
                System.out.println("Could not find anything. Better Luck next time");
        }
       else if (x == 2){
            Storage storage = person.getBackPack();
            try {
                System.out.println("Choose an axe from your inventory. It has to be a Stone (or a stronger kind) Axe.");
                ItemStack itemStack = storage.getItemStackFromPlayer();
                Axe axe = (Axe) itemStack.getItems().get(0);
                int energy = (-1) * (int) (30.0 * Math.pow(2,x-1)/Math.pow(1.6,axe.getType()));
                if (axe.getType() >= 1){
                    int increase = axe.getType() - 1;
                    int rand =(int) (5.0 * Math.pow(2,increase)) ;
                    Random random = new Random();
                    rand = random.nextInt(rand + 1);
                    int num = (int)Math.min((rand)*((double)(getwood(1).getItems().size()/50.0)),getwood(1).getItems().size());
                    if (num == 0)
                    {
                        System.out.println("Could not find anything. Better Luck next time");
                        return null;
                    }
                    if (person.getEnergy().getCurrentValue() + energy >=0) {
                        if (storage.putInItemStack(getwood(1), num) == 1) {
                            System.out.println(num + " Old Lumber where collected ." + "\nEnergy usage : " + (-1)*energy* num );
                            person.getEnergy().changeCurrentValue(energy * num);
                            axe.cut();
                        }
                    }
                    else {
                        System.out.println("You are too tiered");
                    }
                    }
                    else
                        System.out.println("Chose Stronger one");
            }
            catch (Exception e){
                System.out.println("you should chose axe");
            }
        }
        else if (x == 3){
            Storage storage = person.getBackPack();
            try {
                System.out.println("Choose an axe from your inventory. It has to be a Iron (or a stronger kind) Axe.");
                ItemStack itemStack = storage.getItemStackFromPlayer();
                Axe axe = (Axe) itemStack.getItems().get(0);
                int energy = (-1) * (int) (30.0 * Math.pow(2,x-1)/Math.pow(1.6,axe.getType()));
                if (axe.getType() >= 2){
                    if (person.getEnergy().getCurrentValue() + energy >=0) {
                        int increase = axe.getType() - 2;
                        int rand =(int) (5.0 * Math.pow(2,increase)) ;
                        Random random = new Random();
                        rand = random.nextInt(rand + 1);

                        int num = (int)Math.min((rand)*((double)(getwood(2).getItems().size()/50.0)), getwood(2).getItems().size());
                        if (num == 0){
                            System.out.println("Could not find anything. Better Luck next time");
                            return null;
                        }
                        if (storage.putInItemStack(getwood(2), num) == 1) {
                            System.out.println( num + " Pine Lumber where collected .\n" + "Energy usage : " + (-1)*energy* num );
                            person.getEnergy().changeCurrentValue(energy * num);
                            axe.cut();
                        }
                        else
                        System.out.println("you are too tiered");
                    }
                }
                else
                    System.out.println("Chose stronger one");
            }
            catch (Exception e){
                System.out.println("you should chose axe");
            }
        }
        else if (x == 4){
            Storage storage = person.getBackPack();
            try {
                System.out.println("Choose an axe from your inventory. It has to be a Silver (or a stronger kind) Axe.");
                ItemStack itemStack = storage.getItemStackFromPlayer();
                Axe axe = (Axe) itemStack.getItems().get(0);
                if (axe.getType() >= 3){
                    int energy = (-1) * (int) (30.0 * Math.pow(2,x-1)/Math.pow(1.6,axe.getType()));
                    if (person.getEnergy().getCurrentValue() + energy >=0) {
                        int increase = axe.getType() - 3;
                        int rand =(int) (5.0 * Math.pow(2,increase)) ;
                        Random random = new Random();
                        rand = random.nextInt(rand + 1);

                        int num = (int) Math.min((rand)*((double)(getwood(3).getItems().size()/50.0)), getwood(3).getItems().size());

                        if (num == 0)
                        {
                            System.out.println("Could not find anything. Better Luck next time");
                            return null;
                        }
                        if (storage.putInItemStack(getwood(3),num) == 1) {
                            System.out.println(num + " Oak Lumber where collected \n" + "Energy usage : " + (-1) * energy* num);
                            person.getEnergy().changeCurrentValue(energy * num);
                            axe.cut();
                        }
                    }
                    else
                        System.out.println("yoo too tiered");
                }
                else
                    System.out.println("Chose stronger one");
            }
            catch (Exception e){
                System.out.println("you should chose axe");
            }
        }
        else
            System.out.println("Bad commands!");
        return null;
    }
}
