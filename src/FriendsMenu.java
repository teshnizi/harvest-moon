import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;

/**
 * Created by pc on 7/16/2017.
 */
public class FriendsMenu extends Application {
    Stage stage;
    Scene my;
    public FriendsMenu(Stage stage,Scene myscne){
        this.stage = stage;
        this.my = myscne;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,300,485,new ImagePattern(new Image("images\\white-texture.jpg")));

        int num = 0;
        for (String string : Person.friends){

            Button button = new Button(string);
            group.getChildren().add(button);
            button.setLayoutX(70);
            button.setLayoutY(100 + num*50);
            button.setFont(Main.comicFont);
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Inspectbuilder inspectbuilder = new Inspectbuilder(button.getText());
                    try {
                        inspectbuilder.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            num ++;
        }


        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
