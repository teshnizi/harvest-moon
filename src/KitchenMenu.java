import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by pc on 5/10/2017.
 */
public class KitchenMenu extends Menu {
    Kitchen kitchen;
    KitchenMenu(Kitchen kitchen){
        this.kitchen = kitchen;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if( x == 2 ){
            ToolShelfMenu toolShelfMenu = new ToolShelfMenu(kitchen.getToolShelf());
            return toolShelfMenu;
        }
        if(x == 3){
            for(Recipe recipe : kitchen.getRecipes()){
                System.out.println("-------------------");
                System.out.println(recipe.toString());
                System.out.println("-------------------");
            }
        }
        if( x == 1 ){
            System.out.println("Choose a meal:");
            int ID = 0;
            for (Recipe recipe : kitchen.getRecipes()){
                System.out.println((++ID) + "." + recipe.getName());
            }
            Scanner scanner = new Scanner(System.in);
            ID = scanner.nextInt();
            try {
                Recipe recipe = kitchen.getRecipes().get(ID-1);
                recipe.Cook(person,kitchen.getToolShelf());
            }
            catch (Exception E){
                System.out.println("123BadCommand!");
            }
        }
        return null;
    }
    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in kitchen :");
        s.add("1.Cook a meal");
        s.add("2.Check toolShelf");
        s.add("3.Check recipes");
        return s;
    }
}
