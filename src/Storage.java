
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Ellipse;
import javafx.util.Pair;

import java.security.Key;
import java.util.*;

/**
 * 
 */
public class Storage extends Thing {

    /**
     * Default constructor
     */

    boolean isBeingShown = false;



   public ListView viewList(){
       ListView<String> listView = new ListView();
       listView.setLayoutX(15);
       listView.setLayoutY(15);
       for(ItemStack itemStack : itemStacks)
           listView.getItems().addAll(itemStack.toString());
       return listView;
   }

    public Storage(int capacity) {
        this.capacity = capacity;
    }

    private ArrayList<ItemStack> itemStacks = new ArrayList<>();

    public ArrayList<String> itemsInsideStorage() {
        ArrayList<String> s = new ArrayList<>();
        for (ItemStack itemStack : itemStacks)
            for (Item item : itemStack.getItems()) {
                s.add(item.getName());
            }
        return s;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public ArrayList<ItemStack> getItemStacks() {
        return itemStacks;
    }

    public ItemStack findItemStack(String name) {
        for (ItemStack itemStack : itemStacks) {
            if(itemStack.getItems() != null && itemStack.getItems().get(0).getName().equals(name)){
                    return itemStack;
            }
        }
        return null;
    }

    //a method which deletes empty ItemStacks from storage
    public void synchronizeStorage(){
        ArrayList<ItemStack> emptyStacks = new ArrayList<>();
        for(ItemStack itemStack : itemStacks){
            if (itemStack == null || itemStack.getItems() == null || itemStack.getItems().size()==0){
                emptyStacks.add(itemStack);
            }
        }

            for (ItemStack itemStack : emptyStacks) {
                itemStacks.remove(itemStack);
            }
    }

    public int getRemainingCapacity() {
        synchronizeStorage();
        int filledCapacity = 0;
        for(ItemStack itemStack : itemStacks){
            filledCapacity += itemStack.getItems().get(0).getSize();
        }
        return (capacity - filledCapacity);
    }

    private int capacity;

    public Storage(String name, int capacity) {
        super(name);
        this.capacity = capacity;
    }


    public ItemStack getItemStackFromPlayer() {

        synchronizeStorage();

        for (int k = 0; k < itemStacks.size(); k++) {
            System.out.println((k + 1) + "." + itemStacks.get(k).toString());
        }

        Scanner scanner = new Scanner(System.in);
        try {
            int ID = scanner.nextInt();
            ItemStack itemStack = itemStacks.get(ID - 1);
            return itemStack;
        } catch (Exception E) {
            System.out.println("BadCommand!");
            return null;
        }
    }

    /**
     * @param
     * @return
     */

    //this function automatically takes numberToTransfer from source object and adds it to destination storage.

    public int putInItemStack(ItemStack itemStack, int numberToTransfer) {
        synchronizeStorage();
        if(itemStack == null || itemStack.getItems().size() == 0){
            System.out.println("No item found!");
            return 0;
        }
        if(numberToTransfer > itemStack.getItems().size()){
            System.out.println("Not enough property!");
            return 0;
        }

        if(!(itemStack.getItems().get(0) instanceof Tool)) {
            //to check if an stack with current object already exists in storage
            for (ItemStack itSt : itemStacks) {
                if (itSt.getItems().get(0).getName().equals(itemStack.getItems().get(0).getName())) {
                    for (int cnt = 0; cnt < numberToTransfer; cnt++) {
                        itSt.getItems().add(itemStack.getItems().get(itemStack.getItems().size() - 1));
                        itemStack.getItems().remove(itemStack.getItems().size() - 1);
                    }
                    return 1;
                }
            }
        }

        //if no ItemStack with this item type exists in storage:

        if(getRemainingCapacity() >= itemStack.getItems().get(0).getSize()){
            ItemStack newItemStack = new ItemStack();
            for (int cnt = 0; cnt < numberToTransfer; cnt++){
                newItemStack.getItems().add(itemStack.getItems().get(itemStack.getItems().size()-1));
                itemStack.getItems().remove(itemStack.getItems().size()-1);
            }
            itemStacks.add(newItemStack);
            return 1;
        }
        System.out.println(getRemainingCapacity() + " " + itemStack.getItems().get(0).getSize());
        System.out.println("Not enough space in backpack!");
        return 0;
    }

    public void putInItem(Item item){
        ArrayList<Item> a = new ArrayList<>();
        a.add(item);
        ItemStack itemStack = new ItemStack(a);
        putInItemStack(itemStack,1);
    }

    public void takeItemStack(ItemStack itemStack){
        synchronizeStorage();
        if(itemStack == null || itemStack.getItems() == null || itemStack.getItems().size() == 0){
            return;
        }
        for(ItemStack it : itemStacks){
            if (it.getItems().get(0).equals(itemStack.getItems().get(0))){
                if(it.getItems().size() < itemStack.getItems().size()) {
                    System.out.println("Not enough items!");
                    return;
                }
                for(int i =  0 ; i < itemStack.getItems().size(); i++){
                    it.getItems().remove(it.getItems().size()-1);
                }
                return;
            }
        }
    }

    public int numberOf(String name){
        synchronizeStorage();
        if(name == null){
            return 0;
        }
        for(ItemStack it : itemStacks){
            if (it.getItems().get(0).getName().equals(name)){
                return it.getItems().size();
            }
        }
        return 0;
    }


    public ItemStack takeItem(String name, int num){
        synchronizeStorage();
        if(name == null){
            return null;
        }
        for(ItemStack it : itemStacks){
            if (it.getItems().get(0).getName().equals(name)){
                if(it.getItems().size() < num) {
                    System.out.println("Not enough items!");
                    return null;
                }
                ItemStack ret = new ItemStack();
                for(int i =  0 ; i < num; i++){
                    ret.getItems().add(it.getItems().get(it.getItems().size()-1));
                    it.getItems().remove(it.getItems().size()-1);
                }
                synchronizeStorage();
                return ret;
            }
        }
        return null;
    }
}