import java.util.ArrayList;

/**
 * Created by pc on 5/15/2017.
 */
public class Axe extends Tool {


    public Axe (String name, int health, int breakingProbablity, AbilityChange abilityChange ,int type){
       super(name, 3,health, breakingProbablity, abilityChange);
       this.type = type;
       newbuilders = new ArrayList<>();
       repairbuilders = new ArrayList<>();
        if (type == 1){
            newprice = 200;
            newbuilders.add(new Builder(10,Producer.wood(0),newprice));
            newbuilders.add(new Builder(5,Producer.stone(0),newprice));
            repairprice = 20;
            repairbuilders.add(new Builder(5,Producer.wood(0),repairprice));
            repairbuilders.add(new Builder(2,Producer.stone(0),repairprice));
        }
        if (type == 2){

            newprice = 800;
            newbuilders.add(new Builder(8,Producer.wood(1),newprice));
            newbuilders.add(new Builder(4,Producer.stone(1),newprice));

            repairprice = 80;
            repairbuilders.add(new Builder(4,Producer.wood(1),repairprice));
            repairbuilders.add(new Builder(2,Producer.stone(1),repairprice));
        }
        if (type == 3){

            newprice = 2000;
            newbuilders.add(new Builder(6,Producer.wood(2),newprice));
            newbuilders.add(new Builder(3,Producer.stone(2),newprice));

            repairprice = 200;
            repairbuilders.add(new Builder(3,Producer.wood(2),repairprice));
            repairbuilders.add(new Builder(1,Producer.stone(2),repairprice));
        }
        if (type == 4){

            newprice = 7000;
            newbuilders.add(new Builder(4,Producer.wood(3),newprice));
            newbuilders.add(new Builder(2,Producer.stone(3),newprice));

            repairprice = 700;
            repairbuilders.add(new Builder(2,Producer.wood(3),repairprice));
            repairbuilders.add(new Builder(1,Producer.stone(3),repairprice));
        }
        setPrice(newprice);
    }

    public ArrayList<Builder> getNewbuilders() {
        return newbuilders;
    }

    public ArrayList<Builder> getRepairbuilders() {
        return repairbuilders;
    }

    @Override
    public String status() {
        String kind = " ";
        String s;
        int num = 0;
        String broken;
        if (health > 0)
            broken = " Not Broken!";
        else
            broken = "Broken!";
        if (type == 1)
        {
            kind = "Stone";
            num = 150 ;
        }
        if (type == 2)
        {
            kind = "Iron";
            num = 80 ;
        }
        if (type == 3)
        {
            kind = "Silver";
            num = 40 ;
        }
        if (type == 4)
        {
            kind = "Adantanium";
            num = 20 ;
        }
        s = "A " +kind + " axe.\nEnergy required for each use : " + num + "\n" +"Health: "+ health + "\n"  + broken;
        return s;
    }
    public void cut(){
        health -= (5 - type)*5;
    }

    public int getType() {
        return type;
    }

   @Override
    public ArrayList<String> madeof(){
        ArrayList<String > s = new ArrayList<>();
        if (type == 1)
            s.add("this is Stone Axe and made of");
        if (type == 2)
            s.add("this is Iron Axe and made of");
        if (type == 3)
            s.add("this is Silver Axe and made of");
        if (type == 4)
            s.add("this is Adamantium Axe and made of");
        for (Builder builder : newbuilders)
            s.add(builder.getItems().getName() + " x" + builder.getNum());
       s.add("price is: "+newbuilders.get(0).getPrice());
        return  s;
    }
}
