
import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.scene.shape.Rectangle;

import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public abstract class Plant extends Creature implements Serializable{

    /**
     * Default constructor
     */
    public Plant(String name, Season season, int matureAge, int dailyWaterNeed, ItemStack outCome) {
        super(name);
        waterLevel = 0;
        this.season = season;
        this.outCome = outCome;
        this.matureAge = matureAge;
        this.dailyWaterNeed = dailyWaterNeed;
    }

    ItemStack outCome = new ItemStack();
    Boolean alive = true;

    @Override
    public void nextDay(Season season){
        if(season != this.season)
            this.alive = false;
        waterLevel -= dailyWaterNeed;
        if (waterLevel > 0)
            waterLevel = 0;
        age++;
    }

    public void checkIfIsAlive(){
        if(waterLevel < -1) {
            alive = false;
            System.out.println("WaterLevel");
        }
    }

    public ItemStack getOutCome() {
        return outCome;
    }
    public void setOutCome(ItemStack outCome){
        this.outCome = outCome;
    }

    public int getDailyWaterNeed() {
        return dailyWaterNeed;
    }

    int dailyWaterNeed;
    int waterLevel;
    int matureAge;
    Season season;

    public void water() {
        waterLevel++;
    }

}