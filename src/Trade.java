import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by pc on 7/16/2017.
 */
public class Trade extends Application {
    String name;
    static Button accept = new Button("Accept");
    static  boolean mylock = false , youlock = false , myaccept = false , youaccept = false;
    static ArrayList<AbstractMap.SimpleEntry<Text,String>> map = new ArrayList<>();
    public Trade(String s){
        name = s;
        mylock = false;
        youlock = false;
        myaccept = false ;
        youaccept = false;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,620,700,new ImagePattern(new Image("images\\white-texture.jpg")));

        primaryStage.setTitle(Getter.socketname);

        Image upimage = new Image("images\\Up.png");
        Image downimage = new Image("images\\Down.jpg");

        Person person = Main.Eisenhower;
        Storage backpack = person.getBackPack();
        backpack.synchronizeStorage();
        int num =0;




        Text text3 = new Text();
        text3.setX(500);
        text3.setY(550);
        group.getChildren().addAll(text3);
        text3.setFont(Main.comicFont);

        accept.setLayoutX(250);
        accept.setLayoutY(600);
        accept.setFont(Main.comicFont);
        accept.setVisible(false);
        group.getChildren().add(accept);


        Button sell1 = new Button("Lock in");
        sell1.setLayoutX(250);
        sell1.setLayoutY(550);
        sell1.setFont(Main.comicFont);
        group.getChildren().add(sell1);
        sell1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sell1.setCursor(Cursor.HAND);
            }
        });
        map.clear();
        for (ItemStack itemStack : backpack.getItemStacks()){
            Button button = new Button(itemStack.getItems().get(0).getName());
            button.setLayoutX(num/10*200 + 20);
            button.setLayoutY(num%10 *50 + 50);
            button.setPrefSize(140,20);

                Text text = new Text("0");
                text.setX(button.getLayoutX() + 155);
                text.setY(button.getLayoutY() + 17);
                int sellPrice = itemStack.getItems().get(0).getPrice() * 3 / 4;
                map.add(new AbstractMap.SimpleEntry<Text, String>(text,sellPrice + "&" + itemStack.getItems().size() + "&" + itemStack.getItems().get(0).getName()));

                Rectangle up = new Rectangle(button.getLayoutX() + 153 ,button.getLayoutY() - 10,10,10);
                up.setFill(new ImagePattern(upimage));
                up.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        up.setCursor(Cursor.HAND);
                    }
                });
                up.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int num1 = Integer.parseInt(text.getText());
                        int max = itemStack.getItems().size();
                        if (num1 < max) {
                            num1++;
                            text.setText(String.valueOf(num1));
                            int total = 0;
                            text.setText(String.valueOf(num1));
                        }
                    }
                });
                Rectangle down = new Rectangle(button.getLayoutX() + 153 ,button.getLayoutY() + 20,10,10);
                down.setFill(new ImagePattern(downimage));
                down.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        down.setCursor(Cursor.HAND);
                    }
                });
                down.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int num1 = Integer.parseInt(text.getText());
                        if (num1>0) {
                            num1--;
                            text.setText(String.valueOf(num1));
                        }
                    }

                });
                group.getChildren().addAll(button,up,down,text);
                num ++;
        }
        sell1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int total = 0;
                int num3 = 0;
                mylock = true;
                try {
                    DataOutputStream dout1 = new DataOutputStream(Getter.socket.getOutputStream());
                    dout1.writeUTF(Getter.socketname + Protocol.protocol + Protocol.youlock + Protocol.protocol + name);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (youlock)
                    accept.setVisible(true);


            }
        });
        accept.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                myaccept = true;

                try {
                    DataOutputStream dout1 = new DataOutputStream(Getter.socket.getOutputStream());
                    dout1.writeUTF(Getter.socketname + Protocol.protocol + Protocol.acceptsender + Protocol.protocol + name);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (youaccept){
                    for (AbstractMap.SimpleEntry<Text,String> x : map){
                        int num2 = Integer.parseInt(x.getKey().getText());
                        String [] s = x.getValue().split("&");
                        int price = Integer.parseInt(s[0]);
                        int size = Integer.parseInt(s[1]);
                        ItemStack itemStackt = null;
                        if (num2 != 0) {
                            try {
                                DataOutputStream dout1 = new DataOutputStream(Getter.socket.getOutputStream());
                                dout1.writeUTF(Getter.socketname + Protocol.protocol + Protocol.itemtrad + Protocol.protocol + name + Protocol.protocol + s[2] + Protocol.protocol + num2);
                                Main.Eisenhower.getBackPack().takeItem(s[2],num2);
                                Main.Eisenhower.getBackPack().synchronizeStorage();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    primaryStage.close();
                }
            }
        });


        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
