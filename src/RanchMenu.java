import com.sun.javafx.tk.ScreenConfigurationAccessor;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/17/2017.
 */
public class RanchMenu extends StoreMenu{
    Ranch ranch;

    public RanchMenu(Ranch ranch){
        super(ranch);
        this.ranch = ranch;
    }

    @Override
    public ArrayList<String> whereAmI(){
        ArrayList<String> s = new ArrayList<>();
        s.add("This is " + store.getName());
        s.add("1.Buy Animals");
        s.add("2.Check items");
        s.add("3.Buy items");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {

        try {
            Scanner scanner = new Scanner(System.in);
            if (x == 1) {
                System.out.println("What animal do you want to buy?");
                System.out.println("1.Cow $1000");
                System.out.println("2.Sheep $1000");
                System.out.println("3.Chicken $500");
                int ID = scanner.nextInt();
                Animal animal = null;
                String name;
                System.out.println("Choose a name for it:");
                name = scanner.next();

                if(ID == 1)
                    animal = Producer.cow(name);
                else if(ID == 2)
                    animal = Producer.sheep(name);
                else if( ID == 3 )
                    animal = Producer.chicken(name);

                if (person.getMoney() < animal.getPrice()){
                    System.out.println("Not enough Money!");
                    return null;
                }

                AnimalField animalField = null;
                for(AnimalField af : ranch.barn.getAnimalFields()) {
                    if (ID == 1 && af.getName() == "CowField")
                        animalField = af;
                    if (ID == 2 && af.getName() == "SheepField")
                        animalField = af;
                    if (ID == 3 && af.getName() == "ChickenField")
                        animalField = af;
                }

                if (animalField.getAnimals().size() >= animalField.maximumSize){
                    System.out.println("Maximum number of animals reached! You can expand your animal farm in barn.");
                    return null;
                }

                for (Animal animal1 : animalField.getAnimals()) {
                    if (animal1.getName().equals(animal.getName())) {
                        System.out.println("Name is already taken! Choose another name!");
                        return null;
                    }
                }

                person.takeMoney(animal.getPrice());
                animalField.getAnimals().add(animal);
                System.out.println(animal.getName() + " bought!");
                return null;
            }
            super.getCommand(x, person);
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
