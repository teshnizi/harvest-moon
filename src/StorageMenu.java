import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Ahmad on 5/10/2017.
 */
public class StorageMenu extends Menu {

    Storage storage;

    StorageMenu(Storage storage){
        this.storage = storage;
    }

    @Override
    public Menu getCommand(int x, Person person){
        Storage secondStorage = person.getBackPack();
        //put Item in:
        if(x == 1){
            System.out.println("What Item do you want to put inside storage?");
            Scanner scanner = new Scanner(System.in);
            ItemStack itemStack = secondStorage.getItemStackFromPlayer();
            if(itemStack == null){
                System.out.println("Item not found!");
                return null;
            }
            System.out.println("How many of it do you want to put?");
            int numberToTransfer = scanner.nextInt();
            storage.putInItemStack(itemStack, numberToTransfer);
        }

        if(x == 2){
            System.out.println("What item do you want to take out of storage?");
            Scanner scanner = new Scanner(System.in);
            ItemStack itemStack = storage.getItemStackFromPlayer();
            if(itemStack == null){
                System.out.println("Item not found!");
                return null;
            }
            System.out.println("How many of it do you want to take?");
            int numberToTransfer = scanner.nextInt();
            secondStorage.putInItemStack(itemStack, numberToTransfer);
        }
        return null;
    }
    @Override
    public ArrayList<String> whereAmI(){
        ArrayList<String> commands = new ArrayList<>();
        commands.add("You are in storage:");
        commands.add("1.Put Item in");
        commands.add("2.Take Item out");
        return commands;
    }

}
