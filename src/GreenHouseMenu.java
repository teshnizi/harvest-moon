import javafx.scene.chart.ScatterChart;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/14/2017.
 */
public class GreenHouseMenu extends Menu {

    GreenHouse greenHouse;

    public GreenHouseMenu(GreenHouse greenHouse){
        this.greenHouse = greenHouse;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in greenHouse");
        s.add("Current weather: " + greenHouse.getWeatherMachine().getSeason());
        if(!greenHouse.isRepaired){
            s.add("This greenhouse is not Repaired.");
            s.add("1.Repair greenhouse");
        }
        else {
            s.add("0.change Weather");
            for(int i = 0 ; i < greenHouse.getFields().size(); i++){
                s.add((i+1) + "." + greenHouse.getFields().get(i).getName());
            }
        }
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if(greenHouse.isRepaired){
            try{
                if( x == 0){
                    System.out.println("Choose a weather:");
                    System.out.println("1.Spring weather");
                    System.out.println("2.Summer weather");
                    System.out.println("3.Autumn weather");
                    System.out.println("4.Tropical weather");
                    Scanner scanner = new Scanner(System.in);
                    int weather = scanner.nextInt();
                    if(weather == 1)
                        greenHouse.changeWeather(Season.spring);
                    if(weather == 2)
                        greenHouse.changeWeather(Season.summer);
                    if(weather == 3)
                        greenHouse.changeWeather(Season.autumn);
                    if(weather == 4)
                        greenHouse.changeWeather(Season.tropical);
                    return null;
                }
                Field field = greenHouse.getFields().get(x-1);
                FieldMenu fieldMenu = new FieldMenu(field);
                return fieldMenu;
            }
            catch (Exception E){
                System.out.println("BadCommand!");
            }
        }
        else {
            System.out.println("Are you sure you want to repair this greenHouse for $5000 and 20 OldWoods and 20 IronIngots?(\"YES\")");
            Scanner scanner = new Scanner(System.in);
            String cmnd = scanner.next();
            if(cmnd.equals("YES")){
                if(person.getMoney() < 5000){
                    System.out.println("Not enough money!");
                    return null;
                }
                System.out.println("Choose OldWood x20");
                ItemStack itemStack1 = person.getBackPack().getItemStackFromPlayer();
                if(!itemStack1.getItems().get(0).getName().equals("OldWood")){
                    System.out.println("You should choose OldWoods!");
                    return null;
                }
                if(itemStack1.getItems().size() < 20){
                    System.out.println("Not enough OldWoods!");
                    return null;
                }
                System.out.println("Choose IronIngot x20");
                ItemStack itemStack2 = person.getBackPack().getItemStackFromPlayer();
                if(!itemStack2.getItems().get(0).getName().equals("IronIngot")){
                    System.out.println("You should choose IronIngots!");
                    return null;
                }
                if(itemStack2.getItems().size() < 20){
                    System.out.println("Not enough IronIngots!");
                    return null;
                }
                person.getBackPack().takeItem("OldWood",20);
                person.getBackPack().takeItem("IronIngot",20);
                person.takeMoney(5000);
                greenHouse.isRepaired = true;
                System.out.println("GreenHouse repaired!");
            }
        }
        return null;
    }
}
