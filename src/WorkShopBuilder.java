import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by pc on 7/8/2017.
 */
public class WorkShopBuilder extends Application {
    Stage mystage;
    public WorkShopBuilder(Stage stage){
        this.mystage = stage;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Group firstgroup = new Group();
        group.getChildren().addAll(firstgroup);
        Scene scene = new Scene(group,620,700);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.E)
                {
                    primaryStage.close();
                    mystage.show();
                }
            }
        });
        scene.setFill(new ImagePattern(new Image("images\\white-texture.jpg")));
        Button check = new Button("check this shop");
        check.setLayoutX(200);
        check.setLayoutY(150);
        Rectangle rectangle = new Rectangle(40,40);
        rectangle.setFill(new ImagePattern(new Image("images\\hammer.png")));
        firstgroup.getChildren().addAll(rectangle);
        rectangle.setX(200);
        rectangle.setY(50);
        rectangle.setVisible(false);
        Rectangle rectangle1 = new Rectangle(10,10,30,30);
        rectangle1.setFill(new ImagePattern(new Image("images\\backButton.jpg")));
        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle1.setCursor(Cursor.HAND);
            }
        });
        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                group.getChildren().clear();
                group.getChildren().addAll(firstgroup);
            }
        });
        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setVisible(false);
            }
        });
        check.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setX(150);
                rectangle.setY(150);
                rectangle.setVisible(true);
                check.setCursor(Cursor.HAND);
            }
        });
        check.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                group.getChildren().remove(firstgroup);
                Group group1 = new Group();
                check(group1);

                group.getChildren().addAll(rectangle1,group1);
            }
        });

        Button make = new Button("make a tool");
        make.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                group.getChildren().remove(firstgroup);
                Group group1 = new Group();
                make(group1);
                group.getChildren().addAll(rectangle1,group1);
            }
        });
        Button repair = new Button("repair a tool");
        repair.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                group.getChildren().remove(firstgroup);
                Group group1 = new Group();
                repair(group1);
                group.getChildren().addAll(rectangle1,group1);
            }
        });
        Button disasseble = new Button("Disassemble a tool");
        check.setFont(new Font("Comic Sans MS",25));
        make.setFont(new Font("Comic Sans MS",25));
        repair.setFont(new Font("Comic Sans MS",25));
        disasseble.setFont(new Font("Comic Sans MS",25));

        make.setLayoutX(200);
        make.setLayoutY(250);
        repair.setLayoutX(200);
        repair.setLayoutY(350);
        disasseble.setLayoutX(200);
        disasseble.setLayoutY(450);
        make.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setX(150);
                rectangle.setY(250);
                rectangle.setVisible(true);
                make.setCursor(Cursor.HAND);
            }
        });
        repair.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setX(150);
                rectangle.setY(350);
                rectangle.setVisible(true);
                repair.setCursor(Cursor.HAND);
            }
        });
        disasseble.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setX(150);
                rectangle.setY(450);
                rectangle.setVisible(true);
                disasseble.setCursor(Cursor.HAND);
            }
        });
       disasseble.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                group.getChildren().remove(firstgroup);
                Group group1 = new Group();
                disassemble(group1);
                group.getChildren().addAll(rectangle1,group1);
            }
        });
        check.setPrefSize(250,20);
        repair.setPrefSize(250,20);
        disasseble.setPrefSize(250,20);
        make.setPrefSize(250,20);
        firstgroup.getChildren().addAll(check,make,repair,disasseble);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void check(Group group){
        WorkShop workShop = Main.village.getWorkshop();
        ArrayList<Tool> tools = workShop.tools1;
        int num = 0;
        Text text = new Text();
        text.setLayoutX(100);
        text.setLayoutY(500);
        text.setFont(Main.comicFont);
        group.getChildren().addAll(text);
        for (Tool tool : workShop.tools1) {
            Button button = new Button(tool.getName());
            button.setLayoutX(num/12*210 + 100);
            button.setLayoutY(num%12 *30 + 50);
            button.setPrefSize(200,20);
            group.getChildren().addAll(button);
            num++;

            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    ArrayList<String> strings;
                    strings = tool.madeof();
                    String s= " ";
                    for (String string : strings)
                        s+="\n" + string;
                    text.setText(s);
                }
            });
        }
    }
    public void make(Group group){
        WorkShop workShop = Main.village.getWorkshop();
        ArrayList<Tool> tools = workShop.tools1;
        int num = 0;
        Person person = Main.Eisenhower;
        Storage storage = person.getBackPack();
        Text text = new Text();
        text.setLayoutX(100);
        text.setLayoutY(500);
        text.setFont(Main.comicFont);
        group.getChildren().addAll(text);
        for (Tool tool : workShop.tools1) {
            Button button = new Button(tool.getName());
            button.setLayoutX(num/12*210 + 100);
            button.setLayoutY(num%12 *30 + 50);
            button.setPrefSize(200,20);
            group.getChildren().addAll(button);
            num++;

            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (tool.newprice <= person.getMoney() && canMake(tool, storage)) {
                        ItemStack itemStack = new ItemStack();
                        if (tool instanceof Axe)
                            itemStack.getItems().add(Producer.axe(((Axe) tool).type));
                        if (tool instanceof PickAxe)
                            itemStack.getItems().add(Producer.pickAxe(((PickAxe) tool).type));
                        if (tool instanceof Shovel)
                            itemStack.getItems().add(Producer.shovel(((Shovel) tool).type));
                        if (tool instanceof FishingRod)
                            itemStack.getItems().add(Producer.fishingRod(((FishingRod) tool).type));
                        if (tool instanceof WateringCan)
                            itemStack.getItems().add(Producer.wateringCan(((WateringCan) tool).type));
                        storage.putInItemStack(itemStack, 1);
                        person.takeMoney(tool.newprice);
                        text.setText(tool.getName() + " added to BackPack");
                    } else
                       text.setText("Not enough resources!");
                }
            });
        }
    }
    public void repair(Group group){
        int num  =0;
        Text text = new Text();
        text.setLayoutX(100);
        text.setLayoutY(500);
        for (ItemStack itemStack :Main.Eisenhower.getBackPack().getItemStacks()){
            if (itemStack.getItems().get(0) instanceof Tool){
                Button button = new Button(itemStack.getItems().get(0).getName());
                button.setLayoutX(num/12*210 + 100);
                button.setLayoutY(num%12 *30 + 50);
                button.setPrefSize(200,20);
                group.getChildren().addAll(button);
                num++;

                button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        button.setCursor(Cursor.HAND);
                    }
                });
                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Tool tool = (Tool) itemStack.getItems().get(0);
                        Person person = Main.Eisenhower;
                        Storage storage = person.getBackPack();

                        text.setFont(Main.comicFont);
                        group.getChildren().addAll(text);
                        int zarib = 1;
                        if (tool.repairbuilders == null)
                            text.setText("This tool isn't repairable");
                        else {
                            if (tool.getHealth() < 50)
                                zarib = 2;
                            if (tool.getHealth() < 100) {
                                if (person.getMoney() >= tool.repairprice && canRepair(tool, storage)) {
                                    person.giveMoney((-1) * tool.repairprice / zarib);
                                    tool.health = 100;
                                    text.setText(tool.getName() + " Repaired!");
                                } else
                                    text.setText("Not enough resources");
                            } else
                                text.setText("This tool is not broken!");
                        }
                    }
                });
            }
        }

    }
    public void disassemble(Group group) {
        int num = 0;
        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
            if (itemStack.getItems().get(0) instanceof Tool) {
                Button button = new Button(itemStack.getItems().get(0).getName());
                button.setLayoutX(num / 12 * 210 + 100);
                button.setLayoutY(num % 12 * 30 + 50);
                button.setPrefSize(200, 20);
                group.getChildren().addAll(button);
                num++;
                button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        button.setCursor(Cursor.HAND);
                    }
                });
                Text text = new Text();
                text.setLayoutX(100);
                text.setLayoutY(500);
                text.setFont(Main.comicFont);
                group.getChildren().addAll(text);
                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Person person = Main.Eisenhower;
                        Storage storage = person.getBackPack();
                        System.out.println("Choose Item:");
                            Tool tool = (Tool) itemStack.getItems().get(0);
                            if (tool.repairbuilders == null) {
                                text.setText("This tool cant't disassemble");
                                group.getChildren().clear();
                                group.getChildren().add(text);
                                disassemble(group);
                            }
                            else {
                                double zarib = tool.health / 100.0;
                                for (Builder builder : tool.newbuilders) {
                                    ArrayList<Item> items = new ArrayList<>();
                                    if (builder.getItems() instanceof Wood) {
                                        for (int i = 1; i <= (int) Math.floor(builder.getNum() * zarib); i++)
                                            storage.putInItem(Producer.wood(((Wood) builder.getItems()).getType()));
                                    } else if (builder.getItems() instanceof Stone) {
                                        for (int i = 1; i <= (int) Math.floor(builder.getNum() * zarib); i++)
                                            storage.putInItem(Producer.stone(((Stone) builder.getItems()).getType()));
                                    } else {
                                        for (int i = 1; i <= (int) Math.floor(builder.getNum() * zarib); i++)
                                            storage.putInItem(Producer.thread());
                                    }

                                }
                                text.setText("tool has been disassembled!");
                                storage.takeItem(tool.getName(), 1);
                                group.getChildren().clear();
                                group.getChildren().add(text);
                                disassemble(group);

                            }
                    }
                });
            }
        }
    }
    public boolean canRepair(Tool tool, Storage storage){
        Boolean flag = true;
        int h = 0;
        ArrayList<ItemStack> itemStacks = storage.getItemStacks();
        if (tool.repairbuilders == null)
            return false;
        for (Builder builder : tool.repairbuilders) {
            for (ItemStack itemStack : itemStacks) {
                if (itemStack.getItems().get(0).getName().equals(builder.getItems().getName()) && itemStack.getItems().size() >= builder.getNum())
                    h++;
            }
        }
        if (h != tool.repairbuilders.size())
            flag = false;

        if (flag) {
            for (Builder builder : tool.repairbuilders) {
                storage.takeItem(builder.getItems().getName(),builder.getNum());
            }
        }
        return flag;


    }
    public boolean canMake(Tool tool, Storage storage) {
        Boolean flag = true;
        int h = 0;
        ArrayList<ItemStack> itemStacks = storage.getItemStacks();
        for (Builder builder : tool.newbuilders) {
            for (ItemStack itemStack : itemStacks) {
                if (itemStack.getItems().get(0).getName().equals(builder.getItems().getName()) && itemStack.getItems().size() >= builder.getNum())
                    h++;
            }
        }
        if (h != tool.newbuilders.size())
            flag = false;

        if (flag) {
            for (Builder builder : tool.newbuilders) {
                storage.takeItem(builder.getItems().getName(),builder.getNum());
            }
        }
        return flag;
    }
}
