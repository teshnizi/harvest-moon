import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/14/2017.
 */
public class ToolShelfMenu extends Menu {

    ArrayList<ItemStack> shelf;

    public ToolShelfMenu(ArrayList<ItemStack> shelf){
        this.shelf = shelf;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in tool Shelf.");
        s.add("0.Put an Item in shelf");
        int ID = 0;

        for(ItemStack itemStack : shelf) {
            s.add((++ID) + "." + itemStack.getItems().get(0).getName());

        }return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        try {
            if( x == 0 ){
                System.out.println("Choose a tool to put in shelf:");
                ItemStack toolStack = person.getBackPack().getItemStackFromPlayer();
                if (!(toolStack.getItems().get(0) instanceof KitchenTool)){
                    System.out.println("You should choose a KitchenTool!");
                    return null;
                }
                shelf.add(toolStack);
                person.getBackPack().getItemStacks().remove(toolStack);
                System.out.println(toolStack.getItems().get(0).getName() + " added to shelf!");
                return null;
            }
            ItemStack itemStack = shelf.get(x-1);
            System.out.println("1.Status");
            System.out.println("2.Replace tool");
            System.out.println("3.Remove tool");
            Scanner scanner = new Scanner(System.in);
            int cmnd = scanner.nextInt();
            if (cmnd == 1){
                System.out.println(itemStack.getItems().get(0).status());
                return null;
            }
            if (cmnd == 3){
                person.getBackPack().putInItemStack(itemStack, itemStack.getItems().size());
                shelf.remove(itemStack);
                System.out.println("Tool Removed!");
                return null;
            }
            if(cmnd == 2){
                System.out.println("Choose a tool to replace:");
                ItemStack toolStack = person.getBackPack().getItemStackFromPlayer();
                if (!(toolStack.getItems().get(0).getName().equals(itemStack.getItems().get(0).getName()))){
                    System.out.println("You should choose a " + itemStack.getItems().get(0).getName());
                    return null;
                }
                shelf.add(toolStack);
                person.getBackPack().getItemStacks().remove(toolStack);
                person.getBackPack().putInItemStack(itemStack, itemStack.getItems().size());
                shelf.remove(itemStack);
                System.out.println(toolStack.getItems().get(0).getName() + " replaced!");
                return null;
            }
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
