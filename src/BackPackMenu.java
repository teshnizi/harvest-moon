import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/11/2017.
 */
public class BackPackMenu extends Menu {

    Storage backPack;

    public BackPackMenu(Person person){
        this.backPack = person.getBackPack();
    }

    @Override
    public Menu getCommand(int x, Person person){
        try{
            ItemStack itemStack = backPack.getItemStacks().get(x-1);
            System.out.println("What do you want to do with this Item?\n1.Status\n2.Drop");
            if(itemStack.getItems().get(0) instanceof Food){
                System.out.println("3.Eat");
            }
            Scanner scanner = new Scanner(System.in);
            int cmnd = scanner.nextInt();
            if(cmnd == 1){
                System.out.println(itemStack.getItems().get(0).status());
            }
            if(cmnd == 2){
                backPack.getItemStacks().remove(itemStack);
                System.out.println("Item dropped!");
            }
            if(cmnd == 3){
                person.eat((Food)itemStack.getItems().get(0));
                System.out.println(itemStack.getItems().get(0).status());
                itemStack.removeItem(1);
                System.out.println("YUMMY!");
                backPack.synchronizeStorage();
            }
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }

    @Override
    public ArrayList<String> whereAmI(){
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in BackPack, Which item do you want to observe or drop?");
        for(int ID = 0; ID < backPack.getItemStacks().size(); ID++)
            s.add((ID+1) + "." + backPack.getItemStacks().get(ID).toString());
        return s;
    }
}
