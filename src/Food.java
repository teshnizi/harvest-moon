
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Food extends Collectable implements Serializable{

    /**
     * Default constructor
     */
    public Food(String name, int size, int price, EffectOnBody effect) {
        super(name, size, price);
        this.effect = effect;
    }

    public Food(String name, int size, int price, EffectOnBody effect, String description) {
        super(name, size, price);
        this.effect = effect;
        this.description = description;
    }


    int rotLevel = 0;

    EffectOnBody effect;

    public EffectOnBody getEffect() {
        return effect;
    }

    public String status(){
        String s = getName() + "\nrot level: " + rotLevel + "\nEffects on body:\n" + effect.toString();
        return s;
    }

    public Food copy(){
        return new Food(getName(), getSize(), getPrice(), effect);
    }

}