
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Fruit extends Food implements Serializable {

    public Fruit(String name, int size, int price, EffectOnBody effectOnBody) {
        super(name, size, price, effectOnBody);
    }

    public Fruit(String name, int size, int price, EffectOnBody effectOnBody, String description) {
        this(name, size, price, effectOnBody);
        this.description = description;
    }

}