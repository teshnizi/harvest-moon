
import java.util.*;

/**
 * 
 */
public class Farm extends Environment {

    /**
     * Default constructor
     */
    public Farm(String name) {
        super(name);
        fields = new ArrayList<>();
        fruitGardens = new ArrayList<>();
        pond = new Pond();
    }



    @Override
    public void nextDay(Season season){
        for(Field field : fields){
            field.nextDay(season);
        }
        for(FruitGarden fruitGarden: fruitGardens){
            fruitGarden.nextDay(season);
        }
    }

    private ArrayList<Field> fields;

    private ArrayList<FruitGarden> fruitGardens;

    private Pond pond = new Pond();

    private GreenHouse greenHouse;

    public ArrayList<Field> getFields() {
        return fields;
    }

    public void setGreenHouse(GreenHouse greenHouse) {
        this.greenHouse = greenHouse;
    }

    public int addField(String name, int fieldSize){
        /*for(Field field : fields){
            if(field.getName().equals(name)){
                return 0;
            }
        }*/
        Field newField = new Field(name, fieldSize);
        fields.add(newField);
        return 1;
    }
    public int addFruitGarden(String name){
        /*for(Field field : fields){
            if(field.getName().equals(name)){
                return 0;
            }
        }*/
        FruitGarden newFruitGarden = new FruitGarden(name);
        fruitGardens.add(newFruitGarden);
        return 1;
    }

    @Override
    public  ArrayList<String> listOfPlaces(){
        ArrayList<String> ret = new ArrayList<>();

        for(JA neighbour : neighbours)
            ret.add(neighbour.getName());
        for (Place place : places)
            ret.add(place.getName());
        return ret;
    }

    @Override
    public JA findJA(String name) {
        for (JA x : neighbours) {
            if (x.getName().equals(name))
                return x;
        }

        for(Place place : places){
            if(place.getName().equals(name))
                return place;
        }
        return null;
    }
    @Override
    public ArrayList<String> listOfObjects(){
        ArrayList<String> ret = new ArrayList<>();
        for(Field field : fields)
            ret.add(field.getName());
        for(FruitGarden fruitGarden : fruitGardens)
            ret.add(fruitGarden.getName());
        ret.add(greenHouse.getName());
        ret.add("Pond");
        return ret;
    }

    @Override
    public Thing findObject(String objectName) {
        for(Field field : fields){
            if(field.getName().equals(objectName))
                return field;
        }
        //System.out.println(objectName);
        for(FruitGarden fruitGarden : fruitGardens){
            if(fruitGarden.getName().equals(objectName))
                //System.out.println(fruitGarden.getName());
                return fruitGarden;
        }

        if (objectName.equals("GreenHouse"))
            return greenHouse;
        if (objectName.equals("Pond"))
            return pond;
        return null;
    }

    public ArrayList<FruitGarden> getFruitGardens() {
        return fruitGardens;
    }

    public Pond getPond() {
        return pond;
    }


}