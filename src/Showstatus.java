import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by pc on 7/15/2017.
 */
public class Showstatus extends Application {
    static  Text money,name,energy,health,animal;
    static {
        name = new Text("Name :");
        energy = new Text("Energy :");
        health = new Text("Health :");
        animal = new Text("Animals number :");
        money = new Text("Money :");
    }
    String names;
    public Showstatus(String ss){
        names = ss;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,300,485,new ImagePattern(new Image("images\\white-texture.jpg")));

        Rectangle rectangle1 = new Rectangle(10,10,30,30);
        rectangle1.setFill(new ImagePattern(new Image("images\\backButton.jpg")));
        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle1.setCursor(Cursor.HAND);
            }
        });
        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
               Inspectbuilder inspectbuilder = new Inspectbuilder(names);
                try {
                    inspectbuilder.start(primaryStage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        name.setFont(Main.comicFont);
        name.setLayoutX(50);
        name.setLayoutY(50);

        energy.setFont(Main.comicFont);
        energy.setLayoutX(50);
        energy.setLayoutY(100);

        health.setFont(Main.comicFont);
        health.setLayoutX(50);
        health.setLayoutY(150);

        animal.setFont(Main.comicFont);
        animal.setLayoutX(50);
        animal.setLayoutY(200);

        money.setFont(Main.comicFont);
        money.setLayoutX(50);
        money.setLayoutY(250);

        group.getChildren().addAll(money,name,energy,health,animal,rectangle1);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
