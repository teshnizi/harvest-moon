import java.util.ArrayList;

public class WoodType extends Thing {

    /**
     * Default constructor
     */
    private ArrayList<ItemStack> woods;

    public WoodType(String name) {
        super(name);
        woods = new ArrayList<>();
    }
    public ArrayList<ItemStack> getWoods(){
        return woods;
    }



    public void setWoods(ArrayList<ItemStack> a){
        woods = a;
    }
    /**
     * 
     */
    /**
     * @return
     */
    public ArrayList<String> objects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("");
        return s;
    }

    @Override
    public void nextDay(Season season) {

    }
}