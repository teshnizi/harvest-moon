import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;

/**
 * Created by pc on 7/7/2017.
 */
public class PersonMenu extends Application {

    static int i = 0;
    static int j= 0;
    static int k = 0;
    static int jj = 0;
    static int amount = 0;
    static ItemStack activeItemStack = null;
    static ArrayList<Button> buttons = new ArrayList<>();
    static Group group;
    static Button dropButton;
    static Button eatButton;
    Stage mystage;
    public PersonMenu(Scene scene , Stage stage){
        this.pastscene = scene;
        this.mystage = stage;
        i = 0;
        j = 0;
        k = 0;
        jj = 0;
        amount = 0;
    }
    Scene pastscene;
    public static Person person = Main.Eisenhower;
    @Override
    public void start(Stage primaryStage) throws Exception {

        for(Ability ability : Main.Eisenhower.getAbilities()){
            if (ability.getCurrentValue() > ability.getMaxValue()){
                ability.setCurrentValue(ability.getMaxValue());
            }
        }
        Group Homegroup = new Group();
        group = new Group();
        Scene scene = new Scene(Homegroup,620,700, Color.SLATEGRAY);
        scene.setFill(new ImagePattern(new Image("images\\white-texture.jpg")));
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.Q)
                {
                    mystage.show();
                    primaryStage.close();
                }
            }
        });
        Text text = new Text("Person");
        text.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                text.setCursor(Cursor.HAND);
            }
        });
        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                scene.setCursor(Cursor.DEFAULT);
            }
        });
        text.setFont(new Font(14));
        text.setVisible(true);
        text.setLayoutX(5);
        text.setLayoutY(20);
        text.setFill(Color.RED);
        Group backpack = new Group();
        inBackpack(backpack);
        Text text1 = new Text("Backpack");
        text1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                text1.setCursor(Cursor.HAND);
            }
        });
        text1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (Homegroup.getChildren().contains(group))
                    Homegroup.getChildren().remove(group);
                if (!Homegroup.getChildren().contains(backpack)) {
                    backpack.getChildren().clear();
                    scene.setFill(new ImagePattern(new Image("images\\white-texture.jpg")));
                    inBackpack(backpack);
                    Homegroup.getChildren().add(backpack);
                    text.setFill(Color.BLACK);
                    text1.setFill(Color.RED);
                }

            }
        });
        Line line = new Line(55,0,55,30);
        Line line1 = new Line(55,30,1280,30);
        line1.setStroke(Color.RED);
        line.setStroke(Color.RED);
        text.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (Homegroup.getChildren().contains(backpack)){
                    Homegroup.getChildren().remove(backpack);
                    if (!Homegroup.getChildren().contains(group)){
                        scene.setFill(new ImagePattern(new Image("images\\white-texture.jpg")));
                        group.getChildren().clear();
                        i = 0;
                        j = 0;
                        k = 0;
                        jj = 0;
                        amount = 0;
                        inPerson(group);
                        group.getChildren().addAll(line,line1);
                        Homegroup.getChildren().addAll(group);
                        text1.setFill(Color.BLACK);
                        text.setFill(Color.RED);
                    }
                }
            }
        });
        text1.setLayoutX(65);
        text1.setLayoutY(20);
        text1.setFill(Color.BLACK);

        Homegroup.getChildren().addAll(text,text1);
        group.getChildren().addAll(line,line1);
        inPerson(group);
        Homegroup.getChildren().addAll(group);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void inPerson(Group group){


        Text text2 = new Text("Health");
        Text text3 = new Text("Energy");
        Text text5 = new Text("Stamina");
        Text text77 = new Text("Satiety");

        Text text6 = new Text("Money");
        text2.setFont(new Font("Comic Sans MS",20));
        text3.setFont(new Font("Comic Sans MS",20));
        text5.setFont(new Font("Comic Sans MS",20));
        text77.setFont(new Font("Comic Sans MS",20));
        text6.setFont(new Font("Comic Sans MS",20));
        text2.setLayoutX(50);
        text2.setLayoutY(150);
        text3.setLayoutX(50);
        text3.setLayoutY(250);
        //text5.setFont(new Font(25));
        text5.setLayoutX(50);
        text5.setLayoutY(350);
        text77.setLayoutX(50);
        text77.setLayoutY(450);
        //text3.setFont(new Font(25));
        //text2.setFont(new Font(25));
        text2.setFill(Color.GREEN);
        text3.setFill(Color.RED);
        text5.setFill(Color.BLACK);
        text77.setFill(Color.DARKGREEN);
        text6.setLayoutX(50);
        text6.setLayoutY(550);
        text6.setFont(new Font(25));
        text6.setFill(Color.BLUE);
        Text money = new Text("0");
        money.setFont(new Font(20));
        money.setLayoutX(150);
        money.setLayoutY(550);
        Rectangle rectangle = new Rectangle(150,135,0,20);
        Rectangle rectangle1 = new Rectangle(150,235,0,20);
        Rectangle rectangle2 = new Rectangle(150,335,0,20);
        Rectangle rectangle7 = new Rectangle(150,435,0,20);

        Text text4 = new Text("0");
        text4.setLayoutX(rectangle.getWidth() + 150 +10);
        text4.setLayoutY(150);
        text4.setFont(new Font(20));
        Text text7 = new Text("0");
        text7.setLayoutX(rectangle1.getWidth() + 150 +10);
        text7.setLayoutY(250);
        text7.setFont(new Font(20));
        Text text8 = new Text("0");
        text8.setLayoutX(rectangle2.getWidth() + 150 +10);
        text8.setLayoutY(350);
        text8.setFont(new Font(20));
        Text text777 = new Text("0");
        text777.setLayoutX(rectangle2.getWidth() + 150 +10);
        text777.setLayoutY(450);
        text777.setFont(new Font(20));


        Timeline timeline = new Timeline();
        int cycle = (int)Math.floor((person.getHealth().getCurrentValue()/person.getHealth().getMaxValue())*100.0);
        timeline.setCycleCount(cycle);

        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(20), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rectangle.setWidth(i*4);
                text4.setLayoutX(rectangle.getWidth() + 150 +10);
                text4.setText(i+"%");
                i++;
            }
        }));
        Timeline timeline1 = new Timeline();
        int cycle1 = (int)Math.floor((person.getEnergy().getCurrentValue()/person.getEnergy().getMaxValue())*100.0);
        timeline1.setCycleCount(cycle1);
        timeline1.getKeyFrames().add(new KeyFrame(Duration.millis(20), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rectangle1.setWidth(j*4);
                j++;
                text7.setLayoutX(rectangle1.getWidth() + 150 +10);
                text7.setText(j+"%");
            }
        }));
        Timeline timeline2 = new Timeline();
        int cycle2 = (int)Math.floor((person.getStamina().getCurrentValue()/person.getStamina().getMaxValue())*100.0);
        timeline2.setCycleCount(cycle2);
        timeline2.getKeyFrames().add(new KeyFrame(Duration.millis(20), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rectangle2.setWidth(k*4);
                k++;
                text8.setLayoutX(rectangle2.getWidth() + 150 +10);
                text8.setText(k+"%");
            }
        }));

        Timeline timeline7 = new Timeline();

        int cycle7 = (int)Math.floor((person.getSatiety().getCurrentValue()/person.getSatiety().getMaxValue())*100.0);
        timeline7.setCycleCount(cycle7);
        timeline7.getKeyFrames().add(new KeyFrame(Duration.millis(20), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rectangle7.setWidth(jj*4);
                jj++;
                text777.setLayoutX(rectangle7.getWidth() + 150 +10);
                text777.setText(jj+"%");
            }
        }));


        Timeline timeline6 = new Timeline();
        int value = person.getMoney();
        timeline6.setCycleCount(value +1);
        timeline6.getKeyFrames().add(new KeyFrame(Duration.millis(.125), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                money.setText( " $" + amount);
                amount++;
            }
        }));
        timeline.play();
        timeline1.play();
        timeline2.play();
        timeline6.play();
        timeline7.play();
        rectangle.setFill(Color.GREEN);
        rectangle1.setFill(Color.DARKRED);
        rectangle7.setFill(Color.DARKGREEN);
        group.getChildren().addAll(text777,text6,money,text5,rectangle2,rectangle1,text3,text2,rectangle,rectangle7,text4,text77);
        group.getChildren().addAll(text7,text8);
    }
    public void inBackpack(Group group){
        Line line = new Line(0,30,55,30);
        Line line1 = new Line(55,30,55,0);
        Line line2 = new Line(140,0,140,30);
        Line line3 = new Line(140,30,1280,30);
        line.setStroke(Color.RED);
        line1.setStroke(Color.RED);
        line2.setStroke(Color.RED);
        line3.setStroke(Color.RED);


        dropButton = new Button("Drop");
        dropButton.setVisible(false);

        eatButton = new Button("Eat");
        eatButton.setVisible(false);

        int num = 0;
        Label label = new Label("");
        label.setFont(Main.comicFont);
        label.setLayoutX(20);
        label.setLayoutY(500);
        label.setTextFill(Color.DARKRED);

        group.getChildren().addAll(label);
        for (ItemStack itemStack : person.getBackPack().getItemStacks()){

            Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
            button.setLayoutX(20 + 200 * (num/10));
            button.setLayoutY(100 + (num%10)*35);
            button.setFont(Main.comicFont);
            button.setPrefWidth(180);
            num++;
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    label.setText(itemStack.getItems().get(0).status());
                    dropButton.setVisible(true);
                    if (itemStack.getItems().get(0) instanceof Food)
                        eatButton.setVisible(true);
                    else
                        eatButton.setVisible(false);
                    activeItemStack = itemStack;
                }
            });
            group.getChildren().add(button);
            buttons.add(button);
        }
        eatButton.setFont(Main.comicFont);
        eatButton.setPrefWidth(100);
        eatButton.setLayoutX(400);
        eatButton.setLayoutY(450);
        eatButton.setTextFill(Color.GREEN);
        eatButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Main.Eisenhower.getSatiety().changeCurrentValue(5);
                for (Button button : buttons) {
                    if (button.getText().startsWith(activeItemStack.getItems().get(0).getName() + " x")) {
                        if (activeItemStack.getItems().size() == 1) {
                            button.setVisible(false);
                            dropButton.setVisible(false);
                            eatButton.setVisible(false);
                            label.setText("");
                        }
                        else
                            button.setText(activeItemStack.getItems().get(0).getName() + " x" + (activeItemStack.getItems().size()-1));
                    }
                }
                person.eat((Food)activeItemStack.getItems().get(0));
                person.getBackPack().takeItem(activeItemStack.getItems().get(0).getName(), 1);
                System.out.println(activeItemStack.getItems().size());
            }
        });

        dropButton.setFont(Main.comicFont);
        dropButton.setPrefWidth(100);
        dropButton.setLayoutX(260);
        dropButton.setLayoutY(450);
        dropButton.setTextFill(Color.RED);
        dropButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println(activeItemStack.getItems().size());
                for (Button button : buttons) {
                    if (button.getText().startsWith(activeItemStack.getItems().get(0).getName() + " x")) {
                        if (activeItemStack.getItems().size() == 1) {
                            button.setVisible(false);
                            dropButton.setVisible(false);
                            eatButton.setVisible(false);
                            label.setText("");
                        }
                        else
                            button.setText(activeItemStack.getItems().get(0).getName() + " x" + (activeItemStack.getItems().size()-1));
                    }
                }
                person.getBackPack().takeItem(activeItemStack.getItems().get(0).getName(), 1);
                System.out.println(activeItemStack.getItems().size());
            }
        });
        group.getChildren().addAll(dropButton,eatButton);

        group.getChildren().addAll(line,line1,line2,line3);
    }
}
