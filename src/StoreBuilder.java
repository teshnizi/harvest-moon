import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pc on 7/9/2017.
 */
public class StoreBuilder extends Application {
    Store store;
    Image upimage = new Image("images\\Up.png");
    Image downimage = new Image("images\\Down.jpg");
    int constant = Main.village.getRanch().friendly;
    String my;
    int x = 0;
    public StoreBuilder(Store store){
        this.store = store;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group mainGroup = new Group();
        Group group = new Group();
        mainGroup.getChildren().add(group);
        Scene scene = new Scene(mainGroup,1260,700,new ImagePattern(new Image("images\\white-texture.jpg")));

        Rectangle rectangle1 = new Rectangle(10,10,30,30);
        rectangle1.setFill(new ImagePattern(new Image("images\\backButton.jpg")));
        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle1.setCursor(Cursor.HAND);
            }
        });
        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().clear();
                mainGroup.getChildren().addAll(group);
            }
        });


        Text text = new Text(store.getName());
        text.setX(500);
        text.setY(50);
        text.setFont(new Font(40));

        Text text1 = new Text();
        text1.setX(150);
        text1.setY(400);
        text1.setFont(Main.comicFont);
        group.getChildren().add(text1);

        Button about = new Button("About");
        about.setLayoutX(50);
        about.setLayoutY(400);
        about.setFont(Main.comicFont);
        group.getChildren().add(about);
        about.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                about.setCursor(Cursor.HAND);
            }
        });
        Random random = new Random();
        x = random.nextInt();
        x %= 2;

        if (x == 0) {
            my = "Shopkeeper is good";
            constant = -5;
        } else {
            my = "Shopkeeper is bad";
            constant =   5;
        }
        about.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {


                if (Main.village.getRanch().isOnAuction)
                    my += "\nShop is on Auction";
                my += "\n This shop is not good with " + store.string;
                text1.setText(my);
            }
        });

        Button gift = new Button("gift");
        gift.setLayoutX(500);
        gift.setLayoutY(150);
        gift.setFont(Main.comicFont);
        gift.setPrefSize(200,20);
        gift.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                gift.setCursor(Cursor.HAND);
            }
        });
        gift.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().remove(group);
                Group group1 = new Group();
                gift(group1);
                mainGroup.getChildren().add(rectangle1);
                mainGroup.getChildren().add(group1);
                rectangle1.setVisible(true);
            }
        });
        group.getChildren().addAll(gift);

        Button check = new Button("Check items");
        check.setLayoutX(500);
        check.setLayoutY(150);
        check.setFont(Main.comicFont);
        check.setPrefSize(200,20);
        check.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                check.setCursor(Cursor.HAND);
            }
        });
        check.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().clear();
                mainGroup.getChildren().add(rectangle1);
                Group group1 = new Group();
                check(store,group1);
                mainGroup.getChildren().addAll(group1);
            }
        });

        Button sell = new Button("Sell item");
        sell.setLayoutX(500);
        sell.setLayoutY(200);
        sell.setFont(Main.comicFont);
        sell.setPrefSize(200,20);
        sell.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sell.setCursor(Cursor.HAND);
            }
        });
        sell.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().clear();
                mainGroup.getChildren().add(rectangle1);
                Group group1 = new Group();
                sell(store,group1);
                mainGroup.getChildren().addAll(group1);
            }
        });

        Button buy = new Button("Buy items");
        buy.setLayoutX(500);
        buy.setLayoutY(250);
        buy.setFont(Main.comicFont);
        buy.setPrefSize(200,20);
        buy.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setCursor(Cursor.HAND);
            }
        });
        buy.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().clear();
                mainGroup.getChildren().add(rectangle1);
                Group group1 = new Group();
                buy(store,group1);
                mainGroup.getChildren().addAll(group1);
            }
        });

        group.getChildren().addAll(text , check,sell,buy);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void check(Store store , Group group){
        Storage storage = store.storage;
        store.storage.synchronizeStorage();
        int num =0;

        Text text = new Text();
        text.setFont(Main.comicFont);
        text.setX(20);
        text.setY(545);

        Text text1 = new Text();
        text1.setFont(Main.comicFont);
        text1.setX(600);
        text1.setY(545);

        Text text2 = new Text();
        text2.setFont(Main.comicFont);
        text2.setX(750);
        text2.setY(545);

        for (ItemStack itemStack : storage.getItemStacks()){

            if (itemStack.getItems().size() == 0)
                continue;

            Button button = new Button(itemStack.getItems().get(0).getName());
            button.setLayoutX(num/10*200 + 20);
            button.setLayoutY(num%10 *50 + 50);
            button.setPrefSize(140,20);
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    text.setText(itemStack.getItems().get(0).status());
                    int price =Math.max ((int)((50.0 - (double)itemStack.getItems().size())/20.0 * (double)itemStack.getItems().get(0).getPrice())  + (50 - store.firiendly) *5 + constant, 0 );
                    text1.setText("1st item Price: $" + price + "\n(total Price may be more due to lack of items)");
                    text2.setText("Number remained in store :  x" + itemStack.getItems().size());
                }
            });
            group.getChildren().addAll(button);


            num++;
        }
        group.getChildren().addAll(text,text1,text2);
    }
    public void buy(Store store, Group group){
        store.storage.synchronizeStorage();
        Storage storage = store.storage;
        int num =0;
        Person person = Main.Eisenhower;

        Text text3 = new Text();
        text3.setX(500);
        text3.setY(550);
        group.getChildren().addAll(text3);
        text3.setFont(Main.comicFont);

        ArrayList<AbstractMap.SimpleEntry<Text,String>> map = new ArrayList<>();

        Button buy = new Button("Buy");
        buy.setLayoutX(500);
        buy.setLayoutY(600);
        buy.setFont(new Font(25));
        group.getChildren().add(buy);

        for (ItemStack itemStack : storage.getItemStacks()){


            if (itemStack.getItems().size()  == 0)
                continue;

            Button button = new Button(itemStack.getItems().get(0).getName());
            button.setLayoutX(num/10*200 + 20);
            button.setLayoutY(num%10 *50 + 50);
            button.setPrefSize(140,20);


            Text text = new Text("0");
            text.setX(button.getLayoutX() + 155);
            text.setY(button.getLayoutY() + 17);
            map.add(new AbstractMap.SimpleEntry<Text, String>(text,itemStack.getItems().get(0).getPrice() + "&" + itemStack.getItems().size() + "&" +itemStack.getItems().get(0).getName()));


            Rectangle up = new Rectangle(button.getLayoutX() + 153 ,button.getLayoutY() - 10,10,10);
            up.setFill(new ImagePattern(upimage));
            up.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    up.setCursor(Cursor.HAND);
                }
            });
            up.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int num1 = Integer.parseInt(text.getText());
                    int max = itemStack.getItems().size();
                    if (num1 < max) {
                        num1++;
                        text.setText(String.valueOf(num1));
                        int total = 0;
                        for (AbstractMap.SimpleEntry<Text, String> x : map) {
                            int num2 = Integer.parseInt(x.getKey().getText());
                            String[] s = x.getValue().split("&");
                            int price = Integer.parseInt(s[0]);
                            int size = Integer.parseInt(s[1]);
                            total += Math.max((int) calculate(size, num2, price) +(50-store.firiendly)*5*num2 + constant*num2 , 0);
                        }
                        if (total > person.getMoney()) {
                            num1--;
                            text.setText(String.valueOf(num1));
                        } else {
                            text3.setText(" ");
                            text3.setText("Total price : " + String.valueOf(total) + "$");
                        }
                        text.setText(String.valueOf(num1));
                    }
                }
            });

            Rectangle down = new Rectangle(button.getLayoutX() + 153 ,button.getLayoutY() + 20,10,10);
            down.setFill(new ImagePattern(downimage));
            down.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    down.setCursor(Cursor.HAND);
                }
            });
            down.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int num1 = Integer.parseInt(text.getText());
                    if (num1>0) {
                        num1--;
                        text.setText(String.valueOf(num1));
                        int total = 0;
                        for (AbstractMap.SimpleEntry<Text,String> x : map){
                            int num2 = Integer.parseInt(x.getKey().getText());
                            String [] s = x.getValue().split("&");
                            int price = Integer.parseInt(s[0]);
                            int size = Integer.parseInt(s[1]);
                            total +=Math.max((int)calculate(size,num2,price) + (50-store.firiendly)*5*num2 + constant *num2,0);
                        }
                        text3.setText(" ");
                        text3.setText("Total price : " +String.valueOf(total) + "$");
                    }
                }

            });
            group.getChildren().addAll(button,up,down,text);
            num++;
        }
        buy.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setCursor(Cursor.HAND);
            }
        });


        buy.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int total = 0;
                int num3 = 0;
                for (AbstractMap.SimpleEntry<Text,String> x : map){
                    int num2 = Integer.parseInt(x.getKey().getText());
                    String [] s = x.getValue().split("&");
                    int price = Integer.parseInt(s[0]);
                    int size = Integer.parseInt(s[1]);
                    ItemStack itemStackt = null;
                    for (ItemStack itemStack : store.storage.getItemStacks()){
                        if (itemStack.getItems().size() > 0 && itemStack.getItems().get(0).getName().equals(s[2])){
                            itemStackt = itemStack;
                            break;
                        }
                    }
                    if (num2 != 0) {
                        try {
                            if (Main.online) {
                                DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                                dout.writeUTF(sender(store.getName(), itemStackt.getItems().get(0).getName(), num2, -1));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        person.getBackPack().putInItemStack(itemStackt, num2);
                    }
                    total +=Math.max((int)calculate(size,num2,price) + (50-store.firiendly)*5*num2 + constant *num2,0);
                    store.storage.synchronizeStorage();
                }
                person.takeMoney((int)total);
                store.firiendly += total/50;
                if (store.getName().equals("Butchery"))
                {
                    Main.village.getRanch().friendly -=5;
                }
                if (store.getName().equals("Groceries Store"))
                {
                    for (Store store1 : Main.market.getStores())
                    {
                        if (store1.getName().equals("General Store"))
                        {
                            store1.firiendly -= 5;
                        }
                    }
                }
                if (store.getName().equals("General Store"))
                {
                    for (Store store1 : Main.market.getStores())
                    {
                        if (store1.getName().equals("Groceries Store"))
                        {
                            store1.firiendly -= 5;
                        }
                    }
                }
                group.getChildren().clear();
                buy(store,group);
            }
        });
    }
    public double calculate(int size , int num ,int price){
        int totalNum = size;
        double totalPrice = 0;
        for (int i = totalNum ; i > totalNum - num ; i--)
            totalPrice += (50.0 - (double)i)/20.0 * (double)price; //  0 <= i <= 40 take a look at class Store
        return totalPrice;
    }
    public void sell(Store store,Group group){
        Person person = Main.Eisenhower;
        Storage storage = store.storage;
        Storage backpack = person.getBackPack();
        backpack.synchronizeStorage();
        int num =0;

        ArrayList<AbstractMap.SimpleEntry<Text,String>> map = new ArrayList<>();


        Text text3 = new Text();
        text3.setX(500);
        text3.setY(550);
        group.getChildren().addAll(text3);
        text3.setFont(Main.comicFont);

        Button sell1 = new Button("Sell");
        sell1.setLayoutX(500);
        sell1.setLayoutY(600);
        sell1.setFont(new Font(25));
        group.getChildren().add(sell1);
        sell1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sell1.setCursor(Cursor.HAND);
            }
        });

        for (ItemStack itemStack : backpack.getItemStacks()){
            Button button = new Button(itemStack.getItems().get(0).getName());
            button.setLayoutX(num/10*200 + 20);
            button.setLayoutY(num%10 *50 + 50);
            button.setPrefSize(140,20);
            if (store.iteminside.contains(itemStack.getItems().get(0).getName())) {

                Text text = new Text("0");
                text.setX(button.getLayoutX() + 155);
                text.setY(button.getLayoutY() + 17);
                int sellPrice = itemStack.getItems().get(0).getPrice() * 3 / 4;
                map.add(new AbstractMap.SimpleEntry<Text, String>(text,sellPrice + "&" + itemStack.getItems().size() + "&" + itemStack.getItems().get(0).getName()));

                Rectangle up = new Rectangle(button.getLayoutX() + 153 ,button.getLayoutY() - 10,10,10);
                up.setFill(new ImagePattern(upimage));
                up.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        up.setCursor(Cursor.HAND);
                    }
                });
                up.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int num1 = Integer.parseInt(text.getText());
                        int max = itemStack.getItems().size();
                        if (num1 < max) {
                            num1++;
                            text.setText(String.valueOf(num1));
                            int total = 0;
                            for (AbstractMap.SimpleEntry<Text, String> x : map) {
                                int num2 = Integer.parseInt(x.getKey().getText());
                                String[] s = x.getValue().split("&");
                                int price = Integer.parseInt(s[0]);
                                int size = Integer.parseInt(s[1]);
                                total += price*num2;
                            }

                                text3.setText(" ");
                                text3.setText("Total price : " + String.valueOf(total) + "$");
                                text.setText(String.valueOf(num1));
                        }
                    }
                });
                Rectangle down = new Rectangle(button.getLayoutX() + 153 ,button.getLayoutY() + 20,10,10);
                down.setFill(new ImagePattern(downimage));
                down.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        down.setCursor(Cursor.HAND);
                    }
                });
                down.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int num1 = Integer.parseInt(text.getText());
                        if (num1>0) {
                            num1--;
                            text.setText(String.valueOf(num1));
                            int total = 0;
                            for (AbstractMap.SimpleEntry<Text,String> x : map){
                                int num2 = Integer.parseInt(x.getKey().getText());
                                String [] s = x.getValue().split("&");
                                int price = Integer.parseInt(s[0]);
                                int size = Integer.parseInt(s[1]);
                                total += price*num1;
                            }
                            text3.setText(" ");
                            text3.setText("Total price : " +String.valueOf(total) + "$");
                        }
                    }

                });

                group.getChildren().addAll(button,up,down,text);
                num ++;
            }
        }
        sell1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                int total = 0;
                int num3 = 0;
                for (AbstractMap.SimpleEntry<Text,String> x : map){
                    int num2 = Integer.parseInt(x.getKey().getText());
                    String [] s = x.getValue().split("&");
                    int price = Integer.parseInt(s[0]);
                    int size = Integer.parseInt(s[1]);
                    ItemStack itemStackt = null;
                    for (ItemStack itemStack : store.storage.getItemStacks()){
                        if (itemStack.getItems().size() > 0 && itemStack.getItems().get(0).getName().equals(s[2])){
                            itemStackt = itemStack;
                            break;
                        }
                    }
                    if (num2 != 0) {
                        try {
                            if (Main.online) {
                                DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                                dout.writeUTF(sender(store.getName(), itemStackt.getItems().get(0).getName(), num2, 1));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        for (int j =0 ; j < num2 ;j ++)
                            store.storage.putInItem(itemStackt.getItems().get(0));

                        person.getBackPack().takeItem(itemStackt.getItems().get(0).getName(), num2);
                    }
                    total += price*num2;
                    person.getBackPack().synchronizeStorage();
                    store.storage.synchronizeStorage();
                }
                person.giveMoney(total);
                group.getChildren().clear();
                sell(store,group);
            }
        });
    }
    public void gift(Group group) {

        int num = 0;
        Label question = new Label("Choose a Gift:");
        question.setFont(new Font("Comic Sans MS", 25));
        question.setTextFill(Color.DARKGREEN);
        question.setLayoutX(200);
        question.setLayoutY(10);
        group.getChildren().addAll(question);
        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
            Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
            button.setLayoutX(20 + 200 * (num / 10));
            button.setLayoutY(80 + (num % 10) * 35);
            button.setFont(Main.comicFont);
            button.setPrefWidth(180);
            group.getChildren().addAll(button);
            num++;
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println(itemStack.getItems().get(0));

                    store.firiendly +=itemStack.getItems().get(0).getPrice()/25;
                    Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(),1);
                    Main.Eisenhower.getBackPack().synchronizeStorage();
                    group.getChildren().clear();
                    gift(group);
                }
            });
        }
    }
    public String sender(String store, String itemname, int num , int type)
    {
        String s = null;
        if (type == 1)
            s=Protocol.add ;
        if (type == -1)
            s = Protocol.sub;
        String ans =  Getter.socketname + Protocol.protocol + Protocol.market + Protocol.protocol+store +Protocol.protocol+itemname + Protocol.protocol + num + Protocol.protocol + s ;
        return ans;
    }
}
