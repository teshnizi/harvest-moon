
import javafx.util.Pair;

import java.io.IOException;
import java.util.*;

/**
 * 
 */
public class Cave extends Environment {

    static ArrayList<Storage> storages;
    public static HashSet<Pair<Storage,Pair<Integer,Integer>>> res = new HashSet<>();
    public static ArrayList<Pair<Integer,Integer>> my = new ArrayList<>();
    static
    {
        my.add(new Pair<>(5,5));
        my.add(new Pair<>(5,9));
        my.add(new Pair<>(5,12));
        my.add(new Pair<>(8,6));
        my.add(new Pair<>(8,10));
        my.add(new Pair<>(8,12));
        my.add(new Pair<>(12,13));
        my.add(new Pair<>(12,4));
        my.add(new Pair<>(12,6));
        my.add(new Pair<>(17,7));
        my.add(new Pair<>(17,15));
        my.add(new Pair<>(17,4));
        my.add(new Pair<>(17,9));
        my.add(new Pair<>(25,13));
    }
    private StoneType rocks;
    @Override
    public ArrayList<String> listOfObjects() {
        ArrayList<String > ret = new ArrayList<>();
        ret.add(rocks.getName());
        return ret;
    }

    public void setStorages(ArrayList<Storage> storages) {
        this.storages = storages;
    }

    @Override
    public ArrayList<String> listOfPlaces() {
        ArrayList<String > ret = new ArrayList<>();
        for (JA ja: neighbours)
            ret.add(ja.getName());
        return ret;
    }

    @Override
    public JA findJA(String name) {
        for (JA ja : neighbours)
            if (ja.getName().equals(name))
                return ja;
        return null;
    }

    @Override
    public Thing findObject(String objectName) {
        if (objectName.equals(rocks.getName()))
            return rocks;
        return null;
    }

    public Cave(String name, StoneType rockType) {
        super(name);
        this.rocks = rockType;
    }

    @Override
    public void nextDay(Season season) throws IOException {
        if (Person.age %3 == 1){
            storages.clear();
            for (int i  = 0 ; i < 5 ; i ++)
            {
                Storage storage = new Storage(10);
                for (int j = 0 ; j < 10 ; j++)
                    storage.putInItem(Producer.stone(0));
                storages.add(storage);
            }
            for (int i = 1 ;i < 4;i++ ){
                for (int j = 0 ; j < 3; j ++)
                {
                    Storage storage = new Storage(10);
                    for (int k  = 0 ; k < 10 ;  k++)
                        storage.putInItem(Producer.stone(i));
                    storages.add(storage);
                }
            }
            res = placcc(my);
        }
    }
    public static HashSet<Pair<Storage,Pair<Integer,Integer>>>  placcc(ArrayList<Pair<Integer,Integer>> d){
        HashSet<Pair<Storage,Pair<Integer,Integer>>>  p = new HashSet<>();
        int num = 0;
        for (Storage storage : storages){
            int k1 = 0;
            for (Pair<Integer,Integer> pair :d){
                if (k1 == num){
                 p.add(new Pair<>(storage,pair));
                }
                k1++;
            }
           num++;
        }
        return p;
    }
}