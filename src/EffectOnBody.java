
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class EffectOnBody implements Serializable{

    /**
     * Default constructor
     */
    public EffectOnBody(ArrayList<AbilityChange> abilityChanges) {
        this.abilityChanges = abilityChanges;
    }

    private ArrayList<AbilityChange> abilityChanges = new ArrayList<>();

    private int cost;

    public ArrayList<AbilityChange> getAbilityChanges() {
        return abilityChanges;
    }


    public void affect(){
        for(AbilityChange abilityChange : abilityChanges){
            abilityChange.affect();
        }
    }

    @Override
    public String toString(){
        String s = new String();
        for (AbilityChange abilityChange: abilityChanges){
            s = s + abilityChange.toString() + "\n";
        }
        return s;
    }
}