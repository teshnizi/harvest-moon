import java.util.ArrayList;

/**
 * Created by pc on 7/16/2017.
 */
public class Everything {
    static Storage storage = new Storage(1000000);
    static {
        storage.putInItem(Producer.stone(0));
        storage.putInItem(Producer.stone(1));
        storage.putInItem(Producer.stone(2));
        storage.putInItem(Producer.stone(3));
        storage.putInItem(Producer.wood(0));
        storage.putInItem(Producer.wood(1));
        storage.putInItem(Producer.wood(2));
        storage.putInItem(Producer.wood(3));
        storage.putInItem(Producer.pear());
        storage.putInItem(Producer.peach());
        storage.putInItem(Producer.garlic());
        storage.putInItem(Producer.bean());
        storage.putInItem(Producer.cabbage());
        storage.putInItem(Producer.ale());
        storage.putInItem(Producer.lemon());
        storage.putInItem(Producer.pomegranate());
        storage.putInItem(Producer.cucumber());
        storage.putInItem(Producer.watermelon());
        storage.putInItem(Producer.onion());
        storage.putInItem(Producer.turnip());
        storage.putInItem(Producer.apple());
        storage.putInItem(Producer.orange());
        storage.putInItem(Producer.potato());
        storage.putInItem(Producer.carrot());
        storage.putInItem(Producer.tomato());
        storage.putInItem(Producer.melon());
        storage.putInItem(Producer.pineapple());
        storage.putInItem(Producer.strawberry());
        storage.putInItem(Producer.pepper());
        storage.putInItem(Producer.oil());
        storage.putInItem(Producer.flour());
        storage.putInItem(Producer.sugar());
        storage.putInItem(Producer.salt());
        storage.putInItem(Producer.cheese());
        storage.putInItem(Producer.bread());
        storage.putInItem(Producer.milk());
        storage.putInItem(Producer.oil());
        storage.putInItem(Producer.egg());
        storage.putInItem(Producer.pickAxe(1));
        storage.putInItem(Producer.pickAxe(2));
        storage.putInItem(Producer.pickAxe(3));
        storage.putInItem(Producer.pickAxe(4));
        storage.putInItem(Producer.shovel(1));
        storage.putInItem(Producer.shovel(2));
        storage.putInItem(Producer.shovel(3));
        storage.putInItem(Producer.shovel(4));
        storage.putInItem(Producer.axe(1));
        storage.putInItem(Producer.axe(2));
        storage.putInItem(Producer.axe(3));
        storage.putInItem(Producer.axe(4));
        storage.putInItem(Producer.fishingRod(1));
        storage.putInItem(Producer.fishingRod(2));
        storage.putInItem(Producer.fishingRod(3));
        storage.putInItem(Producer.fishingRod(4));
        storage.putInItem(Producer.wateringCan(1));
        storage.putInItem(Producer.wateringCan(2));
        storage.putInItem(Producer.wateringCan(3));
        storage.putInItem(Producer.wateringCan(4));
        storage.putInItem(Producer.wateringCan(5));
        storage.putInItem(Producer.wateringCan(6));
        storage.putInItem(Producer.wateringCan(7));
        storage.putInItem(Producer.wateringCan(8));
        storage.putInItem(Producer.knife());
        storage.putInItem(Producer.fryingPan());
        storage.putInItem(Producer.agitator());
        storage.putInItem(Producer.pot());
        storage.putInItem(Producer.furnace());
        storage.putInItem(Producer.thread());
        storage.putInItem(Producer.alfalfa());
        storage.putInItem(Producer.grain());
        storage.putInItem(Producer.wool());
        storage.putInItem(Producer.seedOf(Producer.garlicCrop()));
        storage.putInItem(Producer.seedOf(Producer.beanCrop()));
        storage.putInItem(Producer.seedOf(Producer.cabbageCrop()));
        storage.putInItem(Producer.seedOf(Producer.aleCrop()));
        storage.putInItem(Producer.seedOf(Producer.cucumberCrop()));
        storage.putInItem(Producer.seedOf(Producer.watermelonCrop()));
        storage.putInItem(Producer.seedOf(Producer.onionCrop()));
        storage.putInItem(Producer.seedOf(Producer.turnipCrop()));
        storage.putInItem(Producer.seedOf(Producer.potatoCrop()));
        storage.putInItem(Producer.seedOf(Producer.carrotCrop()));
        storage.putInItem(Producer.seedOf(Producer.tomatoCrop()));
        storage.putInItem(Producer.seedOf(Producer.melonCrop()));
        storage.putInItem(Producer.seedOf(Producer.pineappleCrop()));
        storage.putInItem(Producer.seedOf(Producer.strawberryCrop()));
        storage.putInItem(Producer.seedOf(Producer.pepperCrop()));
        storage.putInItem(Producer.urine());
        storage.putInItem(Producer.ammonia());
        storage.putInItem(Producer.cowMeat());
        storage.putInItem(Producer.sheepMeat());
        storage.putInItem(Producer.chickenMeat());
        storage.putInItem(Producer.fishMeat());
        storage.putInItem(Producer.medicine(1));
        storage.putInItem(Producer.medicine(2));
        storage.putInItem(Producer.medicine(3));
    }
}
