import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by pc on 5/18/2017.
 */
public class WorkShopMenu extends Menu {
    WorkShop workShop;

    public WorkShopMenu(WorkShop workShop) {
        this.workShop = workShop;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("you are in WorkShop");
        s.add("1. Check this shop\n" +
                "2. Make a tool\n" +
                "3. Repair a tool\n" +
                "4. Disassemble a tool");
        return s;
    }
    public boolean canRepair(Tool tool, Storage storage){
        Boolean flag = true;
        int h = 0;
        ArrayList<ItemStack> itemStacks = storage.getItemStacks();
        for (Builder builder : tool.repairbuilders) {
            for (ItemStack itemStack : itemStacks) {
                if (itemStack.getItems().get(0).getName().equals(builder.getItems().getName()) && itemStack.getItems().size() >= builder.getNum())
                    h++;
            }
        }
        if (h != tool.repairbuilders.size())
            flag = false;

        if (flag) {
            for (Builder builder : tool.repairbuilders) {
                storage.takeItem(builder.getItems().getName(),builder.getNum());
            }
        }
        return flag;


    }
    public boolean canMake(Tool tool, Storage storage) {
        Boolean flag = true;
        int h = 0;
        ArrayList<ItemStack> itemStacks = storage.getItemStacks();
        for (Builder builder : tool.newbuilders) {
            for (ItemStack itemStack : itemStacks) {
                if (itemStack.getItems().get(0).getName().equals(builder.getItems().getName()) && itemStack.getItems().size() >= builder.getNum())
                   h++;
            }
        }
        if (h != tool.newbuilders.size())
            flag = false;

        if (flag) {
            for (Builder builder : tool.newbuilders) {
                        storage.takeItem(builder.getItems().getName(),builder.getNum());
            }
        }
        return flag;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if (x == 1) {

            int num = 1;
            for (Tool tool : workShop.tools1) {
                System.out.println(num + "." + tool.getName());
                num++;
            }

            Scanner reader = new Scanner(System.in);
            try {
                String s = reader.next();
                int t = Integer.parseInt(s);
                ArrayList<String> s2 = new ArrayList<>();
                if (t <= 24) {
                    int num1 = 1;
                    for (Tool tool : workShop.tools1) {
                        if (num1 == t) {

                            s2 = tool.madeof();
                            for (String s3 : s2)
                                System.out.println(s3);
                        }
                        num1++;
                    }
                }
            } catch (Exception e) {
                System.out.println("BadCommand!");
            }
        }
        if (x == 2) {
            int num = 1;
            for (Tool tool : workShop.tools1) {
                System.out.println(num + "." + tool.getName());
                num++;
            }
            Scanner reader = new Scanner(System.in);

            try {
                String s1 = reader.next();
                int x1 = Integer.parseInt(s1);
                Storage storage = person.getBackPack();
                if (x1 <= 24) {
                    int num1 = 1;
                    for (Tool tool : workShop.tools1) {
                        if (num1 == x1) {
                            if (tool.newprice <= person.getMoney() && canMake(tool, storage)) {
                                ItemStack itemStack = new ItemStack();
                                if (tool instanceof Axe)
                                    itemStack.getItems().add(Producer.axe(((Axe) tool).type));
                                if (tool instanceof PickAxe)
                                    itemStack.getItems().add(Producer.pickAxe(((PickAxe) tool).type));
                                if (tool instanceof Shovel)
                                    itemStack.getItems().add(Producer.shovel(((Shovel) tool).type));
                                if (tool instanceof FishingRod)
                                    itemStack.getItems().add(Producer.fishingRod(((FishingRod) tool).type));
                                if (tool instanceof WateringCan)
                                    itemStack.getItems().add(Producer.wateringCan(((WateringCan) tool).type));
                                storage.putInItemStack(itemStack, 1);
                                person.takeMoney(tool.newprice);
                                System.out.println(tool.getName() + " added to BackPack");
                            } else
                                System.out.println("Not enough resources!");
                        }
                        num1++;
                    }
                }
            } catch (Exception e) {
            System.out.println("You should choose a Tool!");
            }
        }
        if (x == 3){
            System.out.println("Choose your item from BackPack:");
            Storage storage = person.getBackPack();
            try {
                ItemStack itemStack = storage.getItemStackFromPlayer();
                Tool tool = (Tool) itemStack.getItems().get(0);
                int zarib = 1;
                if (tool.getHealth() < 50)
                    zarib = 2;
                if (tool.getHealth() < 100) {
                    if (person.getMoney() >= tool.repairprice && canRepair(tool,storage)) {
                        person.giveMoney((-1)*tool.repairprice /zarib);
                        tool.health =100;
                        System.out.println(tool.getName() + " Repaired!");
                    } else
                        System.out.println("Not enough resources");
                }
                else
                    System.out.println("This tool is not broken!");
            }
            catch (Exception e)
            {
                System.out.println("You should choose a Tool");
            }
        }
        if (x == 4)
        {
            Storage storage = person.getBackPack();
            System.out.println("Choose Item:");
            ItemStack itemStack = storage.getItemStackFromPlayer();
            if (itemStack.getItems().get(0) instanceof Tool){
                Tool tool = (Tool) itemStack.getItems().get(0);
                double zarib = tool.health/100.0;
                for (Builder builder : tool.newbuilders){
                    ArrayList<Item> items = new ArrayList<>();
                    if (builder.getItems() instanceof Wood){
                        for (int i =1;i <= (int)Math.floor(builder.getNum()*zarib) ; i++)
                        storage.putInItem(Producer.wood(((Wood) builder.getItems()).getType()));
                    }
                    else if (builder.getItems() instanceof Stone) {
                        for (int i =1; i <= (int)Math.floor(builder.getNum()*zarib) ; i++)
                        storage.putInItem(Producer.stone(((Stone) builder.getItems()).getType()));
                    }
                    else
                    {
                        for (int i =1; i <= (int)Math.floor(builder.getNum()*zarib) ; i++)
                            storage.putInItem(Producer.thread());
                    }

                }
                System.out.println("tool has been disassembled");
                storage.takeItem(tool.getName(),1);
            }
            else
                System.out.println("You should choose an Item!");
        }
        return null;

    }
}
