
import java.util.*;

/**
 * has InstrumentBlocks
 */
public class MachineRoom extends Place {

    /**
     * Default constructor
     */
    public MachineRoom(String name) {
        super(name);
    }

    boolean hasJuicer;

    private ArrayList<Machine> machines = new ArrayList<>();

    public ArrayList<Machine> getMachines() {
        return machines;
    }
}