import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/13/2017.
 */
public class FruitGardenMenu extends Menu {

    FruitGarden fruitGarden;

    public FruitGardenMenu(FruitGarden fruitGarden){
        this.fruitGarden = fruitGarden;
    }
    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in FruitGarden: " + fruitGarden.getName());
        s.add("0.Buy a new tree");
        s.add("List of trees:");
        for (int i = 0; i < fruitGarden.trees.size(); i++){
            Tree tree = fruitGarden.trees.get(i);
            if(tree.isBought)
                s.add((i+1) + "." + tree.getName());
        }
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if( x == 0 ){
            System.out.println("What tree do you want to buy?");
            for (int i = 0; i < fruitGarden.trees.size(); i++){
                System.out.println((i+1) + "." + fruitGarden.trees.get(i).getName());
            }
            Scanner scanner = new Scanner(System.in);
            int num = scanner.nextInt();
            try{
                Tree tree = fruitGarden.trees.get(num-1);
                if(tree.isBought){
                    System.out.println("You already have bought this tree!");
                    return null;
                }
                System.out.println("Price of this tree is: $" + tree.getPrice());
                System.out.println("Do you want to buy it?(if yes enter \"YES\")");

                String s = scanner.next();
                if(s.equals("YES")){
                    if(person.getMoney() >= tree.getPrice()){
                        tree.isBought = true;
                        person.takeMoney(tree.getPrice());
                        System.out.println("Tree Bought!");
                    }
                    else {
                        System.out.println("Not enough money!");
                    }
                }
            }
            catch (Exception e){
                System.out.println("BadCommand!");
            }
            return null;
        }
        try {
            Tree tree = fruitGarden.trees.get(x-1);
            if(!tree.isBought){
                System.out.println("You should buy this tree first!");
                return null;
            }
            return new TreeMenu(tree);
        }
        catch (Exception e){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
