import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/18/2017.
 */
public class LaboratoryMenu extends Menu{

    Laboratory laboratory;

    public LaboratoryMenu(Laboratory laboratory){
        this.laboratory = laboratory;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in Lab.");
        if(!laboratory.getMachineRoom().hasJuicer){
            s.add("0.Juicer   price: $1000");
        }
        int ID = 0;
        for(Machine machine : laboratory.getMachines()){
            s.add((++ID) + "." + machine.getName() + "   price: $" + machine.getPrice());
        }
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        try {
            Scanner scanner = new Scanner(System.in);
            if (x == 0){
                if (laboratory.getMachineRoom().hasJuicer){
                    System.out.println("BadCommand!");
                    return null;
                }
                if (person.getMoney() < 1000){
                    System.out.println("Not enough money!");
                    return null;
                }
                System.out.println("A machine for juicing fruits.");
                System.out.println("Are you sure you want to buy this machine?(YES/NO)");

                String s = scanner.next();
                if(!s.equals("YES"))
                    return null;
                person.takeMoney(1000);
                System.out.println("Juicer Bought!");
                laboratory.getMachineRoom().hasJuicer = true;
                return null;
            }
            Machine machine = laboratory.getMachines().get(x-1);
            if (person.getMoney() < machine.getPrice()){
                System.out.println("Not enough money!");
                return null;
            }
            System.out.println(machine.status());
            System.out.println("Are you sure you want to buy this machine?(YES/NO)");
            String s = scanner.next();
            if(!s.equals("YES"))
                return null;
            person.takeMoney(machine.getPrice());
            laboratory.getMachineRoom().getMachines().add(machine);
            laboratory.getMachines().remove(machine);
            System.out.println("Machine bought!");
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
