
import java.util.*;

/**
 * 
 */
public class Kitchen extends Thing {

    /**
     * Default constructor
     */
    public Kitchen() {
    }

    private ArrayList<ItemStack> toolShelf = new ArrayList<>();
    private ArrayList<Recipe> recipes = new ArrayList<>();

    public ArrayList<ItemStack> getToolShelf() {
        return toolShelf;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    public ItemStack findItemStack(String name) {
        for (ItemStack itemStack : toolShelf){
            if (itemStack.getItems().get(0).getName().equals(name))
                return itemStack;
        }
        return null;
    }

    public Kitchen(String name) {
        super(name);
    }

    public void checkRecipes(){
        for (Recipe recipe : recipes){
            System.out.println(recipe.getName());
        }
    }
}