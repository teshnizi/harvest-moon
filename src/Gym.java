
import java.util.*;

/**
 * 
 */
public class Gym extends Thing {

    public Gym(String name, ArrayList<Exercise> exercises) {
        super(name);
        this.exercises = exercises;
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    ArrayList<Exercise> exercises;
}