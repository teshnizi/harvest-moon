import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/14/2017.
 */
public class ClinicMenu extends StoreMenu {

    Clinic clinic;

    public ClinicMenu(Clinic clinic) {
        super(clinic);
        this.clinic = clinic;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in Clinic");
        s.add("1.Heal me!");
        s.add("2.Check items");
        s.add("3.Buy items");

        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if(x == 1){
            try {
                double hp = person.getHealth().getCurrentValue();
                System.out.println("Do you want to be Healed for $1000?(YES)");
                Scanner scanner = new Scanner(System.in);
                String cmnd = scanner.next();
                if(cmnd.equals("YES")){
                    if (person.getMoney() < 100){
                        System.out.println("Not Enough Money!");
                        return null;
                    }
                    person.getHealth().changeCurrentValue(1000);
                    person.takeMoney(100);
                    System.out.println("Healing done!");
                }
            }
            catch (Exception E){
                System.out.println("BadCommand!");
            }
            return null;
        }
        super.getCommand(x,person);
        return null;
    }

}
