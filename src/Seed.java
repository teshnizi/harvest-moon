
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Seed extends Resource  implements Serializable {

    /**
     * Default constructor
     */
    public Seed(String name, Crop crop, int price) {
        super(name,1,price);
        this.crop = crop;
    }

    Crop crop;

    public Crop getCrop() {
        return crop;
    }

    @Override
    public String status(){
        return "Seed of " + crop.status();
    }
    //TO DO .....
    public Seed Clone(){
        return null;
    }
}