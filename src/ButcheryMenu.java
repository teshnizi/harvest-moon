import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/17/2017.
 */
public class ButcheryMenu extends StoreMenu{

    public ButcheryMenu(Butchery butchery) {
        super(butchery);
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("This is " + store.getName());
        s.add("1.Sell animals");
        s.add("2.Check items");
        s.add("3.Buy items");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {

        try {
            Scanner scanner = new Scanner(System.in);
            if (x == 1) {
                System.out.println("Choose an animal:");

                int ID = 0;
                for (AnimalField animalField : ((Butchery)store).barn.getAnimalFields())
                    System.out.println((++ID) + "." + animalField.getName());
                ID = scanner.nextInt();
                AnimalField animalField = ((Butchery)store).barn.getAnimalFields().get(ID-1);
                ID = 0;
                for(Animal animal : animalField.getAnimals())
                    System.out.println((++ID) + "." + animal.getName());
                ID = scanner.nextInt();
                Animal animal = animalField.getAnimals().get(ID-1);
                System.out.println("Do you want to sell " + animal.getName() + " for $" + animal.getPrice() + " ?(YES/NO)");
                String cmnd = scanner.next();
                if(cmnd.equals("YES")){
                    person.giveMoney(animal.getPrice());
                    System.out.println(animal.getName() + " sold!");
                    animalField.getAnimals().remove(animal);
                }
                return null;
            }
        }
        catch (Exception E){
            System.out.println("BadCommand!");
            return null;
        }
        super.getCommand(x, person);

        return null;
    }
}
