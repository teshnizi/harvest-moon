import org.omg.PortableServer.ServantActivator;

import java.io.Serializable;

/**
 * Created by Tigerous215 on 5/12/2017.
 */
public class Collectable extends Item implements Serializable{

    public Collectable(String name, int size, int price) {
        super(name, size, price);
    }
}
