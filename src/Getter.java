import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by pc on 7/13/2017.
 */
public class Getter  implements Runnable  {
    static Socket socket;
    static boolean ishost = false;
    static String hostname;
    static String socketname;
    Thread t = null;
    public Getter(Socket socket , String name){
        this.socket = socket;
        socketname = name;
        t = new Thread(this);
        t.start();
    }
    @Override
    public void run(){
        while (true){
            if (socket != null) {
                try {
                    DataInputStream din = new DataInputStream(socket.getInputStream());
                    if (socket.isConnected()) {
                        String s = din.readUTF();
                        System.out.println(s);
                        String[] strings = s.split(Protocol.protocol);
                        if (strings.length > 1) {
                            if (strings[1].equals(Protocol.play)) {
                                Main.startgame = true;
                                Main.timeline.play();
                                FarmBuilder.infarms = true;
                            } else if (strings[1].equals(Protocol.character)) {
                                if (strings[2].equals(Protocol.villages)) {
                                    if (VillageBuilder.invillage) {
                                        handler(VillageBuilder.chars, strings[0], Protocol.villages, strings[3], Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
                                    }
                                }
                                else if (strings[2].equals((Protocol.market))){
                                    if (MarketBuilder.inmarket)
                                    {
                                        handler(MarketBuilder.chars, strings[0], Protocol.market, strings[3], Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
                                    }
                                }
                                else if (strings[2].equals((Protocol.forest))){
                                    if (ForestBuilder.inforest)
                                    {
                                        handler(ForestBuilder.chars, strings[0], Protocol.forest, strings[3], Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
                                    }
                                }
                                else if (strings[2].equals(Protocol.cave)){
                                    if (CaveBuilder.incave){
                                        handler(CaveBuilder.chars, strings[0], Protocol.cave, strings[3], Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
                                    }
                                }
                            }
                            else if (strings[1].equals(Protocol.clinic))
                            {
                                String itemname = strings[2];
                                int number = Integer.parseInt(strings[3]);
                                getclinicitem(itemname,number);
                            }
                            else if (strings[1].equals(Protocol.market)){
                                String storename = strings[2];
                                String item = strings[3];
                                int num = Integer.parseInt(strings[4]);
                                int type = 0;
                                if (strings[5].equals(Protocol.add))
                                    type =1;
                                else
                                    type =-1;
                                storechanger(storename,item,num,type);
                            }
                            else if (strings[1].equals(Protocol.foretsource)){
                               forest(Integer.parseInt(strings[2]),Integer.parseInt(strings[3]),Integer.parseInt(strings[4]));
                            }
                            else if (strings[1].equals(Protocol.cavesource)){
                                cave(Integer.parseInt(strings[2]),Integer.parseInt(strings[3]),Integer.parseInt(strings[4]));
                            }
                            else if (strings[1].equals((Protocol.reject)))
                            {
                                if (strings[2].equals(socketname))
                                {
                                    Platform.runLater(() -> {
                                       UserBuilder.online.setText("you are rejected");
                                    });
                                }
                            }
                            else if (strings[1].equals(Protocol.chatelert)){
                                if (strings[2].equals(socketname)){
                                    Platform.runLater(() -> {
                                        if (!ChatRoom.inchat){
                                            Stage stage = new Stage();
                                            try {
                                                ChatRoom chatRoom = new ChatRoom(socket,socketname,strings[0],new Image("images\\white-texture.jpg"));
                                                chatRoom.start(stage);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }
                            else if (strings[1].equals(Protocol.privatchatroom) && strings[3].equals(socketname)){
                                if (strings[2].equals(Protocol.text)){
                                    ChatRoom.tta = (strings[0] + " Says : " + strings[4]);
                                }
                                if (strings[2].equals(Protocol.images)){
                                    ChatRoom.sendimage = strings[4];
                                }
                                if (strings[2].equals(Protocol.music))
                                {
                                    ChatRoom.sendmusic = strings[4];
                                }
                            }
                            else if (strings[1].equals(Protocol.Showstatusrequest) && strings[2].equals(socketname)){
                                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                                dout.writeUTF(socketname + Protocol.protocol + Protocol.status + Protocol.protocol + strings[0] + Protocol.protocol + socketname + Protocol.protocol + Main.Eisenhower.getEnergy().getCurrentValue()
                                + Protocol.protocol + Main.Eisenhower.getHealth().getCurrentValue() + Protocol.protocol +  Barn.animalFields.size() +  Protocol.protocol + Main.Eisenhower.getMoney()
                                );
                            }
                            else if (strings[1].equals(Protocol.status) && strings[2].equals(socketname)){
                                Showstatus.name.setText("Name : " + strings[3]);
                                Showstatus.energy.setText("Energy : " + strings[4]);
                                Showstatus.health.setText("Health : " + strings[5]);
                                Showstatus.animal.setText("Animals number :"  + strings[6]);
                                Showstatus.money.setText("Money : "  + strings[7]);
                            }
                            else if (strings[1].equals(Protocol.friendrequest) && strings[2].equals(socketname)){
                                Platform.runLater(() -> {
                                    AcceptBuilder acceptBuilder = new AcceptBuilder(strings[0]);
                                    Stage stage = new Stage();
                                    try {
                                        acceptBuilder.start(stage);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                });
                            }
                            else if (strings[1].equals(Protocol.removerequst)&&strings[2].equals(socketname)){
                                if (Person.friends.contains(strings[0])){
                                    Person.friends.remove(strings[0]);
                                }
                            }
                            else if (strings[1].equals(Protocol.friendanswer) && strings[2].equals(socketname)){
                                if (!Person.friends.contains(strings[0])){
                                    Person.friends.add(strings[0]);
                                }
                            }
                            else if (strings[1].equals(Protocol.accepttrade) && strings[2].equals(socketname)){
                                Platform.runLater(() -> {
                                    Accepttrade accepttrade = new Accepttrade(strings[0]);
                                    Stage stage = new Stage();
                                    try {
                                        accepttrade.start(stage);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                });
                            }
                            else if (strings[1].equals(Protocol.opentrade) && strings[2].equals(socketname)){
                                Platform.runLater(()->{
                                    Trade trade = new Trade(strings[0]);
                                    Stage stage = new Stage();
                                    try {
                                        trade.start(stage);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                });
                            }
                            else if (strings[1].equals(Protocol.youlock) && strings[2].equals(socketname)){
                                Trade.youlock = true;
                                if (Trade.mylock){
                                    Platform.runLater(()->{
                                        Trade.accept.setVisible(true);
                                    });
                                }
                            }
                            else if (strings[1].equals(Protocol.acceptsender) && strings[2].equals(socketname)){
                                Trade.youaccept  =true;
                                if (Trade.myaccept){
                                    for (AbstractMap.SimpleEntry<Text,String> x :Trade.map){
                                        int num2 = Integer.parseInt(x.getKey().getText());
                                        String [] s3 = x.getValue().split("&");
                                        int price = Integer.parseInt(s3[0]);
                                        int size = Integer.parseInt(s3[1]);
                                        ItemStack itemStackt = null;
                                        if (num2 != 0) {
                                            try {
                                                DataOutputStream dout1 = new DataOutputStream(Getter.socket.getOutputStream());
                                                dout1.writeUTF(Getter.socketname + Protocol.protocol + Protocol.itemtrad + Protocol.protocol + strings[0] + Protocol.protocol + s3[2] + Protocol.protocol + num2);
                                                Main.Eisenhower.getBackPack().takeItem(s3[2],num2);
                                                Main.Eisenhower.getBackPack().synchronizeStorage();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                            else if (strings[1].equals(Protocol.itemtrad) && strings[2].equals(socketname)){
                                for (ItemStack itemStack : Everything.storage.getItemStacks()){
                                    if (itemStack.getItems().get(0).getName().equals(strings[3]))
                                    {
                                        int number= Integer.parseInt(strings[4]);
                                        for (int i = 0; i <number ; i++) {
                                            Main.Eisenhower.getBackPack().putInItem(itemStack.getItems().get(0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    } catch(IOException e){
                        e.printStackTrace();
                        break;
                    }
                }

        }
    }
    public void handler(ArrayList<Pair<Pair<String,ImageView>,Pair<Integer,Integer>>> cha, String myname, String place, String imagesadress , int x , int y){
        int flag = 0;
        Pair<Pair<String,ImageView>,Pair<Integer,Integer>> t1 = null , kk =null;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : cha){
            if (p.getKey().getKey().equals(myname))
            {
                kk =p;
                t1 = new Pair<>(new Pair<>(myname,p.getKey().getValue()),new Pair<>(x,y));
                flag++;
                if (place.equals(Protocol.villages))
                {
                    Pair<Integer,Integer> mypair = new Pair<>(x,y);
                    VillageBuilder.villageAvailableTiles.add(p.getValue());
                    VillageBuilder.villageAvailableTiles.remove(mypair);
                    mypair = MovementHandler.tileToPixel(x,y);
                    if (!VillageBuilder.villageGroup.getChildren().contains(p.getKey().getValue())) {
                        Platform.runLater(() -> {
                            VillageBuilder.villageGroup.getChildren().add(p.getKey().getValue());
                        });
                    }
                        p.getKey().getValue().setX(mypair.getKey());
                    p.getKey().getValue().setY(mypair.getValue());
                    p.getKey().getValue().setImage(new Image(imagesadress));

                    break;
                }
                else if(place.equals(Protocol.market)){
                    Pair<Integer,Integer> mypair = new Pair<>(x,y);
                    MarketBuilder.marketAvailableTiles.add(p.getValue());
                    MarketBuilder.marketAvailableTiles.remove(mypair);
                    if (!MarketBuilder.marketGroup.getChildren().contains(p.getKey().getValue())) {
                        Platform.runLater(() -> {
                            MarketBuilder.marketGroup.getChildren().add(p.getKey().getValue());
                        });
                    }
                    mypair = MovementHandler.tileToPixel(x,y);
                    p.getKey().getValue().setX(mypair.getKey());
                    p.getKey().getValue().setY(mypair.getValue());
                    p.getKey().getValue().setImage(new Image(imagesadress));
                }
                else if (place.equals(Protocol.forest)){
                    Pair<Integer,Integer> mypair = new Pair<>(x,y);
                    ForestBuilder.forestAvailableTiles.add(p.getValue());
                   ForestBuilder.forestAvailableTiles.remove(mypair);
                    if (!ForestBuilder.forestGroup.getChildren().contains(p.getKey().getValue())) {
                        Platform.runLater(() -> {
                            ForestBuilder.forestGroup.getChildren().add(p.getKey().getValue());
                        });
                    }
                    mypair = MovementHandler.tileToPixel(x,y);
                    p.getKey().getValue().setX(mypair.getKey());
                    p.getKey().getValue().setY(mypair.getValue());
                    p.getKey().getValue().setImage(new Image(imagesadress));
                }
                else if (place.equals(Protocol.cave)){
                    Pair<Integer,Integer> mypair = new Pair<>(x,y);
                    CaveBuilder.caveAvailableTiles.add(p.getValue());
                    CaveBuilder.caveAvailableTiles.remove(mypair);
                    if (!CaveBuilder.caveGroup.getChildren().contains(p.getKey().getValue())) {
                        Platform.runLater(() -> {
                            CaveBuilder.caveGroup.getChildren().add(p.getKey().getValue());
                        });
                    }
                    mypair = MovementHandler.tileToPixel(x,y);
                    p.getKey().getValue().setX(mypair.getKey());
                    p.getKey().getValue().setY(mypair.getValue());
                    p.getKey().getValue().setImage(new Image(imagesadress));
                }
            }
        }
        if (flag == 0)
        {
            Pair<Integer,Integer> mypair = new Pair<>(x,y);
            ImageView imageView = new ImageView(new Image(imagesadress));
            Pair<Integer,Integer> mypair1 = MovementHandler.tileToPixel(x,y);
            imageView.setX(mypair1.getKey());
            imageView.setY(mypair1.getValue());
            Pair<Pair<String,ImageView>,Pair<Integer,Integer>> t = new Pair<>(new Pair<>(myname,imageView),new Pair<>(x,y));
            cha.add(t);
            if (place.equals(Protocol.villages)) {
                VillageBuilder.villageAvailableTiles.remove(mypair);
                Platform.runLater(() -> {
                    // code that updates UI
                   VillageBuilder.villageGroup.getChildren().add(imageView);
                });
            }
            else if (place.equals(Protocol.market)){
                MarketBuilder.marketAvailableTiles.remove(mypair);
                Platform.runLater(() -> {
                    // code that updates UI
                    MarketBuilder.marketGroup.getChildren().add(imageView);
                });
            }
            else if (place.equals(Protocol.forest)){
               ForestBuilder.forestAvailableTiles.remove(mypair);
                Platform.runLater(() -> {
                    // code that updates UI
                    ForestBuilder.forestGroup.getChildren().add(imageView);
                });
            }
            else if (place.equals(Protocol.cave)){
                CaveBuilder.caveAvailableTiles.remove(mypair);
                Platform.runLater(() -> {
                    // code that updates UI
                    CaveBuilder.caveGroup.getChildren().add(imageView);
                });
            }
        }
        else
        {
            cha.remove(kk);
            cha.add(t1);
        }
    }
    public void getclinicitem(String name , int num){
        Clinic clinic = Main.village.getClinic();
        Storage storage = clinic.storage;
        storage.takeItem(name,num);
        storage.synchronizeStorage();
    }
    public void storechanger(String store , String item , int num , int type)
    {
       Store store1 = null;
       for (Store store2 : Main.market.getStores()){
           if (store2.getName().equals(store)){
               store1 = store2;
               break;
           }
       }
       if (type == -1)
       {
           store1.storage.takeItem(item,num);
           store1.storage.synchronizeStorage();
       }
       else
       {
           ItemStack itemStack = null;
           for (ItemStack itemStack1 : store1.storage.getItemStacks()){
               if (itemStack1.getItems().get(0).getName().equals(item)){
                   itemStack = itemStack1;
                   break;
               }

           }
           for (int i = 0; i <num ; i++) {
               store1.storage.putInItem(itemStack.getItems().get(0));
           }
       }
    }
    public void forest(int x ,int y , int num){
        Storage storage = null;
        for (Pair<Storage,Pair<Integer,Integer>> p : Forest.res){
            if (p.getValue().equals(new Pair<>(x,y)))
            {
                storage = p.getKey();
                break;
            }
        }
        storage.takeItem(storage.getItemStacks().get(0).getItems().get(0).getName() , num);
        ForestBuilder.repair();
    }
    public void cave(int x ,int y , int num){
        Storage storage = null;
        for (Pair<Storage,Pair<Integer,Integer>> p : Cave.res){
            if (p.getValue().equals(new Pair<>(x,y)))
            {
                storage = p.getKey();
                break;
            }
        }
        storage.takeItem(storage.getItemStacks().get(0).getItems().get(0).getName() , num);
        CaveBuilder.repair();
    }
}
