import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pc on 7/10/2017.
 */
public class RanchBuilder extends Application {
    int j = 0;
    int x = 0;
    int constant = Main.village.getRanch().friendly;
    String s = null;
    boolean flag = false;
    Image cow = new Image("images\\cow.png");
    Image sheep = new Image("images\\sheep.png");
    Image chicken = new Image("images\\chicken.png");

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group mainGroup = new Group();
        Scene scene = new Scene(mainGroup, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));

        Group group = new Group();
        mainGroup.getChildren().addAll(group);

        Text text = new Text("Ranch");
        text.setX(250);
        text.setY(50);
        text.setFont(new Font(40));

        Text text2 = new Text();
        text2.setX(150);
        text2.setY(500);
        text2.setFont(Main.comicFont);

        Text text3 = new Text();
        text3.setLayoutX(150);
        text3.setLayoutY(560);
        text3.setFont(Main.comicFont);

        Rectangle rectangle1 = new Rectangle(10, 10, 30, 30);
        rectangle1.setFill(new ImagePattern(new Image("images\\backButton.jpg")));
        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle1.setCursor(Cursor.HAND);
            }
        });
        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().clear();
                mainGroup.getChildren().addAll(group);
            }
        });

        Text text1 = new Text();
        text1.setX(150);
        text1.setY(400);
        text1.setFont(Main.comicFont);
        group.getChildren().add(text1);

        Button about = new Button("About");
        about.setLayoutX(50);
        about.setLayoutY(400);
        about.setFont(Main.comicFont);
        group.getChildren().add(about);
        about.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                about.setCursor(Cursor.HAND);
            }
        });
        Random random = new Random();
        x = random.nextInt();
        x %= 2;
        about.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String my;
                if (x == 0) {
                    my = "your relations with shopkeeper is warm";
                    constant = Main.village.getRanch().friendly + 5;
                    System.out.println(constant);
                } else {
                    my = "your relations with shopkeeper is cold";
                    constant = Main.village.getRanch().friendly - 5;
                }

                if (Main.village.getRanch().isOnAuction)
                    my += "\nShop is on Auction";
                my += "\n This shopKeeper is not friendly with " + Main.village.getRanch().strings.get(0);
                text1.setText(my);
            }
        });

        Button button = new Button("Buy animals");
        button.setLayoutX(200);
        button.setLayoutY(150);
        button.setFont(Main.comicFont);
        button.setPrefSize(200, 20);
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setCursor(Cursor.HAND);
            }
        });
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().remove(group);
                Group group1 = new Group();
                getanimals(group1, scene);
                mainGroup.getChildren().add(rectangle1);
                mainGroup.getChildren().add(group1);
                rectangle1.setVisible(true);
            }
        });

        Button gift = new Button("Gift");
        gift.setLayoutX(200);
        gift.setLayoutY(200);
        gift.setFont(Main.comicFont);
        gift.setPrefSize(200, 20);
        group.getChildren().add(gift);
        gift.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                gift.setCursor(Cursor.HAND);
            }
        });
        gift.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mainGroup.getChildren().remove(group);
                Group group1 = new Group();
                gift(group1);
                mainGroup.getChildren().add(rectangle1);
                mainGroup.getChildren().add(group1);
                rectangle1.setVisible(true);
            }
        });


        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                text2.setText(" ");
            }
        });

        group.getChildren().addAll(text3, text, text2, button);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void getanimals(Group group, Scene scene) {
        Ranch ranch = Main.village.getRanch();
        Person person = Main.Eisenhower;
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Cow&1000");
        strings.add("Sheep&1000");
        strings.add("Chicken&500");

        Rectangle rectangle = new Rectangle(20,20);
        rectangle.setVisible(false);
        group.getChildren().add(rectangle);



        Text text = new Text(" ");
        text.setLayoutX(50);
        text.setLayoutY(400);
        text.setFont(Main.comicFont);
        group.getChildren().add(text);

        TextField textField = new TextField();
        textField.setStyle("-fx-border-color: darkgreen");
        textField.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (textField.getText().equals("Animal name"))
                    textField.setText("");
                textField.setStyle("-fx-text-fill: black");
            }
        });
        scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override

            public void handle(MouseEvent event) {
                if (textField.getText().equals("")) {
                    textField.setText("Animal name");
                    textField.setStyle("-fx-text-fill : gray");
                }

            }
        });
        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setVisible(false);
            }
        });

        Button buy = new Button("Buy");
        buy.setLayoutX(500);
        buy.setLayoutY(200);
        buy.setFont(Main.comicFont);
        group.getChildren().addAll(buy);
        buy.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setCursor(Cursor.HAND);
            }
        });
        buy.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (!(textField.getText().equals("Animal name") || textField.getText().equals(""))) {
                    String name = textField.getText();
                    Animal animal = null;
                    if (s.equals("Cow"))
                        animal = Producer.cow(name);
                    if (s.equals("Sheep"))
                        animal = Producer.sheep(name);
                    if (s.equals("Chicken"))
                        animal = Producer.chicken(name);
                    AnimalField animalField = null;
                    for (AnimalField af : ranch.barn.getAnimalFields()) {
                        if (s.equals("Cow") && af.getName() == "CowField")
                            animalField = af;
                        if (s.equals("Sheep") && af.getName() == "SheepField")
                            animalField = af;
                        if (s.equals("Chicken") && af.getName() == "ChickenField")
                            animalField = af;
                    }

                    if (animalField.getAnimals().size() >= animalField.maximumSize) {
                        text.setText("Maximum number of animals reached! You can expand your animal farm in barn.");
                    } else {
                        boolean gg = false;
                        for (Animal animal1 : animalField.getAnimals()) {
                            if (animal1.getName().equals(animal.getName())) {
                                text.setText("Name is already taken! Choose another name!");
                                gg = true;
                            }
                        }
                        if (!gg) {
                            int pricee = Math.max(animal.getPrice() + (50 - constant) * 5, 0);
                            if (person.getMoney() >= pricee) {
                                person.takeMoney(pricee);
                                animalField.getAnimals().add(animal);
                                text.setText(animal.getName() + " bought!");
                                Main.village.getRanch().friendly += pricee/50;
                                for (Store store : Main.market.getStores()){
                                    if (store.getName().equals("Butchery")) {
                                        store.firiendly -= 5;
                                        break;
                                    }
                                }
                            } else
                                text.setText("Money is not enough");
                        }
                    }
                } else {
                    textField.setText("Animal name");
                    textField.setStyle("-fx-text-fill: gray");
                }

            }
        });

        int num = 0;
        for (String string : strings) {

            String[] string1 = string.split("&");
            Button button = new Button(string1[0]);
            button.setLayoutX(50);
            button.setLayoutY(100 + (num % 10) * 50);
            button.setPrefSize(150, 30);
            button.setFont(Main.comicFont);
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                    rectangle.setLayoutX(button.getLayoutX() -30);
                    rectangle.setLayoutY(button.getLayoutY());
                    rectangle.setVisible(true);
                    if (button.getText().equals("Cow"))
                        rectangle.setFill(new ImagePattern(cow));
                    if (button.getText().equals("Sheep"))
                        rectangle.setFill(new ImagePattern(sheep));
                    if (button.getText().equals("Chicken"))
                        rectangle.setFill(new ImagePattern(chicken));
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    int mimim = Math.max(0, Integer.parseInt(string1[1]) + (50 - constant) * 5);
                    text.setText("Price : " + mimim + "$");
                    s = button.getText();
                    textField.setLayoutX(button.getLayoutX() + 160);
                    textField.setLayoutY(button.getLayoutY());
                    textField.setPrefSize(2, 30);
                    textField.setText("Animal name");
                    textField.setStyle("-fx-text-fill : gray");
                    Timeline timeline = new Timeline();
                    timeline.setCycleCount(100);
                    j = 0;
                    if (!flag)
                        group.getChildren().addAll(textField);
                    flag = true;
                    timeline.getKeyFrames().addAll(new KeyFrame(Duration.millis(7), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            j++;
                            textField.setPrefSize(j * 2, 30);
                        }
                    }));
                    timeline.play();
                    if (button.getText().equals("Cow")) {
                        text.setText("Price : " + Math.max(1000 + (50-constant) * 5, 0) + "&");
                    } else if (button.getText().equals("Sheep")) {
                        text.setText("Price : " + Math.max(1000 + (50 - constant) * 5, 0) + "&");
                    } else if (button.getText().equals("Chicken")) {
                        text.setText("Price : " + Math.max(500 + (50 - constant) * 5, 0) + "&");
                    }
                }
            });
            group.getChildren().add(button);
            num++;
        }
    }

    public void gift(Group group) {

        int num = 0;
        Label question = new Label("Choose a Gift:");
        question.setFont(new Font("Comic Sans MS", 25));
        question.setTextFill(Color.DARKGREEN);
        question.setLayoutX(200);
        question.setLayoutY(10);
        group.getChildren().addAll(question);
        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
            Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
            button.setLayoutX(20 + 200 * (num / 10));
            button.setLayoutY(80 + (num % 10) * 35);
            button.setFont(Main.comicFont);
            button.setPrefWidth(180);
            group.getChildren().addAll(button);
            num++;
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println(itemStack.getItems().get(0));

                    Main.village.getRanch().friendly+=itemStack.getItems().get(0).getPrice()/25;
                    Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(),1);
                    Main.Eisenhower.getBackPack().synchronizeStorage();
                    group.getChildren().clear();
                    gift(group);
                }
            });
        }
    }
}