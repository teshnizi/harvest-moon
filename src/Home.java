
import java.util.*;

/**
 * 
 */
public class Home extends Place {

    /**
     * Default constructor
     */
    public Home(String name) {
        super(name);
        bed = new Bed("Bed");
        kitchen = new Kitchen("Kitchen");
        storageBox = new Storage("StorageBox", 1000);
    }


    private Bed bed; //1
    private Kitchen kitchen;  //2
    private Storage storageBox;  //3

    public Kitchen getKitchen() {
        return kitchen;
    }
    public Storage getStorageBox() {
        return storageBox;
    }

    public void setKitchen(Kitchen kitchen) {
        this.kitchen = kitchen;
    }

    {
        commands.add("1.Bed");
        commands.add("2.Kitchen");
        commands.add("3.StorageBox");
    }

    @Override
    public Thing findObject(String name){
        if(name.equals("Bed"))return bed;
        if(name.equals("Kitchen"))return kitchen;
        if(name.equals("StorageBox"))return storageBox;
        return null;
    }

    @Override
    public ArrayList<String> listOfObjects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("Bed");
        s.add("StorageBox");
        s.add("Kitchen");
        return s;
    }

}