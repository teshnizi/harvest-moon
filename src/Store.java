
import jdk.nashorn.internal.ir.WhileNode;

import java.util.*;

/**
 * 
 */
public class Store extends Thing {

    /**
     * Default constructor
     */
    public Store(String name) {
        super(name);
        if (name.equals("Groceries Store"))
            string = "General Store";
        if (name.equals("General Store"))
            string = "Groceries Store";
        if (name.equals("Butchery"))
            string = "Ranch";
    }

    Storage storage = new Storage("Shop", 1000000);

    Person person;

    boolean isOnAuction;
    String string = null;
    public ArrayList<String> iteminside = new ArrayList<>();

    int day = 0;
    public int firiendly = 50;

    @Override
    public void nextDay(Season season){
        day++;
        isOnAuction = false;
        if (day%7 == 0)
            isOnAuction = true;

        storage.getItemStacks().clear();
        if(getName().equals("Groceries Store")) {
            for (int i = 0; i < 10; i++) {
                storage.putInItem(Producer.pear());
                storage.putInItem(Producer.peach());
                storage.putInItem(Producer.garlic());
                storage.putInItem(Producer.bean());
                storage.putInItem(Producer.cabbage());
                storage.putInItem(Producer.ale());
                storage.putInItem(Producer.lemon());
                storage.putInItem(Producer.pomegranate());
                storage.putInItem(Producer.cucumber());
                storage.putInItem(Producer.watermelon());
                storage.putInItem(Producer.onion());
                storage.putInItem(Producer.turnip());
                storage.putInItem(Producer.apple());
                storage.putInItem(Producer.orange());
                storage.putInItem(Producer.potato());
                storage.putInItem(Producer.carrot());
                storage.putInItem(Producer.tomato());
                storage.putInItem(Producer.melon());
                storage.putInItem(Producer.pineapple());
                storage.putInItem(Producer.strawberry());
                storage.putInItem(Producer.pepper());
                storage.putInItem(Producer.oil());
                storage.putInItem(Producer.flour());
                storage.putInItem(Producer.sugar());
                storage.putInItem(Producer.salt());
                storage.putInItem(Producer.cheese());
                storage.putInItem(Producer.bread());
                storage.putInItem(Producer.milk());
                storage.putInItem(Producer.oil());
                storage.putInItem(Producer.egg());

                for (Fruit fruit : Main.addedFruits)
                    storage.putInItem(Producer.copyFruit(fruit));

                for (Food food : Main.addedFoods)
                    storage.putInItem(Producer.copyFood(food));

            }
            iteminside.clear();
            for (ItemStack itemStack : storage.getItemStacks())
                iteminside.add(itemStack.getItems().get(0).getName());
        }
        if(getName().equals("General Store")) {
            for (int i = 0; i < 1; i++) {
                storage.putInItem(Producer.pickAxe(1));
                storage.putInItem(Producer.pickAxe(2));
                storage.putInItem(Producer.pickAxe(3));
                storage.putInItem(Producer.pickAxe(4));
                storage.putInItem(Producer.shovel(1));
                storage.putInItem(Producer.shovel(2));
                storage.putInItem(Producer.shovel(3));
                storage.putInItem(Producer.shovel(4));
                storage.putInItem(Producer.axe(1));
                storage.putInItem(Producer.axe(2));
                storage.putInItem(Producer.axe(3));
                storage.putInItem(Producer.axe(4));
                storage.putInItem(Producer.fishingRod(1));
                storage.putInItem(Producer.fishingRod(2));
                storage.putInItem(Producer.fishingRod(3));
                storage.putInItem(Producer.fishingRod(4));
                storage.putInItem(Producer.wateringCan(1));
                storage.putInItem(Producer.wateringCan(2));
                storage.putInItem(Producer.wateringCan(3));
                storage.putInItem(Producer.wateringCan(4));
                storage.putInItem(Producer.wateringCan(5));
                storage.putInItem(Producer.wateringCan(6));
                storage.putInItem(Producer.wateringCan(7));
                storage.putInItem(Producer.wateringCan(8));
                storage.putInItem(Producer.knife());
                storage.putInItem(Producer.fryingPan());
                storage.putInItem(Producer.agitator());
                storage.putInItem(Producer.pot());
                storage.putInItem(Producer.furnace());
            }
            for (int i = 0; i < 10; i++) {
                storage.putInItem(Producer.thread());
                storage.putInItem(Producer.alfalfa());
                storage.putInItem(Producer.grain());
                storage.putInItem(Producer.wool());
                storage.putInItem(Producer.seedOf(Producer.garlicCrop()));
                storage.putInItem(Producer.seedOf(Producer.beanCrop()));
                storage.putInItem(Producer.seedOf(Producer.cabbageCrop()));
                storage.putInItem(Producer.seedOf(Producer.aleCrop()));
                storage.putInItem(Producer.seedOf(Producer.cucumberCrop()));
                storage.putInItem(Producer.seedOf(Producer.watermelonCrop()));
                storage.putInItem(Producer.seedOf(Producer.onionCrop()));
                storage.putInItem(Producer.seedOf(Producer.turnipCrop()));
                storage.putInItem(Producer.seedOf(Producer.potatoCrop()));
                storage.putInItem(Producer.seedOf(Producer.carrotCrop()));
                storage.putInItem(Producer.seedOf(Producer.tomatoCrop()));
                storage.putInItem(Producer.seedOf(Producer.melonCrop()));
                storage.putInItem(Producer.seedOf(Producer.pineappleCrop()));
                storage.putInItem(Producer.seedOf(Producer.strawberryCrop()));
                storage.putInItem(Producer.seedOf(Producer.pepperCrop()));
                ItemStack itemStack = new ItemStack();
                for (Crop crop : Main.addedCrops){
                    storage.putInItem(Producer.seedOf(new Crop(crop.getName(),crop.season,crop.matureAge,crop.maxNumberOfHarvests,crop.dailyWaterNeed,
                            new ItemStack(Producer.copyFruit((Fruit)crop.getOutCome().getItems().get(0))))));
                }
                for (Resource resource : Main.addedResources){
                    storage.putInItem(Producer.copyResource(resource));
                }
                for (KitchenTool kitchenTool : Main.addedKitchenTools){
                    storage.putInItem(Producer.copyKitchenTool(kitchenTool));
                }
                storage.putInItem(Producer.urine());
                storage.putInItem(Producer.ammonia());
            }
            iteminside.clear();
            for (ItemStack itemStack : storage.getItemStacks())
                iteminside.add(itemStack.getItems().get(0).getName());
        }
        if(getName().equals("Butchery")) {
            for (int i = 0; i < 10; i++) {
                storage.putInItem(Producer.cowMeat());
                storage.putInItem(Producer.sheepMeat());
                storage.putInItem(Producer.chickenMeat());
                storage.putInItem(Producer.fishMeat());
            }
            iteminside.clear();
            for (ItemStack itemStack : storage.getItemStacks())
                iteminside.add(itemStack.getItems().get(0).getName());
        }

        if(getName().equals("Huckster")) {
            storage.getItemStacks().clear();
            for (int i = 0; i < 1; i++) {
                ArrayList<Item> arit = Producer.rareItems();
                for (Item item : arit)
                    storage.putInItem(item);
            }
        }

        for(ItemStack itemStack : storage.getItemStacks()){
            while(itemStack.getItems().size() > 40){
                itemStack.getItems().remove(itemStack.getItems().size()-1);
            }
            if (itemStack.getItems().get(0) instanceof Tool)
                while (itemStack.getItems().size() > 10){
                    itemStack.getItems().remove(itemStack.getItems().size()-1);
                }

        }
    }
}