
import java.util.*;

/**
 * 
 */
public abstract class Menu {

    /**
     * Default constructor
     */
    public Menu() {
    }

    public abstract ArrayList<String> whereAmI();
    public abstract Menu getCommand(int x, Person person);
    private Thing currentObject;

}