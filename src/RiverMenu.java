import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pc on 5/15/2017.
 */
public class RiverMenu extends Menu {
    private River river;
    public RiverMenu(River river){this.river = river;}
    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in River : ");
        s.add("1.Start Fishing");
        return s;
    }
    public boolean probellity(int prob){
        Random random = new Random();
        int n = random.nextInt(100) + 1;
        for (int i = 1 ;i <= prob ; i++) {
            if (i == n)
                return true;
        }
        return false;
    }
    @Override
    public Menu getCommand(int x, Person person) {
        Storage storage = person.getBackPack();
        if (x == 1) {
            try {
                ItemStack itemStack = storage.getItemStackFromPlayer();
                FishingRod fishingRod = (FishingRod) itemStack.getItems().get(0);
                if (fishingRod.canUseTool()) {
                    if (probellity((fishingRod.getType()+1)*20)) {
                        if (storage.putInItemStack(river.getFish(), Math.min(river.getFish().getItems().size(), 1)) == 1) {
                            System.out.println("1 number fish was caught." + "\nEnergy usage : " + (5-fishingRod.getType())*5);
                            fishingRod.fish();
                        }
                    }
                    else
                        System.out.println("Can't catch fish. try again");
                }
                else
                    System.out.println("you are too tiered");
            }
        catch (Exception e){
            System.out.println("you should chose fishing rod");
        }
        }
        return null;
    }
}
