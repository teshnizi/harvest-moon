
import java.util.*;

/**
 * 
 */
public class GreenHouse extends Thing {

    public boolean isRepaired;

    /**
     * Default constructor
     */
    public GreenHouse(String name) {
        super(name);
        fields = new ArrayList<>();
        for(int i = 0 ; i < 4; i++){
            fields.add(new Field("Field"+(i+1),9));
        }
    }

    private ArrayList<Field> fields;

    public ArrayList<Field> getFields() {
        return fields;
    }

    private  WeatherMachine weatherMachine = new WeatherMachine("WeatherMachine");

    public WeatherMachine getWeatherMachine() {
        return weatherMachine;
    }

    public void changeWeather(Season season) {
        weatherMachine.changeWeather(season);
    }

    @Override
    public void nextDay(Season season){
        for (Field field : fields){
            field.nextDay(weatherMachine.getSeason());
        }
    }
    /**
     * @return
     */
    public ArrayList<String> objects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("");
        return s;
    }

}