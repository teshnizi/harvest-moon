
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/18/2017.
 */
public class MachineRoomMenu extends Menu {

    MachineRoom machineRoom;

    public MachineRoomMenu(MachineRoom machineRoom){
        this.machineRoom = machineRoom;
    }
    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in machine room");
        if(machineRoom.hasJuicer){
            s.add("0.Juicer");
        }
        int ID = 0 ;
        for (Machine machine : machineRoom.getMachines()){
            s.add((++ID) + "." +  machine.getName());
        }
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        try {
            if( x == 0 ){
                if(!machineRoom.hasJuicer){
                    return null;
                }
                System.out.println("Choose a fruit:");
                if(person.getBackPack().getRemainingCapacity() < 1){
                    System.out.println("Not enough capacity!");
                    return null;
                }
                ItemStack it = person.getBackPack().getItemStackFromPlayer();
                if (!(it.getItems().get(0) instanceof Fruit)){
                    System.out.println("You should choose a fruit!");
                    return null;
                }
                Fruit fruit = (Fruit)it.getItems().get(0);
                person.getBackPack().takeItem(fruit.getName(),1);
                person.getBackPack().putInItem(Producer.juice(fruit));
                System.out.println(fruit.getName() + " juiced!");
                return null;
            }
            Machine machine = machineRoom.getMachines().get(x-1);
            System.out.println(machine.status());
            System.out.println("Do you want to use this machine?(YES/NO)");
            Scanner scanner = new Scanner(System.in);
            String s = scanner.next();
            if(!s.equals("YES")){
                return null;
            }

            if(person.getBackPack().getRemainingCapacity() < machine.outputSize()){
                System.out.println("Not enough space in backpack!");
                return null;
            }

            for (ItemStack itemStack : machine.getInputItems()){
                System.out.println("Choose " + itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size() + ":");
                ItemStack it = person.getBackPack().getItemStackFromPlayer();
                if(!it.getItems().get(0).getName().equals(itemStack.getItems().get(0).getName())){
                    System.out.println("You should choose " + itemStack.getItems().get(0).getName() + " !");
                    return null;
                }
                if(itemStack.getItems().size() > it.getItems().size()){
                    System.out.println("Not enough items!");
                    return null;
                }
            }


            for (ItemStack itemStack : machine.getInputItems()){
                person.getBackPack().takeItem(itemStack.getItems().get(0).getName(), itemStack.getItems().size());
            }

            for (ItemStack itemStack : machine.getOutPutItems()){
                ItemStack it = new ItemStack();
                for (Item item : itemStack.getItems()){
                    if (item instanceof Food)
                        it.getItems().add(new Food(item.getName(),item.getSize(),item.getPrice(),((Food) item).getEffect()));
                    if(item instanceof Resource)
                        it.getItems().add(new Resource(item.getName(), item.getSize(), item.getPrice()));
                }
                person.getBackPack().putInItemStack(it,it.getItems().size());
            }

            System.out.println("Transformation completed!");
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
