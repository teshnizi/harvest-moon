import java.util.ArrayList;
import java.util.Random;

/**
 * Created by pc on 5/15/2017.
 */;

public class StoneMenu extends Menu {
    StoneType rockType;
    public StoneMenu(StoneType rockType ){
        this.rockType = rockType;
    }

    @Override
    public ArrayList<String> whereAmI() {
        if (rockType.inforest) {
            ArrayList<String> s = new ArrayList<>();
            s.add("1.Collect Stones");
            return s;
        }
        else
        {
            ArrayList<String> s = new ArrayList<>();
            s.add("1.Collect Stones");
            s.add("2.Collect Iron Ore");
            s.add("3.Collect Silver Ore");
            s.add("4.Collect Adamantium Ore");
            return s;
        }
    }
    public ItemStack getrocks(int i){
        int num = 0;
        for (ItemStack itemStack: rockType.getRocks()){
            if (num == i)
                return itemStack;
        }
        return null;
    }
    @Override
    public Menu getCommand(int x, Person person) {
        if (rockType.inforest) {
            if (x == 1) {
                try {
                    Storage storage = person.getBackPack();
                    ItemStack itemStack = getrocks(0);
                    if (storage.putInItemStack(itemStack, Math.min(2, itemStack.getItems().size())) == 1) {
                        System.out.println(Math.min(10, itemStack.getItems().size()) + " Old Stones where collected \n" + "Energy usage : " + 30);
                        person.getEnergy().changeCurrentValue(-30);
                    }
                } catch (Exception e) {
                    System.out.println("Bad comment!");
                }
            }
        }
        else {
            Storage storage = person.getBackPack();
            if (x == 1) {
                try {
                    ItemStack itemStack = getrocks(0);
                    if (storage.putInItemStack(itemStack, Math.min(10, itemStack.getItems().size())) == 1) {
                        System.out.println(Math.min(2, itemStack.getItems().size()) + " Old Stones where collected\n" + "Energy usage : " + 30);
                        person.getEnergy().changeCurrentValue(-30);
                    }
                } catch (Exception e) {
                    System.out.println("Bad commands! ");
                }
            }
             else if (x == 2) {
                try {
                    ItemStack itemStack = getrocks(1);
                    System.out.println("Choose a PickAxe from your inventory. It has to be a Stone (or a stronger kind) PickAxe .");
                    ItemStack Pickaxe1 = storage.getItemStackFromPlayer();
                    PickAxe pickAxe = (PickAxe) Pickaxe1.getItems().get(0);
                    if (pickAxe.getType() >= 2){
                        int increase = pickAxe.getType() - 2;;
                        int rand =(int) (2.0 * Math.pow(2,increase)) ;
                        Random random = new Random();
                        rand = random.nextInt(rand + 1);
                        int energy = (-1) * (int) (30.0 * Math.pow(2,x-1)/Math.pow(1.6,pickAxe.getType()));
                        if (person.getEnergy().getCurrentValue() + energy >=0) {
                            int num =(int) Math.min((rand)*((double)(getrocks(1).getItems().size()/50.0)), getrocks(1).getItems().size());
                            if (num == 0)
                            {
                                System.out.println("Could not find anything. Better Luck next time");
                                return null;
                            }
                            if (storage.putInItemStack(itemStack, num) == 1)
                            {
                                System.out.println(num + "Iron Ores where collected " + "\nEnergy usage : " + (-1)*energy * num);
                                person.getEnergy().changeCurrentValue(energy * num);
                                pickAxe.collectOre();
                            }
                        }
                        else
                            System.out.println("you are too tired");
                    }
                    else
                        System.out.println("Choose stronger one");
                }
                catch (Exception e) {
                    System.out.println("you should choose pickaxe");
                }
            }
             else if (x == 3){
                try {
                    ItemStack itemStack = getrocks(2);
                    System.out.println("Choose a PickAxe from your inventory. It has to be a Iron (or a stronger kind) PickAxe .");
                    ItemStack Pickaxe1 = storage.getItemStackFromPlayer();
                    PickAxe pickAxe = (PickAxe) Pickaxe1.getItems().get(0);
                    if (pickAxe.getType() >= 3){
                        int increase = pickAxe.getType() - 3;
                        int rand =(int) (2.0 * Math.pow(2,increase)) ;
                        Random random = new Random();
                        rand = random.nextInt(rand + 1);
                        int energy = (-1) * (int) (30.0 * Math.pow(2,x-1)/Math.pow(1.6,pickAxe.getType()));
                        if (person.getEnergy().getCurrentValue() + energy >=0) {
                            int num = (int) Math.min((rand)*((double)(getrocks(2).getItems().size()/50.0)), getrocks(2).getItems().size());
                            if (num == 0)
                            {
                                System.out.println("Could not find anything. Better Luck next time");
                                return null;
                            }
                            if (storage.putInItemStack(itemStack, num) == 1)
                            {
                                System.out.println(num + "Silver Ores where collected" + "\nEnergy usage : " + (-1)*energy* num );
                                person.getEnergy().changeCurrentValue(energy * num);
                                pickAxe.collectOre();
                            }
                        }
                        else
                            System.out.println("you are too tired");
                    }
                    else
                        System.out.println("Choose stronger one");
                }
                catch (Exception e) {
                    System.out.println("you should choose pickaxe");
                }

            }
            else if (x == 4)
            {
                try {
                    ItemStack itemStack = getrocks(3);
                    System.out.println("Choose a PickAxe from your inventory. It has to be a Silver (or a stronger kind) PickAxe .");
                    ItemStack Pickaxe1 = storage.getItemStackFromPlayer();
                    PickAxe pickAxe = (PickAxe) Pickaxe1.getItems().get(0);
                    int energy = (-1) * (int) (30.0 * Math.pow(2,x-1)/Math.pow(1.6,pickAxe.getType()));
                    if (pickAxe.getType() >= 4){
                        if (person.getEnergy().getCurrentValue() + energy >=0) {
                            int increase = pickAxe.getType() - 4;
                            int rand =(int) (2.0 * Math.pow(2,increase)) ;
                            Random random = new Random();
                            rand = random.nextInt(rand + 1);

                            int num = (int) Math.min((rand)*((double)(getrocks(3).getItems().size()/50.0)), getrocks(3).getItems().size());
                            if (num == 0)
                            {
                                System.out.println("Could not find anything. Better Luck next time");
                                return null;
                            }
                            if (storage.putInItemStack(itemStack, num) == 1)
                            {
                                System.out.println(num + "Adamantium Ores where collected." + "\nEnergy usage : " + (-1)*energy* num );
                                person.getEnergy().changeCurrentValue(energy  * num);
                                pickAxe.collectOre();
                            }
                        }
                        else
                            System.out.println("you are too tired");
                    }
                    else
                        System.out.println("Choose stronger one");
                }
                catch (Exception e) {
                    System.out.println("you should choose pickaxe");
                }
            }
            else
                System.out.println("Bad commands");
        }
        return null;
    }
}
