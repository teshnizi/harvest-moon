import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by pc on 5/11/2017.
 */
public class GymMenu extends Menu {

    Gym gym;

    public GymMenu(Gym gym){
        this.gym = gym;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        try{
            Exercise exercise = gym.exercises.get(x-1);
            System.out.println(exercise.toString());
            System.out.println("Are you sure you wanna do this exercise?(YES/NO)");
            Scanner scanner = new Scanner(System.in);
            String cmnd = scanner.next();
            if(!cmnd.equals("YES"))
                return null;
            if(person.getMoney() < exercise.cost){
                System.out.println("Not enough money!");
                return null;
            }
            for (AbilityChange abilityChange : exercise.effect.getAbilityChanges()) {
                if (abilityChange.getAbility().equals(person.getHealth()) && person.getHealth().getMaxValue() >= 1000) {
                    System.out.println("Max value reached!");
                    continue;
                }
                if (abilityChange.getAbility().equals(person.getEnergy()) && person.getEnergy().getMaxValue() >= 2000) {
                    System.out.println("Max value reached!");
                    continue;
                }
                abilityChange.affect();
            }
            person.takeMoney(exercise.cost);
            System.out.println("HOof! Exercise done!");
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in gym.");
        s.add("Exercises You can do:");
        int ID = 0;
        for (Exercise exercise : gym.exercises)
            s.add((++ID) + "." + exercise.getName() + "   price: $" + exercise.cost);
        return s;
    }
}
