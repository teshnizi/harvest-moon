import java.util.ArrayList;

/**
 * Created by Tigerous215 on 5/16/2017.
 */
public class Producer {

    public  Producer(Person person){
        this.person = person;
    }

    static Person person;
    public static Fish fish(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange , abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),0,20,0);
        abilityChange1 = new AbilityChange(person.getHealth(),0,5,0);
        s.add(abilityChange);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fish("fish",5,50,effectOnBody);
    }

    public static Fruit pear(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),0,20,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Pear",1,15,effectOnBody);
    }

    public static Fruit peach(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange = new AbilityChange(person.getHealth(),0,15,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Peach",1,15,effectOnBody);
    }

    public static Fruit garlic(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),2,0,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Garlic",1,15,effectOnBody);
    }

    public static Fruit bean(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),2,0,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Bean",1,10,effectOnBody);
    }

    public static Fruit cabbage(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,10,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Cabbage",1,10,effectOnBody);
    }

    public static Fruit ale(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,10,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,-5,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Ale",1,20,effectOnBody);
    }

    public static Fruit lemon(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),0,20,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Lemon",1,15,effectOnBody);
    }

    public static Fruit pomegranate() {
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,15,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),5,15,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Pomegranate",1,25,effectOnBody);
    }

    public static Fruit cucumber(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),0,10,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Cucumber",1,10,effectOnBody);
    }

    public static Fruit watermelon(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),10,50,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getHealth(),0,10,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Watermelon",1,80,effectOnBody);
    }

    public static Fruit onion(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),5,0,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Onion",1,15,effectOnBody);
    }

    public static Fruit turnip(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),3,0,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),3,0,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Turnip",1,15,effectOnBody);
    }

    public static Fruit apple(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),5,0,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,10,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Apple",1,20,effectOnBody);
    }

    public static Fruit orange(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,10,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),5,0,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Orange",1,20,effectOnBody);
    }

    public static Fruit potato(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,-5,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,10,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Potato",1,25,effectOnBody);
    }

    public static Fruit carrot(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),10,0,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Carrot",1,25,effectOnBody);
    }

    public static Fruit tomato(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,5,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,5,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Tomato",1,10,effectOnBody);
    }

    public static Fruit melon(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,10,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),5,40,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Melon",1,60,effectOnBody);
    }

    public static Fruit pineapple(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),15,15,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),15,15,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Pineapple",1,150,effectOnBody);
    }

    public static Fruit strawberry(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),5,10,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),5,10,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Strawberry",1,50,effectOnBody);
    }

    public static Fruit pepper(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),5,0,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,10,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Fruit("Pepper",1,50,effectOnBody);
    }

    public static Crop garlicCrop(){
        return new Crop("Garlic Crop",Season.spring,3,1,1,new ItemStack(garlic()));
    }

    public static Crop beanCrop(){
        return new Crop("Bean Crop",Season.spring,3,2,1,new ItemStack(bean()));
    }

    public static Crop cabbageCrop(){
        return new Crop("Cabbage Crop",Season.spring,3,1,1,new ItemStack(cabbage()));
    }

    public static Crop aleCrop(){
        return new Crop("Ale Crop",Season.spring,3,3,1,new ItemStack(ale()));
    }

    public static Crop cucumberCrop(){
        return new Crop("Cucumber Crop",Season.summer,3,3,1,new ItemStack(cucumber()));
    }

    public static Crop watermelonCrop(){
        return new Crop("Watermelon Crop",Season.summer,3,3,1,new ItemStack(watermelon()));
    }

    public static Crop onionCrop(){
        return new Crop("Onion Crop",Season.summer,3,1,1,new ItemStack(onion()));
    }

    public static Crop turnipCrop(){
        return new Crop("Turnip Crop",Season.summer,3,1,1,new ItemStack(turnip()));
    }

    public static Crop potatoCrop(){
        return new Crop("Potato Crop",Season.autumn,3,1,1,new ItemStack(potato()));
    }

    public static Crop carrotCrop(){
        return new Crop("Carrot Crop",Season.autumn,3,1,1,new ItemStack(carrot()));
    }

    public static Crop tomatoCrop(){
        return new Crop("Tomato Crop",Season.autumn,3,3,1,new ItemStack(tomato()));
    }

    public static Crop melonCrop(){
        return new Crop("Melon Crop",Season.autumn,3,3,1,new ItemStack(melon()));
    }

    public static Crop pineappleCrop(){
        return new Crop("Pineapple Crop",Season.tropical,3,3,1,new ItemStack(pineapple()));
    }

    public static Crop strawberryCrop() {
        return new Crop("StrawberryCrop",Season.tropical,3,3,1,new ItemStack(strawberry()));
    }

    public static Crop pepperCrop(){
        return new Crop("Pepper Crop",Season.tropical,3,3,1,new ItemStack(pepper()));
    }

    public static Tree peachTree(){
        return new Tree("Peach", Season.spring, 2, 1, peach(), 100, false);
    }

    public static Tree pearTree(){
        return new Tree("Pear", Season.spring, 2, 1, pear(), 100, false);
    }

    public static Tree lemonTree(){
        return new Tree("Lemon", Season.summer, 2, 1, lemon(), 100, false);
    }

    public static Tree pomegranateTree(){
        return new Tree("Pomegranate", Season.summer, 2, 1, pomegranate(), 100, false);
    }

    public static Tree appleTree(){
        return new Tree("Apple", Season.autumn, 2, 1, apple(), 100, false);
    }

    public static Tree orangeTree(){
        return new Tree("Orange", Season.autumn, 2, 1, orange(), 100, false);
    }

    public static Food juice(Fruit fruit){
        ArrayList<AbilityChange> a = new ArrayList<>();
        for(AbilityChange abilityChange : fruit.effect.getAbilityChanges()){
            a.add(new AbilityChange(abilityChange.ability,
                    (int)abilityChange.getMaxValueChange()*5/4,
                    (int)abilityChange.getCurrentValueChange()*5/4,
                    (int)abilityChange.getRefillRateChange()*5/4));
        }
        Food juice = new Food(fruit.getName() + " juice", 1, (int)fruit.getPrice()*5/4,new EffectOnBody(a));
        return juice;
    }

    public static Resource oil(){
        return new Resource("Oil",1,30);
    }

    public static Resource flour(){
        return new Resource("Flour",1,20);
    }

    public static Resource sugar(){
        return new Resource("Sugar",1,10);
    }

    public static Resource salt(){
        return new Resource("Salt",1,10);
    }

    public static Wood wood(int type){
        if(type == 0)
            return new Wood("Branch", 0, 20);
        if(type == 1)
            return new Wood("Old Lumber", 1, 80);
        if(type == 2)
            return new Wood("Pine Lumber", 2, 250);
        if(type == 3)
            return new Wood("Oak Lumber", 3, 1000);
        return null;
    }

    public static Stone stone(int type){
        if(type == 0)
            return new Stone("Stone", 0, 20);
        if(type == 1)
            return new Stone("Iron Ore", 1, 80);
        if(type == 2)
            return new Stone("Silver Ore", 2, 250);
        if(type == 3)
            return new Stone("Adamantium Ore", 3, 1000);
        return null;
    }

    public static Axe axe(int type){
        int breakingprobelity = (5-type)*5;
        int currentValueChange = (6-type)*10;
        String s = " ";
        if (type == 1) {
            s = "Stone Axe";
            currentValueChange = 150;
        }
        if (type == 2) {
            s = "Iron Axe";
            currentValueChange = 80;
        }
        if (type == 3) {
            s = "Silver Axe";
            currentValueChange = 40;
        }
        if (type == 4) {
            s = "Adamantium Axe";
            currentValueChange = 20;
        }
        AbilityChange abilityChange = new AbilityChange(person.getEnergy(),0,(-1)*currentValueChange,0);
        Axe axe = new Axe(s,100 , breakingprobelity , abilityChange,type);
            return axe;
    }

    public static PickAxe pickAxe(int type){
        int breakingprobelity = (5-type)*5;
        int currentValueChange = (6-type)*10;
        String s = " ";
        if (type == 1)
            s = "Stone PickAxe";
        if (type == 2)
            s = "Iron PickAxe";
        if (type == 3)
            s = "Silver PickAxe";
        if (type == 4)
            s= "Adamantium PickAxe";
        return new PickAxe(s,100,breakingprobelity,new AbilityChange(person.getEnergy(),0,(-1)*currentValueChange,0) , type);
    }

    public static Shovel shovel(int type){
        int breakingprobelity = (5-type)*5;
        int currentValueChange = (6-type)*10;
        String s = " ";
        if (type == 1) {
            s = "Stone Shovel";
            currentValueChange = 150;
        }
        if (type == 2) {
            s = "Iron Shovel";
            currentValueChange = 80;
        }
        if (type == 3) {
            s = "Silver Shovel";
            currentValueChange = 40;
        }
        if (type == 4) {
            s = "Adamantium Shovel";
            currentValueChange = 20;
        }
        return new Shovel(s,100,breakingprobelity,new AbilityChange(person.getEnergy(),0,(-1)*currentValueChange, 0),type);
    }

    public static FishingRod fishingRod(int type){
        int breakingprobelity = (5-type)*5;
        int currentValueChange = (6-type)*10;
        String s = " ";
        if (type == 1)
            s = "Small Fishing Rod";
        if (type == 2)
            s = "Old Fishing Rod";
        if (type == 3)
            s = "Pine Fishing Rod";
        if (type == 4)
            s= "Oak Fishing Rod";
        return new FishingRod(s,100,breakingprobelity,new AbilityChange(person.getEnergy(),0,(-1)*currentValueChange,0),type);
    }

    public static WateringCan wateringCan(int type){
        int breakingprobelity = (5-type)*5;
        int currentValueChange = (6-type)*10;
        int maxWaterLevel = (int) Math.pow(2,(type - 1)/4  + (type-1)%4);
        String s=" ";
        if (type == 1)
            s="Small Watering Can";
        if (type == 2)
            s = "Old Watering Can";
        if (type == 3)
            s = "Pine Watering Can";
        if (type == 4)
            s = "Oak Watering Can";
        if (type == 5)
            s = "Stone Watering Can";
        if (type == 6)
            s = "Iron Watering Can";
        if (type == 7)
            s = "Silver Watering Can";
        if (type == 8)
            s = "Adamantium Watering Can";

        return new WateringCan(s,0,maxWaterLevel,100,breakingprobelity,new AbilityChange(person.getEnergy(),0,(-1)*currentValueChange,0),type);
    }

    public static Resource thread(){
        return new Resource("Thread",1,300, "Used for making wool");
    }

    public static Resource alfalfa(){
        return new Resource("Alfalfa",1,20,"Used for feeding cows and sheep");
    }

    public static Resource grain(){
        return new Resource("Grain",1,10,"Used for feeding chickens");
    }

    public static Seed seedOf(Crop crop){
        return new Seed(crop.getName().substring(0,crop.getName().length()-5) + " seed",crop,crop.outCome.getItems().get(0).getPrice() * 5);
    }

    public static Medicine medicine(int type){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        int price = 0;
        String name = "";
        int healVal = 0;

        if ( type == 1 ){
            price = 150;
            name = "SmallMedicine";
            healVal = 100;
        }
        if( type == 2 ){
            price = 350;
            name = "MediumMedicine";
            healVal = 250;
        }
        if(type == 3){
            price = 600;
            name = "LargeMedicine";
            healVal = 500;
        }
        abilityChange = new AbilityChange(person.getHealth(),0,healVal,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Medicine(name,1,price,effectOnBody);
    }

    public static Food milk(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,20,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,20,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Food("Milk",1,60,effectOnBody);
    }

    public static Resource cowMeat(){
        return new Resource("CowMeat",2,100,"Used for cooking");
    }

    public static Resource wool(){
        return new Resource("Wool",1,100);
    }

    public static Resource sheepMeat(){
        return new Resource("SheepMeat",2,150,"Used for cooking");
    }

    public static Resource egg(){
        return new Resource("Egg",1,30,"Used for cooking");
    }

    public static Resource chickenMeat(){
        return new Resource("ChickenMeat",2,70,"Used for cooking");
    }

    public static Resource fishMeat(){
        return new Resource("FishMeat",2,200,"Used for cooking");
    }

    public static Animal cow(String name){
        ArrayList<Item> smpl = new ArrayList<>();
        smpl.add(milk());
        smpl.add(cowMeat());
        return new Animal("Cow " + name,smpl,"Alfalfa","Miking",1,1000);
    }

    public static Animal sheep(String name){
        ArrayList<Item> smpl = new ArrayList<>();
        smpl.add(wool());
        smpl.add(sheepMeat());
        return new Animal("Sheep " + name,smpl,"Alfalfa","Scissors",3,1000);
    }

    public static Animal chicken(String name){
        ArrayList<Item> smpl = new ArrayList<>();
        smpl.add(egg());
        smpl.add(chickenMeat());
        return new Animal("Chicken " + name,smpl,"Grain",null,1,500);
    }

    public static AnimalMedicine animalMedicine(){
        return new AnimalMedicine("Animal Medicine",1,50,100);
    }

    public static KitchenTool fryingPan(){
        return new KitchenTool("FryingPan",1,20,10);
    }

    public static KitchenTool knife(){
        return new KitchenTool("Knife",1,20,20);
    }

    public static KitchenTool agitator(){
        return new KitchenTool("Agitator",1,20,10);
    }

    public static KitchenTool pot(){
        return new KitchenTool("Pot",1,20,20);
    }

    public static KitchenTool furnace(){
        return new KitchenTool("Furnace",1,20,5);
    }

    public static Meal friedPotate(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,-15,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,100,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Meal("FriedPotato",1,100,effectOnBody);
    }

    public static Food cheese(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,30,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,30,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Food("Cheese",1,150,effectOnBody);
    }

    public static Food bread(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),0,5,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,10,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Food("bread",1,150,effectOnBody);
    }

    public static Meal saladShirazi(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),10,60,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,40,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Meal("SaladShirazi",1,100,effectOnBody);
    }

    public static Meal mirzaGhasemi(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange,abilityChange1;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getHealth(),10,30,0);
        s.add(abilityChange);
        abilityChange1= new AbilityChange(person.getEnergy(),0,70,0);
        s.add(abilityChange1);
        effectOnBody = new EffectOnBody(s);
        return new Meal("MirzaGhasemi",1,100,effectOnBody);
    }

    public static Meal cheeseCake(){
        EffectOnBody effectOnBody;
        AbilityChange abilityChange;
        ArrayList<AbilityChange> s = new ArrayList<>();
        s.clear();
        abilityChange= new AbilityChange(person.getEnergy(),0,80,0);
        s.add(abilityChange);
        effectOnBody = new EffectOnBody(s);
        return new Meal("CheeseCake",1,100,effectOnBody);
    }

    public static Recipe friedPotatoRecipe(){
        Recipe recipe = new Recipe("FriedPotato");
        recipe.meal = friedPotate();
        recipe.getTools().add(new ItemStack(knife()));
        recipe.getTools().add(new ItemStack(fryingPan()));
        recipe.getIngredients().add(new ItemStack(potato()));
        recipe.getIngredients().add(new ItemStack(oil()));
        recipe.getIngredients().add(new ItemStack(salt()));
        return recipe;
    }

    public static Recipe saladShiraziRecipe(){
        Recipe recipe = new Recipe("SaladSirazi");
        recipe.meal = saladShirazi();
        recipe.getTools().add(new ItemStack(knife()));
        recipe.getIngredients().add(new ItemStack(cucumber()));
        recipe.getIngredients().add(new ItemStack(tomato()));
        recipe.getIngredients().add(new ItemStack(juice(lemon())));
        return recipe;
    }

    public static Recipe cheeseCakeRecipe(){
        Recipe recipe = new Recipe("CheeseCake");
        recipe.meal = cheeseCake();
        recipe.getTools().add(new ItemStack(knife()));
        recipe.getTools().add(new ItemStack(pot()));
        recipe.getTools().add(new ItemStack(agitator()));
        recipe.getIngredients().add(new ItemStack(milk()));
        recipe.getIngredients().add(new ItemStack(cheese()));
        recipe.getIngredients().add(new ItemStack(egg()));
        recipe.getIngredients().add(new ItemStack(sugar()));
        return recipe;
    }

    public static Recipe mirzaGhasemiRecipe() {
        Recipe recipe = new Recipe("MirzaGhasemi");
        recipe.meal = mirzaGhasemi();
        recipe.getTools().add(new ItemStack(knife()));
        recipe.getTools().add(new ItemStack(fryingPan()));
        recipe.getIngredients().add(new ItemStack(garlic()));
        recipe.getIngredients().add(new ItemStack(ale()));
        recipe.getIngredients().add(new ItemStack(egg()));
        recipe.getIngredients().add(new ItemStack(salt()));
        recipe.getIngredients().add(new ItemStack(tomato()));
        return recipe;
    }

    public static Machine spinningWheel(){
        ItemStack in = new ItemStack(wool());
        in.getItems().add(wool());
        ArrayList<ItemStack> it = new ArrayList<>();
        it.add(in);

        ItemStack out = new ItemStack(thread());
        ArrayList<ItemStack> it2 = new ArrayList<>();
        it2.add(out);

        return new Machine("SpinningWheel",5,it,it2,2000,"A machine to make threads out of wool.");
    }

    public static Machine cheeseMaker(){
        ItemStack in = new ItemStack(milk());
        in.getItems().add(milk());
        ArrayList<ItemStack> it = new ArrayList<>();
        it.add(in);

        ItemStack out = new ItemStack(cheese());
        ArrayList<ItemStack> it2 = new ArrayList<>();
        it2.add(out);

        return new Machine("CheeseMaker",5,it,it2,3000,"A machine to create cheese from milk.");
    }

    public static Exercise runningExercise(){
        AbilityChange abilityChange = new AbilityChange(person.getEnergy(), 50,-10,0,0);
        ArrayList<AbilityChange> abilityChanges = new ArrayList<>();
        abilityChanges.add(abilityChange);
        EffectOnBody effectOnBody = new EffectOnBody(abilityChanges);
        return new Exercise("Running",effectOnBody,50);
    }

    public static  Exercise bodybuildering() {
        AbilityChange abilityChange = new AbilityChange(person.getStamina(), 50,0,5,0);
        ArrayList<AbilityChange> abilityChanges = new ArrayList<>();
        abilityChanges.add(abilityChange);
        EffectOnBody effectOnBody = new EffectOnBody(abilityChanges);
        return new Exercise("BodyBuilding",effectOnBody,100);
    }

    public static Exercise yoga(){
        AbilityChange abilityChange = new AbilityChange(person.getHealth(), 50,0,5,0);
        ArrayList<AbilityChange> abilityChanges = new ArrayList<>();
        abilityChanges.add(abilityChange);
        EffectOnBody effectOnBody = new EffectOnBody(abilityChanges);
        return new Exercise("Yoga",effectOnBody,100);
    }

    public static Resource urine(){
        return new Resource("Urine",1,10,"Used for increasing crops' outcome");
    }

    public static Resource ammonia(){
        return new Resource("Ammonia",1,10,"Used for accelerating crops' growth speed ");
    }

    public static Tool miking(){
        AbilityChange abilityChange = new AbilityChange(person.getEnergy(), 0,-5,0);
        return new Tool("Miking",1,20,5,abilityChange);
    }

    public static Tool scissors(){
        AbilityChange abilityChange = new AbilityChange(person.getEnergy(), 0,-5,0);
        return new Tool("Scissors",1,20,5,abilityChange);
    }

    public static ArrayList<Item> rareItems(){

        ArrayList<Item> items = new ArrayList<>();
        ArrayList<AbilityChange> ac = new ArrayList<>();
        ac.add(new AbilityChange(person.getEnergy(),1000,0,10,0));
        EffectOnBody effectOnBody = new EffectOnBody(ac);
        items.add(new Fruit("Dragon Fruit",1,2000,effectOnBody));


        ArrayList<AbilityChange> ac2 = new ArrayList<>();
        ac2.add(new AbilityChange(person.getHealth(),0,500,20,0));
        EffectOnBody effectOnBody2 = new EffectOnBody(ac2);
        items.add(new Medicine("Heart Of Tarrasque",1,2000,effectOnBody2));

        ArrayList<AbilityChange> ac3 = new ArrayList<>();
        ac3.add(new AbilityChange(person.getHealth(),0,1000,0,0));
        EffectOnBody effectOnBody3 = new EffectOnBody(ac3);
        items.add(new Meal("Baghalagatogh",1,500,effectOnBody3));

        return items;
    }

    public static String cropPictureName(Field field, int i){

        String picName = "";
        if (field.crops.get(i) != null) {
            Crop crop = field.crops.get(i);
            picName = crop.getName().substring(0, crop.getName().length() - 5);
            if (crop.age == 0)
                picName = picName + "Sprout";
            if (crop.age >= 1 && crop.age < crop.matureAge)
                picName = picName + "Young";
            if (crop.age >= crop.matureAge)
                picName = picName + "Mature";
            if (crop.waterLevel <= 0)
                picName = picName + "Dry";
            if (crop.waterLevel > 0)
                picName = picName + "Wet";
            //picName = picName + ".png";
            //System.out.println(picName);
        }else {
            picName = "dry";
            if (field.plowed[i] == true) {
                picName = picName + "Plowed";
            } else {
                picName = picName + "Empty";
            }
        }

        picName = picName + ".png";
        return picName;
    }

    public static String treePictureName(Tree tree){
        if (tree.outCome.getItems().size() == 0 || tree.age < tree.matureAge){
            return "Simple.png";
        }
        return tree.getName()+".png";
    }

    public static Tree copyTree(Tree tree){
        return new Tree(tree.getName(), tree.season, tree.matureAge, tree.dailyWaterNeed, tree.sampleFruit, tree.getPrice(), tree.isBought);
    }

    public static Fruit copyFruit(Fruit fruit){
        return new Fruit(fruit.getName(), fruit.getSize(), fruit.getPrice(), fruit.getEffect(), fruit.description);
    }

    public static ArrayList<Fruit> allFruits(){
        ArrayList<Fruit> fruits = new ArrayList<>();
        fruits.add(Producer.pear());
        fruits.add(Producer.peach());
        fruits.add(Producer.garlic());
        fruits.add(Producer.bean());
        fruits.add(Producer.cabbage());
        fruits.add(Producer.ale());
        fruits.add(Producer.lemon());
        fruits.add(Producer.pomegranate());
        fruits.add(Producer.cucumber());
        fruits.add(Producer.watermelon());
        fruits.add(Producer.onion());
        fruits.add(Producer.turnip());
        fruits.add(Producer.apple());
        fruits.add(Producer.orange());
        fruits.add(Producer.potato());
        fruits.add(Producer.carrot());
        fruits.add(Producer.tomato());
        fruits.add(Producer.melon());
        fruits.add(Producer.pineapple());
        fruits.add(Producer.strawberry());
        fruits.add(Producer.pepper());
        return fruits;
    }

    public static ArrayList<Seed> allSeed(){
        ArrayList<Seed> seeds = new ArrayList<>();
        seeds.add(Producer.seedOf(Producer.garlicCrop()));
        seeds.add(Producer.seedOf(Producer.beanCrop()));
        seeds.add(Producer.seedOf(Producer.cabbageCrop()));
        seeds.add(Producer.seedOf(Producer.aleCrop()));
        seeds.add(Producer.seedOf(Producer.cucumberCrop()));
        seeds.add(Producer.seedOf(Producer.watermelonCrop()));
        seeds.add(Producer.seedOf(Producer.onionCrop()));
        seeds.add(Producer.seedOf(Producer.turnipCrop()));
        seeds.add(Producer.seedOf(Producer.potatoCrop()));
        seeds.add(Producer.seedOf(Producer.carrotCrop()));
        seeds.add(Producer.seedOf(Producer.tomatoCrop()));
        seeds.add(Producer.seedOf(Producer.melonCrop()));
        seeds.add(Producer.seedOf(Producer.pineappleCrop()));
        seeds.add(Producer.seedOf(Producer.strawberryCrop()));
        seeds.add(Producer.seedOf(Producer.pepperCrop()));
        return seeds;
    }

    public static ArrayList<Resource> allResources(){
        ArrayList<Resource> resources = new ArrayList<>();
        resources.add(Producer.thread());
        resources.add(Producer.alfalfa());
        resources.add(Producer.grain());
        resources.add(Producer.wool());
        resources.add(Producer.oil());
        resources.add(Producer.flour());
        resources.add(Producer.sugar());
        resources.add(Producer.salt());
        resources.add(Producer.oil());
        resources.add(Producer.egg());
        return resources;
    }

    public static ArrayList<Item> allItems(){
        ArrayList<Item> items = new ArrayList<>();
        items.add(Producer.cheese());
        items.add(Producer.bread());
        items.add(Producer.milk());
        items.addAll(allFruits());
        items.addAll(allResources());
        return items;
    }

    public static Resource copyResource(Resource resource){
        return new Resource(resource.getName(),resource.getSize(), resource.getPrice());
    }

    public static Food copyFood(Food food){
        return new Food(food.getName(), food.getSize(), food.getPrice(), food.getEffect());
    }

    public static Food copyMeal(Meal meal){
        return new Meal(meal.getName(), meal.getSize(), meal.getPrice(), meal.getEffect());
    }

    public static Medicine copyMedicine(Medicine medicine){
        return new Medicine(medicine.getName(), medicine.getSize(), medicine.getPrice(), medicine.getEffect());
    }

    public static KitchenTool copyKitchenTool(KitchenTool kitchenTool){
        return new KitchenTool(kitchenTool.getName(), kitchenTool.getSize(), kitchenTool.getHealth(),kitchenTool.breakingProbablity);
    }
}
