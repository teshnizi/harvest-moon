/**
 * Created by pc on 7/13/2017.
 */
public class Protocol {
    static String play = "play";
    static String villages = "village";
    static String character = "character";
    static String market = "market";
    static String forest = "forest";
    static String protocol = "]a.a]";
    static String clinic = "clinic";
    static String add = "add";
    static String sub = "sub";
    static String foretsource = "forestSource";
    static String reject = "reject";
    static String music = "music";
    static String text = "text";
    static String images = "images";
    static String chatelert = "chat";
    static String privatchatroom = "privateChatRoom";
    static String Showstatusrequest = "status request";
    static String status = "status";
    static String friendrequest = "friend request";
    static String friendanswer = "answer friend";
    static String removerequst = "friend remove request";
    static String declineanswer = "decline answer";
    static String accepttrade = "accept trade";
    static String opentrade = "open trade";
    static String youlock = "you lock";
    static String acceptsender = "accept sender";
    static String itemtrad = "item trade";
    static String cavesource = "cave source";
    static String cave = "cave";



}
