import java.util.ArrayList;
/**
 * Created by Tigerous215 on 5/9/2017.
 */
public abstract class JA extends Thing{

    ArrayList<JA> neighbours;
    public JA(String name){
        super(name);
        neighbours = new ArrayList<>();
    }
    public ArrayList<JA> getNeighbours() {
        return neighbours;
    }
    public ArrayList<String> listOfPlaces(){
        ArrayList<String> ret = new ArrayList<>();
        for(JA ja: neighbours)
            ret.add(ja.getName());
        return ret;
    }
    public  ArrayList<String> objects(){
        return null;
    }

    public JA findJA(String name) {
        for(JA neighbour : neighbours){
            if(neighbour.getName().equals(name)) {
                return neighbour;
            }
        }
        return null;
    }

    ArrayList<Creature> creatures;
    ArrayList<Item> items;

    public ArrayList<String> listOfObjects(){
        ArrayList<String> ret = new ArrayList<>();
        for(Creature creature : creatures)
            ret.add(creature.getName());
        for(Item item : items)
            ret.add(item.getName());
        return ret;
    }

    public Thing findObject(String objectName) {
        for(Creature creature : creatures)
            if(creature.getName().equals(objectName))
                return creature;
        for(Item item : items)
            if(item.getName().equals(objectName))
                return item;
        return null;
    }
}
