import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by pc on 5/10/2017.
 */
public class BedMenu extends Menu {
    Bed bed;
    ArrayList<Thing> gameThings;
    BedMenu(Bed bed, ArrayList<Thing> gameThings){
        this.bed = bed;
        this.gameThings = gameThings;
    }
    @Override
    public Menu getCommand(int x, Person person)  {
        if (x == 1){
            System.out.println("Good Night! ZZZ...");
            for (Thing thing : gameThings){
                try {
                    thing.nextDay(person.season);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else if (x == 2){
            System.out.println("Game Saved!");
        }
        else
            System.out.println("Bad Command!");
        return null;
    }
    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String > commands = new ArrayList<>();
        commands.add("You are in bed :");
        commands.add("1.Sleep");
        commands.add("2.Save&Quite");
        return commands;
    }
}
