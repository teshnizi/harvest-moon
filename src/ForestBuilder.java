import com.sun.javafx.sg.prism.NGGroup;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Pair;
import sun.security.krb5.internal.KdcErrException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.AnnotatedArrayType;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by pc on 7/11/2017.
 */
public class ForestBuilder extends Application {
    public Forest forest;
    static Group forestGroup;
    static boolean inforest = false;
    static ArrayList<Pair<Pair<String,ImageView>,Pair<Integer,Integer>>> chars = new ArrayList<>();
    static Scene forestScene;
    static int numinimage = 0;
    static HashSet<Pair<Integer, Integer>> forestAvailableTiles = new HashSet<>();
    static ArrayList<Rectangle> rectangles = new ArrayList<>();
    static {

        for (int i = 7; i < 37; i++) {
            for (int j = 0; j < 15; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = -1 ; i < 6 ; i++) {
            for (int j = 14; j < 16; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 6 ; i < 8 ; i++) {
            for (int j = 14; j < 22; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 8 ; i < 9 ; i++) {
            for (int j = 17; j < 22; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 9 ; i < 25 ; i++) {
            for (int j = 17; j < 19; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 23 ; i < 25 ; i++) {
            for (int j = 17; j < 27; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 24 ; i < 35 ; i++) {
            for (int j = 25; j < 27; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 34 ; i < 35 ; i++) {
            for (int j = 21; j < 25; j++) {
                forestAvailableTiles.add(new Pair<>(i, j));
            }
        }


    }

    public ForestBuilder(Forest forest) {
        this.forest = forest;
    }



    @Override
    public void start(Stage primaryStage) throws Exception {
        Image branch = new Image("images\\branch.png");
        Image branch1 = new Image("images\\Cut Tree.png");
        Image branch2 = new Image("images\\Cut Tree1.png");
        Image branch3 = new Image("images\\Cut Tree2.png");
        Image stone = new Image("images\\stone.png");
        Image forest1 = new Image("maps\\forest2.png");
        Button spacebutton = new Button("Inspect");
        spacebutton.setLayoutX(Main.character.getX()-10);
        spacebutton.setLayoutY(Main.character.getY() -25);
        spacebutton.setVisible(false);
        forestGroup = new Group();
        forestGroup.getChildren().addAll(spacebutton);
        forestGroup.getChildren().add(Main.character);
        forestScene = new Scene(forestGroup, 1280, 960, new ImagePattern(forest1));
        int num1 = 0;
        int num2 = 0;
        ArrayList<Integer> a = new ArrayList<>();
            for (Pair<Storage,Pair<Integer,Integer>> t : Forest.res) {
                    forestAvailableTiles.remove(t.getValue());
            }
        for (Pair<Storage,Pair<Integer,Integer>> p : Forest.res){
            num1 ++;
            Rectangle rectangle = new Rectangle(32,48);
            Pair<Integer,Integer> x = MovementHandler.tileToPixel(p.getValue().getKey(),p.getValue().getValue());
            rectangle.setX(x.getKey());
            rectangle.setY(x.getValue());
            rectangles.add(rectangle);
            System.out.println(num1 + " " +p.getValue().getKey() + " " + p.getValue().getValue() );
            forestGroup.getChildren().add(rectangle);
            if (p.getKey().getItemStacks().get(0).getItems().get(0) instanceof Wood)
            {
                Wood wood = (Wood)p.getKey().getItemStacks().get(0).getItems().get(0);
                if (wood.getType() == 0)
                    rectangle.setFill(new ImagePattern(branch));
                if (wood.getType() == 1)
                    rectangle.setFill(new ImagePattern(branch1));
                if (wood.getType() == 2)
                    rectangle.setFill(new ImagePattern(branch2));
                if (wood.getType() == 3)
                    rectangle.setFill(new ImagePattern(branch3));
            }
            else
                rectangle.setFill(new ImagePattern(stone));
        }
        forestScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage % 4 + 1;
                Pair<Integer, Integer> next;
                DataOutputStream dout = null;
                try {
                    if (Main.online)
                    dout = new DataOutputStream(Getter.socket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (Main.Eisenhower.canMove) {
                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png"));
                        MovementHandler.move(2, forestAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\left" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png"));
                        MovementHandler.move(0, forestAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\right" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, forestAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\b" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png "));
                        MovementHandler.move(3, forestAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\f" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()));

                }

                if (Main.character.getX() < 0){
                    Main.character.setX(0);
                    inforest = false;
                    FarmBuilder.infarms = true;
                    Main.farmBuilder.start(primaryStage);

                }
                if (event.getCode() == KeyCode.ESCAPE){
                    Main.Eisenhower.canMove = false;
                    Main.timeline.pause();
                    PauseMenue pauseMenue = new PauseMenue(forestScene);
                    try {
                        pauseMenue.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.E){
                    CaveBuilder caveBuilder = new CaveBuilder();
                    CaveBuilder.incave = true;
                    ForestBuilder.inforest = false;
                    try {
                        caveBuilder.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.B) {
                    //backPackListView = Main.Eisenhower.getBackPack().viewList();
                    //farmGroup.getChildren().addAll(backPackListView);
                    PersonMenu personMenu = new PersonMenu(forestScene, primaryStage);
                    try {
                        Stage mystage = new Stage();
                        mystage.setX(40);
                        personMenu.start(mystage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.I)
                {
                    if (isis()){
                        String h = isisstring();
                        Inspectbuilder inspectbuilder = new Inspectbuilder(h);
                        Stage stage = new Stage();
                        try {
                            inspectbuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Storage mystorag = null;
                    int x1 = 0;
                    int y1 = 0;
                    for (Pair<Storage,Pair<Integer,Integer>> p : forest.res){
                        if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() +1,p.getValue().getValue()))){
                            mystorag = p.getKey();
                            x1 = p.getValue().getKey();
                            y1 = p.getValue().getValue();
                        }
                    }
                    if (mystorag != null && mystorag.getItemStacks().get(0).getItems().size() > 0){
                        ResourceInForestBuilder resourceInForestBuilder = new ResourceInForestBuilder(mystorag,x1,y1);
                        Stage stage = new Stage();
                        try {
                            resourceInForestBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(7,21))){
                        RiverBuilder riverBuilder = new RiverBuilder(Main.forest.getRiver());
                        Stage stage = new Stage();
                        try {
                            riverBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                boolean flag = false;
                for (Pair<Storage,Pair<Integer,Integer>> p : forest.res){
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() +1,p.getValue().getValue()))){
                            spacebutton.setPrefWidth(80);
                            spacebutton.setLayoutX(Main.character.getX() -24);
                            spacebutton.setLayoutY(Main.character.getY() -30);
                            spacebutton.setVisible(true);
                            flag = true;
                    }
                }
                if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(7,21)))
                {
                    spacebutton.setPrefWidth(80);
                    spacebutton.setLayoutX(Main.character.getX() -24);
                    spacebutton.setLayoutY(Main.character.getY() -30);
                    spacebutton.setVisible(true);
                    flag = true;
                }
                 else if(isis() && !flag){
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                    flag = true;
                }
                if (!flag)
                    spacebutton.setVisible(false);
            }
        });
        primaryStage.setScene(forestScene);
        primaryStage.show();
    }
     static public void repair(){
        ArrayList<Pair<Storage,Pair<Integer,Integer>>> pairs = new ArrayList<>();
        for (Pair<Storage,Pair<Integer,Integer>> p : Forest.res){
            if (p.getKey().getItemStacks().get(0).getItems().size() == 0){
                forestAvailableTiles.add(p.getValue());
                pairs.add(p);
                for (Rectangle rectangle : rectangles){
                    if (p.getValue().equals(MovementHandler.pixelToTile(rectangle.getLayoutX(),rectangle.getLayoutY()))){
                        forestGroup.getChildren().remove(rectangle);
                        break;
                    }
                }
            }
        }
        for (Pair<Storage,Pair<Integer,Integer>> p : pairs)
            Forest.res.remove(p);
    }
    public String mystring(String st){
        String ans;
        ans = Getter.socketname + Protocol.protocol + Protocol.character + Protocol.protocol +Protocol.forest +Protocol.protocol+st + Protocol.protocol +MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getKey() +Protocol.protocol  + MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getValue();
        return ans;
    }
    public boolean isis(){
        int i = 0;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                return true;
            }
        }
        return false;
    }
    public String isisstring(){
        String h = null;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                h= p.getKey().getKey();
            }
        }
        return h;
    }
}