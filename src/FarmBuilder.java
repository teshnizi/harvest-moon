import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Pair;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by pc on 7/6/2017.
 */
public class FarmBuilder extends Application {
    static int numinimage = 3;
    Farm mainfarm;
    public FarmBuilder(Farm farm){
        this.mainfarm = farm;
    }
    Group farmGroup;
    Scene farmScene;
    static ListView backPackListView;
    static boolean infarms = false;
    static HashSet<Pair<Integer,Integer>> farmAvailableTiles = new HashSet<>();
    static {
        //**WARNING!: (x,y), unlike arrays
        for(int i = -1 ; i < 21 ; i++){
            farmAvailableTiles.add(new Pair<>(i,14));
            farmAvailableTiles.add(new Pair<>(i,15));
        }
        for (int i = 9; i<14; i++){
            farmAvailableTiles.add(new Pair<>(5,i));
            farmAvailableTiles.add(new Pair<>(6,i));
        }
        for(int i = 8; i<30; i++){
            farmAvailableTiles.add(new Pair<>(21,i));
            farmAvailableTiles.add(new Pair<>(22,i));
        }
        for(int i = 21 ; i <= 40 ; i++){
            farmAvailableTiles.add(new Pair<>(i,8));
            farmAvailableTiles.add(new Pair<>(i,9));
        }
        for(int i = 24 ; i <39; i++)
            for(int j = 11 ; j <26 ; j++)
                farmAvailableTiles.add(new Pair<>(i,j));
        farmAvailableTiles.add(new Pair<>(23,14));
        farmAvailableTiles.add(new Pair<>(23,18));
        farmAvailableTiles.add(new Pair<>(23,22));

        for (int i = 7; i < 21 ; i++)
            farmAvailableTiles.add(new Pair<>(i,23));

        for (int i = 15; i < 26 ; i++)
            farmAvailableTiles.add(new Pair<>(7,i));

        for (int i = 9; i < 20 ; i++) {
            farmAvailableTiles.add(new Pair<>(i, 18));
            farmAvailableTiles.add(new Pair<>(i, 19));
            farmAvailableTiles.add(new Pair<>(i, 20));
            farmAvailableTiles.add(new Pair<>(i, 21));
            farmAvailableTiles.add(new Pair<>(i, 25));
            farmAvailableTiles.add(new Pair<>(i, 26));
            farmAvailableTiles.add(new Pair<>(i, 27));
            farmAvailableTiles.add(new Pair<>(i, 28));
        }

        for (int i = 15; i < 22 ; i++) {
            farmAvailableTiles.add(new Pair<>(12, i));
            farmAvailableTiles.add(new Pair<>(16, i));
        }
        farmAvailableTiles.add(new Pair<>(10, 22));
        farmAvailableTiles.add(new Pair<>(14, 22));
        farmAvailableTiles.add(new Pair<>(18, 22));
        farmAvailableTiles.add(new Pair<>(10, 24));
        farmAvailableTiles.add(new Pair<>(14, 24));
        farmAvailableTiles.add(new Pair<>(18, 24));
        farmAvailableTiles.add(new Pair<>(6, 25));
        farmAvailableTiles.add(new Pair<>(5, 25));
        farmAvailableTiles.add(new Pair<>(14, 13));


    }

    Field[][] fields = new Field[4][4];
    FruitGarden[][] fruitGardens = new FruitGarden[3][2];

    static ImageView[][][][] fieldsUI;
    ImageView[][] fruitGardensUI;

    ImageView[][] getFieldUI(Pair<Integer,Integer> p){
        int x = p.getKey();
        int y = p.getValue();
        x -= 24;
        x /= 4;
        y -= 11;
        y /= 4;
        return fieldsUI[x][y];
    }
    ImageView getFruitGardenUI(Pair<Integer,Integer> p){
            int x = p.getKey();
            int y = p.getValue();

            if (x < 9 || y < 18)
                return null;
            if (x > 19)
                return null;
            if ( 22 <= y && y <= 24)
                return null;
            x -= 9;
            if (x % 4 == 3)
                return null;
            x /= 4;
            y -= 18;
            y /= 6;
            return fruitGardensUI[x][y];
        }


    Field getField(Pair<Integer,Integer> p){
        int x = p.getKey();
        int y = p.getValue();
        if (x < 24 || y < 11)
            return null;

        x -= 24;
        if (x%4 == 3)
            return null;
        x /= 4;
        y -= 11;
        if (y%4 == 3)
            return null;
        y /= 4;
        return fields[x][y];
    }

    FruitGarden getFruitGarden(Pair<Integer, Integer> p){
        int x = p.getKey();
        int y = p.getValue();
        if (x < 9 || y < 18)
            return null;
        if (x > 19)
            return null;
        if ( 22 <= y && y <= 24)
            return null;
        x -= 9;
        if (x % 4 == 3)
            return null;
        x /= 4;
        y -= 18;
        y /= 6;
        return fruitGardens[x][y];
    }

    Crop getCrop(){
        Field field = getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
        if (field == null)
            return null;
        ImageView[][] fieldUI = getFieldUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++) {
                if (fieldUI[i][j].getX() == Main.character.getX() && fieldUI[i][j].getY() - 14 == Main.character.getY()) {
                    return field.crops.get(i + 3 * j);
                }
            }
            return null;
    }

    //Moves character, 0 for right, 1 for up, 2 for left and 3 for down
    @Override
    public void start(Stage primaryStage){

        Image farm = new Image("maps\\farm.png");
        farmGroup = new Group();
        farmScene = new Scene(farmGroup,1280,960);
        fieldsUI = new ImageView[4][4][3][3];
        fruitGardensUI = new ImageView[3][2];

        for (int i = 0 ; i < 3; i++)
            for (int j = 0 ; j < 2 ;j++){
                //System.out.println(mainfarm.getFruitGardens().size());
                fruitGardens[i][j] = mainfarm.getFruitGardens().get(i + 3 * j);
                fruitGardensUI[i][j] = new ImageView();
                fruitGardensUI[i][j].setX(32*(10 + i * 4));
                fruitGardensUI[i][j].setY(32*(19 + j * 7));
                farmGroup.getChildren().add(fruitGardensUI[i][j]);
            }
            drawFruitGardens();
        for (int i = 0 ; i < 4 ; i++)
            for (int j = 0 ; j < 4 ; j++){
                fields[i][j] = mainfarm.getFields().get(j*4 + i);
                for (int k = 0 ; k < 3; k++)
                    for (int l = 0 ; l < 3 ;l++){
                        String picName = Producer.cropPictureName(fields[i][j],3*l + k);
                        //System.out.println(picName);
                        fieldsUI[i][j][k][l] = new ImageView("Images\\miscFieldTiles\\" + picName);
                        fieldsUI[i][j][k][l].setX(32*(24 + i*4 + k));
                        fieldsUI[i][j][k][l].setY(32*(11 + j*4 + l));
                        //System.out.println(fieldsUI[i][j][k][l].getX());
                        farmGroup.getChildren().addAll(fieldsUI[i][j][k][l]);
                    }
            }
        //boolean isShowingBackBack = false;
        farmGroup.getChildren().addAll(Main.character);
        primaryStage.setScene(farmScene);
        primaryStage.show();

        Button plowButton = new Button("Cultivate");
        plowButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(plowButton);
        Button fillButton = new Button("Fill can");
        fillButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(fillButton);
        Button plantButton = new Button("Plant");
        plantButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(plantButton);
        Button waterButton = new Button("Water");
        waterButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(waterButton);
        Button buyTreeButton = new Button("buy Tree");
        buyTreeButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(buyTreeButton);
        Button harvestButton = new Button("Harvest");
        harvestButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(harvestButton);
        Button enterButton = new Button("Enter");
        enterButton.setFont(new Font(12));
        farmGroup.getChildren().addAll(enterButton);

        plantButton.setPrefWidth(80);
        plowButton.setPrefWidth(80);
        waterButton.setPrefWidth(80);
        buyTreeButton.setPrefWidth(80);
        harvestButton.setPrefWidth(80);
        enterButton.setPrefWidth(80);

        plantButton.setVisible(false);
        plowButton.setVisible(false);
        waterButton.setVisible(false);
        buyTreeButton.setVisible(false);
        harvestButton.setVisible(false);
        fillButton.setVisible(false);
        enterButton.setVisible(false);

        farmScene.setFill(new ImagePattern(farm));
        farmScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage%4+1;
                Pair<Integer, Integer> next;

                if (Main.Eisenhower.canMove) {
                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png"));
                        MovementHandler.move(2, farmAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png"));
                        MovementHandler.move(0, farmAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, farmAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png "));
                        MovementHandler.move(3, farmAvailableTiles, Main.character);
                    }
                    System.out.println(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()));
                }

                if(getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY())) != null)
                {
                    plowButton.setLayoutX(Main.character.getX() - 24);
                    plowButton.setLayoutY(Main.character.getY() - 30);
                    plantButton.setLayoutX(Main.character.getX() - 24);
                    plantButton.setLayoutY(Main.character.getY() - 60);
                    plowButton.setVisible(true);
                    plantButton.setVisible(true);
                }
                else{
                    plowButton.setVisible(false);
                    plantButton.setVisible(false);
                }
                if(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(14, 13))
                   ||  MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(5, 25))){
                    enterButton.setLayoutX(Main.character.getX() - 24);
                    enterButton.setLayoutY(Main.character.getY() - 30);
                    enterButton.setVisible(true);
                }
                else {
                    enterButton.setVisible(false);
                }


                if ((getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY())) != null &&
                        getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY())).trees.size() >= 0) ||
                        getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY())) != null){
                    waterButton.setLayoutX(Main.character.getX() - 24);
                    harvestButton.setLayoutX(Main.character.getX() - 24);
                    if(getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY())) == null) {
                        System.out.println("garden");
                        waterButton.setLayoutY(Main.character.getY() - 30);
                        harvestButton.setLayoutY(Main.character.getY() - 60);
                    }else{
                        System.out.println("field");
                        waterButton.setLayoutY(Main.character.getY() - 90);
                        harvestButton.setLayoutY(Main.character.getY() - 120);
                        }
                    waterButton.setVisible(true);
                    harvestButton.setVisible(true);
                    Crop crop = getCrop();
                    if (crop != null && (crop.getOutCome() == null || crop.getOutCome().getItems().size() == 0)){
                        harvestButton.setVisible(false);
                    }
                    if (getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY())) != null && crop == null)
                        harvestButton.setVisible(false);

                    FruitGarden fruitGarden = getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()));
                    if (fruitGarden != null && (fruitGarden.trees.size() == 0 || fruitGarden.trees.get(0).getOutCome() == null || fruitGarden.trees.get(0).getOutCome().getItems().size() == 0 || fruitGarden.trees.get(0).age < fruitGarden.trees.get(0).matureAge))
                        harvestButton.setVisible(false);
                } else {
                    waterButton.setVisible(false);
                    harvestButton.setVisible(false);
                }


                if (getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY())) != null
                        && getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY())).trees.size() == 0){
                    buyTreeButton.setVisible(true);
                    buyTreeButton.setLayoutX(Main.character.getX() - 24);
                    buyTreeButton.setLayoutY(Main.character.getY() - 30);

                }else {
                    buyTreeButton.setVisible(false);
                }

                if(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals( new Pair<Integer,Integer>(19,14)) ||
                        MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals( new Pair<Integer,Integer>(20,14)))
                {
                    fillButton.setLayoutX(19 * 32 + 3);
                    fillButton.setLayoutY(Main.character.getY() - 30);
                    fillButton.setVisible(true);

                    if (event.getCode() == KeyCode.F){
                        Group group = new Group();
                        Label question = new Label("Choose a watering can:");
                        question.setFont(new Font("Comic Sans MS", 25));
                        question.setTextFill(Color.DARKGREEN);
                        question.setLayoutX(10);
                        question.setLayoutY(10);
                        group.getChildren().addAll(question);
                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        int num = 0;
                        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                            Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                            button.setLayoutX(20 + 200 * (num / 10));
                            button.setLayoutY(80 + (num % 10) * 35);
                            button.setFont(Main.comicFont);
                            button.setPrefWidth(180);
                            num++;
                            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    //System.out.println(itemStack.getItems().get(0));
                                    if (!itemStack.getItems().get(0).getName().endsWith("Watering Can")) {
                                        question.setText("You should choose a Watering Can!");
                                        return;
                                    }
                                    WateringCan can = (WateringCan) itemStack.getItems().get(0);
                                    if (!can.canUseTool()){
                                        if (can.getHealth() <= 0)
                                            question.setText("Watering can is broken!");
                                        else
                                            question.setText("You are too tired!");
                                        return;
                                    }
                                    can.fill();
                                    //System.out.println(Main.Eisenhower.getEnergy());
                                    can.abilityChange.affect();
                                    //System.out.println(Main.Eisenhower.getEnergy());
                                    stage.close();
                                }
                            });
                            group.getChildren().add(button);
                        }

                    }

                }
                else{
                    fillButton.setVisible(false);
                }


                if (event.getCode() == KeyCode.W){
                    FruitGarden fruitGarden = getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                    Field field = getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                    if (field != null) {
                        Group group = new Group();
                        Label question = new Label("Choose a watering can:");
                        question.setFont(new Font("Comic Sans MS", 25));
                        question.setTextFill(Color.DARKGREEN);
                        question.setLayoutX(10);
                        question.setLayoutY(10);
                        group.getChildren().addAll(question);
                        ImageView[][] fieldUI = getFieldUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        int num = 0;
                        boolean canWater = true;
                        for (int i = 0; i < 3; i++)
                            for (int j = 0; j < 3; j++) {
                                //System.out.println(field.plowed[i + 3 * j]);
                                if (fieldUI[i][j].getX() == Main.character.getX() && fieldUI[i][j].getY() - 14 == Main.character.getY()) {
                                    if (field.crops.get(i + 3 * j) == null) {
                                        question.setText("There is no crop planted here!");
                                        canWater = false;
                                    }
                                }
                            }
                        if (canWater)
                            for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                button.setLayoutX(20 + 200 * (num / 10));
                                button.setLayoutY(80 + (num % 10) * 35);
                                button.setFont(Main.comicFont);
                                button.setPrefWidth(180);
                                num++;
                                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        //System.out.println(itemStack.getItems().get(0));
                                        if (!itemStack.getItems().get(0).getName().endsWith("Watering Can")) {
                                            question.setText("You should choose a watering can! ");
                                            return;
                                        }
                                        WateringCan can = (WateringCan) itemStack.getItems().get(0);
                                        if (can.getWaterLevel() <= 0) {
                                            question.setText("Watering can is empty!");
                                            return;
                                        }
                                        if(!can.canUseTool()){
                                            if (can.getHealth() <= 0)
                                                question.setText("Watering can is broken! ");
                                            else
                                                question.setText("You are too tired!");
                                            return;
                                        }
                                        Pair<Integer, Integer> p = MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY());
                                        for (int i = 0; i < 3; i++)
                                            for (int j = 0; j < 3; j++) {
                                                //System.out.println(fieldUI[i][j].getY());
                                                //System.out.println(Main.character.getY());
                                                if (fieldUI[i][j].getX() == Main.character.getX() && fieldUI[i][j].getY() - 14 == Main.character.getY()) {
                                                    System.out.println("HMM");
                                                    field.crops.get(i + 3*j).water();
                                                    String cropPic = Producer.cropPictureName(field, i + 3 * j);
                                                    can.water();
                                                    System.out.println(cropPic);
                                                    fieldUI[i][j].setImage(new Image("Images\\miscFieldTiles\\" + cropPic));
                                                }
                                            }

                                        stage.close();
                                    }
                                });
                                group.getChildren().add(button);
                            }
                    }

                    if (fruitGarden != null){
                        Group group = new Group();
                        Label question = new Label("Choose a watering can:");
                        question.setFont(new Font("Comic Sans MS", 25));
                        question.setTextFill(Color.DARKGREEN);
                        question.setLayoutX(10);
                        question.setLayoutY(10);
                        group.getChildren().addAll(question);
                        ImageView fruitGardenUI = getFruitGardenUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        int num = 0;
                        boolean canWater = true;
                        if (fruitGarden.trees.size() == 0 || fruitGarden.trees.get(0) == null) {
                            question.setText("There is no tree planted here!");
                            canWater = false;
                        }
                        if (canWater)
                            for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                button.setLayoutX(20 + 200 * (num / 10));
                                button.setLayoutY(80 + (num % 10) * 35);
                                button.setFont(Main.comicFont);
                                button.setPrefWidth(180);
                                num++;
                                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        //System.out.println(itemStack.getItems().get(0));
                                        if (!itemStack.getItems().get(0).getName().endsWith("Watering Can")) {
                                            question.setText("You should choose a watering can! ");
                                            return;
                                        }
                                        WateringCan can = (WateringCan) itemStack.getItems().get(0);
                                        if (can.getWaterLevel() <= 0) {
                                            question.setText("Watering can is empty!");
                                            return;
                                        }
                                        if(!can.canUseTool()){
                                            if (can.getHealth() <= 0)
                                                question.setText("Watering can is broken! ");
                                            else
                                                question.setText("You are too tired!");
                                            return;
                                        }
                                        Pair<Integer, Integer> p = MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY());

                                        System.out.println("HMM");
                                        fruitGarden.trees.get(0).water();
                                        String treePic = Producer.treePictureName(fruitGarden.trees.get(0));
                                        can.water();
                                        waterButton.setVisible(true);
                                        buyTreeButton.setVisible(false);
                                        System.out.println(treePic);
                                        fruitGardenUI.setImage(new Image("Images\\trees\\" + treePic));
                                        stage.close();
                                    }
                                });
                                group.getChildren().add(button);
                            }
                    }
                }



                if (event.getCode() == KeyCode.B) {
                    if (Main.Eisenhower.canMove){
                        //backPackListView = Main.Eisenhower.getBackPack().viewList();
                        //farmGroup.getChildren().addAll(backPackListView);
                        PersonMenu personMenu = new PersonMenu(farmScene,primaryStage);
                        try {
                            Stage mystage = new Stage();
                            mystage.setX(41);
                            personMenu.start(mystage);
                    } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else {
                        Main.Eisenhower.canMove = true;
                        farmGroup.getChildren().remove(backPackListView);
                    }
                }

                if (event.getCode() == KeyCode.P) {
                    Field field = getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                    if (field != null) {
                        Group group = new Group();
                        Label question = new Label("Choose a seed:");
                        question.setFont(new Font("Comic Sans MS", 25));
                        question.setTextFill(Color.DARKGREEN);
                        question.setLayoutX(10);
                        question.setLayoutY(10);
                        group.getChildren().addAll(question);
                        ImageView[][] fieldUI = getFieldUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        int num = 0;
                        boolean canPlant = true;
                        for (int i = 0; i < 3; i++)
                            for (int j = 0; j < 3; j++) {
                                //System.out.println(field.plowed[i + 3 * j]);
                                if (fieldUI[i][j].getX() == Main.character.getX() && fieldUI[i][j].getY() - 14 == Main.character.getY()) {
                                    if (!field.plowed[i + 3 * j]) {
                                        question.setText("Field is not Cultivated!");
                                        canPlant = false;

                                    }
                                }
                            }
                        if (canPlant)
                            for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                button.setLayoutX(20 + 200 * (num / 10));
                                button.setLayoutY(80 + (num % 10) * 35);
                                button.setFont(Main.comicFont);
                                button.setPrefWidth(180);
                                num++;
                                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        //System.out.println(itemStack.getItems().get(0));
                                        if (!itemStack.getItems().get(0).getName().endsWith("seed")) {
                                            question.setText("You should choose a seed! ");
                                            return;
                                        }

                                        ItemStack seedItemStack = Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(), 1);
                                        Seed seed = (Seed) seedItemStack.getItems().get(0);
                                        Pair<Integer, Integer> p = MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY());
                                        for (int i = 0; i < 3; i++)
                                            for (int j = 0; j < 3; j++) {
                                                //System.out.println(fieldUI[i][j].getY());
                                                //System.out.println(Main.character.getY());
                                                if (fieldUI[i][j].getX() == Main.character.getX() && fieldUI[i][j].getY() - 14 == Main.character.getY()) {
                                                    field.crops.set(i + 3 * j, seed.crop);
                                                    seed.crop.alive = true;
                                                    String cropPic = Producer.cropPictureName(field, i + 3 * j);
                                                    System.out.println(cropPic);
                                                    fieldUI[i][j].setImage(new Image("Images\\miscFieldTiles\\" + cropPic));
                                                }
                                            }

                                        stage.close();
                                    }
                                });
                                group.getChildren().add(button);
                            }
                    }
                }

                if (event.getCode() == KeyCode.H){
                    Field field = getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                    if( field != null ){
                        ImageView[][] fieldUI = getFieldUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                        for (int i = 0; i < 3; i++) {
                            for (int j = 0; j < 3; j++) {
                                if (fieldUI[i][j].getX() == Main.character.getX() && fieldUI[i][j].getY() - 14 == Main.character.getY()) {
                                    Crop crop = field.crops.get(i + 3 * j);
                                    crop.checkIfIsAlive();
                                    if (crop.alive == false) {
                                        //System.out.println("This Crop is Dead!");
                                    }
                                    if (crop.age < crop.matureAge) {
                                        //System.out.println("This crop is too young to harvest!");
                                        return;
                                    }

                                    //if don't harvest fruits for a long time they become junk
                                    if (crop.age >= crop.matureAge * 2) {
                                        crop.getOutCome().changeToJunk();
                                    }
                                    System.out.println(crop.getOutCome());
                                    if (crop.getOutCome().getItems().size() > 0){
                                        ItemStack outcome = crop.getOutCome();
                                        if (Main.Eisenhower.getBackPack().getRemainingCapacity() < outcome.getItems().get(0).getSize()){
                                            return;
                                        }
                                        try {
                                            ArrayList<Item> newCrops = new ArrayList<>();
                                            if (outcome.getItems().get(0) instanceof Fruit) {
                                                Fruit fruit = (Fruit) outcome.getItems().get(0);
                                                Random random = new Random();
                                                int number = random.nextInt() % 3 + 2;
                                                for (int k = 0; k < number; k++) {
                                                    newCrops.add(new Fruit(fruit.getName(), fruit.getSize(), fruit.getPrice(), fruit.getEffect()));
                                                }
                                                if(crop.addedFruitsFertilization){
                                                    outcome.getItems().add(new Fruit(fruit.getName(),fruit.getSize(),fruit.getPrice(),fruit.getEffect()));
                                                    crop.addedFruitsFertilization = false;
                                                }
                                                System.out.println(newCrops.get(0).getName());
                                            }

                                            crop.age = 0;
                                            crop.currentNumberOfHarvests++;

                                            Main.Eisenhower.getBackPack().putInItemStack(outcome, outcome.getItems().size());
                                            crop.setOutCome(new ItemStack(newCrops));
                                            System.out.println(crop.getOutCome().getItems().get(0));
                                            System.out.println("Harvesting Done!");
                                            //outcome.getItems().add()
                                        } catch (Exception e) {
                                            System.out.println("BadCommand!");
                                        }
                                        String picName = Producer.cropPictureName(field,3*j + i);
                                        fieldUI[i][j].setImage(new Image("Images\\miscFieldTiles\\" + picName));
                                    }
                                }
                            }
                        }
                    }
                    FruitGarden fruitGarden = getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                    if (fruitGarden != null){
                        ItemStack outcome = fruitGarden.trees.get(0).getOutCome();
                        Main.Eisenhower.getBackPack().putInItemStack(outcome, outcome.getItems().size());
                        ImageView fruitGardenUI = getFruitGardenUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                        fruitGardenUI.setImage(new Image("Images\\trees\\" + Producer.treePictureName(fruitGarden.trees.get(0))));
                    }
                }

                if (event.getCode() == KeyCode.C) {
                    {
                        Field field = getField(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                        if (field != null) {
                            Group group = new Group();
                            Label question = new Label("Choose a shovel:");
                            question.setFont(new Font("Comic Sans MS", 25));
                            question.setTextFill(Color.DARKGREEN);
                            question.setLayoutX(10);
                            question.setLayoutY(10);
                            group.getChildren().addAll(question);
                            ImageView[][] fieldUI = getFieldUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                            Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.show();
                            int num = 0;
                            for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                button.setLayoutX(20 + 200 * (num / 10));
                                button.setLayoutY(80 + (num % 10) * 35);
                                button.setFont(Main.comicFont);
                                button.setPrefWidth(180);
                                num++;
                                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        //System.out.println(itemStack.getItems().get(0));
                                        if (!itemStack.getItems().get(0).getName().endsWith("Shovel")) {
                                            question.setText("You should choose a shovel!");
                                            return;
                                        }
                                        Shovel shovel = (Shovel) itemStack.getItems().get(0);
                                        if (!shovel.canUseTool()) {
                                            if (shovel.getHealth() <= 0)
                                                question.setText("Shovel is broken!");
                                            else
                                                question.setText("You are too tired!");
                                            return;
                                        }
                                        shovel.plow();
                                        field.plowAll();

                                        for (int i = 0; i < 3; i++)
                                            for (int j = 0; j < 3; j++)
                                            if (field.crops.get(i + 3 * j) == null){
                                                fieldUI[i][j].setImage(new Image("Images\\miscFieldTiles\\dryPlowed.png"));
                                            }
                                        //System.out.println(field.getName() + " Plowed!");
                                        stage.close();
                                    }
                                });
                                group.getChildren().add(button);
                            }
                        }
                    }
                }

                if (event.getCode() == KeyCode.T){
                    FruitGarden fruitGarden = getFruitGarden(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                    if (fruitGarden != null && fruitGarden.trees.size() == 0) {
                        Group group = new Group();
                        Label question = new Label("Choose a tree:");
                        question.setFont(new Font("Comic Sans MS", 25));
                        question.setTextFill(Color.DARKGREEN);
                        question.setLayoutX(10);
                        question.setLayoutY(10);
                        group.getChildren().addAll(question);
                        ImageView fruitGardenUI = getFruitGardenUI(MovementHandler.pixelToTile((int) Main.character.getX(), (int) Main.character.getY()));
                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        int num = 0;
                        Tree[] trees = new Tree[6 + Main.addedTrees.size()];
                        trees[0] = Producer.peachTree();
                        trees[1] = Producer.pearTree();
                        trees[2] = Producer.lemonTree();
                        trees[3] = Producer.pomegranateTree();
                        trees[4] = Producer.appleTree();
                        trees[5] = Producer.orangeTree();
                        for (int i = 0 ; i <  Main.addedTrees.size(); i++ ){
                            trees[6+i] = Producer.copyTree(Main.addedTrees.get(i));
                        }
                        for (Tree tree : trees) {
                            Button button = new Button(tree.getName());
                            button.setLayoutX(210 + 200 * (num / 10));
                            button.setLayoutY(80 + (num % 10) * 50);
                            button.setFont(Main.comicFont);
                            button.setPrefWidth(200);
                            num++;
                            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    if (Main.Eisenhower.getMoney() < tree.getPrice()){
                                        question.setText("Not enough money!");
                                        return;
                                    }
                                    Main.Eisenhower.takeMoney(tree.getPrice());
                                    fruitGarden.trees.add(tree);
                                    tree.isBought = true;
                                    fruitGardenUI.setImage(new Image("Images\\trees\\" + Producer.treePictureName(tree)));
                                    stage.close();
                                }
                            });
                            group.getChildren().add(button);
                        }
                    }
                }
                if (event.getCode() == KeyCode.ESCAPE){
                    Main.Eisenhower.canMove = false;
                    Main.timeline.pause();
                    PauseMenue pauseMenue = new PauseMenue(farmScene);
                    try {
                        pauseMenue.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (Main.character.getX() > farmScene.getWidth()-32){
                    Main.character.setX(0);
                    infarms  = false;
                    VillageBuilder.invillage = true;
                    Main.villageBuilder.start(primaryStage);

                }
                else if (Main.character.getX()  < 32 )
                {
                    //System.out.println("LOLO");
                    //Main.character.setX(128);
                    try {
                        Main.character.setX(0);
                        Main.character.setY(14 * 32 - 14 );
                        ForestBuilder.inforest = true;
                        infarms = false;
                        Main.forestBuilder.start(primaryStage);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.E) {
                    if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(14, 13))) {
                        SoundLoader.door();
                        Main.character.setY(farmScene.getHeight() - 32 - 14);
                        Main.character.setX(farmScene.getWidth() / 2);
                        Main.houseBuilder.start(primaryStage);
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(5, 25)))
                    {
                        Main.character.setX(0);
                        Main.character.setY(32 * 14 - 14);
                        try {
                            SoundLoader.door();
                            Main.barnBuilder.start(primaryStage);
                        } catch (Exception e) {
                            e.printStackTrace();                        }
                    }
                }
                numinimage++;
            }

    });
}

    public void drawFields() {
        for (int i = 0 ; i < 4 ; i++)
            for (int j = 0 ; j < 4 ; j++){
                fields[i][j] = mainfarm.getFields().get(j*4 + i);
                for (int k = 0 ; k < 3; k++)
                    for (int l = 0 ; l < 3 ;l++){
                        String picName = Producer.cropPictureName(fields[i][j],3*l + k);
                        if (fieldsUI[i][j][k][l] != null)
                        fieldsUI[i][j][k][l].setImage(new Image("Images\\miscFieldTiles\\" + picName));
                    }
            }
    }

    public void drawFruitGardens() {
        for (int i = 0 ; i < 3; i++)
            for (int j = 0 ; j < 2 ;j++){
                //System.out.println(fruitGardens[i][j]);
                if (fruitGardens[i][j].trees.size() == 0)
                    fruitGardensUI[i][j].setImage(null);
                else {
                    //System.out.println(fruitGardens[i][j].trees.get(0));
                    String picName = Producer.treePictureName(fruitGardens[i][j].trees.get(0));
                    System.out.println(picName);
                    fruitGardensUI[i][j].setImage(new Image("Images\\trees\\" + picName));
                }
        }
    }
}

