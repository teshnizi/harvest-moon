import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by pc on 7/10/2017.
 */
public class LaboratoryBuilder extends Application {
    Stage mystage;
    int price1 =0;
    int mach = 0;
    int num = 0;
    static  boolean flag = false;
    public LaboratoryBuilder(Stage stage){
        this.mystage = stage;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group maingroup = new Group();
        Scene scene = new Scene(maingroup,620,700,new ImagePattern(new Image("images\\white-texture.jpg")));
        Group group = new Group();
        menu(group);
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.E)
                    primaryStage.close();
            }
        });
        maingroup.getChildren().add(group);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void menu(Group maingroup){
        Laboratory laboratory = Main.village.getLaboratory();

        Text text = new Text("Laboratory");
        text.setX(200);
        text.setY(50);
        text.setFont(new Font(40));
        maingroup.getChildren().add(text);

        Text explain = new Text();
        explain.setLayoutX(50);
        explain.setLayoutY(500);
        explain.setFont(Main.comicFont);

        Text statu = new Text();
        statu.setLayoutX(400);
        statu.setLayoutY(450);
        statu.setFont(Main.comicFont);


        Text price = new Text();
        price.setLayoutX(400);
        price.setLayoutY(500);
        explain.setFont(Main.comicFont);


        Text text1 = new Text("Machines that are in laboratory");
        text1.setX(200);
        text1.setY(100);
        text1.setFont(Main.comicFont);
        maingroup.getChildren().add(text1);

        Button buy = new Button("Buy");
        buy.setLayoutX(400);
        buy.setLayoutY(400);
        buy.setFont(Main.comicFont);
        buy.setVisible(false);

        Button juicer = new Button("Juicer");
        juicer.setLayoutX(50);
        juicer.setLayoutY(150);
        juicer.setFont(Main.comicFont);
        juicer.setPrefSize(200,20);
        juicer.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                juicer.setCursor(Cursor.HAND);
                price1 = 1000;
                explain.setText("A machine for juicing fruits.");
                price.setText("Price : " + price1 + "$");
            }
        });
        juicer.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setVisible(true);
                mach = 0;
            }
        });
        if (flag)
            juicer.setVisible(false);

        buy.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setCursor(Cursor.HAND);
            }
        });
        buy.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (mach == 0)
                {
                    if (Main.Eisenhower.getMoney() >= 1000)
                    {
                        statu.setText("Juicer Bought!");
                        flag =true;
                        Main.Eisenhower.takeMoney(1000);
                        laboratory.getMachineRoom().hasJuicer = true;
                        maingroup.getChildren().clear();
                        num = 0;
                        menu(maingroup);
                    }
                    else
                    {
                        statu.setText("Not enough money!");
                    }
                }
                else{
                    int k = 0;
                    Machine mymachin = null;
                    for (Machine machine : laboratory.getMachines())
                    {
                        k++;
                        if (k == (mach)){
                            mymachin = machine;
                            break;
                        }
                    }
                    System.out.println(mach);
                    if (mymachin.getPrice() <= Main.Eisenhower.getMoney()) {
                        Main.Eisenhower.takeMoney(mymachin.getPrice());
                        laboratory.getMachineRoom().getMachines().add(mymachin);
                        laboratory.getMachines().remove(mymachin);
                        statu.setText(mymachin.getName() + " Bought!");
                        num = 0;
                        mach = 0;
                        maingroup.getChildren().clear();
                        menu(maingroup);
                    }
                    else {
                        statu.setText("Not enough money!");
                    }
                }
            }
        });

        num =0;
        for (Machine machine : laboratory.getMachines()){
            System.out.println(machine.getName());
            Button button = new Button(machine.getName());
            button.setLayoutX(50);
            button.setLayoutY(200 + num%10 * 50);
            button.setFont(Main.comicFont);
            button.setPrefSize(200,20);
            maingroup.getChildren().addAll(button);
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                    price1 = machine.getPrice();
                    price.setText("Price : " + price1 + "$");
                    explain.setText(machine.status());
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    buy.setVisible(true);
                    int t = 0;
                    for (Machine mmachine : laboratory.getMachines()){
                        if (mmachine.getName().equals(button.getText()))
                            break;
                        t++;
                    }
                    mach = t +1;
                }
            });
            num ++;
        }


        maingroup.getChildren().addAll(explain,juicer,price,buy,statu);
    }
}
