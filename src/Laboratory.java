
import java.util.*;

/**
 * 
 */
public class Laboratory extends Place {

    /**
     * Default constructor
     */
    public Laboratory(String name, MachineRoom machineRoom) {
        super(name);
        this.machineRoom = machineRoom;
    }

    private MachineRoom machineRoom;
    private ArrayList<Machine> machines = new ArrayList<>();

    public ArrayList<Machine> getMachines() {
        return machines;
    }

    public MachineRoom getMachineRoom() {
        return machineRoom;
    }
}