
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Item extends Thing implements Serializable{

    /**
     * Default constructor
     */
    public Item(String name, int size) {
        super(name);
        this.size = size;
    }

    public Item(String name, int size, int price){
        this(name, size);
        this.price = price;
        this.size = size;
    }

    public Item(String name, int size, int price, String description){
        this(name, size, price);
        this.description = description;
    }

    @Override
    public String toString(){
        String s = getName();
        return s;
    }

    private int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int size;

    String description = "";

    public int getSize() {
        return size;
    }

    /**
     * @return
     */
    public String status(){
        return getName()+ "\n" + description;
    }

}