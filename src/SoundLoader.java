import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.util.Random;

/**
 * Created by tigerous215 on 7/7/2017.
 */
public class SoundLoader {

    static Random random = new Random();


    public static void cook() {
        MediaPlayer cookSound = new MediaPlayer(new Media(new File(
                "sounds\\cook.wav"
        ).toURI().toString()));
        cookSound.play();
    }


    public static void door() {
        MediaPlayer doorSound = new MediaPlayer(new Media(new File(
                "sounds\\door.mp3"
        ).toURI().toString()));
        doorSound.play();
    }



}
