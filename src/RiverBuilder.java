import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Random;

/**
 * Created by pc on 7/12/2017.
 */
public class RiverBuilder extends Application {
    River river;
    FishingRod myfishingRod = null;
    public RiverBuilder(River river){
        this.river =river;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group  =new Group();
        Scene scene = new Scene(group,620 , 700 , new ImagePattern(new Image("images\\white-texture.jpg")));

        Text sub = new Text();
        sub.setLayoutX(250);
        sub.setLayoutY(40);
        sub.setFont(new Font(25));
        sub.setText("river");
        group.getChildren().add(sub);

        Text text = new Text();
        text.setLayoutX(200);
        text.setLayoutY(100);
        text.setFont(Main.comicFont);
        group.getChildren().addAll(text);

        Button button  = new Button("Fish");
        button.setLayoutX(250);
        button.setLayoutY(500);
        button.setVisible(false);
        button.setFont(Main.comicFont);
        group.getChildren().add(button);

        int num = 0;
        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()){
            if (itemStack.getItems().get(0) instanceof FishingRod){
                FishingRod fishingRod = (FishingRod) itemStack.getItems().get(0);
                Button button1 = new Button(itemStack.getItems().get(0).getName());
                button1.setLayoutX(50);
                button1.setLayoutY(num * 50 + 150);
                button1.setPrefSize(150,30);
                group.getChildren().add(button1);
                button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        myfishingRod = fishingRod;
                        button.setVisible(true);
                    }
                });

                num++;
            }
        }
        if (num == 0)
            text.setText("you haven't FishingRod");
        else
            text.setText("Chose FishingRod");

        Text result = new Text();
        result.setLayoutX(200);
        result.setLayoutY(400);
        result.setFont(Main.comicFont);
        group.getChildren().add(result);
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (myfishingRod.health - (5-myfishingRod.getType())*5 >=0) {
                    if (myfishingRod.canUseTool()) {
                        if (probellity((myfishingRod.getType() + 1) * 20)) {
                            if (Main.Eisenhower.getBackPack().putInItemStack(river.getFish(), Math.min(river.getFish().getItems().size(), 1)) == 1) {
                                result.setText("1 number fish was caught." + "\nEnergy usage : " + (5 - myfishingRod.getType()) * 5);
                                myfishingRod.fish();
                            }
                        } else
                            result.setText("Can't catch fish. try again");
                    } else
                        result.setText("you are too tiered");
                }
                else
                    result.setText("Your FishingRod is broken");
            }
        });


        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public boolean probellity(int prob){
        Random random = new Random();
        int n = random.nextInt(100) + 1;
        for (int i = 1 ;i <= prob ; i++) {
            if (i == n)
                return true;
        }
        return false;
    }
}
