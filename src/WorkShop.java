
import java.util.*;

/**
 * 
 */
public class WorkShop extends Store {

    /**
     * Default constructor
     */
    ArrayList<Tool> tools1;
    public WorkShop(String name) {
        super(name);
        tools1 = new ArrayList<>();
        for (int i = 1 ; i<=4 ; i++)
            tools1.add(Producer.axe(i));
        for (int i = 1 ; i<=4 ; i++)
            tools1.add(Producer.pickAxe(i));
        for (int i = 1 ; i<=4 ; i++)
            tools1.add(Producer.shovel(i));
        for (int i = 1 ; i<=4 ; i++)
            tools1.add(Producer.fishingRod(i));
        for (int i = 1 ; i<=8 ; i++)
            tools1.add(Producer.wateringCan(i));
    }

    /**
     * 
     */
    private ArrayList<Tool> tools;

    public ArrayList<String> objects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("");
        return s;
    }

}