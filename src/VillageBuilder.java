import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by tigerous215 on 7/6/2017.
 */
public class VillageBuilder extends Application {

    static int numinimage = 3;
    static  boolean invillage = false;
    private Village mainVillage;
    static Group villageGroup;
    Scene villageScene;
    ListView backPackListView;
    static ArrayList<Pair<Pair<String,ImageView>,Pair<Integer,Integer>>> chars
            = new ArrayList<>();
    Font menuFont = new Font("Cambria",48);

    static HashSet<Pair<Integer,Integer>> villageAvailableTiles = new HashSet<>();
    static {
        for (int i = -1 ; i < 21 ; i++){
            villageAvailableTiles.add(new Pair<>(i,8));
            villageAvailableTiles.add(new Pair<>(i,9));
        }
        villageAvailableTiles.add(new Pair<>(20,10));
        villageAvailableTiles.add(new Pair<>(19,10));
        for (int i = 20 ; i < 32 ; i++){
            villageAvailableTiles.add(new Pair<>(i,7));
            villageAvailableTiles.add(new Pair<>(i,8));
        }
        villageAvailableTiles.add(new Pair<>(30,6));
        villageAvailableTiles.add(new Pair<>(31,6));
        for (int i = 30 ; i < 36 ; i++){
            villageAvailableTiles.add(new Pair<>(i,4));
            villageAvailableTiles.add(new Pair<>(i,5));
        }
        for (int i = 15 ; i < 25 ; i++)
            for(int j = 11 ; j < 20 ;j++)
                villageAvailableTiles.add(new Pair<>(i,j));

        for (int i = 17 ; i < 23 ; i++)
            for(int j = 13 ; j < 18 ;j++)
                villageAvailableTiles.remove(new Pair<>(i,j));

        villageAvailableTiles.remove(new Pair<>(15,11));
        villageAvailableTiles.remove(new Pair<>(24,11));

        for(int i = 25; i <= 40 ; i++) {
            villageAvailableTiles.add(new Pair<>(i, 15));
            villageAvailableTiles.add(new Pair<>(i, 16));
        }


        for (int i = 2 ; i < 17 ; i++){
            villageAvailableTiles.add(new Pair<>(35,i));
            villageAvailableTiles.add(new Pair<>(36,i));
        }
        for(int i = 6 ; i < 21 ; i++) {
            for (int j = 18; j < 29; j++) {
                villageAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for(int i = 8 ; i < 19 ; i++) {
            for (int j = 20; j < 30; j++) {
                villageAvailableTiles.remove(new Pair<>(i, j));
            }
        }

        villageAvailableTiles.remove(new Pair<>(19,29));
        villageAvailableTiles.remove(new Pair<>(20,29));

        for(int i = 8 ; i < 14 ; i++){
            villageAvailableTiles.add(new Pair<>(i,28));
        }

        for(int i = 21 ; i < 37 ; i++){
            villageAvailableTiles.add(new Pair<>(i,28));
            villageAvailableTiles.add(new Pair<>(i,27));
        }


    }

    public VillageBuilder(Village village){
        this.mainVillage = village;
    }

    @Override
    public void start(Stage primaryStage){
        villageGroup = new Group();
        villageScene = new Scene(villageGroup,1280,960);
        Image village = new Image("maps\\city.png");
        //boolean isShowingBackBack = false;
        villageGroup.getChildren().addAll(Main.character);
        primaryStage.setScene(villageScene);
        primaryStage.show();

        villageScene.setFill(new ImagePattern(village));

        Button inspectbutton = new Button("Inspect");
        inspectbutton.setVisible(false);

        Button spacebutton = new Button("Enter");
        spacebutton.setLayoutX(Main.character.getX()-10);
        spacebutton.setLayoutY(Main.character.getY() -25);
        spacebutton.setVisible(false);
        villageGroup.getChildren().addAll(spacebutton,inspectbutton);
        villageScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage % 4 + 1;
                Pair<Integer, Integer> next;

                if (Main.Eisenhower.canMove) {
                    DataOutputStream dout = null;
                    try {
                        if (Main.online)
                         dout = new DataOutputStream(Getter.socket.getOutputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png"));
                        MovementHandler.move(2, villageAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\left" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png"));
                        MovementHandler.move(0, villageAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\right" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, villageAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\b" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.DOWN) {

                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png"));
                        MovementHandler.move(3, villageAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\f" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //Main.Eisenhower.getBackPack().view();
                        //System.out.println("KEKE");
                    }
                }
                if (event.getCode() == KeyCode.ESCAPE){
                    Main.Eisenhower.canMove = false;
                    Main.timeline.pause();
                    PauseMenue pauseMenue = new PauseMenue(villageScene);
                    try {
                        pauseMenue.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (event.getCode() == KeyCode.B) {
                    //backPackListView = Main.Eisenhower.getBackPack().viewList();
                    //farmGroup.getChildren().addAll(backPackListView);
                    PersonMenu personMenu = new PersonMenu(villageScene, primaryStage);
                    try {
                        Stage mystage = new Stage();
                        mystage.setX(40);
                        personMenu.start(mystage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.E ){
                    if(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(8,8))) {
                        WorkShopBuilder workShopBuilder = new WorkShopBuilder(primaryStage);
                        Stage stage = new Stage();
                        try {
                            workShopBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(14,8))){
                        GymBuilder gymBuilder = new GymBuilder(primaryStage);
                        Stage stage = new Stage();
                        try {
                            gymBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(26,7))){
                        ClinicBuilder clinicBuilder = new ClinicBuilder(primaryStage);
                        Stage stage = new Stage();
                        try {
                            clinicBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(13,18))){
                        LaboratoryBuilder laboratoryBuilder = new LaboratoryBuilder(primaryStage);
                        Stage stage = new Stage();
                        try {
                            laboratoryBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(30,15))){
                        CafeBuilder cafeBuilder = new CafeBuilder();
                        Stage stage = new Stage();
                        try {
                            cafeBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(13,28))){
                        RanchBuilder ranchBuilder = new RanchBuilder();
                        Stage stage = new Stage();
                        try {
                            ranchBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (event.getCode() == KeyCode.I){
                    if (isis()){
                        String h = isisstring();
                        Inspectbuilder inspectbuilder = new Inspectbuilder(h);
                        Stage stage = new Stage();
                        try {
                            inspectbuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(8,8)))
                {
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);

                }
                else if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(14,8))){
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(26,7)))
                {
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(13,18)))
                {
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(30,15)))
                {
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(13,28)))
                {
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if(isis()){
                    inspectbutton.setLayoutX(Main.character.getX() -5);
                    inspectbutton.setLayoutY(Main.character.getY() -25);
                    inspectbutton.setVisible(true);
                }
                else{

                        spacebutton.setVisible(false);
                        inspectbutton.setVisible(false);
                }



                if (Main.character.getX() < 0){
                    invillage = false;
                    FarmBuilder.infarms = true;
                    Main.character.setX(villageScene.getWidth()-32);
                    Main.farmBuilder.start(primaryStage);
                }
                if (Main.character.getX() > villageScene.getWidth()-32){
                    invillage= false;
                    MarketBuilder.inmarket = true;
                    Main.character.setX(0);
                    Main.marketBuilder.start(primaryStage);

                }

                numinimage++;
            }
        });


    }
    public boolean isis(){
        int i = 0;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                return true;
            }
        }
        return false;
    }
    public String isisstring(){
        String h = null;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                h= p.getKey().getKey();
            }
        }
        return h;
    }
    public String mystring(String st){
        String ans;
        ans = Getter.socketname + Protocol.protocol+ Protocol.character + Protocol.protocol +Protocol.villages +Protocol.protocol+st + Protocol.protocol+MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getKey() +Protocol.protocol  + MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getValue();
        return ans;
    }

}
