
import java.util.*;

/**
 * 
 */
public class Barn extends Place {

    /**
     * Default constructor
     */
    public Barn(String name) {
        super(name);
    }

    static ArrayList<AnimalField> animalFields = new ArrayList<>();

    private MachineRoom machineRoom = new MachineRoom("MachineRoom");

    public ArrayList<AnimalField> getAnimalFields() {
        return animalFields;
    }

    public MachineRoom getMachineRoom() {
        return machineRoom;
    }

    /**
     * @return
     */

    @Override
    public ArrayList<String> listOfObjects(){
        ArrayList<String> ret = new ArrayList<>();
        for(AnimalField animalField : animalFields)
            ret.add(animalField.getName());
        ret.add(machineRoom.getName());
        return ret;
    }

    @Override
    public Thing findObject(String objectName) {
        for(AnimalField animalField : animalFields){
            System.out.println(animalField.getName());
            if(animalField.getName().equals(objectName))
                return animalField;
        }
        if (machineRoom.getName().equals(objectName))
            return machineRoom;
        return null;
    }

    @Override
    public void nextDay(Season season){
        for (AnimalField animalField : animalFields)
            animalField.nextDay(season);
    }

}