/**
 * Created by Tigerous215 on 5/16/2017.
 */
public class AnimalMedicine extends Item{

    public AnimalMedicine(String name, int size, int price, int healValue) {
        super(name, size, price);
        this.healValue = healValue;
    }

    @Override
    public String status(){
        return "Used for healing animals. Price : $" + getPrice();
    }
    int healValue;
}
