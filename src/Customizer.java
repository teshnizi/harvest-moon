import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by tigerous215 on 7/14/2017.
 */
public class Customizer extends Application{

    Group group ;
    Scene scene;
    File sproutFile;
    File youngFile;
    File matureFile;


    @Override
    public void start(Stage primaryStage) throws Exception {
        ListView<String> listView = new ListView<>();
        for (Fruit fruit : Main.customFruits)
            listView.getItems().addAll(fruit.getName());
        group = new Group();
        scene = new Scene(group, 1280, 960);
        primaryStage.setScene(scene);
        scene.setFill(Color.AZURE);
        listView.setLayoutX(900);
        listView.setLayoutY(500);
        //scene.setFill(Color.CHOCOLATE);
        Rectangle backButton = new Rectangle(10,10,30,30);
        backButton.setFill(new ImagePattern(new Image("images\\backButton.jpg")));
        backButton.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                backButton.setCursor(Cursor.HAND);
            }
        });
        backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (scene.getRoot() == group){
                    Main.mainstage.setScene(Main.mainMenuScene);
                }
                else
                    scene.setRoot(group);
            }
        });

        Button bb = new Button("Main Menu");
        bb.setLayoutX(10);
        bb.setLayoutY(10);
        bb.setFont(Main.comicFont);
        bb.setShape(new Ellipse(100,50));
        group.getChildren().addAll(bb);
        bb.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                try {
                    FileOutputStream fileOutputStream = new FileOutputStream("custom\\fruits.txt");
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customFruits.size()));
                    for( Fruit fruit : Main.customFruits){
                        objectOutputStream.writeObject(fruit);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\crops.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customCrops.size()));
                    for (Crop crop : Main.customCrops){
                        objectOutputStream.writeObject(crop);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\trees.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customTrees.size()));
                    for (Tree tree : Main.customTrees){
                        objectOutputStream.writeObject(tree);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\meals.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customMeals.size()));
                    for (Meal meal : Main.customMeals){
                        objectOutputStream.writeObject(meal);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\exercises.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customExercises.size()));
                    for (Exercise exercise : Main.customExercises){
                        objectOutputStream.writeObject(exercise);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\kitchenTools.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customKitchenTools.size()));
                    for (KitchenTool kitchenTool : Main.customKitchenTools){
                        objectOutputStream.writeObject(kitchenTool);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\resources.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customResources.size()));
                    for (Resource resource :  Main.customResources){
                        objectOutputStream.writeObject(resource);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\foods.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customFoods.size()));
                    for (Food food :  Main.customFoods){
                        objectOutputStream.writeObject(food);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\machines.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customMachines.size()));
                    System.out.println(Main.customMachines.size());
                    for (Machine machine :  Main.customMachines){
                        objectOutputStream.writeObject(machine);
                        System.out.println(machine.getInputItems());
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();

                    fileOutputStream = new FileOutputStream("custom\\potions.txt");
                    objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(new Integer(Main.customPotions.size()));
                    System.out.println(Main.customPotions.size());
                    for (Medicine medicine :  Main.customPotions){
                        objectOutputStream.writeObject(medicine);
                        System.out.println(medicine);
                    }
                    objectOutputStream.flush();
                    objectOutputStream.close();


                } catch (IOException e) {
                    e.printStackTrace();
                }
                Main.mainstage.setScene(Main.mainMenuScene);
            }
        });
        Button customizePlayerButton = new Button("Player");
        customizePlayerButton.setPrefWidth(200);
        customizePlayerButton.setFont(Main.menuFont);
        customizePlayerButton.setLayoutX(100);
        customizePlayerButton.setLayoutY(100);

        group.getChildren().addAll(customizePlayerButton);
        customizePlayerButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Group group1 = new Group();
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);
                Label health = new Label("Healh: ");
                TextField healthField = new TextField();
                Button healthButton = new Button("Change health");
                health.setFont(Main.comicFont);
                healthField.setFont(Main.comicFont);
                healthButton.setFont(Main.comicFont);
                health.setLayoutX(100);
                healthField.setLayoutX(280);
                healthButton.setLayoutX(500);
                health.setLayoutY(100);
                healthField.setLayoutY(100);
                healthButton.setLayoutY(100);

                healthButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Main.Eisenhower.getAbilities().get(0).setMaxValue(Integer.parseInt(healthField.getText()));
                            Main.Eisenhower.getAbilities().get(0).setCurrentValue(Integer.parseInt(healthField.getText()) * 3 / 4);
                            healthField.clear();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });


                Label energy = new Label("Energy: ");
                TextField energyField = new TextField();
                Button energyButton = new Button("Change energy");
                energy.setFont(Main.comicFont);
                energyField.setFont(Main.comicFont);
                energyButton.setFont(Main.comicFont);
                energy.setLayoutX(100);
                energyField.setLayoutX(280);
                energyButton.setLayoutX(500);
                energy.setLayoutY(150);
                energyField.setLayoutY(150);
                energyButton.setLayoutY(150);

                energyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Main.Eisenhower.getAbilities().get(1).setMaxValue(Integer.parseInt(healthField.getText()));
                            Main.Eisenhower.getAbilities().get(1).setCurrentValue(Integer.parseInt(healthField.getText()) * 3 / 4);
                            energyField.clear();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                Label stamina = new Label("Stamina: ");
                TextField staminaField = new TextField();
                Button staminaButton = new Button("Change stamina");
                stamina.setFont(Main.comicFont);
                staminaField.setFont(Main.comicFont);
                staminaButton.setFont(Main.comicFont);
                stamina.setLayoutX(100);
                staminaField.setLayoutX(280);
                staminaButton.setLayoutX(500);
                stamina.setLayoutY(200);
                staminaField.setLayoutY(200);
                staminaButton.setLayoutY(200);

                staminaButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Main.Eisenhower.getAbilities().get(2).setMaxValue(Integer.parseInt(healthField.getText()));
                            Main.Eisenhower.getAbilities().get(2).setCurrentValue(Integer.parseInt(healthField.getText()) * 3 / 4);
                            staminaField.clear();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });


                Label satiety = new Label("Satiety: ");
                TextField satietyField = new TextField();
                Button satietyButton = new Button("Change satiety");
                satiety.setFont(Main.comicFont);
                satietyField.setFont(Main.comicFont);
                satietyButton.setFont(Main.comicFont);
                satiety.setLayoutX(100);
                satietyField.setLayoutX(280);
                satietyButton.setLayoutX(500);
                satiety.setLayoutY(250);
                satietyField.setLayoutY(250);
                satietyButton.setLayoutY(250);

                satietyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Main.Eisenhower.getAbilities().get(3).setMaxValue(Integer.parseInt(healthField.getText()));
                            Main.Eisenhower.getAbilities().get(3).setCurrentValue(Integer.parseInt(healthField.getText()) * 3 / 4);
                            satietyField.clear();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                group1.getChildren().addAll(health, healthField, healthButton,
                                            energy, energyField, energyButton,
                                            stamina, staminaField, staminaButton,
                                            satiety, satietyField, satietyButton);

                Label capacity = new Label("BackPack capacity: ");
                TextField capacityField = new TextField();
                Button capacityButton = new Button("Change capacity");
                capacity.setFont(Main.comicFont);
                capacityField.setFont(Main.comicFont);
                capacityButton.setFont(Main.comicFont);
                capacity.setLayoutX(100);
                capacityField.setLayoutX(280);
                capacityButton.setLayoutX(500);
                capacity.setLayoutY(300);
                capacityField.setLayoutY(300);
                capacityButton.setLayoutY(300);

                capacityButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Main.Eisenhower.getBackPack().setCapacity(Integer.parseInt(capacityField.getText()));
                            capacityField.clear();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                Label money = new Label("Starting money: ");
                TextField moneyField = new TextField();
                Button moneyButton = new Button("Change money");
                money.setFont(Main.comicFont);
                moneyField.setFont(Main.comicFont);
                moneyButton.setFont(Main.comicFont);
                money.setLayoutX(100);
                moneyField.setLayoutX(280);
                moneyButton.setLayoutX(500);
                money.setLayoutY(350);
                moneyField.setLayoutY(350);
                moneyButton.setLayoutY(350);

                moneyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            Main.Eisenhower.setMoney(Integer.parseInt(capacityField.getText()));
                            moneyField.clear();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                group1.getChildren().addAll(money, moneyField, moneyButton,
                                            capacity, capacityField, capacityButton);

                healthButton.setPrefWidth(200);
                energyButton.setPrefWidth(200);
                staminaButton.setPrefWidth(200);
                satietyButton.setPrefWidth(200);
                capacityButton.setPrefWidth(200);
                moneyButton.setPrefWidth(200);
            }
        });


        //Customize fruits,meals,foods,exercises,potions
        Button fruitButton = new Button("Fruit/Potion\nMeal/Exercise\nFood");
        fruitButton.setPrefWidth(200);
        fruitButton.setPrefHeight(100);
        fruitButton.setFont(new Font(20));
        fruitButton.setLayoutX(400);
        fruitButton.setLayoutY(100);
        group.getChildren().addAll(fruitButton);
        fruitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Group group1 = new Group();
                group1.getChildren().addAll(listView);
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);

                TextField nameField = new TextField();
                Label name = new Label("Name");
                name.setFont(Main.comicFont);
                nameField.setFont(Main.comicFont);
                name.setLayoutX(100);
                nameField.setLayoutX(280);
                name.setLayoutY(100);
                nameField.setLayoutY(100);

                TextField healthField = new TextField("current change");
                Label health = new Label("Health:");
                health.setFont(Main.comicFont);
                healthField.setFont(Main.comicFont);
                health.setLayoutX(100);
                healthField.setLayoutX(280);
                health.setLayoutY(150);
                healthField.setLayoutY(150);
                TextField healthFieldMax = new TextField("max change");
                TextField healthFieldRate = new TextField("rate change");
                healthFieldMax.setFont(Main.comicFont);
                healthFieldRate.setFont(Main.comicFont);
                healthFieldMax.setLayoutX(580);
                healthFieldMax.setLayoutY(150);
                healthFieldRate.setLayoutX(880);
                healthFieldRate.setLayoutY(150);

                TextField energyField = new TextField("current change");
                Label energy = new Label("Energy:");
                energy.setFont(Main.comicFont);
                energyField.setFont(Main.comicFont);
                energy.setLayoutX(100);
                energyField.setLayoutX(280);
                energy.setLayoutY(200);
                energyField.setLayoutY(200);
                TextField energyFieldMax = new TextField("max change");
                TextField energyFieldRate = new TextField("rate change");
                energyFieldMax.setFont(Main.comicFont);
                energyFieldRate.setFont(Main.comicFont);
                energyFieldMax.setLayoutX(580);
                energyFieldMax.setLayoutY(200);
                energyFieldRate.setLayoutX(880);
                energyFieldRate.setLayoutY(200);

                TextField staminaField = new TextField("current change");
                Label stamina = new Label("Stamina:");
                stamina.setFont(Main.comicFont);
                staminaField.setFont(Main.comicFont);
                stamina.setLayoutX(100);
                staminaField.setLayoutX(280);
                stamina.setLayoutY(250);
                staminaField.setLayoutY(250);
                TextField staminaFieldMax = new TextField("max change");
                TextField staminaFieldRate = new TextField("rate change");
                staminaFieldMax.setFont(Main.comicFont);
                staminaFieldRate.setFont(Main.comicFont);
                staminaFieldMax.setLayoutX(580);
                staminaFieldMax.setLayoutY(250);
                staminaFieldRate.setLayoutX(880);
                staminaFieldRate.setLayoutY(250);

                TextField satietyField = new TextField("current change");
                Label satiety = new Label("Satiety");
                satiety.setFont(Main.comicFont);
                satietyField.setFont(Main.comicFont);
                satiety.setLayoutX(100);
                satietyField.setLayoutX(280);
                satiety.setLayoutY(300);
                satietyField.setLayoutY(300);
                TextField satietyFieldMax = new TextField("max change");
                TextField satietyFieldRate = new TextField("rate change");
                satietyFieldMax.setFont(Main.comicFont);
                satietyFieldRate.setFont(Main.comicFont);
                satietyFieldMax.setLayoutX(580);
                satietyFieldMax.setLayoutY(300);
                satietyFieldRate.setLayoutX(880);
                satietyFieldRate.setLayoutY(300);


                TextField priceField = new TextField();
                Label price = new Label("Price:");
                price.setFont(Main.comicFont);
                priceField.setFont(Main.comicFont);
                price.setLayoutX(100);
                priceField.setLayoutX(280);
                price.setLayoutY(350);
                priceField.setLayoutY(350);

                ChoiceBox<String> typeSelector = new ChoiceBox<>();
                typeSelector.getItems().addAll("fruit");
                typeSelector.getItems().addAll("potion");
                typeSelector.getItems().addAll("food");
                typeSelector.getItems().addAll("exercise");
                typeSelector.getItems().addAll("meal");
                typeSelector.setLayoutX(100);
                typeSelector.setLayoutY(500);

                Button makeButton = new Button("Create");
                makeButton.setFont(Main.comicFont);
                makeButton.setShape(new Ellipse(100,50));
                makeButton.setLayoutX(100);
                makeButton.setLayoutY(750);

                makeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        String name = nameField.getText();
                        if (name == null || name.length() == 0)
                            return;
                        nameField.clear();
                        try {

                            AbilityChange healthChange = new AbilityChange(Main.Eisenhower.getHealth(),Integer.parseInt(healthFieldMax.getText()),Integer.parseInt(healthField.getText()),Integer.parseInt(healthFieldRate.getText()));
                            AbilityChange energyChange = new AbilityChange(Main.Eisenhower.getEnergy(),Integer.parseInt(energyFieldMax.getText()),Integer.parseInt(energyField.getText()),Integer.parseInt(energyFieldRate.getText()));
                            AbilityChange staminaChange = new AbilityChange(Main.Eisenhower.getStamina(),Integer.parseInt(staminaFieldMax.getText()),Integer.parseInt(staminaField.getText()),Integer.parseInt(staminaFieldRate.getText()));
                            AbilityChange satietyChange = new AbilityChange(Main.Eisenhower.getSatiety(),Integer.parseInt(satietyFieldMax.getText()),Integer.parseInt(satietyField.getText()),Integer.parseInt(satietyFieldRate.getText()));
                            ArrayList<AbilityChange> arrayList = new ArrayList<>();
                            arrayList.add(healthChange);
                            arrayList.add(energyChange);
                            arrayList.add(staminaChange);
                            arrayList.add(satietyChange);
                            EffectOnBody effectOnBody = new EffectOnBody(arrayList);

                            healthField.clear();
                            energyField.clear();
                            staminaField.clear();
                            satietyField.clear();
                            healthFieldMax.clear();
                            energyFieldMax.clear();
                            staminaFieldMax.clear();
                            satietyFieldMax.clear();
                            healthFieldRate.clear();
                            energyFieldRate.clear();
                            staminaFieldRate.clear();
                            satietyFieldRate.clear();

                            if (typeSelector.getValue() == "fruit") {
                                Fruit fruit = new Fruit(name, 1, Integer.parseInt(priceField.getText()), effectOnBody, "Custom Fruit");
                                Main.customFruits.add(fruit);
                                listView.getItems().addAll(name);
                            }

                            if (typeSelector.getValue() == "food") {
                                Food food = new Food(name, 1, Integer.parseInt(priceField.getText()), effectOnBody, "Custom Fruit");
                                Main.customFoods.add(food);
                                listView.getItems().addAll(name);
                            }

                            if (typeSelector.getValue() == "potion"){
                                Medicine medicine = new Medicine(name,1,Integer.parseInt(priceField.getText()), effectOnBody);
                                Main.customPotions.add(medicine);
                                listView.getItems().addAll(name);
                            }

                            if (typeSelector.getValue() == "exercise"){
                                Exercise exercise = new Exercise(name, effectOnBody, Integer.parseInt(priceField.getText()));
                                Main.customExercises.add(exercise);
                                listView.getItems().addAll(name);
                            }

                            if (typeSelector.getValue() == "meal"){
                                Meal meal = new Meal(name,1,Integer.parseInt(priceField.getText()), effectOnBody);
                                Main.customMeals.add(meal);
                                listView.getItems().addAll(name);
                            }
                            priceField.clear();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                group1.getChildren().addAll(name, nameField, health, healthField, healthFieldMax, healthFieldRate,
                        energyField, energyFieldMax, energyFieldRate, staminaField, staminaFieldMax, staminaFieldRate,
                        satietyField, satietyFieldMax, satietyFieldRate, stamina,
                        satiety,  energy,  price, priceField, makeButton, typeSelector);

            }
        });


        //Customize crops:
        Button cropButton = new Button("Crops");
        cropButton.setPrefWidth(200);
        cropButton.setFont(Main.menuFont);
        cropButton.setLayoutX(700);
        cropButton.setLayoutY(100);
        group.getChildren().addAll(cropButton);
        cropButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Group group1 = new Group();
                group1.getChildren().addAll(listView);
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);

                ChoiceBox<Fruit> fruitChoiceBox = new ChoiceBox<>();
                fruitChoiceBox.getItems().addAll(Main.customFruits);
                fruitChoiceBox.setLayoutX(280);
                fruitChoiceBox.setLayoutY(100);
                Label fruitLabel = new Label("Fruit: ");
                fruitLabel.setLayoutX(100);
                fruitLabel.setLayoutY(100);
                fruitLabel.setFont(Main.comicFont);


                TextField freqField = new TextField();
                Label freq = new Label("Harvesting times:");
                freq.setFont(Main.comicFont);
                freqField.setFont(Main.comicFont);
                freq.setLayoutX(100);
                freqField.setLayoutX(280);
                freq.setLayoutY(150);
                freqField.setLayoutY(150);

                TextField ageField = new TextField();
                Label age = new Label("Mature age:");
                age.setFont(Main.comicFont);
                ageField.setFont(Main.comicFont);
                age.setLayoutX(100);
                ageField.setLayoutX(280);
                age.setLayoutY(200);
                ageField.setLayoutY(200);

                Label season = new Label("Season:");
                season.setFont(Main.comicFont);
                ChoiceBox<String> seasonBox = new ChoiceBox<>();
                seasonBox.getItems().addAll("spring", "summer", "autumn", "winter", "tropical");
                seasonBox.setLayoutX(280);
                seasonBox.setLayoutY(250);
                season.setLayoutX(100);
                season.setLayoutY(250);


                Label sprout = new Label("Sprout Image:");
                Button sproutButton = new Button("Sprout Image");
                sprout.setFont(Main.comicFont);
                sproutButton.setFont(Main.comicFont);
                sprout.setLayoutX(100);
                sproutButton.setLayoutX(280);
                sprout.setLayoutY(300);
                sproutButton.setLayoutY(300);

                Label sproutLabel = new Label();
                Label youngLabel = new Label();
                Label matureLabel = new Label();


                sproutButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        FileChooser fileChooser = new FileChooser();
                        fileChooser.getExtensionFilters().clear();
                        fileChooser.getExtensionFilters().addAll(
                                new FileChooser.ExtensionFilter("Image Files", "*.png"));

                        try {
                            sproutFile = fileChooser.showOpenDialog(primaryStage);
                            sproutLabel.setText(sproutFile.getName());
                            sproutLabel.setFont(Main.comicFont);
                            sproutLabel.setTextFill(Color.GREEN);
                            sproutLabel.setLayoutX(430);
                            sproutLabel.setLayoutY(300);

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                Label young = new Label("Young Image:");
                Button youngButton = new Button("Young Image");
                young.setFont(Main.comicFont);
                youngButton.setFont(Main.comicFont);
                young.setLayoutX(100);
                youngButton.setLayoutX(280);
                young.setLayoutY(350);
                youngButton.setLayoutY(350);

                youngButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        FileChooser fileChooser = new FileChooser();
                        fileChooser.getExtensionFilters().clear();
                        fileChooser.getExtensionFilters().addAll(
                                new FileChooser.ExtensionFilter("Image Files", "*.png"));

                        try {
                            youngFile = fileChooser.showOpenDialog(primaryStage);
                            youngLabel.setText(youngFile.getName());
                            youngLabel.setFont(Main.comicFont);
                            youngLabel.setTextFill(Color.GREEN);
                            youngLabel.setLayoutX(430);
                            youngLabel.setLayoutY(350);

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                Label mature = new Label("Mature Image:");
                Button matureButton = new Button("Mature Image");
                mature.setFont(Main.comicFont);
                matureButton.setFont(Main.comicFont);
                mature.setLayoutX(100);
                matureButton.setLayoutX(280);
                mature.setLayoutY(400);
                matureButton.setLayoutY(400);

                matureButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        FileChooser fileChooser = new FileChooser();
                        fileChooser.getExtensionFilters().clear();
                        fileChooser.getExtensionFilters().addAll(
                                new FileChooser.ExtensionFilter("Image Files", "*.png"));

                        try {
                            matureFile = fileChooser.showOpenDialog(primaryStage);
                            matureLabel.setText(matureFile.getName());
                            matureLabel.setFont(Main.comicFont);
                            matureLabel.setTextFill(Color.GREEN);
                            matureLabel.setLayoutX(430);
                            matureLabel.setLayoutY(400);

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });


                Button makeButton = new Button("Create");
                makeButton.setFont(Main.comicFont);
                makeButton.setShape(new Ellipse(100,50));
                makeButton.setLayoutX(100);
                makeButton.setLayoutY(550);

                makeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Fruit fruit = fruitChoiceBox.getValue();
                        String name = fruit.getName();
                        try {
                            FileInputStream fileInputStream;
                            byte[] b = new byte[1000000];
                            FileOutputStream fout;

                            fileInputStream = new FileInputStream(sproutFile);
                            fout = new FileOutputStream("Images\\miscFieldTiles\\" + name + "SproutDry.png");
                            fileInputStream.read(b);
                            fout.write(b);
                            fout.close();
                            fout =  new FileOutputStream("Images\\miscFieldTiles\\" + name + "SproutWet.png");
                            fout.write(b);
                            fout.close();

                            fileInputStream = new FileInputStream(youngFile);
                            fout = new FileOutputStream("Images\\miscFieldTiles\\" + name + "YoungDry.png");
                            fileInputStream.read(b);
                            fout.write(b);
                            fout.close();
                            fout =  new FileOutputStream("Images\\miscFieldTiles\\" + name + "YoungWet.png");
                            fout.write(b);
                            fout.close();

                            fileInputStream = new FileInputStream(matureFile);
                            fout = new FileOutputStream("Images\\miscFieldTiles\\" + name + "MatureDry.png");
                            fileInputStream.read(b);
                            fout.write(b);
                            fout.close();
                            fout =  new FileOutputStream("Images\\miscFieldTiles\\" + name + "MatureWet.png");
                            fout.write(b);
                            fout.close();

                            sproutLabel.setText("");
                            youngLabel.setText("");
                            matureLabel.setText("");


                            ItemStack outcome = new ItemStack();
                            outcome.getItems().add(Producer.copyFruit(fruit));
                            Crop crop = new Crop(name + " Crop", Season.valueOf(seasonBox.getValue()),
                                    Integer.parseInt(ageField.getText()),Integer.parseInt(freqField.getText()),1, outcome);
                            Main.customCrops.add(crop);
                            ageField.clear();
                            freqField.clear();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                group1.getChildren().addAll(fruitLabel, fruitChoiceBox, freq, freqField, age, ageField,
                        season, seasonBox, sprout, sproutButton, young, youngButton, mature, matureButton,
                        sproutLabel, youngLabel, matureLabel, makeButton);
            }
        });


        //Customize trees
        Button treeButton = new Button("Trees");
        treeButton.setPrefWidth(200);
        treeButton.setFont(Main.menuFont);
        treeButton.setLayoutX(1000);
        treeButton.setLayoutY(100);
        group.getChildren().addAll(treeButton);
        treeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Group group1 = new Group();
                group1.getChildren().addAll(listView);
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);

                ChoiceBox<Fruit> fruitChoiceBox = new ChoiceBox<>();
                fruitChoiceBox.getItems().addAll(Main.customFruits);
                fruitChoiceBox.setLayoutX(280);
                fruitChoiceBox.setLayoutY(150);
                Label fruitLabel = new Label("Fruit: ");
                fruitLabel.setLayoutX(100);
                fruitLabel.setLayoutY(150);
                fruitLabel.setFont(Main.comicFont);


                Label season = new Label("Season:");
                season.setFont(Main.comicFont);
                ChoiceBox<String> seasonBox = new ChoiceBox<>();
                seasonBox.getItems().addAll("spring", "summer", "autumn", "winter", "tropical");
                seasonBox.setLayoutX(280);
                seasonBox.setLayoutY(200);
                season.setLayoutX(100);
                season.setLayoutY(200);

                TextField ageField = new TextField();
                Label age = new Label("Mature age:");
                age.setFont(Main.comicFont);
                ageField.setFont(Main.comicFont);
                age.setLayoutX(100);
                ageField.setLayoutX(280);
                age.setLayoutY(250);
                ageField.setLayoutY(250);

                TextField priceField = new TextField();
                Label price = new Label("Price:");
                price.setFont(Main.comicFont);
                priceField.setFont(Main.comicFont);
                price.setLayoutX(100);
                priceField.setLayoutX(280);
                price.setLayoutY(300);
                priceField.setLayoutY(300);


                Label youngLabel = new Label();
                Label matureLabel = new Label();

                Label mature = new Label("Mature Image:");
                Button matureButton = new Button("Mature Image");
                mature.setFont(Main.comicFont);
                matureButton.setFont(Main.comicFont);
                mature.setLayoutX(100);
                matureButton.setLayoutX(280);
                mature.setLayoutY(350);
                matureButton.setLayoutY(350);

                matureButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        FileChooser fileChooser = new FileChooser();
                        fileChooser.getExtensionFilters().clear();
                        fileChooser.getExtensionFilters().addAll(
                                new FileChooser.ExtensionFilter("Image Files", "*.png"));

                        try {
                            matureFile = fileChooser.showOpenDialog(primaryStage);
                            matureLabel.setText(matureFile.getName());
                            matureLabel.setFont(Main.comicFont);
                            matureLabel.setTextFill(Color.GREEN);
                            matureLabel.setLayoutX(430);
                            matureLabel.setLayoutY(350);

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });


                Button makeButton = new Button("Create");
                makeButton.setFont(Main.comicFont);
                makeButton.setShape(new Ellipse(100,50));
                makeButton.setLayoutX(100);
                makeButton.setLayoutY(550);

                makeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Fruit fruit = fruitChoiceBox.getValue();
                        String name = fruit.getName();
                        try {
                            FileInputStream fileInputStream;
                            byte[] b = new byte[1000000];
                            FileOutputStream fout;

                            fileInputStream = new FileInputStream(matureFile);
                            fout = new FileOutputStream("Images\\trees\\" + name + ".png");
                            fileInputStream.read(b);
                            fout.write(b);
                            fout.close();

                            youngLabel.setText("");
                            matureLabel.setText("");


                            ItemStack outcome = new ItemStack();
                            outcome.getItems().add(Producer.copyFruit(fruit));
                            System.out.println(seasonBox.getValue() + " " + ageField.getText());
                            Tree tree = new Tree(name, Season.valueOf(seasonBox.getValue()),Integer.parseInt(ageField.getText()),1,fruit,Integer.parseInt(priceField.getText()),true);
                            Main.customTrees.add(tree);
                            ageField.clear();
                            priceField.clear();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                group1.getChildren().addAll(fruitLabel, fruitChoiceBox, age, ageField,
                        price, priceField,
                        season, seasonBox, mature, matureButton,
                        youngLabel, matureLabel, makeButton);
            }
        });


        //Customize tools and resources
        Button customItemButton = new Button("Tool/Resource");
        customItemButton.setPrefWidth(200);
        customItemButton.setPrefHeight(100);
        customItemButton.setFont(new Font(25));
        customItemButton.setLayoutX(100);
        customItemButton.setLayoutY(250);
        group.getChildren().addAll(customItemButton);
        customItemButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                Group group1 = new Group();
                group1.getChildren().addAll(listView);
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);

                TextField nameField = new TextField();
                Label name = new Label("Name");
                name.setFont(Main.comicFont);
                nameField.setFont(Main.comicFont);
                name.setLayoutX(100);
                nameField.setLayoutX(380);
                name.setLayoutY(100);
                nameField.setLayoutY(100);

                TextField priceField = new TextField();
                Label price = new Label("Price:");
                price.setFont(Main.comicFont);
                priceField.setFont(Main.comicFont);
                price.setLayoutX(100);
                priceField.setLayoutX(380);
                price.setLayoutY(150);
                priceField.setLayoutY(150);

                TextField healthField = new TextField();
                Label health = new Label("Health(for tool only):");
                health.setFont(Main.comicFont);
                healthField.setFont(Main.comicFont);
                health.setLayoutX(100);
                healthField.setLayoutX(380);
                health.setLayoutY(250);
                healthField.setLayoutY(250);

                TextField probField = new TextField();
                Label prob = new Label("Breaking prob(for tool only):");
                prob.setFont(Main.comicFont);
                probField.setFont(Main.comicFont);
                prob.setLayoutX(100);
                probField.setLayoutX(380);
                prob.setLayoutY(200);
                probField.setLayoutY(200);

                ChoiceBox<String> typeSelector = new ChoiceBox<>();
                typeSelector.getItems().addAll("Kitchen Utensil");
                typeSelector.getItems().addAll("Resource");

                typeSelector.setLayoutX(100);
                typeSelector.setLayoutY(500);

                Button makeButton = new Button("Create");
                makeButton.setFont(Main.comicFont);
                makeButton.setShape(new Ellipse(100,50));
                makeButton.setLayoutX(100);
                makeButton.setLayoutY(750);

                makeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        String name = nameField.getText();
                        if (name == null || name.length() == 0)
                            return;
                        nameField.clear();
                        try {
                            if (typeSelector.getValue() == "Kitchen Utensil") {
                                KitchenTool kitchenTool = new KitchenTool(name, 1,Integer.parseInt(healthField.getText()),Integer.parseInt(probField.getText()));
                                Main.customKitchenTools.add(kitchenTool);
                                listView.getItems().addAll(name);
                                probField.clear();
                                healthField.clear();

                            }
                            if (typeSelector.getValue() == "Resource"){
                                Resource resource = new Resource(name,1,Integer.parseInt(priceField.getText()));
                                Main.customResources.add(resource);
                                listView.getItems().addAll(name);
                            }
                            priceField.clear();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                group1.getChildren().addAll(typeSelector, makeButton, price, priceField, name, nameField, health, healthField, prob, probField);
            }
        });

        //Customize machines
        Button customMachineButton = new Button("Machine");
        customMachineButton.setPrefWidth(200);
        customMachineButton.setPrefHeight(100);
        customMachineButton.setFont(new Font(25));
        customMachineButton.setLayoutX(400);
        customMachineButton.setLayoutY(250);
        group.getChildren().addAll(customMachineButton);
        customMachineButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ArrayList<Item> allItems = Producer.allItems();
                allItems.addAll(Main.customFoods);
                allItems.addAll(Main.customFruits);
                allItems.addAll(Main.customMeals);
                Group group1 = new Group();
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);

                TextField nameField = new TextField();
                Label name = new Label("Name");
                name.setFont(Main.comicFont);
                nameField.setFont(Main.comicFont);
                name.setLayoutX(100);
                nameField.setLayoutX(280);
                name.setLayoutY(450);
                nameField.setLayoutY(450);

                TextField priceField = new TextField();
                Label price = new Label("Price");
                price.setFont(Main.comicFont);
                priceField.setFont(Main.comicFont);
                price.setLayoutX(100);
                priceField.setLayoutX(280);
                price.setLayoutY(500);
                priceField.setLayoutY(500);

                ChoiceBox<Item> choiceBox = new ChoiceBox<>();
                choiceBox.getItems().addAll(allItems);
                choiceBox.setLayoutX(280);
                choiceBox.setLayoutY(100);
                Label itemLabel = new Label("Item: ");
                itemLabel.setLayoutX(100);
                itemLabel.setLayoutY(100);
                itemLabel.setFont(Main.comicFont);

                Button inputButton = new Button("Input");
                inputButton.setFont(Main.comicFont);
                inputButton.setShape(new Ellipse(100,50));
                inputButton.setLayoutX(600);
                inputButton.setLayoutY(225);

                Button outputButton = new Button("Output");
                outputButton.setFont(Main.comicFont);
                outputButton.setShape(new Ellipse(100,50));
                outputButton.setLayoutX(600);
                outputButton.setLayoutY(525);

                ArrayList<ItemStack> input = new ArrayList<>();
                ArrayList<ItemStack> output = new ArrayList<>();

                ListView<String> inputList = new ListView<>();
                ListView<String> outputList = new ListView<>();

                inputList.setLayoutX(700);
                outputList.setLayoutX(700);
                inputList.setLayoutY(100);
                outputList.setLayoutY(400);
                inputList.setPrefHeight(250);
                outputList.setPrefHeight(250);

                TextField numField = new TextField();
                Label num = new Label("Number");
                num.setFont(Main.comicFont);
                numField.setFont(Main.comicFont);
                num.setLayoutX(100);
                numField.setLayoutX(280);
                num.setLayoutY(150);
                numField.setLayoutY(150);

                inputButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int num = Integer.parseInt(numField.getText());
                        Item item = choiceBox.getValue();
                        ItemStack itemStack = null;
                        if (item instanceof Fruit) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyFruit((Fruit) item));
                        }
                        else if (item instanceof Food) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyFood((Food) item));
                        }
                        else if (item instanceof Resource) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyResource((Resource) item));
                        }
                        else if (item instanceof Meal) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyMeal((Meal) item));
                        }
                        input.add(itemStack);
                        String s = itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size();
                        System.out.println(s);
                        inputList.getItems().addAll(s);
                    }
                });

                outputButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int num = Integer.parseInt(numField.getText());
                        Item item = choiceBox.getValue();
                        ItemStack itemStack = null;
                        if (item instanceof Fruit) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyFruit((Fruit) item));
                        }
                        else if (item instanceof Food) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyFood((Food) item));
                        }
                        else if (item instanceof Resource) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyResource((Resource) item));
                        }
                        else if (item instanceof Meal) {
                            itemStack = new ItemStack();
                            for (int i = 0; i < num; i++)
                                itemStack.getItems().add(Producer.copyMeal((Meal) item));
                        }
                        output.add(itemStack);
                        String s = itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size();
                        System.out.println(s);
                        outputList.getItems().addAll(s);
                    }
                });

                Button makeButton = new Button("Create");
                makeButton.setFont(Main.comicFont);
                makeButton.setShape(new Ellipse(100,50));
                makeButton.setLayoutX(200);
                makeButton.setLayoutY(650);

                makeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
//                        System.out.println(input);
//                        System.out.println(output);
                        ArrayList<ItemStack> tmpIn = new ArrayList<>();
                        ArrayList<ItemStack> tmpOut = new ArrayList<>();

                        for (ItemStack itemStack : input)
                            tmpIn.add(itemStack);
                        for (ItemStack itemStack : output)
                            tmpOut.add(itemStack);

                        Machine machine = new Machine(nameField.getText(), 1, tmpIn, tmpOut, Integer.parseInt(priceField.getText()), nameField.getText());
                        //System.out.println(machine.getInputItems());
                        Main.customMachines.add(machine);
                        priceField.clear();
                        nameField.clear();
                        input.clear();
                        output.clear();
                        inputList.getItems().clear();
                        outputList.getItems().clear();
                    }
                });
                group1.getChildren().addAll(name, nameField, choiceBox, itemLabel, inputList, inputButton,
                        outputButton, outputList, num, numField, price, priceField, makeButton);
            }
        });


        //Customize gameItems
        Button chooseGameItems = new Button("Select customs");
        chooseGameItems.setPrefWidth(200);
        chooseGameItems.setPrefHeight(100);
        chooseGameItems.setFont(new Font(25));
        chooseGameItems.setLayoutX(700);
        chooseGameItems.setLayoutY(250);
        group.getChildren().addAll(chooseGameItems);
        chooseGameItems.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Group group1 = new Group();
                scene.setRoot(group1);
                group1.getChildren().addAll(backButton);

                int num = 0;
                for (Fruit obj : Main.customFruits){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedFruits.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                    Main.addedFruits.add(obj);
                            } else {
                                Main.addedFruits.remove(obj);
                            }
                        }
                    });
                }

                for (Food obj : Main.customFoods){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedFoods.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedFoods.add(obj);
                            } else {
                                Main.addedFoods.remove(obj);
                            }
                        }
                    });
                }
                for (Crop obj : Main.customCrops){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedCrops.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedCrops.add(obj);
                            } else {
                                Main.addedCrops.remove(obj);
                            }
                        }
                    });
                }

                for (Tree obj : Main.customTrees){
                    CheckBox checkBox = new CheckBox(obj.getName() + " Tree");
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedTrees.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedTrees.add(obj);
                            } else {
                                Main.addedTrees.remove(obj);
                            }
                        }
                    });
                }
                for (Resource obj : Main.customResources){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedResources.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedResources.add(obj);
                            } else {
                                Main.addedResources.remove(obj);
                            }
                        }
                    });
                }
                for (Meal obj : Main.customMeals){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedMeals.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedMeals.add(obj);
                            } else {
                                Main.addedMeals.remove(obj);
                            }
                        }
                    });
                }

                for (Machine obj : Main.customMachines){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedMachines.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedMachines.add(obj);
                            } else {
                                Main.addedMachines.remove(obj);
                            }
                        }
                    });
                }

                for (Medicine obj : Main.customPotions){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedPotions.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedPotions.add(obj);
                            } else {
                                Main.addedPotions.remove(obj);
                            }
                        }
                    });
                }

                for (Exercise obj : Main.customExercises){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedExercises.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedExercises.add(obj);
                            } else {
                                Main.addedExercises.remove(obj);
                            }
                        }
                    });
                }

                for (KitchenTool obj : Main.customKitchenTools){
                    CheckBox checkBox = new CheckBox(obj.getName());
                    checkBox.setLayoutX(50 + (num/15) * 100);
                    checkBox.setLayoutY(50 + (num%15) * 50);
                    num++;
                    group1.getChildren().addAll(checkBox);
                    if (Main.addedKitchenTools.contains(obj))
                        checkBox.setSelected(true);
                    checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                        public void changed(ObservableValue<? extends Boolean> ov,
                                            Boolean old_val, Boolean new_val) {
                            if (new_val){
                                Main.addedKitchenTools.add(obj);
                            } else {
                                Main.addedKitchenTools.remove(obj);
                            }
                        }
                    });
                }

                Button OKButton = new Button("OK");
                OKButton.setFont(Main.comicFont);
                OKButton.setShape(new Ellipse(100,50));
                OKButton.setLayoutX(200);
                OKButton.setLayoutY(650);

            }
        });
    }
}
