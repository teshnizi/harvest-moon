
import com.sun.deploy.net.proxy.ProxyUnavailableException;

import java.util.*;

/**
 * 
 */
public class Clinic extends Store {

    /**
     * Default constructor
     */
    public Clinic(String name) {
        super(name);
    }

    @Override
    public void nextDay(Season season){
        storage.getItemStacks().clear();
        for(int i = 0 ; i < 10 ; i ++ ){
            storage.putInItem(Producer.medicine(1));
            storage.putInItem(Producer.medicine(2));
            storage.putInItem(Producer.medicine(3));
            for (Medicine medicine : Main.addedPotions){
                storage.putInItem(Producer.copyMedicine(medicine));
            }
        }
    }

}