import java.io.Serializable;

/**
 * 
 */
public enum Season  implements Serializable {
    spring,
    summer,
    autumn,
    winter,
    tropical
}