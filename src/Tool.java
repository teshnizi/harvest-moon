import java.util.ArrayList;

/**
 * 
 */
public class Tool extends Item {

    /**
     * Default constructor
     */
     int type;
     int newprice;
     int repairprice;
     ArrayList<Builder> newbuilders;
     ArrayList<Builder> repairbuilders;
    public Tool(String name, int size, int health, int breakingProbability, AbilityChange abilityChange) {
        super(name, size);
        this.health = health;
        this.abilityChange = abilityChange;
        this.breakingProbablity = breakingProbability;
    }
    public ArrayList<String > madeof(){
        ArrayList<String >s =new ArrayList<>();
        s.add("s");
        return s;
    }

    int health;

    int breakingProbablity;

    public int getHealth() {
        return health;
    }

    private int dissembleCost;

    private int power;

    AbilityChange abilityChange;

    //checks if person has enough energy for using item
    public boolean canUseTool(){
        if (getHealth() <= 0)
            return false;
        return abilityChange.canAffect();
    }

    /**
     * @return
     */
    public int repairCost() {
        // TODO implement here
        return 0;
    }

}