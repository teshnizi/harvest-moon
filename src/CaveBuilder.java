import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by tigerous215 on 7/13/2017.
 */
public class CaveBuilder extends Application {

    static Group caveGroup = new Group();
    static Scene scene;
    static boolean incave = false;
    static ArrayList<Pair<Pair<String,ImageView>,Pair<Integer,Integer>>> chars = new ArrayList<>();
    static ArrayList<Rectangle> rectangles = new ArrayList<>();
    static HashSet<Pair<Integer, Integer>> caveAvailableTiles = new HashSet<>();

    static {
        for (int i = 1; i < 39; i++) {
            for (int j = 1; j < 29; j++) {
                caveAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for (int i = 17; i < 20; i++) {
            for (int j = 29; j < 31; j++) {
                caveAvailableTiles.add(new Pair<>(i, j));
            }
        }
    }

    int numinimage = 0;
    @Override
    public void start(Stage primaryStage) throws Exception {
        scene = new Scene(caveGroup, 1280, 960);
        Image backGround = new Image("maps\\cave.png");
        Image branch = new Image("images\\branch.png");
        Image branch1 = new Image("images\\Cut Tree.png");
        Image branch2 = new Image("images\\Cut Tree1.png");
        Image branch3 = new Image("images\\Cut Tree2.png");
        scene.setFill(new ImagePattern(backGround));
        Button spacebutton = new Button("Inspect");
        spacebutton.setLayoutX(Main.character.getX()-10);
        spacebutton.setLayoutY(Main.character.getY() -25);
        spacebutton.setVisible(false);
        primaryStage.setScene(scene);
        primaryStage.show();
        caveGroup.getChildren().addAll(Main.character,spacebutton);
        int num1 = 0;
        for (Pair<Storage,Pair<Integer,Integer>> p : Cave.res){
            num1 ++;
            Rectangle rectangle = new Rectangle(32,48);
            Pair<Integer,Integer> x = MovementHandler.tileToPixel(p.getValue().getKey(),p.getValue().getValue());
            rectangle.setX(x.getKey());
            rectangle.setY(x.getValue());
            rectangles.add(rectangle);
            System.out.println(num1 + " " +p.getValue().getKey() + " " + p.getValue().getValue() );
            caveGroup.getChildren().add(rectangle);
            if (p.getKey().getItemStacks().get(0).getItems().get(0) instanceof Stone)
            {
                Stone wood = (Stone) p.getKey().getItemStacks().get(0).getItems().get(0);
                if (wood.getType() == 0)
                    rectangle.setFill(new ImagePattern(branch));
                if (wood.getType() == 1)
                    rectangle.setFill(new ImagePattern(branch1));
                if (wood.getType() == 2)
                    rectangle.setFill(new ImagePattern(branch2));
                if (wood.getType() == 3)
                    rectangle.setFill(new ImagePattern(branch3));
            }
        }
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage % 4 + 1;
                Pair<Integer, Integer> next;
                DataOutputStream dout = null;
                try {
                    if (Main.online)
                        dout = new DataOutputStream(Getter.socket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (Main.Eisenhower.canMove) {
                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png"));
                        MovementHandler.move(2, caveAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                                dout.writeUTF(mystring("Images\\left" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png"));
                        MovementHandler.move(0, caveAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                                dout.writeUTF(mystring("Images\\right" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, caveAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                                dout.writeUTF(mystring("Images\\b" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png "));
                        MovementHandler.move(3, caveAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                                dout.writeUTF(mystring("Images\\f" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
                if (event.getCode() == KeyCode.B) {
                    //backPackListView = Main.Eisenhower.getBackPack().viewList();
                    //farmGroup.getChildren().addAll(backPackListView);
                    PersonMenu personMenu = new PersonMenu(scene, primaryStage);
                    try {
                        Stage mystage = new Stage();
                        mystage.setX(40);
                        personMenu.start(mystage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (event.getCode() == KeyCode.E){
                    incave = false;
                    ForestBuilder.inforest = true;
                    ForestBuilder forestBuilder = new ForestBuilder(Main.forest);
                    try {
                        forestBuilder.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.ESCAPE){
                    Main.Eisenhower.canMove = false;
                    Main.timeline.pause();
                    PauseMenue pauseMenue = new PauseMenue(scene);
                    try {
                        pauseMenue.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.I) {
                    if (isis()){
                        String h = isisstring();
                        Inspectbuilder inspectbuilder = new Inspectbuilder(h);
                        Stage stage = new Stage();
                        try {
                            inspectbuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Storage mystorag = null;
                    int x1 = 0;
                    int y1 = 0;
                    for (Pair<Storage, Pair<Integer, Integer>> p : Cave.res) {
                        if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1, p.getValue().getValue()))) {
                            mystorag = p.getKey();
                            x1 = p.getValue().getKey();
                            y1 = p.getValue().getValue();
                        }
                    }
                    if (mystorag != null && mystorag.getItemStacks().get(0).getItems().size() > 0) {
                        ResourceinCave resourceInForestBuilder = new ResourceinCave(mystorag, x1, y1);
                        Stage stage = new Stage();
                        try {
                            resourceInForestBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (Main.character.getY() > 29 * 32){
                    Main.character.setY(21 * 32 -14);
                    Main.character.setX(34 * 32);
                    try {
                        Main.forestBuilder.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                boolean flag = false;
                System.out.println(Cave.res.size() + " sda");
                for (Pair<Storage,Pair<Integer,Integer>> p : Cave.res){
                    if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() +1,p.getValue().getValue()))){
                        spacebutton.setPrefWidth(80);
                        spacebutton.setLayoutX(Main.character.getX() -24);
                        spacebutton.setLayoutY(Main.character.getY() -30);
                        spacebutton.setVisible(true);
                        flag = true;
                    }
                }
                 if(isis() && !flag){
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                    flag = true;
                }

                if (!flag)
                    spacebutton.setVisible(false);
            }
        });
    }
    static public void repair(){
        ArrayList<Pair<Storage,Pair<Integer,Integer>>> pairs = new ArrayList<>();
        for (Pair<Storage,Pair<Integer,Integer>> p : Cave.res){
            if (p.getKey().getItemStacks().get(0).getItems().size() == 0){
                caveAvailableTiles.add(p.getValue());
                pairs.add(p);
                for (Rectangle rectangle : rectangles){
                    if (p.getValue().equals(MovementHandler.pixelToTile(rectangle.getLayoutX(),rectangle.getLayoutY()))){
                        caveGroup.getChildren().remove(rectangle);
                        break;
                    }
                }
            }
        }
        for (Pair<Storage,Pair<Integer,Integer>> p : pairs)
            Cave.res.remove(p);
    }
    public String mystring(String st){
        String ans;
        ans = Getter.socketname + Protocol.protocol + Protocol.character + Protocol.protocol +Protocol.cave +Protocol.protocol+st + Protocol.protocol +MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getKey() +Protocol.protocol  + MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getValue();
        return ans;
    }
    public boolean isis(){
        int i = 0;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                return true;
            }
        }
        return false;
    }
    public String isisstring(){
        String h = null;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                h= p.getKey().getKey();
            }
        }
        return h;
    }
}
