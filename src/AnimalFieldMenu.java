import java.util.ArrayList;

/**
 * Created by Tigerous215 on 5/15/2017.
 */
public class AnimalFieldMenu extends Menu{

    AnimalField animalField;

    public AnimalFieldMenu(AnimalField animalField){
        this.animalField = animalField;
    }
    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in AnimalField: " + animalField.getName());
        s.add("Maximum number of animals: " + animalField.maximumSize);
        s.add("0.Expand this animalField");
        s.add("List of animals:");
        int ID = 0;
        for(Animal animal : animalField.getAnimals()){
            s.add((++ID) + "." + animal.getName());
        }
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if(x == 0){
            System.out.println("Materials needed for expanding field by 1 block:");
            System.out.println("Stone x10, Old Lumber x5, $200");
            if(person.getMoney() < 200){
                System.out.println("Not enough money!");
                return null;
            }
            System.out.println("Choose Stone x5:");
            ItemStack itemStack = person.getBackPack().getItemStackFromPlayer();
            if (!((itemStack.getItems().get(0) instanceof Stone))){
                System.out.println("You should choose Stone!");
                return null;
            }
            if(! (((Stone)itemStack.getItems().get(0)).getType() == 0)){
                System.out.println("You should choose Stone!");
                return null;
            }
            if(itemStack.getItems().size() < 10){
                System.out.println("Not enough stones!");
                return null;
            }
            System.out.println("Choose Old Lumber x5:");
            ItemStack itemStack2 = person.getBackPack().getItemStackFromPlayer();
            if (!((itemStack2.getItems().get(0) instanceof Wood))){
                System.out.println("You should choose Old Lumber!");
                return null;
            }
            if(! (((Wood)itemStack2.getItems().get(0)).getType() == 1)){
                System.out.println("You should choose Old Lumber!");
                return null;
            }
            if(itemStack.getItems().size() < 5){
                System.out.println("Not enough old lumbers!");
                return null;
            }

            person.takeMoney(200);
            person.getBackPack().takeItem("Old Lumber",5);
            person.getBackPack().takeItem("Stone",10);
            animalField.maximumSize++;
            System.out.println("Field expanded!");
            return null;
        }
        try{
            Animal animal = animalField.getAnimals().get(x-1);
            return new AnimalMenu(animal);
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
