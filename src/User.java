import com.sun.org.apache.xpath.internal.operations.String;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;

/**
 * Created by pc on 7/13/2017.
 */
public class User extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,1280,960);
        scene.setFill(Color.AZURE);
        TextField user = new TextField();
        user.setText("username");
        user.setStyle("-fx-text-fill: gray");
        user.setLayoutX(550);
        user.setLayoutY(200);
        ServerBuild serverBuild = new ServerBuild();
        serverBuild.start();

        user.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (user.getText().equals("username")||user.getText() == null) {
                    user.clear();
                    user.setStyle("-fx-text-fill: chocolate");
                    user.setStyle("-fx-border-color: darkgreen");
                }
            }
        });
        group.getChildren().add(user);

        final ChoiceBox fontChBox =
                new ChoiceBox<>(FXCollections.observableArrayList("Host", "User"));
        fontChBox.setLayoutX(590);
        fontChBox.setLayoutY(250);

        TextField port = new TextField();
        port.setText("port");
        port.setStyle("-fx-text-fill: gray");
        port.setLayoutX(550);
        port.setLayoutY(300);
        port.setVisible(false);
        port.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                port.setStyle("-fx-text-fill: black");
            }
        });

        Button bb = new Button("Main Menu");
        bb.setLayoutX(10);
        bb.setLayoutY(10);
        bb.setFont(Main.comicFont);
        bb.setShape(new Ellipse(100,50));
        group.getChildren().addAll(bb);
        bb.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Main.mainstage.setScene(Main.mainMenuScene);
            }
        });

        Button singnup = new Button("SignUp");
        singnup.setLayoutX(600);
        singnup.setLayoutY(400);
        singnup.setStyle("-fx-background-color: white");

        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if ( fontChBox.getValue() != null && fontChBox.getValue().equals("Host"))
                {
                    port.setVisible(true);
                }
                if ( fontChBox.getValue() != null && fontChBox.getValue().equals("User"))
                {
                    port.setVisible(false);
                }
                singnup.setStyle("-fx-text-fill:black");
                singnup.setStyle("-fx-background-color:white");
            }
        });
        singnup.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                singnup.setStyle("-fx-text-fill: darkgreen");
                singnup.setStyle("-fx-background-color: chartreuse");
            }
        });
        singnup.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (fontChBox.getValue() != null)
                {
                    if (fontChBox.getValue().equals("Host")){


                        File file = new File("server.txt");
                        FileWriter fileWriter = null;
                        try {
                            fileWriter = new FileWriter(file,true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        BufferedWriter bw = new BufferedWriter(fileWriter);
                        if (port.getText().length()  == 4)
                        {
                            int n = Integer.parseInt(port.getText());
                            try {
                                bw.write(user.getText() + " " + port.getText());
                                bw.newLine();
                                bw.close();
                                Getter.ishost = true;
                                Doing doing = new Doing(n,user.getText());
                                doing.start();
                                Socket socket = new Socket("localhost",n);

                                Main.online = true;
                                Getter getter = new Getter(socket,user.getText());
                                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                                DataInputStream din = new DataInputStream(socket.getInputStream());
                                dout.writeUTF(user.getText());

                                //System.out.println("alikkk");
                                Main.curent = true;
                                //System.out.println(Main.Eisenhower.age+ " u");
                                Main.forest.nextDay(Main.Eisenhower.season);
                                //System.out.println("dasdad");
                                UserBuilder userBuilder = new UserBuilder(user.getText(),0,n);
                                userBuilder.start(primaryStage);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    else
                    {
                        UserBuilder userBuilder = new UserBuilder(user.getText(),1,00);
                        try {
                            userBuilder.start(primaryStage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        group.getChildren().addAll(fontChBox,port,singnup);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
class Doing extends Thread{
    java.lang.String namess;
    int n;
    public Doing(int n , java.lang.String s){
        this.n = n;
        namess = s;
    }
    @Override
    public void run(){
        try {
            Server server = new Server(n,namess);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
class ServerBuild extends Thread{
    java.lang.String name;
    @Override
    public void run(){
        try {
            Server server = new Server(Main.serverport,"main");
            Socket socket1 = new Socket("localhost",Main.serverport);
            DataOutputStream dout1 = new DataOutputStream(socket1.getOutputStream());
            dout1.writeUTF("main");
        } catch (Exception e) {
            e.printStackTrace();
            this.stop();
        }
    }
}

