
import java.util.*;

/**
 * 
 */
public class Medicine extends Food {

    /**
     * Default constructor
     */
    public Medicine(String name, int size, int price, EffectOnBody effectOnBody){
        super(name, size, price, effectOnBody);
    }

    @Override
    public String status(){
        return getName() + "\nHealing: " + effect.getAbilityChanges().get(0).getCurrentValueChange() + "\nPrice: " + getPrice();
    }

}