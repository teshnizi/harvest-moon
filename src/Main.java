
import java.io.*;
import java.net.Socket;
import java.sql.Time;
import java.util.*;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Material;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.transform.*;

import javafx.animation.AnimationTimer;
import javafx.util.Duration;
import javafx.util.Pair;


public class Main extends Application {
    static Stage mainstage;
    static int numinimage = 1;
    static int serverport = 1130;
    static boolean curent = true;
    static  Timeline timeline;
    static boolean online = false;
    static Font comicFont = new Font("Comic Sans MS",15);
    static Farm mainFarm = new Farm("MainFarm");
    static Person Eisenhower = new Person("EisenHower", 10000, mainFarm, Season.spring);
    static ArrayList<Thing> gameThings = new ArrayList<>();
    static Barn barn = new Barn("Barn");
    static Font menuFont = new Font("Cambria",48);
    static int dayTime = 0;
    static Forest forest = new Forest("Forest", null, null);

    static {
        barn.getNeighbours().add(mainFarm);
        gameThings.add(barn);

        Eisenhower.getAbilities().add(new Ability("Health", 500, 450, 5));
        Eisenhower.getAbilities().add(new Ability("Energy", 1000, 900, 100));
        Eisenhower.getAbilities().add(new Ability("Stamina", 100, 90, 100));
        Eisenhower.getAbilities().add(new Ability("Satiety", 100, 90, -1));
        Producer.person = Eisenhower;
    }
    static Market market = new Market("Market");
    static Village village = new Village("Village", barn);
    static Home home = new Home("Home");

    public static void main(String[] args) throws IOException {

        System.out.println(javafx.scene.text.Font.getFamilies());


        AnimalField cowField = new AnimalField("CowField");
        AnimalField sheepField = new AnimalField("SheepField");
        AnimalField chickenField = new AnimalField("ChickenField");


        Eisenhower.getBackPack().putInItem(Producer.seedOf(Producer.garlicCrop()));
        Eisenhower.getBackPack().putInItem(Producer.seedOf(Producer.garlicCrop()));
        Eisenhower.getBackPack().putInItem(Producer.seedOf(Producer.garlicCrop()));
        Eisenhower.getBackPack().putInItem(Producer.alfalfa());
        Eisenhower.getBackPack().putInItem(Producer.alfalfa());
        Eisenhower.getBackPack().putInItem(Producer.alfalfa());
        Eisenhower.getBackPack().putInItem(Producer.alfalfa());
        Eisenhower.getBackPack().putInItem(Producer.animalMedicine());
        Eisenhower.getBackPack().putInItem(Producer.grain());
        Eisenhower.getBackPack().putInItem(Producer.scissors());

        /*for (int i = 0 ;i < 6; i++){
            cowField.getAnimals().add(null);
        }
        for (int i = 0 ;i < 6; i++){
            sheepField.getAnimals().add(null);
        }
        for (int i = 0 ;i < 6; i++){
            chickenField.getAnimals().add(null);
        }*/

        sheepField.getAnimals().add(Producer.sheep("Shangool"));
        sheepField.getAnimals().add(Producer.sheep("Mangool"));
        sheepField.getAnimals().add(Producer.sheep("Habbe"));
        chickenField.getAnimals().add(Producer.chicken("morghy"));

        Getter.ishost = true;

        barn.getAnimalFields().add(cowField);
        barn.getAnimalFields().add(sheepField);
        barn.getAnimalFields().add(chickenField);
        mainFarm.getPlaces().add(barn);

        village.setCityHall(new CityHall("CityHall", mainFarm));

        ArrayList<Meal> meals = new ArrayList<>();
        meals.add(Producer.saladShirazi());
        meals.add(Producer.mirzaGhasemi());
        meals.add(Producer.cheeseCake());
        meals.add(Producer.friedPotate());
        Cafe cafe = new Cafe("Cafe", meals);
        village.setCafe(cafe);

        gameThings.add(market);
        market.getStores().add(new Store("Groceries Store"));
        market.getStores().add(new Store("General Store"));
        market.getStores().add(new Butchery("Butchery", barn));
        village.getNeighbours().add(market);
        village.getNeighbours().add(mainFarm);
        mainFarm.getNeighbours().add(village);
        market.getNeighbours().add(village);

        for (int i = 0 ; i < 16; i ++)
            mainFarm.addField("field", 9);

        for (int i = 0 ; i < 6 ; i++)
            mainFarm.addFruitGarden("fruit garden");
        ArrayList<Storage> storages = new ArrayList<>();
        for (int i  =0 ;i <  5 ;i++){
            Storage storage = new Storage(10);
            for (int j = 0 ; j < 10 ; j++){
                storage.putInItem(Producer.wood(0));
            }
            storages.add(storage);
        }
        for (int i = 1 ;i < 4;i++ ){
            for (int j = 0 ; j < 3; j ++)
            {
                Storage storage = new Storage(10);
                for (int k  = 0 ; k < 10 ;  k++)
                    storage.putInItem(Producer.wood(i));
                storages.add(storage);
            }
        }
        for (int i  = 0 ; i < 5 ; i ++)
        {
            Storage storage = new Storage(10);
            for (int j = 0 ; j < 10 ; j++)
                storage.putInItem(Producer.stone(0));
            storages.add(storage);
        }
        forest.setStorages(storages);
        forest.res = Forest.placew(storages);


        WoodType woods = new WoodType("Woods");
        ArrayList<Item> items = new ArrayList<>();
        ArrayList<Item> items1 = new ArrayList<>();
        ArrayList<Item> items2 = new ArrayList<>();
        ArrayList<Item> items3 = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            items.add(Producer.wood(0));
            items1.add(Producer.wood(1));
            items2.add(Producer.wood(2));
            items3.add(Producer.wood(3));
        }
        ArrayList<ItemStack> woodstack = new ArrayList<>();
        woodstack.add(new ItemStack(items));
        woodstack.add(new ItemStack(items1));
        woodstack.add(new ItemStack(items2));
        woodstack.add(new ItemStack(items3));
        woods.setWoods(woodstack);
        Eisenhower.getBackPack().putInItemStack(new ItemStack(Producer.axe(1)), 1);
        StoneType rocks = new StoneType("rocks", true);
        ArrayList<Item> rockitems = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            rockitems.add(Producer.stone(0));
        }
        Eisenhower.getBackPack().putInItemStack(new ItemStack(rockitems), 10);
        ArrayList<ItemStack> rockstacks = new ArrayList<>();
        rockstacks.add(new ItemStack(rockitems));
        rocks.setRocks(rockstacks);
        //Forest forest = new Forest("Forest", woods, rocks);
        forest.setRockTypes(rocks);
        forest.setWoodTypes(woods);
        mainFarm.getNeighbours().add(forest);
        forest.getNeighbours().add(mainFarm);
        StoneType rockType = new StoneType("rocks", false);
        ArrayList<Item> rockTypes = new ArrayList<>();
        ArrayList<Item> rockTypes1 = new ArrayList<>();
        ArrayList<Item> rockTypes2 = new ArrayList<>();
        ArrayList<Item> rockTypes3 = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            rockTypes.add(Producer.stone(1));
            rockTypes1.add(Producer.stone(2));
            rockTypes2.add(Producer.stone(3));
            rockTypes3.add(Producer.stone(4));
        }
        rockstacks.clear();
        rockstacks.add(new ItemStack(rockTypes));
        rockstacks.add(new ItemStack(rockTypes1));
        rockstacks.add(new ItemStack(rockTypes2));
        rockstacks.add(new ItemStack(rockTypes3));
        rockType.setRocks(rockstacks);
        Cave cave = new Cave("Cave", rockType);
        forest.setRiver(new River("river"));
        forest.getNeighbours().add(cave);
        cave.getNeighbours().add(forest);
        GreenHouse greenHouse = new GreenHouse("GreenHouse");
        mainFarm.setGreenHouse(greenHouse);

        home.getNeighbours().add(mainFarm);
        mainFarm.getPlaces().add(home);

        Laboratory laboratory = new Laboratory("Laboratory", barn.getMachineRoom());
        laboratory.getMachines().add(Producer.cheeseMaker());
        laboratory.getMachines().add(Producer.spinningWheel());

        village.setLaboratory(laboratory);

        Eisenhower.getBackPack().putInItem(Producer.shovel(1));
        Eisenhower.getBackPack().putInItem(Producer.wateringCan(3));
        Eisenhower.getBackPack().putInItem(Producer.fishingRod(1));
        ArrayList<AbilityChange> abilityChanges = new ArrayList<>();
        abilityChanges.add(new AbilityChange(Eisenhower.getSatiety(), 0, 5, 0));
        abilityChanges.add(new AbilityChange(Eisenhower.getEnergy(), 0, 5, 0));

        ItemStack oldWoodStack = new ItemStack();
        ItemStack ironIngotStack = new ItemStack();
        for (int i = 0; i < 25; i++) {
            oldWoodStack.getItems().add(Producer.wood(0));
            ironIngotStack.getItems().add(Producer.stone(0));
        }
        Eisenhower.getBackPack().putInItemStack(oldWoodStack, 22);
        Eisenhower.getBackPack().putInItemStack(ironIngotStack, 22);

        Kitchen kitchen = new Kitchen();
        kitchen.getRecipes().add(Producer.mirzaGhasemiRecipe());
        kitchen.getRecipes().add(Producer.cheeseCakeRecipe());
        kitchen.getRecipes().add(Producer.friedPotatoRecipe());
        kitchen.getRecipes().add(Producer.saladShiraziRecipe());

        home.setKitchen(kitchen);

        gameThings.add(mainFarm);
        gameThings.add(village);
        gameThings.add(Eisenhower);
        gameThings.add(forest);
        Eisenhower.getBackPack().putInItem(Producer.knife());
        String command;
        Scanner scanner = new Scanner(System.in);
        Stack<Menu> menus = new Stack<>();

        for (Thing thing : gameThings) {
            thing.nextDay(Eisenhower.season);
        }

        Eisenhower.getBackPack().putInItem(Producer.tomato());
        Eisenhower.getBackPack().putInItem(Producer.cucumber());
        Eisenhower.getBackPack().putInItem(Producer.salt());

        launch(args);

        boolean trigger = false;
        while (true) {
            if (Eisenhower.getHelath().getCurrentValue() <= 0) {
                System.out.println("You are dead!");
                break;
            }
            if (trigger) {
                trigger = !trigger;
                if (menus.size() == 0) {
                    System.out.println("-----------------------------------");
                    ArrayList<String> outPut = Eisenhower.whereAmI();
                    for (String string : outPut) {
                        System.out.println(string);
                    }

                    continue;
                } else {
                    Menu menu = menus.peek();
                    //System.out.println("!!!!");
                    ArrayList<String> outPut = menu.whereAmI();
                    //System.out.println("????");
                    System.out.println("-----------------------------------");
                    for (String string : outPut) {
                        System.out.println(string);
                    }

                    continue;
                }
            }
            command = scanner.nextLine();
            trigger = !trigger;


            try {

                if (command.equals("slp")) {
                    for (Thing thing : gameThings)
                        thing.nextDay(Eisenhower.season);
                }
                if (menus.size() == 0) {

                    if (command.equals("WhereAmI") || command.equals("wai")) {
                        ArrayList<String> outPut = Eisenhower.whereAmI();
                        for (String string : outPut) {
                            System.out.println(string);
                        }
                        trigger = false;
                        continue;
                    }
                    if (command.startsWith("goto")) {
                        String placeName = command.substring(5);
                        Eisenhower.goTo(placeName);
                        continue;
                    }
                    if (command.startsWith("inspect")) {
                        String objectName = command.substring(8);
                        Thing object = Eisenhower.getJa().findObject(objectName);

                        if (object == null) {
                            System.out.println("No Object Found!");
                            continue;
                        }
                        if (object instanceof Storage) {
                            Menu mn = new StorageMenu((Storage) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Bed) {
                            Menu mn = new BedMenu((Bed) object, gameThings);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Pond) {
                            Menu mn = new PondMenu((Pond) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Field) {
                            Menu mn = new FieldMenu((Field) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof FruitGarden) {
                            Menu mn = new FruitGardenMenu((FruitGarden) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Clinic) {
                            Menu mn = new ClinicMenu((Clinic) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Kitchen) {
                            Menu mn = new KitchenMenu((Kitchen) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof GreenHouse) {
                            Menu mn = new GreenHouseMenu((GreenHouse) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof WoodType) {
                            Menu mn = new WoodMenu((WoodType) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof AnimalField) {
                            Menu mn = new AnimalFieldMenu((AnimalField) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof StoneType) {
                            Menu mn = new StoneMenu((StoneType) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Store) {
                            if (object instanceof Butchery) {
                                Menu mn = new ButcheryMenu((Butchery) object);
                                menus.add(mn);
                                continue;
                            }
                            if (object instanceof Ranch) {
                                Menu mn = new RanchMenu((Ranch) object);
                                menus.add(mn);
                                continue;
                            }
                            if (object instanceof WorkShop) {
                                Menu mn = new WorkShopMenu((WorkShop) object);
                                menus.add(mn);
                                continue;
                            }

                            Menu mn = new StoreMenu((Store) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Cafe) {
                            CafeMenu mn = new CafeMenu((Cafe) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof MachineRoom) {
                            MachineRoomMenu mn = new MachineRoomMenu((MachineRoom) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof Laboratory) {
                            LaboratoryMenu mn = new LaboratoryMenu((Laboratory) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof River) {
                            Menu mn = new RiverMenu((River) object);
                            menus.add(mn);
                            continue;
                        }
                        if (object instanceof CityHall) {
                            Menu mn = new CityHallMenu((CityHall) object);
                            menus.add(mn);
                            continue;
                        }


                        continue;
                    }
                    if (command.equals("BackPack")) {
                        Menu backPackMenu = new BackPackMenu(Eisenhower);
                        menus.add(backPackMenu);
                        continue;
                    }
                    if (command.equals("Stats")) {
                        for (String stat : Eisenhower.stats())
                            System.out.println(stat);
                    }
                } else {
                    Menu menu = menus.peek();

                    if (command.equals("WhereAmI") || command.equals("wai")) {

                        ArrayList<String> outPut = menu.whereAmI();
                        for (String string : outPut) {
                            System.out.println(string);
                        }
                        trigger = false;
                        continue;
                    }
                    if (command.equals("Back")) {
                        menus.pop();
                        continue;
                    }
                    try {
                        int x = Integer.parseInt(command);
                        Menu mn = menu.getCommand(x, Eisenhower);
                        if (mn != null)
                            menus.add(mn);
                    } catch (Exception e) {
                        System.out.println("Bad Command!");
                    }

                }
            } catch (Exception e) {
                System.out.println("Bad Command!");
            }
        }
    }
    public static void setVillageGroup(ImageView imageView){
        villageBuilder.villageGroup.getChildren().add(imageView);
    }

    static Group mainMenuGroup;
    static Scene mainMenuScene;

    static FarmBuilder farmBuilder = new FarmBuilder(mainFarm);
    static VillageBuilder villageBuilder = new VillageBuilder(village);
    static ForestBuilder forestBuilder = new ForestBuilder(forest);
    static MarketBuilder marketBuilder = new MarketBuilder(market);
    static HouseBuilder houseBuilder = new HouseBuilder(home);
    static BarnBuilder barnBuilder = new BarnBuilder(barn);
    static Customizer customizer = new Customizer();
    static boolean startgame = false;


    //static Image farm = new Image("maps\\farm.png");
    static ImageView character ;
    static final MediaView mainThemeView = new MediaView();
    static Iterator<String> itr ;

    static ArrayList<Fruit> customFruits = new ArrayList<>();
    static ArrayList<Crop> customCrops = new ArrayList<>();
    static ArrayList<Tree> customTrees = new ArrayList<>();

    public static void play(String mediaFile){
        Media media = new Media(new File(mediaFile).toURI().toString());
        MediaPlayer player = new MediaPlayer(media);

        mainThemeView.setMediaPlayer(player);
        player.play();
        player.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                player.stop();
                if (itr.hasNext()) {
                    //Plays the subsequent files
                    play(itr.next());
                }
                return;
            }
        });
    }
    @Override
    public void start(Stage primaryStage) throws Exception {

        try {
            FileInputStream fin;
            ObjectInputStream oin;
            Integer num;

            File file = new File("custom\\fruits.txt");
            if (file.canRead()) {
                fin = new FileInputStream("custom\\fruits.txt");
                oin = new ObjectInputStream(fin);
                num = (Integer) oin.readObject();
                System.out.println(num);
                for (int i = 0; i < num; i++) {
                    Fruit fruit = (Fruit) oin.readObject();
                    customFruits.add(fruit);
                }
            }
            file = new File("custom\\crops.txt");
            if (file.canRead()) {
                fin = new FileInputStream("custom\\crops.txt");
                oin = new ObjectInputStream(fin);
                num = (Integer) oin.readObject();
                System.out.println(num);
                for (int i = 0; i < num; i++) {
                    Crop crop = (Crop) oin.readObject();
                    System.out.println(crop);
                    customCrops.add(crop);
                }
            }
            file = new File("custom\\trees.txt");
            if (file.canRead()) {
                fin = new FileInputStream("custom\\trees.txt");
                oin = new ObjectInputStream(fin);
                num = (Integer) oin.readObject();
                System.out.println(num);

                for (int i = 0; i < num; i++) {
                    Tree tree = (Tree)oin.readObject();
                    System.out.println(tree);
                    customTrees.add(tree);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
            mainMenuGroup = new Group();
            mainstage = primaryStage;
            character = new ImageView(new Image("Images\\normalFront.png"));

         timeline = new Timeline(new KeyFrame(Duration.millis(1000), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dayTime++;
                Eisenhower.getStamina().changeCurrentValue(1);
                Eisenhower.getSatiety().changeCurrentValue(-0.1);
                //System.out.println(Eisenhower.getSatiety());
                //System.out.println("LOL");

                if (dayTime % 5 == 0){
                    for (Thing thing : gameThings) {
                        try {
                            thing.nextDay(Eisenhower.season);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        farmBuilder.drawFields();
                        farmBuilder.drawFruitGardens();
                    }
                    System.out.println("Day " + dayTime/5);
                }
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);


        character.setX(32 * 15);
        character.setY(32 * 15 - 14);

        //villageBuilder.start(primaryStage);

        mainMenuScene = new Scene(mainMenuGroup, 1280, 960);
        primaryStage.setScene(mainMenuScene);
        primaryStage.show();

        ImageView backGround = new ImageView(new Image("Images\\BackGround2.gif"));
        backGround.setFitHeight(960);
        backGround.setFitWidth(1280);

        mainMenuGroup.getChildren().addAll(backGround);

        Button playButton = new Button("PLAY");
        playButton.setTextFill(Color.DIMGREY);
        playButton.setShape(new Ellipse(100, 50));
        playButton.setFont(menuFont);

        Button customButton = new Button("CUSTOMIZE");
        customButton.setTextFill(Color.DIMGREY);
        customButton.setShape(new Ellipse(100, 50));
        customButton.setFont(menuFont);

        Button exitButton = new Button("EXIT");
        exitButton.setTextFill(Color.DIMGREY);
        exitButton.setShape(new Ellipse(100, 50));
        exitButton.setFont(menuFont);

        Button multi = new Button("MultiPlayer");
        multi.setTextFill(Color.DIMGREY);
        multi.setShape(new Ellipse(100, 50));
        multi.setFont(Main.menuFont);
        multi.setLayoutX((mainMenuScene.getWidth()*3) / 4 - 120);
        multi.setLayoutY((mainMenuScene.getHeight()*3) / 4 - 40);
        multi.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                User user = new User();
                try {
                    user.start(primaryStage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mainMenuGroup.getChildren().addAll(playButton, exitButton, multi, customButton);


        List<String> list = new ArrayList<String>();
        for (int i = 0 ; i < 40 ;i++)
            list.add("musics\\" + (Math.abs(i%5)) + ".mp3");
        itr = list.iterator();
        play(itr.next());
        mainMenuGroup.getChildren().add(mainThemeView);


        playButton.setLayoutX(mainMenuScene.getWidth() / 4 - 80);
        playButton.setLayoutY(mainMenuScene.getHeight() / 4 - 40);
        exitButton.setLayoutX(mainMenuScene.getWidth() / 4 - 80);
        exitButton.setLayoutY(3 * mainMenuScene.getHeight() / 4 - 40);
        customButton.setLayoutX(3 * mainMenuScene.getWidth() / 4 - 120);
        customButton.setLayoutY(mainMenuScene.getHeight() / 4 - 40);


        playButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                timeline.play();
                try {
                    farmBuilder.start(primaryStage);
                    online = false;
                    Getter.ishost = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        exitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.close();
            }
        });

        customButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    customizer.start(primaryStage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}