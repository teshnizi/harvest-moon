
import java.util.*;

/**
 * has TreeBlocks
 */
public class FruitGarden extends Thing {

    /**
     * Default constructor
     */
    public FruitGarden(String name) {
        super(name);
        /*trees.add(Producer.peachTree());
        trees.add(Producer.pearTree());
        trees.add(Producer.lemonTree());
        trees.add(Producer.pomegranateTree());
        trees.add(Producer.appleTree());
        trees.add(Producer.orangeTree());*/
    }

    ArrayList<Tree> trees = new ArrayList<>();


    /**
     * @return
     */
    public ArrayList<String> objects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("");
        return s;
    }

    public void nextDay(Season season){
        for(Tree tree : trees){
            if(tree.isBought)
                tree.nextDay(season);
        }
    }

}