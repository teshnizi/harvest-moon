import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.management.ObjectName;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.management.PlatformLoggingMXBean;
import java.lang.management.PlatformManagedObject;
import java.net.Socket;

/**
 * Created by pc on 6/6/2017.
 */
public class ChatRoom extends Application implements Runnable {
    Socket soc;
    String recive = new String();
    TextField tf;
    HBox ta;
    static boolean inchat = false;
    Button btnSend, btnClose;
    String sendTo;
    String LoginName =  "ahmad";
    Thread t = null;
    MediaPlayer mediaPlay ;
    String protocol = "]a.a]";
    File ff = new File("musics\\ah.mp3");
    Media med = new Media(ff.toURI().toString());
    DataOutputStream dout;
    DataInputStream din;
    static String tta , tre;
    static String sendimage , curentImage , sendmusic,curentmusic;
    Image userimage;
    public ChatRoom(Socket socket, String LoginName, String chatwith , Image userimage) throws IOException {
        this.LoginName = LoginName;
        sendTo = chatwith;
        tf = new TextField();
        ta = new HBox();
        inchat = true;
        btnSend = new Button("Send");
        btnClose = new Button("Close");
        soc = socket;
        textFlow = new TextFlow();
        File file = new File("images\\images1.jpg");
        curentImage = sendimage;
        curentmusic = sendmusic;
        tta = ("adsd");
        this.userimage = userimage;
        din = new DataInputStream(soc.getInputStream());
        dout = new DataOutputStream(soc.getOutputStream());
    }
    Object o;
    int num1 = 0;
    final ScrollPane sp = new ScrollPane();
    static TextFlow textFlow ;
    public static  void main(String[] args){
        launch(args);
    }
    @Override
    public void start(Stage stage) {
        textFlow.setLineSpacing(10);
        TextField textField = new TextField();
        File file = new File("images\\images1.jpg");
        File file1 = new File("images\\images5.jpg");
        File file2 = new File("images\\images3.jpg");
        Image image = new Image(file.toURI().toString());
        Image image1 = new Image(file1.toURI().toString());
        Image image2 = new Image(file2.toURI().toString());
        HBox hBox1 = new HBox();
        Circle cc = new Circle(15);
        if (userimage !=null)
            cc.setFill(new ImagePattern(userimage));
        else
        {
            File fg = new File("images\\index.jpg");
            cc.setFill(new ImagePattern(new Image(fg.toURI().toString())));
        }
        hBox1.setAlignment(Pos.CENTER);
        Text tg = new Text("  " +LoginName + "         ");
        Circle cx = new Circle(15);
        File fg = new File("images\\images7.jpg");
        cx.setFill(new ImagePattern(new Image(fg.toURI().toString())));
        cx.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cx.setCursor(Cursor.HAND);
            }
        });
        cx.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inchat = false;
                stage.close();
            }
        });
        hBox1.getChildren().addAll(
                cc,
                tg,
                cx
        );
        sendimage = file.toURI().toString();
        Rectangle rectangle = new Rectangle();
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle();
        rectangle.setWidth(25);
        rectangle.setHeight(31);
        rectangle1.setWidth(25);
        rectangle1.setHeight(27);
        rectangle2.setWidth(25);
        rectangle2.setHeight(27);
        rectangle.setFill(new ImagePattern(image));
        rectangle1.setFill(new ImagePattern(image1));
        rectangle2.setFill(new ImagePattern(image2));
        rectangle.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setCursor(Cursor.HAND);
            }
        });
        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle1.setCursor(Cursor.HAND);
            }
        });
        rectangle2.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle2.setCursor(Cursor.HAND);
            }
        });
        tre = tta;
        curentImage = sendimage;
        curentmusic = sendmusic;
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().addAll(new KeyFrame(Duration.millis(400), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tre != tta) {
                    tre = tta;
                    Text ttt = new Text("\n" + tre);
                    ttt.setFill(Color.GREEN);
                    ttt.setStyle("-fx-font-style: italic");
                    textFlow.getChildren().addAll(ttt);
                    mediaPlay = new MediaPlayer(med);
                    mediaPlay.play();
                }
                if (curentImage != sendimage) {
                    curentImage = sendimage;
                    System.out.println(sendimage.toString());
                    Rectangle rectangle3 = new Rectangle();
                    rectangle3.setWidth(100);
                    rectangle3.setHeight(80);
                    Image image3 = new Image(sendimage);
                    if (sendimage != null) {
                        rectangle3.setFill(new ImagePattern(image3));
                        Text tr = new Text("\n" + recive + " send img :\n");
                        tr.setFill(Color.GREEN);
                        tr.setStyle("-fx-font-style: italic");
                        textFlow.getChildren().addAll(tr, rectangle3);
                        mediaPlay = new MediaPlayer(med);
                        mediaPlay.play();
                    }
                }
                if (curentmusic != sendmusic) {
                    curentmusic = sendmusic;
                    Media media = new Media(sendmusic);
                    Circle circle = new Circle(20);
                    File file3 = new File("images4.jpg");
                    File file4 = new File("images6.jpg");
                    Image image3 = new Image(file4.toURI().toString());
                    Image image5 = new Image(file3.toURI().toString());
                    mediaPlay = new MediaPlayer(med);
                    mediaPlay.play();
                    circle.setFill(new ImagePattern(image5));
                    MediaPlayer mediaPlayer = new MediaPlayer(media);
                    circle.setOnMouseMoved(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            circle.setCursor(Cursor.HAND);
                        }
                    });
                    circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (num1 == 0) {
                                mediaPlayer.play();
                                num1 = 1;
                                circle.setFill(new ImagePattern(image3));
                            } else {
                                mediaPlayer.stop();
                                num1 = 0;
                                circle.setFill(new ImagePattern(image5));
                            }
                        }
                    });

                    Text tr = new Text("\n" + recive + " send music :\n");
                    tr.setFill(Color.GREEN);
                    tr.setStyle("-fx-font-style: italic");
                    textFlow.getChildren().addAll(tr, circle);
                }
            }
        }));
        timeline.play();
        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Select Sound");
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Sound Files", "*.mp3"));
                File selectedFile = fileChooser.showOpenDialog(null);
                if (selectedFile != null) {
                    Media media = new Media(selectedFile.toURI().toString());
                    Circle circle = new Circle(20);
                    File file3 = new File("images4.jpg");
                    File file4 = new File("images6.jpg");
                    Image image3 = new Image(file4.toURI().toString());
                    Image image5 = new Image(file3.toURI().toString());
                    circle.setFill(new ImagePattern(image5));
                    MediaPlayer mediaPlayer = new MediaPlayer(media);
                    circle.setOnMouseMoved(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            circle.setCursor(Cursor.HAND);
                        }
                    });
                    circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (num1 == 0) {
                                mediaPlayer.play();
                                num1 = 1;
                                circle.setFill(new ImagePattern(image3));
                            } else {
                                mediaPlayer.stop();
                                num1 = 0;
                                circle.setFill(new ImagePattern(image5));
                            }
                        }
                    });
                    try {
                        dout.writeUTF(LoginName + Protocol.protocol + Protocol.chatelert + Protocol.protocol + sendTo);
                        dout.writeUTF(LoginName + Protocol.protocol+Protocol.privatchatroom + Protocol.protocol +Protocol.music + Protocol.protocol + sendTo + Protocol.protocol+ selectedFile.toURI().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Text tr = new Text("\n");
                    textFlow.getChildren().addAll(tr, circle);

                }

            }
        });
        rectangle2.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle2.setCursor(Cursor.HAND);
            }
        });

        rectangle.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Select Images files");
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.gif"));
                File selectedFile = fileChooser.showOpenDialog(null);
                if (selectedFile != null) {
                    Image image5 = new Image(selectedFile.toURI().toString());
                    Rectangle rectangle3 = new Rectangle();
                    rectangle3.setWidth(100);
                    rectangle3.setHeight(80);
                    rectangle3.setFill(new ImagePattern(image5));
                    Text tr = new Text("\n");
                    textFlow.getChildren().addAll(tr, rectangle3);
                    try {
                        dout.writeUTF(LoginName + Protocol.protocol + Protocol.chatelert + Protocol.protocol + sendTo);
                        dout.writeUTF(LoginName + Protocol.protocol+Protocol.privatchatroom + Protocol.protocol +Protocol.images + Protocol.protocol + sendTo + Protocol.protocol+ selectedFile.toURI().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        Button button = new Button("Send");
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setCursor(Cursor.HAND);
            }
        });
        button.setPrefSize(50, 30);
        VBox container = new VBox();
        container.getChildren().addAll(hBox1);
        VBox box = new VBox();
        box.getChildren().addAll(sp, textFlow);
        container.setPadding(new Insets(10));
        container.getChildren().addAll(box, new Separator(), new HBox(textField, button, rectangle, rectangle1));
        textField.setPrefSize(20, 20);
        textField.setMaxWidth(180);
        VBox.setVgrow(sp, Priority.ALWAYS);
        VBox.setVgrow(textFlow, Priority.ALWAYS);
        textField.prefWidthProperty().bind(container.widthProperty().subtract(button.prefWidthProperty()));

        textField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                button.fire();
            }
        });

        button.setOnAction(e -> {
            Text text;
            String msg = textField.getText();
            msg = msg.replace(":)", new String(Character.toChars(0x1f603)) + " ");
            msg = msg.replace(";)", new String(Character.toChars(0x1f609)) + " ");
            msg = msg.replace(":*", new String(Character.toChars(0x1f618)) + " ");
            msg = msg.replace(":E", new String(Character.toChars(0x1f620)) + " ");
            msg = msg.replace(":(", new String(Character.toChars(0x1f621)) + " ");
            msg = msg.replace(":O", new String(Character.toChars(0x1f631)) + " ");
            msg = msg.replace("B)", new String(Character.toChars(0x1f60E)) + " ");
            msg = msg.replace(":D", new String(Character.toChars(0x1f602)) + " ");
            msg = msg.replace(":|", new String(Character.toChars(0x1f610)) + " ");
            msg = msg.replace("<3", new String(Character.toChars(0x2764)) + " ");
            if (textFlow.getChildren().size() == 0) {
                text = new Text(msg);
            } else {

                text = new Text("\n" + msg);
            }

            textFlow.getChildren().add(text);
            try {
                dout.writeUTF(LoginName + Protocol.protocol + Protocol.chatelert + Protocol.protocol + sendTo);
                dout.writeUTF(LoginName + Protocol.protocol+Protocol.privatchatroom + Protocol.protocol+Protocol.text + Protocol.protocol + sendTo + Protocol.protocol+ msg);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            textField.clear();
            textField.requestFocus();
        });
        VBox vb = new VBox();
        vb.getChildren().addAll(textFlow);
        sp.setVmax(440);
        sp.setPrefSize(300, 485);
        sp.setContent(vb);
        sp.vvalueProperty().bind((ObservableValue<? extends Number>) vb.heightProperty());
        //sp.setPannable(true);
        Scene scene = new Scene(container, 300, 485);

        stage.setScene(scene);
        stage.setTitle("ChatBox");
        stage.show();
    }
    @Override
    public void run() {
        while (true && !soc.isClosed()) {
            try {
                //ta.getItems().add("\n" + sendTo + " Says :" + din.readUTF());
                if (soc.isConnected()) {
                    String s = din.readUTF();
                    String[] mm = s.split(Protocol.protocol);
                    if (mm.length > 2 &&mm[2].equals(Getter.socketname)) {
                        s = mm[1];
                        recive = mm[1];
                        if (s.equals(Protocol.text)) {
                            tta = (mm[0] + " Says : " + mm[3]);
                        } else if (s.equals(Protocol.images)) {
                            sendimage = mm[3];

                        } else if (s.equals(Protocol.music)) {


                            sendmusic = mm[3];
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    stop();
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
                break;
            }
        }
    }

}

