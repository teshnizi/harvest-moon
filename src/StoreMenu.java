import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/16/2017.
 */
public class StoreMenu extends Menu {

    Store store;

    public StoreMenu(Store store){
        this.store = store;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("This is " + store.getName());
        if (store.isOnAuction){
            s.add("Today we have auction!");
        }
        else
            s.add("This store has auction in " + (7 - (store.day%7)) + " days!");
        s.add("1.Sell items");
        s.add("2.Check items");
        s.add("3.Buy items");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        store.storage.synchronizeStorage();
        try {
            Scanner scanner = new Scanner(System.in);
            if (x == 1) {
                System.out.println("Choose an item:");
                ItemStack itemStack = person.getBackPack().getItemStackFromPlayer();
                int sellPrice = itemStack.getItems().get(0).getPrice() * 3 / 4;
                System.out.println("Price per item: $" + sellPrice + ".  How many of it do you want to sell?");
                int num = scanner.nextInt();
                if (num > itemStack.getItems().size()) {
                    System.out.println("Not enough items");
                    return null;
                }
                person.getBackPack().takeItem(itemStack.getItems().get(0).getName(), num);
                person.giveMoney(num * sellPrice);
                System.out.println("Items sold! you earned $" + num * sellPrice);
                return null;
            }

            if( x == 2 ){
                System.out.println("Items inside shop: ");
                int ID = 0;
                for (ItemStack itemStack : store.storage.getItemStacks()){
                    System.out.println((++ID) + "." + itemStack.getItems().get(0).getName());
                }
                ID = scanner.nextInt();
                ItemStack itemStack = store.storage.getItemStacks().get(ID - 1);
                Item it = itemStack.getItems().get(0);
                System.out.println(itemStack.getItems().get(0).status() + " x" + itemStack.getItems().size());
                return null;
            }

            if( x == 3) {
                System.out.println("Choose an item:");
                int ID = 0;
                for (ItemStack itemStack : store.storage.getItemStacks()){
                    System.out.println((++ID) + "." + itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                }
                ID = scanner.nextInt();
                ItemStack itemStack = store.storage.getItemStacks().get(ID - 1);
                int price = itemStack.getItems().get(0).getPrice();
                if(store.isOnAuction)price = price*3/4;
                if (itemStack.getItems().get(0) instanceof Tool)
                    price /= 2;
                System.out.println("1st item Price: $" + (50.0 - (double)itemStack.getItems().size())/20.0 * (double)price + " (total Price may be more expensive due to lack of items)");
                System.out.println("How many of " + itemStack.getItems().get(0).getName() + " do you want to buy?");
                int num = scanner.nextInt();
                if (num > itemStack.getItems().size()) {
                    System.out.println("Not enough property in shop!");
                    return null;
                }
                double totalPrice = 0;
                int totalNum = itemStack.getItems().size();
                for (int i = totalNum ; i > totalNum - num ; i--)
                    totalPrice += (50.0 - (double)i)/20.0 * (double)price; //  0 <= i <= 40 take a look at class Store
                System.out.println("Total price: $" + totalPrice);
                System.out.println("Are you sure?(YES/NO)");
                String cmnd = scanner.next();
                if (cmnd.equals("YES")) {
                    if (person.getMoney() < totalPrice) {
                        System.out.println("Not enough money!");
                        return null;
                    }
                    if (person.getBackPack().getRemainingCapacity() < itemStack.getItems().get(0).getSize()) {
                        System.out.println("Not enough space in backpack!");
                        return null;
                    }
                    if (num > itemStack.getItems().size()) {
                        System.out.println("Not enough property in shop!");
                        return null;
                    }
                    person.takeMoney((int)totalPrice);
                    System.out.println(num + "x " + itemStack.getItems().get(0).getName() + " bought!");
                    person.getBackPack().putInItemStack(itemStack, num);
                }
            }
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
