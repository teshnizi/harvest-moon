import java.util.ArrayList;

/**
 * Created by pc on 5/14/2017.
 */
public class ForestMenu extends Menu{
    Forest forest;
    public ForestMenu (Forest forest){this.forest = forest;}

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("---This place is : ");
        s.add("Forest");
        s.add("---Places which you can go from here : ");
        s.add("MainFarm");
        s.add("Cave");
        s.add("---Object you can interact with:");
        s.add("Woods");
        s.add("Rocks");
        s.add("River");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        return null;
    }
}
