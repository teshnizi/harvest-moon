import java.io.Serializable;

/**
 * 
 */
public class Resource extends Collectable  implements Serializable {

    /**
     * Default constructor
     */
    public Resource(String name, int size, int price) {
        super(name, size, price);
    }
    public Resource(String name, int size, int price, String descripion) {
        super(name, size, price);
        this.description = descripion;
    }

}