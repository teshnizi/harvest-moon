
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Crop extends Plant implements Serializable{

    /**
     * Default constructor
     */
    public Crop(String name, Season season, int matureAge, int maxNumberOfHarvests, int dailyWaterNeed, ItemStack outCome) {
        super(name,season,matureAge,dailyWaterNeed,outCome);
        this.maxNumberOfHarvests = maxNumberOfHarvests;
        this.seed = seed;
        this.alive = true;
    }

    public Crop(Crop crop){
        this(crop.getName(), crop.season, crop.matureAge, crop.maxNumberOfHarvests, crop.dailyWaterNeed, crop.outCome);
        this.alive = true;
    }


    int maxNumberOfHarvests;
    int currentNumberOfHarvests = 0;
    Seed seed;
    Field fatherField;
    boolean addedFruitsFertilization;


    @Override
    public void nextDay(Season season){
        super.nextDay(season);
        checkIfIsAlive();
    }

    @Override
    public void checkIfIsAlive(){
        super.checkIfIsAlive();
        if(currentNumberOfHarvests >= maxNumberOfHarvests){
            System.out.println("MAXNUM");
            alive = false;
        }
        if(age >= (maxNumberOfHarvests+2)*matureAge){
            System.out.println("AGE");
            alive = false;
        }
    }

    @Override
    public String status(){
        String s = getName() + "\nWater Level: " + waterLevel + "\nSeason: " + season
                + "\nMature age: " + matureAge + "\nCurrent Age: " + age + "\nMax number of harvests: " + maxNumberOfHarvests
                + "\nCurrent number of harvests: " + currentNumberOfHarvests + "\nOutcome: " + outCome.toString();
        return s;
    }

}