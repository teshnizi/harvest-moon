
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Ability extends Thing  implements Serializable {

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public void setCurrentValue(double currentValue) {
        this.currentValue = currentValue;
    }


    /**
     * Default constructor
     */


    public Ability(String name, int maxValue, int currentValue, int refillRate) {
        super(name);
        this.maxValue = maxValue;
        this.currentValue = currentValue;
        this.refillRate = refillRate;
    }

    public Ability(String name, int maxValue, int currentValue, int refillRate, int consumptionRate) {
        super(name);
        this.maxValue = maxValue;
        this.currentValue = currentValue;
        this.refillRate = refillRate;
        this.consumptionRate = consumptionRate;
    }

    @Override
    public String toString(){
        if (currentValue == 0 && maxValue == 0 && refillRate == 0 )
            return "";
        String s = getName() + " CV: " + currentValue + " MV: " + maxValue + " RR: " + refillRate;
        return s;
    }

    private double maxValue;

    private double refillRate;

    private double currentValue;

    private double consumptionRate;

    public double getCurrentValue() {
        return currentValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public double getRefillRate() {
        return refillRate;
    }


    /**
     * @param val 
     * @return
     */
    public void drain(int val) {
        // TODO implement here
    }

    /**
     * @param val 
     * @return
     */
    public void fill(int val) {
        // TODO implement here
    }

    public void changeCurrentValue(double currentValueChange){
        currentValue += currentValueChange;
        int tt =(int) ((maxValue/2)*((double)(Person.abilities.get(0).getCurrentValue()/Person.abilities.get(0).getMaxValue())) + maxValue/2);
        if(currentValue > tt)currentValue = tt;
        if(currentValue < 0 ) currentValue = 0;
    }

    public void changeRefillRate(double refillRateChange) {
        refillRate += refillRateChange;
    }

    public void changeMaxValue(double maxValueChange) {
        maxValue += maxValueChange;
    }
}