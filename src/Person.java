
import java.util.*;

/**
 * 
 */
public class Person extends Thing {


    /**
     * Default constructor
     */
    boolean canMove = true;
    static ArrayList<String> friends = new ArrayList<>();
    public Person(String name, int money, JA ja, Season season) {
        super(name);
        this.money = money;
        this.ja = ja;
        currentTool = null;
        backPack = new Storage(100);
        activeMissions = new ArrayList<>();
        abilities = new ArrayList<>();
        eatableFoods = new ArrayList<>();
        this.season = season;
        age = 1;
    }
    static int age;
    Season season;
    boolean isSeek = false;

    public void setMoney(int money) {
        this.money = money;
    }

    public Storage getBackPack() {
        return backPack;
    }

    public ArrayList<String> whereAmI(){
        ArrayList<String> ret = new ArrayList<>();
        ret.add("\n---This Place is:");
        ret.add(ja.getName());
        ret.add("\n---Places which you can go from here:");
        ArrayList<String> tmp = ja.listOfPlaces();
        if(tmp != null)
            for (String place : tmp)
                ret.add(place);
        ret.add("\n---Objects you can interact with:");
        tmp = ja.listOfObjects();
        if(tmp != null)
            for(String obj: tmp)
                ret.add(obj);
        ret.add("\nEnter \"BackPack\" to inspect BackPack");
        ret.add("\nEnter \"Stats\" to inspect Eisenhower");
        return ret;
    }

    public ArrayList<String> stats(){
        ArrayList<String> s = new ArrayList<>();
        s.add("Name: " + getName());
        s.add("Money: $" + money);
        s.add("Sickness: " + isSeek);
        for(Ability ability : abilities)
            s.add(ability.toString());
        return s;
    }

    public void giveMoney(int money){
        this.money += money;
    }

    public ArrayList<String> help(){
        return ja.objects();
    }

     private JA ja;

    public JA getJa() {
        return ja;
    }

    public int getMoney() {
        return money;
    }


    @Override
    public void nextDay(Season season){
        for(Ability ability : abilities){
            ability.changeCurrentValue(ability.getRefillRate());
        }
        if(getSatiety().getCurrentValue() <= 0){
            getHealth().changeCurrentValue(-10);
        }
        if (isSeek){
            getHealth().changeCurrentValue(-10);
        }
        Random random = new Random();
        double d = random.nextDouble();
        d = d * Math.log10(getHealth().getCurrentValue());
        if ( d < 0.1 ){
            isSeek = true;
        }
        age++;
        if (age == 15){
            age = 1;
            if(season == Season.spring)
                season = Season.summer;
            else if (season == Season.summer)
                season = Season.autumn;
            else if (season == Season.autumn)
                season = Season.winter;
            else if (season == Season.winter)
                season = Season.spring;
        }
        //System.out.println(backPack.getItemStacks());
        backPack.synchronizeStorage();
        for (ItemStack itemStack : backPack.getItemStacks()){
            if (itemStack.getItems().size() == 0)
                continue;
            if (itemStack.getItems().get(0) instanceof Food){
                ItemStack removables = new ItemStack();
                for (Item item : itemStack.getItems()) {
                    //System.out.println(item.getName());
                    ((Food) item).rotLevel++;
                    if (((Food)item).rotLevel >=7){
                        removables.getItems().add(item);
                    }
                }
                for (Item rm : removables.getItems()){
                    itemStack.getItems().remove(rm);
                }
                //backPack.synchronizeStorage();
            }
        }
        backPack.synchronizeStorage();
    }
    private int money;

    private Tool currentTool;

    private Storage backPack;

    private ArrayList<Mission> activeMissions = new ArrayList<>();

     static ArrayList<Ability> abilities = new ArrayList<>();

    public ArrayList<Ability> getAbilities() {
        return abilities;
    }

    private ArrayList<Food> eatableFoods = new ArrayList<>();

    public Ability getHealth(){
        return abilities.get(0);
    }

    public Ability getEnergy(){
        return abilities.get(1);
    }

    public Ability getStamina(){
        return abilities.get(2);
    }

    public Ability getSatiety(){ return abilities.get(3); }

    public void eat(Food food){
        food.getEffect().affect();
        if (food instanceof Medicine)
            isSeek = false;
    }
    /**
     * @return
     */
    public int chooseTool() {
        // TODO implement here
        return 0;
    }

    /**
     * @param name
     * @return
     */
    public int goTo(String name) {

        JA j = ja.findJA(name);
        if( j == null ) {
            return 0;
        }
        abilities.get(1).changeCurrentValue(-10);
        ja = j;
        return 1;
    }

    /**
     * @param money 
     * @return
     */
    public int takeMoney(int money) {
        this.money -= money;
        return 0;
    }

    /**
     * @return
     */
    private int healingCost() {
        // TODO implement here
        return 0;
    }
}