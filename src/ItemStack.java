import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Tigerous215 on 5/12/2017.
 */
public class ItemStack  implements Serializable {

    private ArrayList<Item> items = new ArrayList<>();

    public ArrayList<Item> getItems() {
        return items;
    }

    public ItemStack(Item item){
        items.add(item);
    }
    public ItemStack(){
    }
    public ItemStack(ArrayList<Item> items){
        this.items = items;
    }

    public void setItems(ArrayList<Item> items){
        this.items = items;
    }

    public void changeToJunk(){
        ArrayList<Item> tmp = new ArrayList<>();
        for (int i = 0; i < items.size(); i++){
            tmp.add(new Junk());
        }
        items = tmp;
    }
    public void removeItem(int num){
        if(items == null || items.size() < num){
            items = null;
            return;
        }
        for(int i = 0 ; i < num; i++){
            items.remove(items.size()-1);
        }
    }

    @Override
    public String toString(){
        if(items == null || items.size() == 0)
            return "Empty Stack";
        return items.get(0).getName() + " x" + items.size();
    }
}
