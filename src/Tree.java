
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class Tree extends Plant implements Serializable{

    /**
     * Default constructor
     */
    public Tree(String name, Season season, int matureAge, int dailyWaterNeed, Fruit sampleFruit, int price, Boolean isBought) {
        super(name, season, matureAge, dailyWaterNeed, new ItemStack(new ArrayList<Item>()));
        this.price = price;
        this.isBought = isBought;
        this.sampleFruit = sampleFruit;
    }
    private int price;
    boolean isBought;
    Fruit sampleFruit;

    public int getPrice() {
        return price;
    }
    @Override
    public String toString(){
        return getName() + "   season:" + season + "   matureAge:" + matureAge + "  daily water need:" + dailyWaterNeed + "  waterLevel:" + waterLevel;
    }
    @Override
    public void nextDay(Season season){
        matureAge = 0;
        if(season != this.season){
            age = 0;
            alive = false;
            return;
        }
        alive = true;
        if(!isBought)return;
        outCome = new ItemStack(new ArrayList<Item>());
        System.out.println("HERE!");
        if(waterLevel >= dailyWaterNeed){
            waterLevel = 0;
            Random random = new Random();
            int num = random.nextInt()%3 + 2;
            for(int i = 0; i < num; i++){
                outCome.getItems().add(new Fruit(sampleFruit.getName(),sampleFruit.getSize(),sampleFruit.getPrice(),sampleFruit.getEffect()));
            }
            System.out.println("HERE2!");
        }
        this.age++;
    }
}