import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.security.Key;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by tigerous215 on 7/12/2017.
 */
public class BarnBuilder extends Application{
    static int numinimage = 3;
    private Barn mainBarn;

    ListView backPackListView;
    Font menuFont = new Font("Cambria",48);

    static HashSet<Pair<Integer,Integer>> barnAvailableTiles = new HashSet<>();
    static {
        for (int i = -1 ; i < 37 ; i++){
            barnAvailableTiles.add(new Pair<>(i,14));
            barnAvailableTiles.add(new Pair<>(i,13));
        }
        for(int i = 22 ;i <= 26; i++) {
            for (int j = 15; j <= 23; j++) {
                barnAvailableTiles.add(new Pair<>(i, j));
            }
        }

        for(int i = 6 ;i <= 7; i++) {
            for (int j = 14; j <= 18 ; j++) {
                barnAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for(int i = 5 ;i <= 6; i++) {
            for (int j = 11; j <= 13; j++) {
                barnAvailableTiles.add(new Pair<>(i, j));
            }
        }
        for(int i = 25 ;i <= 26; i++) {
            for (int j = 11; j <= 13; j++) {
                barnAvailableTiles.add(new Pair<>(i, j));
            }
        }
    }


    public BarnBuilder(Barn barn){
        this.mainBarn = barn;
        System.out.println(mainBarn.getAnimalFields().size());
    }

    @Override
    public void start(Stage primaryStage){
        ImageView machineView = new ImageView("Images\\machine.gif");
        machineView.setX(32 * 22);
        machineView.setY(32 * 24);

        Group barnGroup = new Group();
        barnGroup.getChildren().addAll(machineView);
        Scene barnScene = new Scene(barnGroup, 1280,960);
        Button inspectButton = new Button("Inspect");
        {
            inspectButton.setVisible(false);
            inspectButton.setFont(new Font(12));
            inspectButton.setPrefWidth(80);
            barnGroup.getChildren().addAll(inspectButton);
        }

        {
            cowPlaces[0] = new Pair<Integer,Integer>(2,21);
            cowPlaces[1]  = new Pair<Integer,Integer>(6,21);
            cowPlaces[2]  = new Pair<Integer,Integer>(10,21);
            cowPlaces[3]  = new Pair<Integer,Integer>(2,27);
            cowPlaces[4]  = new Pair<Integer,Integer>(6,27);
            cowPlaces[5]  = new Pair<Integer,Integer>(10,27);
            for(int i = 0 ; i < 6; i++)
                sheepPlaces[i] =  new Pair<>(cowPlaces[i].getKey() - 1, cowPlaces[i].getValue() - 19);
            for (int i = 0 ; i < 6; i++)
                chickenPlaces[i] = new Pair<>(sheepPlaces[i].getKey() + 20, sheepPlaces[i].getValue());

            for (int i = 0 ; i < 6; i++){

                cowsUI[i] = new ImageView();
                cowsUI[i].setX(cowPlaces[i].getKey() * 32);
                cowsUI[i].setY(cowPlaces[i].getValue() * 32);
                barnGroup.getChildren().addAll(cowsUI[i]);

                sheepsUI[i] = new ImageView();
                sheepsUI[i].setX(sheepPlaces[i].getKey() * 32);
                sheepsUI[i].setY(sheepPlaces[i].getValue() * 32);
                barnGroup.getChildren().addAll(sheepsUI[i]);

                chickensUI[i] = new ImageView();
                chickensUI[i].setX(chickenPlaces[i].getKey() * 32);
                chickensUI[i].setY(chickenPlaces[i].getValue() * 32);
                barnGroup.getChildren().addAll(chickensUI[i]);
            }

        };
        //barnScene = new Scene(barnGroup,1280,960);
        Image barn = new Image("maps\\barn.png");
        barnGroup.getChildren().addAll(Main.character);
        primaryStage.setScene(barnScene);
        primaryStage.show();
        drawAnimals();
        barnScene.setFill(new ImagePattern(barn));
        barnScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage % 4 + 1;
                Pair<Integer, Integer> next;
                if (Main.Eisenhower.canMove) {

                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png "));
                        MovementHandler.move(2, barnAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png"));
                        MovementHandler.move(0, barnAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, barnAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png"));
                        MovementHandler.move(3, barnAvailableTiles, Main.character);
                    }
                    System.out.println(MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()));
                }

                Pair<Integer, Integer> currentPlace = MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY());
               // System.out.println(currentPlace);
                if (currentPlace.equals(new Pair<>(6,11)) || currentPlace.equals(new Pair<>(5,11))
                        || currentPlace.equals(new Pair<>(6,18)) || currentPlace.equals(new Pair<>(7,18))
                        || currentPlace.equals(new Pair<>(26,11)) || currentPlace.equals(new Pair<>(25,11))
                        || (currentPlace.getKey() > 20 && currentPlace.getValue() == 23)){
                    inspectButton.setLayoutX(Main.character.getX()-28);
                    inspectButton.setLayoutY(Main.character.getY()-30);
                    inspectButton.setVisible(true);
                }else {
                    inspectButton.setVisible(false);
                }
                if (Main.character.getX() < 0){
                    Main.character.setX(5*32);
                    Main.character.setY(25*32-14);
                    Main.farmBuilder.start(primaryStage);
                }

                if (event.getCode() == KeyCode.I){
                    AnimalField animalField = null;
                    if (currentPlace.equals(new Pair<>(6,11)) || currentPlace.equals(new Pair<>(5,11)))
                        animalField = mainBarn.getAnimalFields().get(1);
                    if(currentPlace.equals(new Pair<>(6,18)) || currentPlace.equals(new Pair<>(7,18)))
                        animalField = mainBarn.getAnimalFields().get(0);
                    if(currentPlace.equals(new Pair<>(26,11)) || currentPlace.equals(new Pair<>(25,11)))
                        animalField = mainBarn.getAnimalFields().get(2);


                    //Machines
                    if(currentPlace.getKey() > 20 && currentPlace.getValue() == 23){
                        Group group1 = new Group();
                        Scene scene = new Scene(group1, 620, 700);
                        scene.setFill(new ImagePattern(new Image("Images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();

                        Button machines = new Button("List of machines");
                        Button utilize = new Button("Utilize machines");

                        group1.getChildren().addAll(machines,utilize);

                        machines.setPrefWidth(200);
                        utilize.setPrefWidth(200);

                        machines.setLayoutX(210);
                        utilize.setLayoutX(210);

                        machines.setLayoutY(300);
                        utilize.setLayoutY(350);

                        Rectangle rectangle1 = new Rectangle(10, 10, 30, 30);
                        rectangle1.setFill(new ImagePattern(new Image("Images\\backButton.jpg")));
                        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                rectangle1.setCursor(Cursor.HAND);
                            }
                        });
                        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                scene.setRoot(group1);
                            }
                        });

                        machines.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                group2.getChildren().addAll(rectangle1);
                                int num = 0;
                                Label description = new Label();
                                description.setFont(Main.comicFont);
                                description.setLayoutY(400);
                                description.setLayoutX(40);
                                group2.getChildren().addAll(description);

                                scene.setRoot(group2);
                                for (Machine machine : mainBarn.getMachineRoom().getMachines()) {
                                    Button button = new Button(machine.getName());
                                    button.setLayoutX(20 + 200 * (num / 10));
                                    button.setLayoutY(80 + (num % 10) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            String s = "Input: \n";
                                            for (ItemStack itemStack : machine.getInputItems())
                                                s = s + itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size() + "\n";
                                            s = s + "Output: \n";
                                            for (ItemStack itemStack : machine.getOutPutItems())
                                                s = s + itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size() + "\n";
                                            description.setText(s);
                                        }
                                    });
                                    group2.getChildren().addAll(button);
                                }
                                if (mainBarn.getMachineRoom().hasJuicer){
                                    Button button = new Button("Juicer");
                                    button.setLayoutX(20 + 200 * (num / 10));
                                    button.setLayoutY(80 + (num % 10) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            description.setText("Juices fruits");
                                        }
                                    });
                                    group2.getChildren().addAll(button);
                                }
                            }
                        });

                        utilize.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                group2.getChildren().addAll(rectangle1);
                                int num = 0;
                                Label description = new Label();
                                description.setFont(Main.comicFont);
                                description.setLayoutY(400);
                                description.setLayoutX(40);
                                group2.getChildren().addAll(description);

                                scene.setRoot(group2);
                                for (Machine machine : mainBarn.getMachineRoom().getMachines()) {
                                    Button button = new Button(machine.getName());
                                    button.setLayoutX(20 + 200 * (num / 10));
                                    button.setLayoutY(80 + (num % 10) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            for(ItemStack itemStack : machine.getInputItems()){
                                                if (Main.Eisenhower.getBackPack().numberOf(itemStack.getItems().get(0).getName()) < itemStack.getItems().size()){
                                                    description.setText("Not enough items");
                                                    return;
                                                }
                                            }
                                            for(ItemStack itemStack : machine.getInputItems()){
                                                Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(), itemStack.getItems().size());
                                            }
                                            for (ItemStack itemStack : machine.getOutPutItems()){
                                                ItemStack it = new ItemStack();
                                                for (Item item : itemStack.getItems()){
                                                    if (item instanceof Food)
                                                        it.getItems().add(new Food(item.getName(),item.getSize(),item.getPrice(),((Food) item).getEffect()));
                                                    if(item instanceof Resource)
                                                        it.getItems().add(new Resource(item.getName(), item.getSize(), item.getPrice()));
                                                }
                                                Main.Eisenhower.getBackPack().putInItemStack(it,it.getItems().size());
                                            }
                                            description.setText("Machine Utilized!");
                                        }
                                    });
                                    group2.getChildren().addAll(button);
                                }

                                if (mainBarn.getMachineRoom().hasJuicer){
                                    Button button = new Button("Juicer");
                                    button.setLayoutX(20 + 200 * (num / 10));
                                    button.setLayoutY(80 + (num % 10) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    group2.getChildren().addAll(button);
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            Group group3 = new Group();
                                            Label question = new Label("Choose a fruit:");
                                            question.setFont(new Font("Comic Sans MS", 25));
                                            question.setTextFill(Color.DARKGREEN);
                                            question.setLayoutX(10);
                                            question.setLayoutY(10);
                                            group3.getChildren().addAll(question);
                                            Scene scene = new Scene(group3, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                                            Stage stage = new Stage();
                                            stage.setScene(scene);
                                            stage.show();
                                            int num = 0;
                                                for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                                    Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                                    button.setLayoutX(20 + 200 * (num / 10));
                                                    button.setLayoutY(80 + (num % 10) * 35);
                                                    button.setFont(Main.comicFont);
                                                    button.setPrefWidth(180);
                                                    num++;
                                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                                        @Override
                                                        public void handle(MouseEvent event) {
                                                            //System.out.println(itemStack.getItems().get(0));
                                                            if (!(itemStack.getItems().get(0) instanceof Fruit)) {
                                                                question.setText("You should choose a fruit! ");
                                                                return;
                                                            }

                                                            Fruit fruit = (Fruit)itemStack.getItems().get(0);
                                                            Main.Eisenhower.getBackPack().takeItem(fruit.getName(),1);
                                                            Main.Eisenhower.getBackPack().putInItem(Producer.juice(fruit));
                                                            System.out.println(fruit.getName() + " juiced!");
                                                            stage.close();
                                                        }
                                                    });
                                                    group3.getChildren().add(button);
                                                }
                                        }
                                    });
                                }
                            }
                        });
                        return;
                    }


                    if (animalField == null)
                        return;
                    Group allAnimals = new Group();
                    Scene scene = new Scene(allAnimals,620, 700,  new ImagePattern(new Image("images\\white-texture.jpg")));
                    Stage stage = new Stage();
                    stage.setScene(scene);
                    stage.show();
                    Rectangle rectangle1 = new Rectangle(10, 10, 30, 30);
                    rectangle1.setFill(new ImagePattern(new Image("Images\\backButton.jpg")));
                    rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            rectangle1.setCursor(Cursor.HAND);
                        }
                    });
                    rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            scene.setRoot(allAnimals);
                        }
                    });
                    int num = 0;
                    ArrayList<Animal> animals = animalField.getAnimals();
                    for(int i = 0 ; i < animals.size(); i++){
                        Animal animal = animals.get(i);
                        Button button = new Button(animal.getName());
                        button.setPrefWidth(180);
                        button.setLayoutX(220);
                        button.setLayoutY(150 + num++ * 50);
                        AnimalField finalAnimalField = animalField;
                        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group innerGroup = new Group();
                                innerGroup.getChildren().addAll(rectangle1);
                                Label label = new Label(animal.toString());
                                label.setFont(Main.comicFont);
                                label.setLayoutY(50);
                                label.setLayoutX(50);

                                Button feedButton = new Button("Feed");
                                feedButton.setPrefWidth(180);
                                feedButton.setFont(Main.comicFont);
                                feedButton.setTextFill(Color.DARKGREEN);
                                feedButton.setLayoutX(220);
                                feedButton.setLayoutY(300);


                                Button sellButton = new Button("Sell for: $" + (animal.getPrice() * animal.health/100));
                                sellButton.setFont(Main.comicFont);
                                sellButton.setPrefWidth(180);
                                sellButton.setTextFill(Color.RED);
                                sellButton.setLayoutX(220);
                                sellButton.setLayoutY(350);

                                Button healButton = new Button("Heal");
                                healButton.setFont(Main.comicFont);
                                healButton.setPrefWidth(180);
                                healButton.setTextFill(Color.GREEN);
                                healButton.setLayoutX(220);
                                healButton.setLayoutY(400);

                                Button getButton = new Button("Get outcome");
                                getButton.setFont(Main.comicFont);
                                getButton.setPrefWidth(180);
                                getButton.setTextFill(Color.DARKBLUE);
                                getButton.setLayoutX(220);
                                getButton.setLayoutY(450);

                                sellButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        finalAnimalField.getAnimals().remove(animal);
                                        System.out.println(finalAnimalField.getAnimals().size());
                                        Main.Eisenhower.takeMoney(-(animal.getPrice() * animal.health/100));
                                        stage.close();
                                        drawAnimals();
                                        allAnimals.getChildren().clear();
                                        innerGroup.getChildren().clear();
                                    }
                                });

                                feedButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        Group group = new Group();
                                        Label question = new Label("Choose a(n) " + animal.getFoodName());
                                        question.setFont(new Font("Comic Sans MS", 25));
                                        question.setTextFill(Color.DARKGREEN);
                                        question.setLayoutX(10);
                                        question.setLayoutY(10);
                                        group.getChildren().addAll(question);
                                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                                        Stage stage = new Stage();
                                        stage.setScene(scene);
                                        stage.show();
                                        int num = 0;
                                        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                            Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                            button.setLayoutX(20 + 200 * (num / 10));
                                            button.setLayoutY(80 + (num % 10) * 35);
                                            button.setFont(Main.comicFont);
                                            button.setPrefWidth(180);
                                            num++;
                                            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                                @Override
                                                public void handle(MouseEvent event) {
                                                    //System.out.println(itemStack.getItems().get(0));
                                                    if (!itemStack.getItems().get(0).getName().equals(animal.getFoodName())) {
                                                        question.setText("You should choose a(n) " + animal.getFoodName() + "!");
                                                        return;
                                                    }
                                                    Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(), 1);
                                                    animal.Feed();
                                                    label.setText(animal.toString());
                                                    //System.out.println(field.getName() + " Plowed!");
                                                    stage.close();
                                                }
                                            });
                                            group.getChildren().add(button);
                                        }
                                    }
                                });

                                healButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        Group group = new Group();
                                        Label question = new Label("Choose an animal medicine ");
                                        question.setFont(new Font("Comic Sans MS", 25));
                                        question.setTextFill(Color.GREEN);
                                        question.setLayoutX(10);
                                        question.setLayoutY(10);
                                        group.getChildren().addAll(question);
                                        Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                                        Stage stage = new Stage();
                                        stage.setScene(scene);
                                        stage.show();
                                        int num = 0;
                                        for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                            Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                            button.setLayoutX(20 + 200 * (num / 10));
                                            button.setLayoutY(80 + (num % 10) * 35);
                                            button.setFont(Main.comicFont);
                                            button.setPrefWidth(180);
                                            num++;
                                            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                                @Override
                                                public void handle(MouseEvent event) {
                                                    //System.out.println(itemStack.getItems().get(0));
                                                    if (!itemStack.getItems().get(0).getName().equals("Animal Medicine")) {
                                                        question.setText("You should choose an animal medicine!");
                                                        return;
                                                    }
                                                    Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(), 1);
                                                    animal.heal();
                                                    label.setText(animal.toString());
                                                    //System.out.println(field.getName() + " Plowed!");
                                                    stage.close();
                                                }
                                            });
                                            group.getChildren().addAll(button);
                                        }
                                    }
                                });

                                getButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {

                                        if (animal.getToolName() != null) {
                                            Group group = new Group();
                                            Label question = new Label("Choose a(n) " + animal.getToolName());
                                            question.setFont(new Font("Comic Sans MS", 25));
                                            question.setTextFill(Color.GREEN);
                                            question.setLayoutX(10);
                                            question.setLayoutY(10);
                                            group.getChildren().addAll(question);
                                            Scene scene = new Scene(group, 620, 700, new ImagePattern(new Image("images\\white-texture.jpg")));
                                            Stage stage = new Stage();
                                            stage.setScene(scene);
                                            stage.show();
                                            int num = 0;
                                            for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                                Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                                button.setLayoutX(20 + 200 * (num / 10));
                                                button.setLayoutY(80 + (num % 10) * 35);
                                                button.setFont(Main.comicFont);
                                                button.setPrefWidth(180);
                                                num++;
                                                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                                    @Override
                                                    public void handle(MouseEvent event) {
                                                        //System.out.println(itemStack.getItems().get(0));
                                                        if (!itemStack.getItems().get(0).getName().equals(animal.getToolName())) {
                                                            question.setText("You should choose a(n) " + animal.getToolName() + "!");
                                                            return;
                                                        }
                                                        Tool tool = (Tool)itemStack.getItems().get(0);
                                                        if (!tool.canUseTool()){
                                                            if (tool.getHealth() <= 0){
                                                                question.setText("Tool is broken!");
                                                                return;
                                                            }
                                                            question.setText("You are too tired!");
                                                            return;
                                                        }
                                                        tool.abilityChange.affect();
                                                        int totalSize = 0;
                                                        for (ItemStack itemStack1 : animal.getOutcome())
                                                            totalSize += itemStack1.getItems().get(0).getSize();
                                                        if (totalSize > Main.Eisenhower.getBackPack().getRemainingCapacity()){
                                                            question.setText("Not enough space");
                                                            return;
                                                        }
                                                        for (ItemStack itemStack1 : animal.getOutcome()){
                                                            Main.Eisenhower.getBackPack().putInItemStack(itemStack1, itemStack1.getItems().size());
                                                        }
                                                        label.setText(animal.toString());
                                                        stage.close();
                                                    }
                                                });
                                                group.getChildren().addAll(button);
                                            }
                                        }else {
                                            for (ItemStack itemStack1 : animal.getOutcome()){
                                                Main.Eisenhower.getBackPack().putInItemStack(itemStack1, itemStack1.getItems().size());
                                            }
                                            stage.close();
                                        }
                                    }
                                });
                                
                                if (animal.getOutcome().size() > 0)
                                    innerGroup.getChildren().addAll(getButton);
                                innerGroup.getChildren().addAll(feedButton,sellButton,healButton);
                                innerGroup.getChildren().addAll(label);
                                scene.setRoot(innerGroup);
                            }
                        });
                        allAnimals.getChildren().addAll(button);
                    }
                }

                if (event.getCode() == KeyCode.B) {
                    if (Main.Eisenhower.canMove){
                        //backPackListView = Main.Eisenhower.getBackPack().viewList();
                        //farmGroup.getChildren().addAll(backPackListView);
                        PersonMenu personMenu = new PersonMenu(barnScene,primaryStage);
                        try {
                            Stage mystage = new Stage();
                            mystage.setX(41);
                            personMenu.start(mystage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else {
                        Main.Eisenhower.canMove = true;
                        barnGroup.getChildren().remove(backPackListView);
                    }
                }

            }
        });
    }
    Pair<Integer, Integer>[] cowPlaces = new Pair[6];
    Pair<Integer, Integer>[] sheepPlaces = new Pair[6];
    Pair<Integer, Integer>[] chickenPlaces = new Pair[6];
    ImageView[] cowsUI = new ImageView[6];
    ImageView[] sheepsUI = new ImageView[6];
    ImageView[] chickensUI = new ImageView[6];


    void drawAnimals(){
        System.out.println(mainBarn);
        for (int i = 0 ; i < 6; i++)
        {
            cowsUI[i].setImage(null);
            sheepsUI[i].setImage(null);
            chickensUI[i].setImage(null);

        }
        ArrayList<Animal> cows = mainBarn.getAnimalFields().get(0).getAnimals();
        for(int i = 0 ; i < cows.size(); i++) {
            if (cows.get(i) == null) {
                cowsUI[i].setImage(null);
                continue;
            } else {
                cowsUI[i].setImage(new Image("Images\\barnAnimals\\cow.png"));
            }
        }
        ArrayList<Animal> sheeps = mainBarn.getAnimalFields().get(1).getAnimals();
        for(int i = 0 ; i < sheeps.size(); i++) {
            if (sheeps.get(i) == null) {
                sheepsUI[i].setImage(null);
                continue;
            } else {
                sheepsUI[i].setImage(new Image("Images\\barnAnimals\\sheep.png"));
            }
        }
        ArrayList<Animal> chickens = mainBarn.getAnimalFields().get(2).getAnimals();
        for(int i = 0 ; i < chickens.size(); i++) {
            if (chickens.get(i) == null) {
                chickensUI[i].setImage(null);
                continue;
            } else {
                chickensUI[i].setImage(new Image("Images\\barnAnimals\\chicken.png"));
            }
        }
    }
}
