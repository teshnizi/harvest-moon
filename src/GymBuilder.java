import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by pc on 7/8/2017.
 */
public class GymBuilder extends Application {
    Stage mystage;
    public GymBuilder(Stage stage){
        this.mystage = stage;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,620,700,new ImagePattern(new Image("images\\white-texture.jpg")));
        Text text = new Text("Gym");
        text.setX(250);
        text.setY(50);
        text.setFont(new Font(40));
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.E) {
                    primaryStage.close();
                    mystage.show();
                }
            }
        });
        Rectangle rectangle = new Rectangle(30,30);
        rectangle.setFill(new ImagePattern(new Image("images\\yoga.png")));
        rectangle.setVisible(false);

        Rectangle rectangle1 = new Rectangle(30,30);
        rectangle1.setFill(new ImagePattern(new Image("images\\running.png")));
        rectangle1.setVisible(false);

        Rectangle rectangle2 = new Rectangle(30,30);
        rectangle2.setFill(new ImagePattern(new Image("images\\dumbbell.png")));
        rectangle2.setVisible(false);

        Text text1 = new Text("Exercises:");
        text1.setX(150);
        text1.setY(90);
        text1.setFont(new Font(25));
        Text text2 = new Text();
        text2.setX(100);
        text2.setY(600);
        text2.setFont(Main.comicFont);
        group.getChildren().add(text1);
        Gym gym = Main.village.getGym();
        Button button = new Button("Yoga");
        button.setLayoutY(150);
        button.setLayoutX(200);
        button.setPrefSize(200,20);
        button.setFont(Main.comicFont);
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setCursor(Cursor.HAND);
                text2.setText("this exercise increases your health by 50 and costs 100$");
                rectangle.setLayoutX(160);
                rectangle.setLayoutY(150);
                rectangle.setVisible(true);
            }
        });
        Button button1 = new Button("Running");
        button1.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button1.setCursor(Cursor.HAND);
                text2.setText("this exercise increases your energy by 50 and costs 50$");
                rectangle1.setLayoutX(160);
                rectangle1.setLayoutY(200);
                rectangle1.setVisible(true);
            }
        });
        button1.setLayoutY(200);
        button1.setLayoutX(200);
        button1.setPrefSize(200,20);
        button1.setFont(Main.comicFont);
        Button button2 = new Button("Bodybuilding");
        button2.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button2.setCursor(Cursor.HAND);
                text2.setText("this exercise increases your stamina by 50 costs 100$");
                rectangle2.setLayoutX(160);
                rectangle2.setLayoutY(250);
                rectangle2.setVisible(true);
            }
        });
        button2.setLayoutY(250);
        button2.setLayoutX(200);
        Text text3 = new Text();
        button2.setPrefSize(200,20);
        button2.setFont(Main.comicFont);
        Person person = Main.Eisenhower;
        scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                text2.setText(" ");
                text3.setText(" ");
                rectangle.setVisible(false);
                rectangle1.setVisible(false);
                rectangle2.setVisible(false);
            }
        });

        text3.setLayoutX(200);
        text3.setLayoutY(560);
        text3.setFont(Main.comicFont);
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Exercise exercise = gym.exercises.get(1);

                if (person.getMoney() < exercise.cost) {
                    text3.setText("Not enough money!");
                } else {
                    boolean flag = false;
                    for (AbilityChange abilityChange : exercise.effect.getAbilityChanges()) {
                        if (abilityChange.getAbility().equals(person.getHealth()) && person.getHealth().getMaxValue() >= 1000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        if (abilityChange.getAbility().equals(person.getEnergy()) && person.getEnergy().getMaxValue() >= 2000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        if (abilityChange.getAbility().equals(person.getStamina()) && person.getSatiety().getMaxValue() >= 2000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        abilityChange.affect();
                        flag = true;
                    }
                    if (flag) {
                        person.takeMoney(exercise.cost);
                        text3.setText(text3.getText() + "\nHOof! Exercise done!");
                    }
                }
            }
        });
        button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Exercise exercise = gym.exercises.get(0);

                if (person.getMoney() < exercise.cost) {
                    text3.setText("Not enough money!");
                } else {
                    boolean flag = false;
                    for (AbilityChange abilityChange : exercise.effect.getAbilityChanges()) {
                        if (abilityChange.getAbility().equals(person.getHealth()) && person.getHealth().getMaxValue() >= 1000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        if (abilityChange.getAbility().equals(person.getEnergy()) && person.getEnergy().getMaxValue() >= 2000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        if (abilityChange.getAbility().equals(person.getStamina()) && person.getSatiety().getMaxValue() >= 2000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        abilityChange.affect();
                        flag = true;
                    }
                    if (flag) {
                        person.takeMoney(exercise.cost);
                        text3.setText(text3.getText() + "\nHOof! Exercise done!");
                    }
                }
            }
        });
        button2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Exercise exercise = gym.exercises.get(2);

                if (person.getMoney() < exercise.cost) {
                    text3.setText("Not enough money!");
                } else {
                    boolean flag = false;
                    for (AbilityChange abilityChange : exercise.effect.getAbilityChanges()) {
                        if (abilityChange.getAbility().equals(person.getHealth()) && person.getHealth().getMaxValue() >= 1000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        if (abilityChange.getAbility().equals(person.getEnergy()) && person.getEnergy().getMaxValue() >= 2000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        if (abilityChange.getAbility().equals(person.getStamina()) && person.getSatiety().getMaxValue() >= 2000) {
                            text3.setText("Max value reached!");
                            continue;
                        }
                        abilityChange.affect();
                        flag = true;
                    }
                    if (flag) {
                        person.takeMoney(exercise.cost);
                        text3.setText(text3.getText() + "\nHOof! Exercise done!");
                    }
                }
            }
        });

        int num = 0;
        for (Exercise exercise : Main.addedExercises){


            // agar tamrin anjam shavad "for" zir bayad ejra shavad:
            /*for (AbilityChange abilityChange : exercise.effect.getAbilityChanges()){
                abilityChange.affect();
            }*/

        }
        group.getChildren().addAll(button,button1,button2);
        group.getChildren().add(text);
        group.getChildren().addAll(text2,text3,rectangle,rectangle1,rectangle2);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
