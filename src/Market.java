
import java.util.*;

/**
 * 
 */
public class Market extends Environment {



    /**
     * 
     */
    private ArrayList<Store> stores;

    public ArrayList<Store> getStores() {
        return stores;
    }

    /**
     * Default constructor
     *
     * @param name
     */
    public Market(String name) {
        super(name);
        stores = new ArrayList<>();
    }

    @Override
    public void nextDay(Season season){
        for(Store store : stores)
            store.nextDay(season);
    }

    @Override
    public Thing findObject(String objectName) {
        for (Store store : stores){
            if(store.getName().equals(objectName))
                return store;
        }
        return null;
    }

    @Override
    public ArrayList<String> listOfObjects() {
        ArrayList<String> s = new ArrayList<>();
        for(Store store : stores)
            s.add(store.getName());
        return s;
    }


    @Override
    public  ArrayList<String> listOfPlaces(){
        ArrayList<String> ret = new ArrayList<>();

        for(JA neighbour : neighbours)
            ret.add(neighbour.getName());
        for (Place place : places)
            ret.add(place.getName());
        return ret;
    }

}