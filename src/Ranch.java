
import java.util.*;

/**
 * 
 */
public class Ranch extends Store {

    /**
     * Default constructor
     */

    Barn barn;
    public  int friendly = 50;
    ArrayList<String> strings = new ArrayList<>();

    public Ranch(String name, Barn barn) {
        super(name);
        this.barn = barn;
        strings.add("Butchery");
    }

    @Override
    public void nextDay(Season season) {
        for (int i = 0; i < 10; i++) {
            storage.putInItem(Producer.animalMedicine());
            storage.putInItem(Producer.grain());
            storage.putInItem(Producer.alfalfa());
        }
    }

}