
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * 
 */
public class Thing implements Serializable{

    /**
     * Default constructor
     */
    public Thing(){
    }
    public Thing(String name) {
        this.name = name;
        commands = new ArrayList<>();
    }

    /**
     * 
     */
    private String name;
    public String getName(){
        return name;
    }
    /**
     * 
     */
    public ArrayList<String> commands;

    public ArrayList<String> getCommands() {
        return commands;
    }

    public void nextDay(Season season) throws IOException {};
    /**
     * @return
     */

}