/**
 * Created by Tigerous215 on 5/14/2017.
 */
public class KitchenTool extends Tool {
    public KitchenTool(String name, int size, int health, int breakingProbablity){
        super(name, size, health, breakingProbablity, null);
    }

    @Override
    public String status(){
        return this.getName() + "\nUsed for cooking";
    }
}
