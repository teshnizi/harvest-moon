
import jdk.management.resource.internal.inst.FileOutputStreamRMHooks;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;

/**
 * has CropBlocks
 */
public class Field extends Thing  implements Serializable {

    /**
     * Default constructor
     */
    public Field(String name, int maximumSize) {
        super(name);
        this.maximumSize = maximumSize;
    }

    @Override
    public void nextDay(Season season){
        for (Crop crop : crops)
        if (crop != null){
            crop.nextDay(season);
        }
        for (int i = 0 ; i < 9 ; i++)
            if (crops.get(i) == null || !(crops.get(i).alive)){
                crops.set(i,null);
            }
    }


    ArrayList<Crop> crops = new ArrayList<>();
    {
        for (int i = 0 ; i < 9 ; i++)
            crops.add(null);
    }
    boolean[] plowed = new boolean[9];
    {
        for (int i = 0 ; i < 9 ; i++)
                plowed[i] = false;
    }

    public void plowAll(){
        for (int i = 0 ; i < 9 ; i++)
                plowed[i] = true;
    }
    public ArrayList<Crop> getCrops() {
        return crops;
    }

    int maximumSize;

    public void plantCrop(Crop crop){
        if(crops.size() >= maximumSize)
            return;
        crops.add(crop);
    }


    /**
     * @return
     */
    public String status() {
        // TODO implement here
        return "";
    }

    /**
     * @return
     */
    public void water() {
        // TODO implement here
    }

    /**
     * @return
     */
    public ArrayList<String> objects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("");
        return s;
    }

}