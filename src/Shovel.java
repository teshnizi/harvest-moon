import java.util.ArrayList;

/**
 * Created by Tigerous215 on 5/11/2017.
 */
public class Shovel extends Tool {
    /**
     * Default constructor
     *
     * @param name
     */

    public Shovel(String name, int health, int breakingProbablity, AbilityChange abilityChange , int type) {
        super(name, 3, health, breakingProbablity, abilityChange);
        this.type = type;
        newbuilders = new ArrayList<>();
        repairbuilders = new ArrayList<>();
        if (type == 1){

            newprice = 100;
            newbuilders.add(new Builder(5,Producer.wood(0),newprice));
            newbuilders.add(new Builder(5,Producer.stone(0),newprice));

            repairprice = 10;
            repairbuilders.add(new Builder(2,Producer.wood(0),repairprice));
            repairbuilders.add(new Builder(2,Producer.stone(0),repairprice));
        }
        if (type == 2){

            newprice = 500;

            newbuilders.add(new Builder(4,Producer.wood(1),newprice));
            newbuilders.add(new Builder(4,Producer.stone(1),newprice));

            repairprice = 50;
            repairbuilders.add(new Builder(2,Producer.wood(1),repairprice));
            repairbuilders.add(new Builder(2,Producer.stone(1),repairprice));
        }
        if (type == 3){

            newprice = 1000;
            newbuilders.add(new Builder(3,Producer.wood(2),newprice));
            newbuilders.add(new Builder(3,Producer.stone(2),newprice));

            repairprice = 100;
            repairbuilders.add(new Builder(1,Producer.wood(2),repairprice));
            repairbuilders.add(new Builder(1,Producer.stone(2),repairprice));
        }
        if (type == 4){

            newprice = 4000;
            newbuilders.add(new Builder(2,Producer.wood(3),newprice));
            newbuilders.add(new Builder(2,Producer.stone(3),newprice));

            repairprice = 400;
            repairbuilders.add(new Builder(1,Producer.wood(3),repairprice));
            repairbuilders.add(new Builder(1,Producer.stone(3),repairprice));
        }
        setPrice(newprice);
    }

    public void plow(){
        health -= (5 - type)*5;
        abilityChange.affect();
    }

    @Override
    public String status(){
        String kind = " ";
        String s;
        int range = 0;
        int num = 0;
        String broken;
        if (health > 0)
            broken = " Not Broken!";
        else
            broken = "Broken!";
        if (type == 1)
        {
            kind = "Stone";
            range = 1;
            num = 150 ;
        }
        if (type == 2)
        {
            kind = "Iron";
            range = 2;
            num = 80 ;
        }
        if (type == 3)
        {
            kind = "Silver";
            range = 3;
            num = 40 ;
        }
        if (type == 4)
        {
            kind = "Adantanium";
            range = 4;
            num = 20 ;
        }
        s = kind + " Shovel. With this shovel you can dig " + range + " ranges in one go.\nEnergy required for each use : " + num + "\n" +"Health: "+ health + "\n"  +  broken;
        return s;
    }
    @Override
    public ArrayList<String > madeof()
    {
        ArrayList<String > s = new ArrayList<>();
        if (type == 1)
            s.add("this is Stone Shovel and made of");
        if (type == 2)
            s.add("this is Iron Shovel and made of");
        if (type == 3)
            s.add("this is Silver Shovel and made of");
        if (type == 4)
            s.add("this is Adamantium Shovel and made of");
        for (Builder builder : newbuilders)
            s.add(builder.getItems().getName() + " x" + builder.getNum());
        s.add("price is: "+newbuilders.get(0).getPrice());
        return  s;
    }
}
