import sun.security.tools.keytool.Resources_es;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/11/2017.
 */
public class CropMenu extends Menu{


    public CropMenu(Crop crop, Field fatherField){
        this.crop = crop;
        this.fatherField = fatherField;
    }

    Crop crop;
    Field fatherField;

    @Override
    public Menu getCommand(int x, Person person){
        Storage backPack = person.getBackPack();

        if(crop.alive) {
            if (x == 1) {
                if (crop.age >= crop.matureAge * 2) {
                    crop.getOutCome().changeToJunk();
                }
                System.out.println(crop.status());
                return null;
            }

            //Water crop
            if (x == 2) {
                try {
                    System.out.println("Choose A watering Can:");
                    ItemStack tmp = backPack.getItemStackFromPlayer();
                    WateringCan wateringCan = (WateringCan) tmp.getItems().get(0);
                    if (wateringCan.getHealth() == 0) {
                        System.out.println("Watering can is broken!");
                        return null;
                    }
                    if (wateringCan.getWaterLevel() >= 1 && wateringCan.canUseTool()) {
                        System.out.println("Watering Done!!");
                        wateringCan.water();
                        crop.water();
                    } else {
                        System.out.println("Watering Can is empty!");
                    }
                } catch (Exception e) {
                    System.out.println("You should choose a watering can!");
                }
            }

            //Harvest
            if (x == 3) {
                crop.checkIfIsAlive();
                if (crop.alive == false) {
                    System.out.println("This Crop is Dead!");
                }
                if (crop.age < crop.matureAge) {

                    System.out.println("This crop is too young to harvest!");
                    return null;
                }

                //if don't harvest fruits for a long time they become junk
                if (crop.age >= crop.matureAge * 2) {
                    crop.getOutCome().changeToJunk();
                }
                System.out.println("This plant has:");
                ItemStack outcome = crop.getOutCome();
                System.out.println(outcome.toString());

                try {
                    ArrayList<Item> newCrops = new ArrayList<>();
                    if (outcome.getItems().get(0) instanceof Fruit) {
                        Fruit fruit = (Fruit) outcome.getItems().get(0);
                        Random random = new Random();
                        int number = random.nextInt() % 3 + 2;
                        for (int k = 0; k < number; k++) {
                            newCrops.add(new Fruit(fruit.getName(), fruit.getSize(), fruit.getPrice(), fruit.getEffect()));
                        }
                        if(crop.addedFruitsFertilization){
                            outcome.getItems().add(new Fruit(fruit.getName(),fruit.getSize(),fruit.getPrice(),fruit.getEffect()));
                            crop.addedFruitsFertilization = false;
                        }
                        System.out.println(newCrops.get(0).getName());
                    }

                    crop.age = 0;
                    crop.currentNumberOfHarvests++;

                    backPack.putInItemStack(outcome, outcome.getItems().size());
                    crop.setOutCome(new ItemStack(newCrops));
                    System.out.println(crop.getOutCome().getItems().get(0));
                    System.out.println("Harvesting Done!");
                    //outcome.getItems().add()
                } catch (Exception e) {
                    System.out.println("BadCommand!");
                }
                System.out.println(crop.alive);
            }

            if( x == 5 ) {
                System.out.println("Choose a fertilizer:");
                ItemStack itemStack = person.getBackPack().getItemStackFromPlayer();
                if (!(itemStack.getItems().get(0).getName().equals("Urine") || itemStack.getItems().get(0).getName().equals("Ammonia"))) {
                    System.out.println("You should choose a fertilizer!");
                    return null;
                }
                try {
                    Resource fertilizer = (Resource) itemStack.getItems().get(0);
                    if(fertilizer.getName().equals("Urine"))
                        crop.addedFruitsFertilization = true;
                    if (fertilizer.getName().equals("Ammonia"))
                        if(crop.age < crop.matureAge)
                            crop.age++;
                    return null;
                }
                catch (Exception E){
                    System.out.println("BadCommand!");
                    return null;
                }
            }
            if(x == 4){
                fatherField.getCrops().remove(crop);
                System.out.println("Crop destroyed!");
            }
        }
        else {
            if(x == 1){
                fatherField.getCrops().remove(crop);
                System.out.println("Crop Destroyed!");
            }
        }
        return null;
    }

    @Override
    public ArrayList<String> whereAmI(){
        System.out.println(crop.waterLevel);
        crop.checkIfIsAlive();
        ArrayList<String> s = new ArrayList<>();
        if(crop.alive) {
            s.add("This is a Crop: " + crop.getName());
            s.add("1.Status");
            s.add("2.Water This Crop");
            s.add("3.Harvest Crop");
            s.add("4.Destroy Crop");
            s.add("5.Fertilize Crop");
        }
        else {
            s.add("This is a dead Crop");
            s.add("1.Destroy");
        }
        return s;
    }
}
