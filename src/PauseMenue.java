import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;

/**
 * Created by pc on 7/16/2017.
 */
public class PauseMenue extends Application {
    Scene scene;
    public PauseMenue(Scene scene){
       this.scene = scene;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene menuscenc = new Scene(group,1280,960);

        ImageView backGround = new ImageView(new Image("Images\\BackGround2.gif"));
        backGround.setFitHeight(960);
        backGround.setFitWidth(1280);

        group.getChildren().addAll(backGround);

        Button playButton = new Button("Resume");
        playButton.setTextFill(Color.DIMGREY);
        playButton.setShape(new Ellipse(100, 50));
        playButton.setFont(Main.menuFont);
        playButton.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Main.Eisenhower.canMove = true;
                Main.timeline.play();
                playButton.setCursor(Cursor.HAND);
            }
        });
        playButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setScene(scene);
                primaryStage.show();
            }
        });

        Button customButton = new Button("Friends");
        customButton.setTextFill(Color.DIMGREY);
        customButton.setShape(new Ellipse(100, 50));
        customButton.setFont(Main.menuFont);
        customButton.setVisible(false);
        if (Person.friends.size() > 0)
            customButton.setVisible(true);
        customButton.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                customButton.setCursor(Cursor.HAND);
            }
        });
        customButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                FriendsMenu friendsMenu = new FriendsMenu(primaryStage,scene);
                Stage stage = new Stage();
                try {
                    friendsMenu.start(stage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Button exitButton = new Button("EXIT");
        exitButton.setTextFill(Color.DIMGREY);
        exitButton.setShape(new Ellipse(100, 50));
        exitButton.setFont(Main.menuFont);
        exitButton.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                exitButton.setCursor(Cursor.HAND);
            }
        });
        exitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Platform.exit();
            }
        });

        playButton.setLayoutX(menuscenc.getWidth() / 4 - 80);
        playButton.setLayoutY(menuscenc.getHeight() / 4 - 40);
        exitButton.setLayoutX(menuscenc.getWidth() / 4 - 80);
        exitButton.setLayoutY(3 * menuscenc.getHeight() / 4 - 40);
        customButton.setLayoutX(3 * menuscenc.getWidth() / 4 - 120);
        customButton.setLayoutY(menuscenc.getHeight() / 4 - 40);

        group.getChildren().addAll(playButton,customButton,exitButton);
        primaryStage.setScene(menuscenc);
        primaryStage.show();
    }
}
