
import java.util.*;

/**
 * 
 */
public class Wood extends Resource {

    /**
     * Default constructor
     */
    private int type;
    public Wood(String name ,int type , int price){
        super(name, 1, price);
        this.type = type;
    }

    public int getType() {
        return type;
    }
}