import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by pc on 7/10/2017.
 */
public class CafeBuilder extends Application {
    int num =0;
    int food = 0;
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,600,720,new ImagePattern(new Image("images\\white-texture.jpg")));

        Text text = new Text("Cafe");
        text.setX(250);
        text.setY(50);
        text.setFont(new Font(40));
        group.getChildren().add(text);

        Text text1 = new Text("Item inside Cafe");
        text1.setX(200);
        text1.setY(100);
        text1.setFont(Main.comicFont);
        group.getChildren().add(text1);

        Cafe cafe = Main.village.getCafe();

        num = 0;

        Text explain = new Text();
        explain.setLayoutX(50);
        explain.setLayoutY(400);
        explain.setFont(Main.comicFont);
        group.getChildren().addAll(explain);

        Text statu = new Text();
        statu.setLayoutX(400);
        statu.setLayoutY(500);
        statu.setFont(Main.comicFont);
        group.getChildren().addAll(statu);

        Button buy = new Button("Buy");
        buy.setLayoutX(400);
        buy.setLayoutY(400);
        buy.setFont(Main.comicFont);
        buy.setVisible(false);
        group.getChildren().addAll(buy);
        buy.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                buy.setCursor(Cursor.HAND);
            }
        });
        buy.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Person person = Main.Eisenhower;
                Meal meal = null;
                int k = 0;
                for (Meal meal1 : cafe.meals)
                {
                    if(k == food)
                    {
                        meal = meal1;
                    }
                    k++;
                }
                if(person.getMoney() <= meal.getPrice()){
                    statu.setText("Sorry, but you cannot afford this meal!");
                }
                else {
                    person.takeMoney(meal.getPrice());
                    meal.effect.affect();
                    statu.setText("YUMMY!");
                }
            }
        });


        for (Meal meal : cafe.meals){

            Button button = new Button(meal.getName());
            button.setLayoutX(50 + 150*(num/10));
            button.setLayoutY(100 + 50*(num%10));
            button.setPrefSize(150,20);
            button.setFont(Main.comicFont);
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setCursor(Cursor.HAND);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    explain.setText(meal.status() + "Price : " + meal.getPrice() + "$");
                    buy.setVisible(true);
                    int k = 0 ;
                    for(Meal meal1 : cafe.meals){
                        if (button.getText().equals(meal1.getName()))
                            break;
                        k++;
                    }
                    food = k;
                }
            });


            group.getChildren().addAll(button);
            num++;
        }

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
