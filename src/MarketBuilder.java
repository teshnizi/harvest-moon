import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by tigerous215 on 7/6/2017.
 */
public class MarketBuilder extends Application{

    static int numinimage = 3;
    private Market mainMarket;
    static boolean inmarket = false;
    static Group marketGroup;
    Scene marketScene;
    ListView backPackListView;
    Font menuFont = new Font("Cambria",48);
    static ArrayList<Pair<Pair<String,ImageView>,Pair<Integer,Integer>>> chars = new ArrayList<>();
    static HashSet<Pair<Integer,Integer>> marketAvailableTiles = new HashSet<>();
    static {

        for(int i = -1 ; i < 28 ; i++) {
            marketAvailableTiles.add(new Pair<>(i, 15));
            marketAvailableTiles.add(new Pair<>(i, 16));
        }
    }
    public MarketBuilder(Market market){
        this.mainMarket = market;
    }

    @Override
    public void start(Stage primaryStage){
        Button spacebutton = new Button("Enter");
        spacebutton.setLayoutX(Main.character.getX()-10);
        spacebutton.setLayoutY(Main.character.getY() -25);
        spacebutton.setVisible(false);

        Button inspectbutton = new Button("Inspect");
        inspectbutton.setVisible(false);

        marketGroup = new Group();
        marketGroup.getChildren().addAll(spacebutton,inspectbutton);
        marketScene = new Scene(marketGroup,1280,960);
        Image market = new Image("maps\\market.png");
        marketGroup.getChildren().addAll(Main.character);
        primaryStage.setScene(marketScene);
        primaryStage.show();

        marketScene.setFill(new ImagePattern(market));
        marketScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage % 4 + 1;
                Pair<Integer, Integer> next;
                DataOutputStream dout = null;
                try {
                    if (Main.online)
                    dout = new DataOutputStream(Getter.socket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (Main.Eisenhower.canMove) {
                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png"));
                        MovementHandler.move(2, marketAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\left" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png"));
                        MovementHandler.move(0, marketAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\right" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, marketAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\b" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png"));
                        MovementHandler.move(3, marketAvailableTiles, Main.character);
                        try {
                            if (Main.online)
                            dout.writeUTF(mystring("Images\\f" + numinimage + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (event.getCode() == KeyCode.B) {
                    //backPackListView = Main.Eisenhower.getBackPack().viewList();
                    //farmGroup.getChildren().addAll(backPackListView);
                    PersonMenu personMenu = new PersonMenu(marketScene, primaryStage);
                    try {
                        Stage mystage = new Stage();
                        mystage.setX(40);
                        personMenu.start(mystage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.I){
                    if (isis()){
                        String h = isisstring();
                        Inspectbuilder inspectbuilder = new Inspectbuilder(h);
                        Stage stage = new Stage();
                        try {
                            inspectbuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (event.getCode() == KeyCode.ESCAPE){
                    Main.Eisenhower.canMove = false;
                    Main.timeline.pause();
                    PauseMenue pauseMenue = new PauseMenue(marketScene);
                    try {
                        pauseMenue.start(primaryStage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (event.getCode() == KeyCode.E ) {
                    if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(8, 15)) || MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(9, 15))) {
                        Market market1 = Main.market;
                        Store mystore = null;
                        for (Store store : market1.getStores()) {
                            if (store.getName().equals("General Store")) {
                                mystore = store;
                                break;
                            }
                        }
                        StoreBuilder storeBuilder = new StoreBuilder(mystore);
                        Stage stage = new Stage();
                        try {
                            storeBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(17, 15)) || MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(18, 15))){

                        Market market1 = Main.market;
                        Store mystore = null;
                        for (Store store : market1.getStores()) {
                            if (store.getName().equals("Groceries Store")) {
                                mystore = store;
                                break;
                            }
                        }
                        StoreBuilder storeBuilder = new StoreBuilder(mystore);
                        Stage stage = new Stage();
                        try {
                            storeBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(25, 15)) || MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(26, 15))){

                        Market market1 = Main.market;
                        Store mystore = null;
                        for (Store store : market1.getStores()) {
                            if (store.getName().equals("Butchery")) {
                                mystore = store;
                                break;
                            }
                        }
                        StoreBuilder storeBuilder = new StoreBuilder(mystore);
                        Stage stage = new Stage();
                        try {
                            storeBuilder.start(stage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(8,15))|| MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(9, 15)))
                {
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);

                }
                else if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(17, 15))||MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(18, 15))){
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if (MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(25, 15))||MovementHandler.pixelToTile(Main.character.getX(), Main.character.getY()).equals(new Pair<>(26, 15))){
                    spacebutton.setLayoutX(Main.character.getX() -5);
                    spacebutton.setLayoutY(Main.character.getY() -25);
                    spacebutton.setVisible(true);
                }
                else if(isis()){
                    inspectbutton.setLayoutX(Main.character.getX() -5);
                    inspectbutton.setLayoutY(Main.character.getY() -25);
                    inspectbutton.setVisible(true);
                }
                else {
                    spacebutton.setVisible(false);
                    inspectbutton.setVisible(false);
                }
                if (Main.character.getX() < 0){
                    inmarket = false;
                    VillageBuilder.invillage = true;
                    Main.character.setX(marketScene.getWidth()-32);
                    Main.villageBuilder.start(primaryStage);
                }
                numinimage++;
            }
        });
    }
    public boolean isis(){
        int i = 0;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                return true;
            }
        }
        return false;
    }
    public String isisstring(){
        String h = null;
        for (Pair<Pair<String,ImageView>,Pair<Integer,Integer>> p : chars){
            if (MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).equals(new Pair<>(p.getValue().getKey() + 1 , p.getValue().getValue())))
            {
                h= p.getKey().getKey();
            }
        }
        return h;
    }
    public String mystring(String st){
        String ans;
        ans = Getter.socketname + Protocol.protocol + Protocol.character + Protocol.protocol +Protocol.market +Protocol.protocol+st + Protocol.protocol +MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getKey() +Protocol.protocol  + MovementHandler.pixelToTile(Main.character.getX(),Main.character.getY()).getValue();
        return ans;
    }
}
