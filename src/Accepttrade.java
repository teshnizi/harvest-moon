import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by pc on 7/16/2017.
 */
public class Accepttrade extends Application {
    String name;
    public Accepttrade(String s){
        name =s;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,300,485,new ImagePattern(new Image("images\\white-texture.jpg")));

        Button accept = new Button("Accept");
        accept.setLayoutX(70);
        accept.setLayoutY(150);
        accept.setPrefSize(150,30);
        accept.setStyle("-fx-background-color: white");
        accept.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                accept.setStyle("-fx-background-color: green");
                accept.setCursor(Cursor.HAND);
            }
        });
        accept.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    DataOutputStream dout  = new DataOutputStream(Getter.socket.getOutputStream());
                    dout.writeUTF(Getter.socketname + Protocol.protocol + Protocol.opentrade + Protocol.protocol+name);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Trade trade = new Trade(name);
                try {
                    trade.start(primaryStage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Button decline  = new Button("Decline");
        decline.setLayoutX(70);
        decline.setLayoutY(250);
        decline.setPrefSize(150,30);
        decline.setStyle("-fx-background-color: white");
        decline.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                decline.setStyle("-fx-background-color: red");
                decline.setCursor(Cursor.HAND);
            }
        });
        decline.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.close();
            }
        });

        group.getChildren().addAll(accept,decline);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
