import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/19/2017.
 */
public class CityHallMenu extends Menu{

    CityHall cityHall;

    public CityHallMenu(CityHall cityHall){
        this.cityHall = cityHall;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in CityHall");
        s.add("1.Buy new Field $2000");
        s.add("2.Buy new FruitGarden $2000");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        Scanner scanner = new Scanner(System.in);
        try{
            if(person.getMoney() < 2000){
                System.out.println("You poor! Get out of my face!");
                return null;
            }
            System.out.println("Choose a name: ");
            String s = scanner.nextLine();
            if ( x == 1 )
                cityHall.farm.getFields().add(new Field(s,9));
            else
                cityHall.farm.getFruitGardens().add(new FruitGarden(s));
            person.takeMoney(2000);
            System.out.println("Here is the Deed! thank's for purchasing!");
        }
        catch (Exception E){
            System.out.println("BadCommand!");
            return null;
        }
        return null;
    }
}
