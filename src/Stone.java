import sun.misc.*;

/**
 * Created by pc on 5/15/2017.
 */
public class Stone extends Resource {
    private int type;
    public Stone(String name , int type, int price){
        super(name , 1, price);
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
