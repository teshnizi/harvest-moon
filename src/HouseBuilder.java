import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by tigerous215 on 7/6/2017.
 */
public class HouseBuilder extends Application {


    static int numinimage = 3;
    private Home mainHome;
    Group homeGroup;
    Scene homeScene;
    ListView backPackListView;
    Font menuFont = new Font("Cambria",48);

    static HashSet<Pair<Integer,Integer>> houseAvailableTiles = new HashSet<>();
    static {

        for(int i = 15 ; i < 34 ; i++) {
            for(int j = 7 ; j < 25 ; j++) {
                houseAvailableTiles.add(new Pair<>(i,j));
            }
        }

        for(int i = 9 ; i < 13 ; i++) {
            for(int j = 11 ; j < 15 ; j++) {
                houseAvailableTiles.add(new Pair<>(i,j));
            }
        }

        for(int i = 20 ; i < 34 ; i++) {
            for(int j = 17 ; j < 25 ; j++) {
                houseAvailableTiles.remove(new Pair<>(i,j));
            }
        }

        for(int i = 23 ; i < 40 ; i++) {
            for(int j = 7 ; j < 30 ; j++) {
                houseAvailableTiles.remove(new Pair<>(i,j));
            }
        }

        for(int i = 15 ; i < 19 ; i++) {
            for(int j = 9 ; j < 12 ; j++) {
                houseAvailableTiles.remove(new Pair<>(i,j));
            }
        }
        houseAvailableTiles.remove(new Pair<>(15,8));
        houseAvailableTiles.remove(new Pair<>(15,7));

        for (int i = 10 ; i < 16 ; i++){
            houseAvailableTiles.add(new Pair<>(i,20));
            houseAvailableTiles.add(new Pair<>(i,21));
        }
        houseAvailableTiles.add(new Pair<>(13,13));
        houseAvailableTiles.add(new Pair<>(13,14));
        houseAvailableTiles.add(new Pair<>(14,13));
        houseAvailableTiles.add(new Pair<>(14,14));
        for(int i = 18 ; i < 22; i++){
            for (int j = 24 ; j < 31 ; j++){
                houseAvailableTiles.add(new Pair<>(i, j));
            }
        }
    }
    public HouseBuilder(Home home){
        this.mainHome = home;
    }


    @Override
    public void start(Stage primaryStage){
        homeGroup = new Group();
        homeScene = new Scene(homeGroup,1280,960);
        Image home = new Image("maps\\house.png");
        Button inspectButton = new Button("Inspect");
        homeGroup.getChildren().addAll(inspectButton);
        homeGroup.getChildren().addAll(Main.character);
        primaryStage.setScene(homeScene);
        primaryStage.show();

        homeScene.setFill(new ImagePattern(home));
        homeScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                numinimage = numinimage % 4 + 1;
                Pair<Integer, Integer> next;
                if (Main.Eisenhower.canMove) {

                    if (event.getCode() == KeyCode.LEFT) {
                        Main.character.setImage(new Image("Images\\left" + numinimage + ".png"));
                        MovementHandler.move(2, houseAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.RIGHT) {
                        Main.character.setImage(new Image("Images\\right" + numinimage + ".png "));
                        MovementHandler.move(0, houseAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.UP) {
                        Main.character.setImage(new Image("Images\\b" + numinimage + ".png"));
                        MovementHandler.move(1, houseAvailableTiles, Main.character);
                    }
                    if (event.getCode() == KeyCode.DOWN) {
                        Main.character.setImage(new Image("Images\\f" + numinimage + ".png"));
                        MovementHandler.move(3, houseAvailableTiles, Main.character);
                    }
                }
                if ((Main.character.getX() <= 12 *32 && Main.character.getY() <= 14 * 32 )
                        || (Main.character.getX() <= 12 *32 && Main.character.getY() > 15 * 32)
                        || (Main.character.getX() > 12 *32 && Main.character.getY() <= 9 * 32)){
                    inspectButton.setVisible(true);
                    inspectButton.setLayoutX(Main.character.getX() - 24);
                    inspectButton.setLayoutY(Main.character.getY() - 30);
                } else {
                    inspectButton.setVisible(false);
                }

                if (event.getCode() == KeyCode.B){
                    //backPackListView = Main.Eisenhower.getBackPack().viewList();
                    //farmGroup.getChildren().addAll(backPackListView);
                    PersonMenu personMenu = new PersonMenu(homeScene,primaryStage);
                    try {
                        Stage mystage = new Stage();
                        mystage.setX(40);
                        personMenu.start(mystage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (event.getCode() == KeyCode.I){


                    if (Main.character.getX() <= 12 *32 && Main.character.getY() > 15 * 32){
                        System.out.println("ZZZ!");
                        Rectangle dimmer = new Rectangle(0, 0, homeScene.getWidth(),homeScene.getHeight());
                        dimmer.setFill(Color.color(0,0,0,0));
                        homeGroup.getChildren().addAll(dimmer);
                        final double[] numAndRate = {0, 0.1};
                        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                if (numAndRate[0] > 1) {
                                    numAndRate[1] = -0.1;
                                    numAndRate[0] = 1;
                                }
                                dimmer.setFill(Color.color(0,0,0,numAndRate[0]));
                                System.out.println(numAndRate[0]);
                                numAndRate[0] += numAndRate[1];
                                if (numAndRate[0] >= 1)
                                    numAndRate[1] = -0.1;
                            }
                        }));
                        timeline.setCycleCount(18);
                        timeline.setOnFinished(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                homeGroup.getChildren().remove(dimmer);
                                Main.dayTime = 0;
                                for (Thing thing : Main.gameThings){
                                    try {
                                        thing.nextDay(Main.Eisenhower.season);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        });
                        timeline.play();
                    }


                    if (Main.character.getX() > 12 *32 && Main.character.getY() <= 9 * 32){
                        Storage storage = Main.home.getStorageBox();
                        Group group1 = new Group();
                        Scene scene = new Scene(group1, 620, 700);
                        scene.setFill(new ImagePattern(new Image("Images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();
                        Button put = new Button("Put items in storage");
                        Button take = new Button("Take items from storage");
                        put.setPrefWidth(200);
                        take.setPrefWidth(200);
                        put.setLayoutX(210);
                        take.setLayoutX(210);
                        put.setLayoutY(200);
                        take.setLayoutY(250);
                        Rectangle rectangle1 = new Rectangle(10, 10, 30, 30);
                        rectangle1.setFill(new ImagePattern(new Image("Images\\backButton.jpg")));
                        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                rectangle1.setCursor(Cursor.HAND);
                            }
                        });
                        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                scene.setRoot(group1);
                            }
                        });

                        group1.getChildren().addAll(put,take);
                        ArrayList<Button> buttons = new ArrayList<>();

                        put.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                scene.setRoot(group2);
                                Label question = new Label("Choose an Item:");
                                question.setFont(new Font("Comic Sans MS", 25));
                                question.setTextFill(Color.DARKGREEN);
                                question.setLayoutX(60);
                                question.setLayoutY(10);
                                group2.getChildren().addAll(question,rectangle1);
                                int num = 0;
                                for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                    if (itemStack.getItems().size() == 0)
                                        continue;
                                    Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                    buttons.add(button);
                                    button.setLayoutX(20 + 200 * (num / 15));
                                    button.setLayoutY(80 + (num % 15) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            for (Button button : buttons) {
                                                if (button.getText().startsWith(itemStack.getItems().get(0).getName() + " x")) {
                                                    if (itemStack.getItems().size() == 1) {
                                                        button.setVisible(false);
                                                    }
                                                    else
                                                        button.setText(itemStack.getItems().get(0).getName() + " x" + (itemStack.getItems().size()-1));
                                                }
                                            }
                                            mainHome.getStorageBox().putInItemStack(itemStack,1);
                                        }
                                    });
                                    group2.getChildren().add(button);
                                }
                            }
                        });

                        take.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                scene.setRoot(group2);
                                Label question = new Label("Choose an Item:");
                                question.setFont(new Font("Comic Sans MS", 25));
                                question.setTextFill(Color.DARKGREEN);
                                question.setLayoutX(60);
                                question.setLayoutY(10);
                                group2.getChildren().addAll(question,rectangle1);
                                int num = 0;
                                for (ItemStack itemStack : mainHome.getStorageBox().getItemStacks()) {
                                    Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                    buttons.add(button);
                                    button.setLayoutX(20 + 200 * (num / 15));
                                    button.setLayoutY(80 + (num % 15) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            for (Button button : buttons) {
                                                if (button.getText().startsWith(itemStack.getItems().get(0).getName() + " x")) {
                                                    if (Main.Eisenhower.getBackPack().getRemainingCapacity() < itemStack.getItems().get(0).getSize()){
                                                        question.setText("Not enough space in your backpack!");
                                                        return;
                                                    }
                                                    if (itemStack.getItems().size() == 1) {
                                                        button.setVisible(false);
                                                    }
                                                    else
                                                        button.setText(itemStack.getItems().get(0).getName() + " x" + (itemStack.getItems().size()-1));
                                                }
                                            }
                                            Main.Eisenhower.getBackPack().putInItemStack(itemStack,1);
                                        }
                                    });
                                    group2.getChildren().add(button);
                                }
                            }
                        });
                    }

                    //Kitchen:
                    if(Main.character.getX() <= 12 *32 && Main.character.getY() <= 14 * 32 ){
                        Group group1 = new Group();
                        Scene scene = new Scene(group1, 620, 700);
                        scene.setFill(new ImagePattern(new Image("Images\\white-texture.jpg")));
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.show();

                        Button put = new Button("Put item in shelf");
                        Button take = new Button("Take item from shelf");
                        Button recipes = new Button("Recipes");
                        Button cook = new Button("Cook");

                        group1.getChildren().addAll(put,take,recipes,cook);

                        put.setPrefWidth(200);
                        take.setPrefWidth(200);
                        recipes.setPrefWidth(200);
                        cook.setPrefWidth(200);

                        put.setLayoutX(210);
                        take.setLayoutX(210);
                        recipes.setLayoutX(210);
                        cook.setLayoutX(210);

                        put.setLayoutY(200);
                        take.setLayoutY(250);
                        recipes.setLayoutY(300);
                        cook.setLayoutY(350);

                        Rectangle rectangle1 = new Rectangle(10, 10, 30, 30);
                        rectangle1.setFill(new ImagePattern(new Image("Images\\backButton.jpg")));
                        rectangle1.setOnMouseMoved(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                rectangle1.setCursor(Cursor.HAND);
                            }
                        });
                        rectangle1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                scene.setRoot(group1);
                            }
                        });

                        //recipes button:
                        recipes.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                group2.getChildren().addAll(rectangle1);
                                int num = 0;
                                Label description = new Label();
                                description.setFont(Main.comicFont);
                                description.setLayoutY(400);
                                description.setLayoutX(40);
                                group2.getChildren().addAll(description);

                                scene.setRoot(group2);
                                for (Recipe recipe : mainHome.getKitchen().getRecipes()) {
                                    Button button = new Button(recipe.getName());
                                    button.setLayoutX(20 + 200 * (num / 10));
                                    button.setLayoutY(80 + (num % 10) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            description.setText(recipe.toString());
                                        }
                                    });
                                    group2.getChildren().addAll(button);
                                }
                            }
                        });

                        //put and take button:

                        ArrayList<Button> buttons = new ArrayList<>();

                        put.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                scene.setRoot(group2);
                                Label question = new Label("Choose a kitchen utensil:");
                                question.setFont(new Font("Comic Sans MS", 25));
                                question.setTextFill(Color.DARKGREEN);
                                question.setLayoutX(60);
                                question.setLayoutY(10);
                                group2.getChildren().addAll(question,rectangle1);
                                int num = 0;
                                for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()) {
                                    if (itemStack.getItems().size() == 0)
                                        continue;
                                    if (! (itemStack.getItems().get(0) instanceof KitchenTool))
                                        continue;
                                    Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                    buttons.add(button);
                                    button.setLayoutX(20 + 200 * (num / 15));
                                    button.setLayoutY(80 + (num % 15) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            for (Button button : buttons) {
                                                if (button.getText().startsWith(itemStack.getItems().get(0).getName() + " x")) {
                                                    if (itemStack.getItems().size() == 1) {
                                                        button.setVisible(false);
                                                    }
                                                    else
                                                        button.setText(itemStack.getItems().get(0).getName() + " x" + (itemStack.getItems().size()-1));
                                                }
                                            }
                                            mainHome.getKitchen().getToolShelf().add(itemStack);
                                            System.out.println(mainHome.getKitchen().getToolShelf());
                                            Main.Eisenhower.getBackPack().getItemStacks().remove(itemStack);
                                        }
                                    });
                                    group2.getChildren().add(button);
                                }
                            }
                        });

                        take.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                scene.setRoot(group2);
                                Label question = new Label("Choose an Item:");
                                question.setFont(new Font("Comic Sans MS", 25));
                                question.setTextFill(Color.DARKGREEN);
                                question.setLayoutX(60);
                                question.setLayoutY(10);
                                group2.getChildren().addAll(question,rectangle1);
                                int num = 0;
                                for (ItemStack itemStack : mainHome.getKitchen().getToolShelf()) {
                                    System.out.println(itemStack.getItems().size());
                                    Button button = new Button(itemStack.getItems().get(0).getName() + " x" + itemStack.getItems().size());
                                    buttons.add(button);
                                    button.setLayoutX(20 + 200 * (num / 15));
                                    button.setLayoutY(80 + (num % 15) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            for (Button button : buttons) {
                                                if (button.getText().startsWith(itemStack.getItems().get(0).getName() + " x")) {
                                                    if (Main.Eisenhower.getBackPack().getRemainingCapacity() < itemStack.getItems().get(0).getSize()){
                                                        question.setText("Not enough space in your backpack!");
                                                        return;
                                                    }
                                                    if (itemStack.getItems().size() == 1) {
                                                        button.setVisible(false);
                                                    }
                                                    else
                                                        button.setText(itemStack.getItems().get(0).getName() + " x" + (itemStack.getItems().size()-1));
                                                }
                                            }
                                            Main.Eisenhower.getBackPack().putInItemStack(itemStack,1);

                                            for(int i = mainHome.getKitchen().getToolShelf().size()-1; i>= 0; i--){
                                                if (mainHome.getKitchen().getToolShelf().get(i).getItems().size() == 0){
                                                    mainHome.getKitchen().getToolShelf().remove(mainHome.getKitchen().getToolShelf().get(i));
                                                }
                                            }
                                        }
                                    });
                                    group2.getChildren().add(button);
                                }
                            }
                        });


                        cook.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                Group group2 = new Group();
                                group2.getChildren().addAll(rectangle1);
                                int num = 0;
                                Label description = new Label();
                                description.setFont(Main.comicFont);
                                description.setLayoutY(400);
                                description.setLayoutX(40);
                                group2.getChildren().addAll(description);

                                scene.setRoot(group2);
                                for (Recipe recipe : mainHome.getKitchen().getRecipes()) {
                                    Button button = new Button(recipe.getName());
                                    button.setLayoutX(20 + 200 * (num / 10));
                                    button.setLayoutY(80 + (num % 10) * 35);
                                    button.setFont(Main.comicFont);
                                    button.setPrefWidth(180);
                                    num++;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            for (ItemStack itemStack: recipe.getIngredients()){
                                                if (Main.Eisenhower.getBackPack().numberOf(itemStack.getItems().get(0).getName()) < itemStack.getItems().size())
                                                {
                                                    description.setText("More " + itemStack.getItems().get(0).getName() + " is needed!");
                                                    return;
                                                }
                                            }
                                            for(ItemStack itemStack : recipe.getTools()){
                                                boolean has = false;
                                                for (ItemStack hv : mainHome.getKitchen().getToolShelf()){
                                                    if (hv.getItems().get(0).getName().equals(hv.getItems().get(0).getName())){
                                                        has = true;
                                                    }
                                                    if (has == false){
                                                        description.setText(itemStack.getItems().get(0).getName() + " not found in tool Shelf!");
                                                        return;
                                                    }
                                                }
                                            }

                                            for(ItemStack itemStack : recipe.getTools()) {
                                                KitchenTool kt = (KitchenTool)itemStack.getItems().get(0);
                                                if(!(kt).canUseTool()){
                                                    if (kt.getHealth() <= 0){
                                                        description.setText(kt.getName() + " is broken!");
                                                        return;
                                                    }
                                                    else {
                                                        description.setText("You are too tired!");
                                                        return;
                                                    }
                                                }
                                            }

                                            for (ItemStack itemStack: recipe.getIngredients()){
                                                Main.Eisenhower.getBackPack().takeItem(itemStack.getItems().get(0).getName(), itemStack.getItems().size());
                                            }

                                            for(ItemStack itemStack : recipe.getTools()) {
                                                KitchenTool kt = (KitchenTool)itemStack.getItems().get(0);
                                                kt.abilityChange.affect();
                                                kt.health --;
                                            }
                                            description.setText("Cooking done!");
                                        }
                                    });
                                    group2.getChildren().addAll(button);
                                }
                            }
                        });
                    }
                }
                if (Main.character.getY() > homeScene.getHeight()-20){
                    Main.character.setX(32 * 14);
                    Main.character.setY(32 * 13 - 14);
                    Main.farmBuilder.start(primaryStage);
                }
                numinimage++;
            }
        });
    }
}
