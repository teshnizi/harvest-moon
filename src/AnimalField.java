
import java.util.*;

/**
 * has AnimalBlocks
 */
public class AnimalField extends Thing {

    /**
     * Default constructor
     */
    public AnimalField(String name) {
        super(name);
        maximumSize = 4;
    }

    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    private ArrayList<Animal> animals = new ArrayList<>();
    /**
     * @return
     */
    int maximumSize;
    public ArrayList<String> objects(){
        ArrayList<String> s = new ArrayList<>();
        s.add("");
        return s;
    }

    @Override
    public void nextDay(Season season){
        for (Animal animal : animals)
            if(animal != null)
                animal.nextDay(season);
    }

}