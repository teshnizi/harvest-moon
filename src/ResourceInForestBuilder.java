import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by pc on 7/12/2017.
 */
public class ResourceInForestBuilder extends Application{
    Storage storage ;
    Axe axe = null;
    int x11,y11;
    public ResourceInForestBuilder(Storage storage ,int cc , int dd){
        this.storage = storage;
        x11 = cc;
        y11 = dd;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group  =new Group();
        Scene scene = new Scene(group,620 , 700 , new ImagePattern(new Image("images\\white-texture.jpg")));
        Text sub = new Text();
        sub.setLayoutX(250);
        sub.setLayoutY(40);
        sub.setFont(new Font(25));
        sub.setText(storage.getItemStacks().get(0).getItems().get(0).getName());
        group.getChildren().add(sub);

        Text text = new Text();
        text.setLayoutX(200);
        text.setLayoutY(100);
        text.setFont(Main.comicFont);
        text.setText("number of resources remain : " + storage.getItemStacks().get(0).getItems().size());
        group.getChildren().addAll(text);

        Text text1 = new Text();
        text1.setLayoutX(200);
        text1.setLayoutY(150);
        text1.setFont(Main.comicFont);
        group.getChildren().add(text1);

        Text cho = new Text();
        cho.setLayoutX(200);
        cho.setLayoutY(200);
        cho.setFont(Main.comicFont);
        group.getChildren().add(cho);

        Text result = new Text();
        result.setLayoutX(200);
        result.setLayoutY(400);
        result.setFont(Main.comicFont);
        group.getChildren().add(result);

        Button button  = new Button("Collect");
        button.setLayoutX(250);
        button.setLayoutY(500);
        button.setVisible(false);
        button.setFont(Main.comicFont);
        group.getChildren().add(button);
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setCursor(Cursor.HAND);
            }
        });

        if (storage.getItemStacks().get(0).getItems().get(0) instanceof Wood){
            Wood wood = (Wood) storage.getItemStacks().get(0).getItems().get(0);
            if (wood.getType() == 0)
            {
                button.setVisible(true);
                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        int ans= collectedwood(0,0,10,storage);
                        int energy = energyUse(0,0);
                        if (ans == 0)
                        {
                            result.setText("Could not find anything. Better Luck next time");
                        }
                        else
                        {
                            if (Main.Eisenhower.getEnergy().getCurrentValue()+ energy >= 0){
                                String n = storage.getItemStacks().get(0).getItems().get(0).getName();
                                int n1 = storage.getItemStacks().get(0).getItems().size();
                                if (Main.Eisenhower.getBackPack().putInItemStack(storage.getItemStacks().get(0),ans)==  1){
                                    if (Main.online)
                                    {
                                        try {
                                            DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                                            dout.writeUTF(mystring(x11,y11,ans));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    result.setText(ans + " Branches where collected ." + "\nEnergy usage : " + (-1)*energy* ans );
                                    Main.Eisenhower.getEnergy().changeCurrentValue(energy * ans);
                                    text.setText("number of resources remain : " + n1);
                                    if ( storage.getItemStacks().get(0).getItems().size() == 0)
                                    {
                                        ForestBuilder.repair();
                                        primaryStage.close();
                                    }
                                }
                            }
                            else
                                result.setText("You are too tiered");
                        }
                    }
                });
            }
            else{

                int kl = 0;
                for (ItemStack itemStack : Main.Eisenhower.getBackPack().getItemStacks()){
                    if (itemStack.getItems().get(0) instanceof Axe)
                    {
                        Axe pre = (Axe)itemStack.getItems().get(0);
                        if (pre.getType() >= wood.getType()){
                            Button button1 = new Button(itemStack.getItems().get(0).getName());
                            button1.setLayoutX(50);
                            button1.setLayoutY(kl * 50 + 150);
                            button1.setPrefSize(150,30);
                            group.getChildren().add(button1);
                            button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    button.setVisible(true);
                                    axe = pre;
                                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                        @Override
                                        public void handle(MouseEvent event) {
                                            int num = collectedwood(wood.getType(),axe.getType() , 5,storage);
                                            int energy = energyUse(wood.getType(),axe.getType());
                                            if (axe.health - 5*(5-axe.getType()) >= 0)
                                            {
                                                if (Main.Eisenhower.getEnergy().getCurrentValue() + energy >= 0)
                                                {
                                                    if (num == 0)
                                                    {
                                                        result.setText("Could not find anything. Better Luck next time");
                                                    }
                                                    else {
                                                        String n = storage.getItemStacks().get(0).getItems().get(0).getName();
                                                        int n1 = storage.getItemStacks().get(0).getItems().size();
                                                        if (Main.Eisenhower.getBackPack().putInItemStack(storage.getItemStacks().get(0), num) == 1) {
                                                            if (Main.online)
                                                            {
                                                                try {
                                                                    DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                                                                    dout.writeUTF(mystring(x11,y11,num));
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                            result.setText(num  + " " + n + " where collected ." + "\nEnergy usage : " + (-1)*energy* num );
                                                            text.setText("number of resources remain : " + n1);
                                                            Main.Eisenhower.getEnergy().changeCurrentValue(energy * num);
                                                            axe.cut();
                                                            if ( storage.getItemStacks().get(0).getItems().size() == 0)
                                                            {
                                                                ForestBuilder.repair();
                                                                primaryStage.close();
                                                            }
                                                        }
                                                    }

                                                }
                                                else {
                                                    result.setText("You are too tiered");
                                                }

                                            }
                                            else
                                                result.setText("Your axe is Broken");
                                        }
                                    });
                                }
                            });
                            kl++;
                        }
                    }
                }

            }
        }
        else {
            Stone stone = (Stone)storage.getItemStacks().get(0).getItems().get(0);
            button.setVisible(true);
            button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    int ans= collectedStone(0,0,10,storage);
                    int energy = energyUse(0,0);
                    if (ans == 0)
                    {
                        result.setText("Could not find anything. Better Luck next time");
                    }
                    else
                    {
                        if (Main.Eisenhower.getEnergy().getCurrentValue()+ energy >= 0){
                            String n = storage.getItemStacks().get(0).getItems().get(0).getName();
                            int n1 = storage.getItemStacks().get(0).getItems().size();
                            if (Main.Eisenhower.getBackPack().putInItemStack(storage.getItemStacks().get(0),ans)==  1){
                                if (Main.online)
                                {
                                    try {
                                        DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                                        dout.writeUTF(mystring(x11,y11,ans));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                result.setText(ans + " Stones where collected ." + "\nEnergy usage : " + (-1)*energy* ans );
                                Main.Eisenhower.getEnergy().changeCurrentValue(energy * ans);
                                text.setText("number of resources remain : " + n1);
                                if ( storage.getItemStacks().get(0).getItems().size() == 0)
                                {
                                    ForestBuilder.repair();
                                    primaryStage.close();
                                }
                            }
                        }
                        else
                            result.setText("You are too tiered");
                    }
                }
            });
        }
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public int collectedwood(int woodtype , int axetype , int whole , Storage m){
        int increase = axetype - woodtype;
        int rand =(int) (5.0 * Math.pow(2,increase)) ;
        Random random = new Random();
        rand = random.nextInt(rand + 1);
        int ans = (int)Math.min((rand)*((double)(m.getItemStacks().get(0).getItems().size()/(double)whole)),m.getItemStacks().get(0).getItems().size());
        return ans;
    }
    public int collectedStone(int woodtype , int axetype , int whole , Storage m){
        int increase = axetype - woodtype;
        int rand =(int) (2.0 * Math.pow(2,increase)) ;
        Random random = new Random();
        rand = random.nextInt(rand + 1);
        double n  = 1.0;
        for (int i  = 0 ; i < rand ; i++){
            n*=((double)((double)m.getItemStacks().get(0).getItems().size()/(double)whole));
        }
        n*=100;
        int n1 = (int) n;
        int k = random.nextInt()%100 +1;
        if (k<=n1)
            return rand;
        else
            return 0;
    }
    public int energyUse(int woodtype , int axetype ){
        int energy = (-1) * (int) (30.0 * Math.pow(2,woodtype)/Math.pow(1.6,axetype));
        return energy;
    }
    public String mystring(int num1 ,int num2 ,int num3){
        String ans;
        ans = Getter.socketname + Protocol.protocol+ Protocol.foretsource + Protocol.protocol + num1 + Protocol.protocol + num2 + Protocol.protocol + num3;
        return ans;
    }
}
