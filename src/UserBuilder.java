import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.xml.soap.Text;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by pc on 7/13/2017.
 */
public class UserBuilder extends Application {
    int port;
    int type1;
    int portll = 0;
    int chosen = 0;
    String servername = null;
    String user;
    static javafx.scene.text.Text online = new javafx.scene.text.Text();
    public UserBuilder(String name , int type , int port){
        this.type1 = type;
        this.port = port;
        user = name;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,1280,960);
        scene.setFill(Color.AZURE);
        if(type1 == 1) {
            online.setText("Online server . click to join");
            online.setFont(Main.comicFont);
            online.setX(100);
            online.setY(100);
            group.getChildren().add(online);
            Button join = new Button("Join");
            join.setLayoutX(500);
            join.setLayoutY(600);
            join.setVisible(false);
            join.setFont(new Font(25));
            group.getChildren().add(join);
            scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override


                public void handle(MouseEvent event) {
                    if (Main.startgame)
                    {
                        Main.farmBuilder.start(primaryStage);
                    }
                }
            });

            File file1 = new File("socket.txt");
            FileWriter fileReader1 = new FileWriter(file1,true);
            String s1;
            BufferedWriter br1 = new BufferedWriter(fileReader1);
            join.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    try {
                        Socket socket = new Socket("localhost",portll);
                        Getter getter = new Getter(socket,user);
                        Main.online = true;
                        Getter.ishost = false;
                        Main.curent = true;
                        Getter.hostname = servername;
                        online.setText("you joined  .wait to host accept you");
                        DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                        DataInputStream din = new DataInputStream(socket.getInputStream());
                        dout.writeUTF(user);
                        br1.write(servername + " "  + user);
                        br1.newLine();
                        br1.close();
                        Main.forest.nextDay(Main.Eisenhower.season);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });



            File file = new File("server.txt");
            FileReader fileReader = new FileReader(file);
            String s;
            BufferedReader br = new BufferedReader(fileReader);
            int num = 0;
            while ((s=br.readLine())!=null){
                Button button = new Button(s);
                button.setLayoutX(100);
                button.setLayoutY(150+50*num);
                button.setFont(Main.comicFont);
                button.setPrefSize(150,30);
                button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        button.setCursor(Cursor.HAND);
                    }
                });
                group.getChildren().add(button);
                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        join.setVisible(true);
                        String [] s1 = button.getText().split(" ");
                        portll = Integer.parseInt(s1[1]);
                        servername = s1[0];
                    }
                });
                num++;
            }

        }
        else
        {

            javafx.scene.text.Text text = new javafx.scene.text.Text();
            text.setLayoutX(100);
            text.setLayoutY(100);
            text.setFont(Main.comicFont);
            text.setText("choose");
            group.getChildren().add(text);

            Button refresh = new Button("refresh");
            refresh.setLayoutX(500);
            refresh.setLayoutY(500);
            group.getChildren().addAll(refresh);

            File file = new File("socket.txt");
            FileReader fileReader = new FileReader(file);
            String s;
            ArrayList<Button> buttons = new ArrayList<>();
            ArrayList<Button> buttons1 = new ArrayList<>();
            BufferedReader br = new BufferedReader(fileReader);
            int num = 0 ;
                while ((s=br.readLine()) != null){
                String [] s1 = s.split(" ");
                if (s1[0].equals(user)) {
                    javafx.scene.text.Text text1 = new javafx.scene.text.Text();
                    Button button = new Button(s1[1]);
                    buttons.add(button);
                    button.setLayoutX(100);
                    button.setLayoutY(100 + 50 * num);
                    text1.setLayoutX(button.getLayoutX() + 200);
                    text1.setLayoutY(button.getLayoutY() + 20);
                    text1.setText("");
                    text1.setFont(Main.comicFont);
                    button.setPrefSize(150,30);
                    button.setFont(Main.comicFont);
                    button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            button.setCursor(Cursor.HAND);
                        }
                    });
                    group.getChildren().addAll(button,text1);
                    button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (buttons1.contains(button)) {
                                buttons1.remove(button);
                                text1.setText("rejected");
                                text1.setFill(Color.RED);
                                button.setStyle("-fx-background-color: red");
                                chosen++;
                            }
                            else {
                                buttons1.add(button);
                                text1.setText("Accepted");
                                text1.setFill(Color.GREEN);
                                button.setStyle("-fx-background-color: green");
                                chosen--;
                            }
                        }
                    });
                }
            }
            Button play1 = new Button("Play");
            play1.setLayoutX(600);
            play1.setLayoutY(500);
            play1.setVisible(false);
            group.getChildren().addAll(play1);
            play1.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    for (Button button : buttons)
                    {
                        if (!buttons1.contains(button)){
                            try {
                                Socket socket = Getter.socket;
                                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                                dout.writeUTF(user + Protocol.protocol + Protocol.reject + Protocol.protocol + button.getText());
                                dout.writeUTF(button.getText() + Protocol.protocol+"LOGOUT");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    for (Button button : buttons) {
                        if (buttons1.contains(button)) {
                            try {
                                Socket socket = Getter.socket;
                                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                               dout.writeUTF(user + Protocol.protocol + Protocol.play);
                                System.out.println(1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    FarmBuilder.infarms = true;
                    Main.timeline.play();
                    FarmBuilder farmBuilder = new FarmBuilder(Main.mainFarm);
                    farmBuilder.start(Main.mainstage);
                }
            });

            scene.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (chosen > 0) {
                        play1.setVisible(true);
                    }
                    else
                        play1.setVisible(false);
                }
            });

            refresh.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    File file = new File("socket.txt");
                    FileReader fileReader = null;
                    try {
                        fileReader = new FileReader(file);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    String s;
                    for (Button button : buttons)
                        group.getChildren().remove(button);
                    buttons.clear();
                    buttons1.clear();
                    chosen = 0;
                    BufferedReader br = new BufferedReader(fileReader);
                    int num = 0 ;
                    try {
                        while ((s=br.readLine()) != null){
                            String [] s1 = s.split(" ");
                            if (s1[0].equals(user)) {
                                Button button = new Button(s1[1]);
                                javafx.scene.text.Text text1 = new javafx.scene.text.Text();
                                    buttons.add(button);
                                    button.setLayoutX(100);
                                    button.setLayoutY(150 + 50 * num);
                                text1.setLayoutX(button.getLayoutX() + 200);
                                text1.setLayoutY(button.getLayoutY() + 20);
                                text1.setText("");
                                text1.setFont(Main.comicFont);
                                button.setPrefSize(150,30);
                                button.setFont(Main.comicFont);
                                button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                    @Override
                                    public void handle(MouseEvent event) {
                                        if (buttons1.contains(button)) {
                                            text1.setText("Rejected");
                                            text1.setFill(Color.RED);
                                            buttons1.remove(button);
                                            button.setStyle("-fx-background-color: red");
                                            chosen--;
                                        }
                                        else {
                                            text1.setText("Accepted");
                                            text1.setFill(Color.GREEN);
                                            buttons1.add(button);
                                            button.setStyle("-fx-background-color: green");
                                            chosen++;
                                        }
                                    }
                                });
                                    group.getChildren().addAll(button,text1);

                            }
                            num++;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
