import java.net.*;
import java.util.*;
import java.io.*;

public class Server
{
     Vector ClientSockets;
     ArrayList<String> LoginNames;
    String names;

    Server(int port , String names) throws Exception
    {
        ServerSocket soc=new ServerSocket(port);
        ClientSockets=new Vector();
        LoginNames=new ArrayList<>();
        if (names.equals("main")) {
            PrintWriter printWriter = new PrintWriter("socket.txt", "UTF-8");
            PrintWriter printWriter1 = new PrintWriter("server.txt", "UTF-8");
        }
        this.names = names;
        while(true)
        {
            Socket CSoc=soc.accept();
            AcceptClient obClient=new AcceptClient(CSoc);
        }
    }

    class AcceptClient extends Thread
    {
        Socket ClientSocket;
        DataInputStream din;
        DataOutputStream dout;
        AcceptClient (Socket CSoc) throws Exception
        {
            ClientSocket=CSoc;

            din=new DataInputStream(ClientSocket.getInputStream());
            dout=new DataOutputStream(ClientSocket.getOutputStream());

            String LoginName=din.readUTF();

            System.out.println("User Logged In :" + LoginName);
            LoginNames.add(LoginName);
            System.out.println(LoginNames.size());
            ClientSockets.add(ClientSocket);

            start();
        }

        public void run()
        {
            while(true && !ClientSocket.isClosed())
            {

                try
                {
                    String msgFromClient=new String();

                    msgFromClient=din.readUTF();
                    String [] st=msgFromClient.split(Protocol.protocol);
                    String [] st1=msgFromClient.split(Protocol.protocol);
                    System.out.println(st[0]);
                    String Sendto=st[0];
                    String MsgType=st[1];
                    int iCount=0;

                    if(MsgType.equals("LOGOUT"))
                    {
                        for(iCount=0;iCount<LoginNames.size();iCount++)
                        {
                            if(LoginNames.get(iCount).equals(Sendto))
                            {
                                LoginNames.remove(iCount);
                                ClientSockets.removeElementAt(iCount);
                                System.out.println("User " + Sendto +" Logged Out ...");
                                break;
                            }

                        }
                        LoginNames.remove(Sendto);

                    }
                    else {
                            for (iCount = 0; iCount < LoginNames.size(); iCount++) {
                                if (!LoginNames.get(iCount).equals(Sendto)) {
                                    Socket tSoc = (Socket) ClientSockets.elementAt(iCount);
                                    if (!tSoc.isClosed()) {
                                        DataOutputStream tdout = new DataOutputStream(tSoc.getOutputStream());
                                        tdout.writeUTF(msgFromClient);
                                    }
                                }
                            }
                    }
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    break;
                }



            }
        }
    }
}