
import javafx.util.Pair;

import java.io.*;
import java.util.*;

/**
 * 
 */
public class Forest extends Environment {


    private WoodType woodTypes;

    private StoneType rockTypes;
    private ArrayList<Storage> storages;
    private River river;
    public static HashSet<Pair<Storage,Pair<Integer,Integer>>> res = new HashSet<>();
    /**
     * Default constructor
     *
     * @param name
     */
    public Forest(String name , WoodType woodType , StoneType rockType) {
        super(name);
        woodTypes = woodType;
        rockTypes= rockType;
    }

    public void setStorages(ArrayList<Storage> storages) {
        this.storages = storages;
    }

    public void setRiver(River river) {
        this.river = river;
    }

    public void setRockTypes(StoneType rockTypes) {
        this.rockTypes = rockTypes;
    }

    public void setWoodTypes(WoodType woodTypes) {
        this.woodTypes = woodTypes;
    }

    @Override
    public ArrayList<String> listOfPlaces() {
       ArrayList<String > ret = new ArrayList<>();
       for (JA ja : neighbours)
           ret.add(ja.getName());
       for (Place place : places)
           ret.add(place.getName());
       return ret;
    }

    public River getRiver() {
        return river;
    }

    @Override
    public ArrayList<String> listOfObjects() {
        ArrayList<String >ret = new ArrayList<>();
        ret.add(woodTypes.getName());
        ret.add(rockTypes.getName());
        ret.add(river.getName());
        return ret;
    }
    @Override
    public Thing findObject(String objectName) {
       if (rockTypes.getName().equals(objectName))
           return rockTypes;
       if (woodTypes.getName().equals(objectName))
           return woodTypes;
        if (river.getName().equals(objectName))
            return river;
        return null;
    }

    @Override
    public JA findJA(String name) {
        for (JA x : neighbours)
            if (x.getName().equals(name))
                return x;
        for (Place place : places)
            if (place.getName().equals(name))
                return place;
        return null;
    }

    @Override
    public void nextDay(Season season) throws IOException {
        if (Person.age % 3 == 1 || Main.curent) {
            Main.curent = false;
            if (Getter.ishost) {
                storages.clear();
                for (int i = 0; i < 5; i++) {
                    Storage storage = new Storage(10);
                    for (int j = 0; j < 10; j++) {
                        storage.putInItem(Producer.wood(0));
                    }
                    storages.add(storage);
                }
                for (int i = 1; i < 4; i++) {
                    for (int j = 0; j < 3; j++) {
                        Storage storage = new Storage(10);
                        for (int k = 0; k < 10; k++)
                            storage.putInItem(Producer.wood(i));
                        storages.add(storage);
                    }
                }
                for (int i = 0; i < 5; i++) {
                    Storage storage = new Storage(10);
                    for (int j = 0; j < 10; j++)
                        storage.putInItem(Producer.stone(0));
                    storages.add(storage);
                }
                HashSet<Integer> k = cho();
                res = Forest.placew1(k,storages);
                System.out.println("hoset");
                int tt = 50 - river.getFish().getItems().size();
                for (int i = 1; i <= tt; i++)
                    river.getFish().getItems().add(Producer.fish());
                if (Main.online) {
                    System.out.println("yuyuyu");
                    PrintWriter printWriter = new PrintWriter(Getter.socketname + ".txt", "UTF-8");
                    File file = new File(Getter.socketname + ".txt");
                    FileWriter fileWriter = new FileWriter(file);
                    BufferedWriter bw = new BufferedWriter(fileWriter);
                    for (Integer integer :k){
                        bw.write(String.valueOf(integer));
                        bw.newLine();
                    }
                    bw.close();
                }
            } else {
                storages.clear();
                for (int i = 0; i < 5; i++) {
                    Storage storage = new Storage(10);
                    for (int j = 0; j < 10; j++) {
                        storage.putInItem(Producer.wood(0));
                    }
                    storages.add(storage);
                }
                for (int i = 1; i < 4; i++) {
                    for (int j = 0; j < 3; j++) {
                        Storage storage = new Storage(10);
                        for (int k = 0; k < 10; k++)
                            storage.putInItem(Producer.wood(i));
                        storages.add(storage);
                    }
                }
                for (int i = 0; i < 5; i++) {
                    Storage storage = new Storage(10);
                    for (int j = 0; j < 10; j++)
                        storage.putInItem(Producer.stone(0));
                    storages.add(storage);
                }
                System.out.println(Getter.hostname +" user");
                File file = new File(Getter.hostname + ".txt");
                FileReader fileReader = new FileReader(file);
                BufferedReader br = new BufferedReader(fileReader);
                String s;
                HashSet<Integer> l = new HashSet<>();
                while ((s=br.readLine()) !=null)
                {
                    l.add(Integer.parseInt(s));
                }
                res = placew1(l,storages);
            }
        }
    }
    public static HashSet<Integer>  cho(){
        HashSet<Integer> hashSet = new HashSet<>();
        Random random = new Random();
        while (hashSet.size() < 19)
        {
            int c = random.nextInt()%74;
            if (c < 0)
                c*=(-1);
            hashSet.add(c +1);
        }
        return hashSet;
    }
    public static  HashSet<Pair<Storage,Pair<Integer,Integer>>> placew(ArrayList<Storage> storages1){
        HashSet<Pair<Storage,Pair<Integer,Integer>>> hashSet = new HashSet<>();
        HashSet<Integer> k = cho();
        int num = 0;
        for (Storage storage : storages1){
            int k1 = 0;
            for (Integer integer : k){
                if (k1 == num) {
                    hashSet.add(new Pair<>(storage, new Pair<Integer, Integer>((integer % 15 ) * 2 + 7, (integer/15)*3 + 1)));
                    break;
                }
                k1 ++;
            }
            num++;
        }
        return hashSet;

    }
    public static  HashSet<Pair<Storage,Pair<Integer,Integer>>> placew1(HashSet<Integer> k,ArrayList<Storage> storages1){
        HashSet<Pair<Storage,Pair<Integer,Integer>>> hashSet = new HashSet<>();
        int num = 0;
        for (Storage storage : storages1){
            int k1 = 0;
            for (Integer integer : k){
                if (k1 == num) {
                    hashSet.add(new Pair<>(storage, new Pair<Integer, Integer>((integer % 15 ) * 2 + 7, (integer/15)*3 + 1)));
                    break;
                }
                k1 ++;
            }
            num++;
        }
        return hashSet;
    }
}