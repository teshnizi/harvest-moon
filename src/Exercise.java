import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Tigerous215 on 5/18/2017.
 */
public class Exercise extends Thing implements Serializable{

    public Exercise(String name,EffectOnBody effectOnBody, int cost){
        super(name);
        this.effect = effectOnBody;
        this.cost = cost;
    }

    int cost;

    EffectOnBody effect;

    @Override
    public String toString()
    {
        String s = getName() + "\n";
        for (AbilityChange abilityChange : effect.getAbilityChanges()){
            s = s + abilityChange.toString() + " \n";
        }
        return s;
    }
}
