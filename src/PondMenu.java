import java.awt.event.WindowAdapter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by pc on 5/10/2017.
 */
public class PondMenu extends Menu {
    Pond pond;
    PondMenu(Pond pond){
        this.pond = pond;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if (x == 1)
        {
           Storage backPack = person.getBackPack();
           System.out.println("Choose a Watering Can:");
           try{
               WateringCan wateringCan = (WateringCan) backPack.getItemStackFromPlayer().getItems().get(0);
               wateringCan.fill();
               if(person.getEnergy().getCurrentValue() < 3){
                   System.out.println("You are too tired!");
                   return null;
               }
               person.getEnergy().changeCurrentValue(-2);
               System.out.println(wateringCan.getName() + " filled!");
           }
           catch (Exception E){
               System.out.println("BadCommand!");
           }
        }
        return null;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String > commands = new ArrayList<>();
        commands.add("You are in Pond:");
        commands.add("1.Fill WateringCan");
        return commands;
    }
}
