import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by pc on 7/15/2017.
 */
public class Inspectbuilder extends Application {
    String name;
    public Inspectbuilder(String s){
        name = s;
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group group = new Group();
        Scene scene = new Scene(group,300,485,new ImagePattern(new Image("images\\white-texture.jpg")));

        Button chat = new Button("Chat");
        chat.setFont(Main.comicFont);
        chat.setPrefSize(150,30);
        chat.setLayoutX(70);
        chat.setLayoutY(100);
        chat.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                chat.setCursor(Cursor.HAND);
            }
        });
        chat.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    ChatRoom chatRoom = new ChatRoom(Getter.socket,Getter.socketname,name,new Image("images\\white-texture.jpg"));
                    chatRoom.start(primaryStage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        Button showstatus = new Button("Show status");
        showstatus.setFont(Main.comicFont);
        showstatus.setPrefSize(150,30);
        showstatus.setLayoutX(70);
        showstatus.setLayoutY(150);
        showstatus.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                showstatus.setCursor(Cursor.HAND);
            }
        });
        showstatus.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                    dout.writeUTF(Getter.socketname + Protocol.protocol + Protocol.Showstatusrequest + Protocol.protocol + name);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Showstatus showstatus1 = new Showstatus(name);
                try {
                    showstatus1.start(primaryStage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Button friend = new Button();
        friend.setFont(Main.comicFont);
        friend.setPrefSize(150,30);
        friend.setLayoutX(70);
        friend.setLayoutY(200);
        if (Person.friends.contains(name))
            friend.setText("Remove from friends");
        else
            friend.setText("Add to friends");
        friend.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                friend.setCursor(Cursor.HAND);
            }
        });
        friend.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (friend.getText().equals("Add to friends")){

                    try {
                        DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                        dout.writeUTF(Getter.socketname + Protocol.protocol + Protocol.friendrequest + Protocol.protocol + name);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    try {
                        DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                        dout.writeUTF(Getter.socketname + Protocol.protocol + Protocol.removerequst + Protocol.protocol + name);
                        if (Person.friends.contains(name))
                            Person.friends.remove(name);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Button invit = new Button("Invite to trade");
        invit.setFont(Main.comicFont);
        invit.setPrefSize(150,30);
        invit.setLayoutX(70);
        invit.setLayoutY(250);
        invit.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                invit.setCursor(Cursor.HAND);
            }
        });
        invit.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    DataOutputStream dout = new DataOutputStream(Getter.socket.getOutputStream());
                    dout.writeUTF(Getter.socketname + Protocol.protocol + Protocol.accepttrade + Protocol.protocol + name);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        group.getChildren().addAll(chat,showstatus,friend,invit);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
