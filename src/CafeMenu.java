import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/18/2017.
 */
public class CafeMenu extends Menu{

    Cafe cafe;

    public CafeMenu(Cafe cafe){
        this.cafe = cafe;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in cafe");
        s.add("1.Dining Table");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        try {
            if (x == 1) {
                System.out.println("Choose a Meal:");
                int ID = 0;
                for (Meal meal : cafe.meals) {
                    System.out.println((++ID) + "." + meal.getName() + " $" + meal.getPrice());
                }
                Scanner scanner = new Scanner(System.in);
                ID = scanner.nextInt();
                Meal meal = cafe.meals.get(ID-1);
                System.out.println(meal.status());
                System.out.println("Do you want to buy this meal?(YES/NO)");
                String s = scanner.next();
                if(!s.equals("YES"))
                    return null;
                if(person.getMoney() <= meal.getPrice()){
                    System.out.println("Sorry, but you cannot afford this meal!");
                    return null;
                }
                person.takeMoney(meal.getPrice());
                meal.effect.affect();
                System.out.println("YUMMY!");
            }
        }
        catch (Exception E){
            System.out.println("BadCommand!");
        }
        return null;
    }
}
