/**
 * Created by Tigerous215 on 5/19/2017.
 */
public class CityHall extends Thing{

    Farm farm;

    public CityHall(String name, Farm farm){
        super(name);
        this.farm = farm;
    }
}
