
import java.util.*;

/**
 * 
 */
public class WeatherMachine extends Item {

    /**
     * Default constructor
     */
    public WeatherMachine(String name) {
        super(name,5);
    }

    @Override
    public String status() {
        return "Weather Machine.\nUsed for changing weather.";
    }

    /**
     * 
     */
    private Season season;

    public Season getSeason() {
        return season;
    }

    /**
     * @param season 
     * @return
     */
    public void changeWeather(Season season) {
        System.out.println("LOL");
        this.season = season;
    }

}