import java.util.ArrayList;

/**
 * Created by pc on 5/18/2017.
 */
public class Builder {
    private int num;
    private Item items;
    private  int price;
    public Builder(int num , Item item , int price){
        this.num = num;
        this.items = item;
        this.price = price;
    }

    public int getNum() {
        return num;
    }

    public Item getItems() {
        return items;
    }

    public int getPrice() {
        return price;
    }
}
