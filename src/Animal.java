
import com.sun.org.apache.regexp.internal.RE;

import java.util.*;

/**
 * 
 */
public class Animal extends Creature {

    /**
     * Default constructor
     */
    public Animal(String name) {
        super(name);
    }

    public Animal(String name, ArrayList<Item> sampleOutcomes, String foodName, String toolName, int matureAge, int price){
        this(name);
        this.sampleOutcomes = sampleOutcomes;
        this.toolName = toolName;
        this.foodName = foodName;
        this.matureAge = matureAge;
        this.price = price;
        health = 100;
        isFed = false;
    }

    public String getToolName() {
        return toolName;
    }

    public void heal(){
        health = 100;
    }

    public int getPrice() {
        return price;
    }

    private boolean isFed;

    int health;

    private int price;

    private int matureAge;

    private String foodName;

    private ArrayList<Item> sampleOutcomes;

    private String toolName;

    boolean isSeek;

    public ArrayList<Item> getSampleOutcomes() {
        return sampleOutcomes;
    }

    private ArrayList<ItemStack> outcome = new ArrayList<>();

    public ArrayList<ItemStack> getOutcome() {
        return outcome;
    }

    public String getFoodName() {
        return foodName;
    }

    /**
     * @param tool 
     * @param person 
     * @return
     */


    public int getResources(Tool tool, Person person) {
        return 0;
    }

    @Override
    public void nextDay(Season season){
        outcome.clear();
        age++;
        if(isFed){
            health += 5;
        }
        else {
            health -= 7;
        }
        if(isSeek){
            health -= 3;
        }
        if(health < 0){
            health = 0;
        }
        if(health > 100){
            health = 100;
        }
        Random random = new Random();
        double d = random.nextDouble();
        d = d * Math.log10(health);
        if ( d < 0.1 ){
            isSeek = true;
        }
        if(age < matureAge)
            return;
        if(age == matureAge)
            age = 0;
        if(health >= 50){
            if(isFed){
                for(Item item : sampleOutcomes) {
                    if(item instanceof Food)
                        outcome.add(new ItemStack(new Food(item.getName(), item.getSize(), item.getPrice(), ((Food)item).getEffect())));
                    if (item instanceof Resource)
                        outcome.add(new ItemStack(new Resource(item.getName(),item.getSize(),item.getPrice())));
                }
                isFed = false;
            }
        }
    }

    @Override
    public String toString(){
        String s = getName() + "    Sickness: " + isSeek + "    health: " + health + "   is Fed: " + isFed;
        if( matureAge > 1 ) s = s + "\n  days to give outcome: " + (matureAge - age) ;
        s = s +"\n tool" + toolName + "\n currently gainAble Outcomes:";
        for (int i = 0; i < outcome.size(); i++)
            s = s + " " +outcome.get(i);
        return s;
    }

    public void Feed() {
        isFed = true;
    }
}