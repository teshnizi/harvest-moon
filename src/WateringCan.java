
import java.util.*;

/**
 * 
 */
public class WateringCan extends Tool {

    /**
     * Default constructor
     */
    public WateringCan(String name , int waterLevel , int maxWaterLevel, int health, int breakingProbablity, AbilityChange abilityChange , int type) {
        super(name, 2, health, breakingProbablity, abilityChange);
        this.maxWaterLevel=  maxWaterLevel;
        this.waterLevel = waterLevel;
        newbuilders = new ArrayList<>();
        repairbuilders = new ArrayList<>();
        this.type = type;;
        if (type == 1){

            newprice = 50;
            newbuilders.add(new Builder(5,Producer.wood(0),newprice));

            repairprice = 5;
            repairbuilders.add(new Builder(2,Producer.wood(0),repairprice));
        }
        if (type == 2){

            newprice = 200;
            newbuilders.add(new Builder(4,Producer.wood(1),newprice));

            repairprice = 20;
            repairbuilders.add(new Builder(2,Producer.wood(1),repairprice));
        }
        if (type == 3){

            newprice = 500;
            newbuilders.add(new Builder(3,Producer.wood(2),newprice));

            repairprice = 50;
            repairbuilders.add(new Builder(1,Producer.wood(2),repairprice));
        }
        if (type == 4){

            newprice = 2000;
            newbuilders.add(new Builder(2,Producer.wood(3),newprice));

            repairprice = 200;
            repairbuilders.add(new Builder(1,Producer.wood(3),repairprice));
        }
        if (type == 5){

            newprice = 50;
            newbuilders.add(new Builder(5,Producer.stone(0),newprice));

            repairprice = 5;
            repairbuilders.add(new Builder(2,Producer.stone(0),repairprice));
        }
        if (type == 6){

            newprice = 200;
            newbuilders.add(new Builder(4,Producer.stone(1),newprice));

            repairprice = 20;
            repairbuilders.add(new Builder(2,Producer.stone(1),repairprice));
        }
        if (type == 7){

            newprice = 500;
            newbuilders.add(new Builder(3,Producer.wood(2),newprice));

            repairprice = 50;
            repairbuilders.add(new Builder(1,Producer.wood(2),repairprice));
        }
        if (type == 8){

            newprice = 2000;
            newbuilders.add(new Builder(2,Producer.wood(3),newprice));

            repairprice = 200;
            repairbuilders.add(new Builder(1,Producer.wood(3),repairprice));
        }
        setPrice(newprice);
    }

    public void fill(){
        this.waterLevel = maxWaterLevel;
    }

    private int maxWaterLevel;
    private int waterLevel;

    public int getWaterLevel() {
        return waterLevel;
    }
    public void water(){
        if(waterLevel > 0)
            waterLevel--;
        health -=((type -1)%4 + 1)*5;
        abilityChange.affect();
    }
    @Override
    public String status(){
        String kind = " ";
        String s;
        int rang = 0;
        int num = 0;
        String broken;
        if (health > 0)
            broken = " Not Broken!";
        else
            broken = "Broken!";
        if (type == 1)
        {
            kind = "Small";
            rang = 1;
            num = 40 ;
        }
        if (type == 2)
        {
            kind = "Old";
            rang = 2;
            num = 30 ;
        }
        if (type == 3)
        {
            kind = "Pine";
            rang = 3;
            num = 20 ;
        }
        if (type == 4)
        {
            kind = "Oak";
            rang = 9;
            num = 10 ;
        }
        if (type == 5)
        {
            kind = "Stone";
            rang = 2;
            num = 80 ;
        }
        if (type == 6)
        {
            kind = "Iron";
            rang = 4;
            num = 60 ;
        }
        if (type == 7)
        {
            kind = "Silver";
            rang = 8;
            num = 40 ;
        }
        if (type == 8)
        {
            kind = "Adantanium";
            rang = 16;
            num = 20 ;
        }
        s = "A "+ kind +" sprinkler. With this sprinkler you\ncan water " +rang+ " range in one go. It has the capacity to water " + waterLevel+" fields of the farm.\nWater level: "+waterLevel+ " of " +maxWaterLevel +"\nEnergy required for each use: " + num +"\n"+"Health: "+ health + "\n"  +  broken;
        return s;
    }
    @Override
    public ArrayList<String> madeof(){
        ArrayList<String > s = new ArrayList<>();
        if (type == 1)
            s.add("this is Small  Watering Can and made of");
        if (type == 2)
            s.add("this is Old Watering Can and made of");
        if (type == 3)
            s.add("this is Pine Watering Can and made of");
        if (type == 4)
            s.add("this is Oak  Watering Can and made of");
        if (type == 5)
            s.add("this is Stone Watering Can and made of");
        if (type == 6)
            s.add("this is Iron Watering Can and made of");
        if (type == 7)
            s.add("this is Silver Watering Can and made of");
        if (type == 8)
            s.add("this is Adantanium Watering Can and made of");
        for (Builder builder : newbuilders)
            s.add(builder.getItems().getName() + " x" + builder.getNum());
        s.add("price is: "+newbuilders.get(0).getPrice());
        return  s;
    }
}