
import java.util.*;

/**
 * 
 */
public abstract class Environment extends JA {

    /**
     * Default constructor
     */
    public Environment(String name) {
        super(name);
        places = new ArrayList<>();
    }

    ArrayList<Place> places;

    public ArrayList<Place> getPlaces() {
        return places;
    }

    public ArrayList<String> listOfPlaces(){
        ArrayList<String> ret = new ArrayList<>();
        for(JA ja: neighbours)
            ret.add(ja.getName());
        for(Place place: places)
            ret.add(place.getName());
        return ret;
    }

    @Override
    public JA findJA(String name) {
        for(JA neighbour : neighbours){
            if(neighbour.getName().equals(name))
                return neighbour;
        }
        for(Place place : places){
            if(place.getName().equals(name))
                return place;
        }
        return null;
    }

}