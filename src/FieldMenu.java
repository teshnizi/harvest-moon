import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tigerous215 on 5/12/2017.
 */
public class FieldMenu extends Menu {

    Field field;

    public FieldMenu(Field field){
        this.field = field;
    }

    @Override
    public ArrayList<String> whereAmI() {
        ArrayList<String> s = new ArrayList<>();
        s.add("You are in Field: " +  field.getName());
        s.add("Plowed: " + field.plowed);
        s.add("Maximum plants: " + field.maximumSize);
        s.add("0.Plant new Crop");
        s.add("1.Plow field");
        s.add("2.Destroy plants");
        s.add("List of Plants: ");

        //!!!!  if you change this k+3 , go change x-3 at down of this page   !!!!
        for(int k = 0 ; k < field.getCrops().size(); k++){
            s.add((k+3) + "." + field.getCrops().get(k).getName());
        }
        s.add("-----------");
        s.add((field.getCrops().size() + 3) + ".Expand field");
        return s;
    }

    @Override
    public Menu getCommand(int x, Person person) {
        if(x == 0){
            if(!field.plowed[0]){
                System.out.println("You should plow this field first!");
                return null;
            }
            try {
                System.out.println("Choose a seed to plant: ");
                Storage backPack = person.getBackPack();
                ItemStack seedStack = backPack.getItemStackFromPlayer();
                Seed sampleSeed = (Seed)seedStack.getItems().get(0);
                person.getEnergy().changeCurrentValue(-1);
                field.plantCrop(sampleSeed.getCrop());
                System.out.println(seedStack.getItems().get(0).getName() + " Planted!");
                seedStack.getItems().remove(0);

            }
            catch (Exception e){
                System.out.println("BadCommand!");

            }
            return null;
        }

        if(x == 1){
            try {
                System.out.println("Choose a shovel for plowing: ");
                Storage backPack = person.getBackPack();
                ItemStack shovelStack = backPack.getItemStackFromPlayer();
                Shovel shovel = (Shovel) shovelStack.getItems().get(0);
                if(!shovel.canUseTool()){
                    System.out.println("You are too tired!");
                    return null;
                }
                shovel.plow();

                field.plowed[0] = true;
                System.out.println(field.getName() + " Plowed!");
            }
            catch (Exception e){
                System.out.println("BadCommand!");
            }
            return null;
        }

        if(x == 2){
            try{
                System.out.println("Are you sure? If yes enter \"YES\"");
                Scanner scanner = new Scanner(System.in);
                String s = scanner.next();
                if(!s.equals("YES"))
                    return null;
                field.plowed[0] = false;
                field.getCrops().clear();
            }
            catch (Exception e){
                System.out.println("BadCommand!");
            }
            return null;
        }

        if(x == field.getCrops().size()+3){
            System.out.println("Materials needed for expanding field by 1 block:");
            System.out.println("Stone x5, Branch x5, $100");
            if(person.getMoney() < 100){
                System.out.println("Not enough money!");
                return null;
            }
            System.out.println("Choose Stone x5:");
            ItemStack itemStack = person.getBackPack().getItemStackFromPlayer();
            if (!((itemStack.getItems().get(0) instanceof Stone))){
                System.out.println("You should choose Stone!");
                return null;
            }
            if(! (((Stone)itemStack.getItems().get(0)).getType() == 0)){
                System.out.println("You should choose Stone!");
                return null;
            }
            System.out.println("Choose Branch x5:");
            ItemStack itemStack2 = person.getBackPack().getItemStackFromPlayer();
            if (!((itemStack2.getItems().get(0) instanceof Wood))){
                System.out.println("You should choose Branch!");
                return null;
            }
            if(! (((Wood)itemStack2.getItems().get(0)).getType() == 0)){
                System.out.println("You should choose Branch!");
                return null;
            }

            person.takeMoney(100);
            person.getBackPack().takeItem("Branch",5);
            person.getBackPack().takeItem("Stone",5);
            field.maximumSize++;
            System.out.println("Field expanded!");
            return null;
        }

        //if we reach here a plant has been chosen!
        try{
            Crop crop = field.crops.get(x-3);
            return new CropMenu(crop, field);
        }
        catch (Exception e){
            System.out.println("Bad Command!");
        }
        return null;
    }
}
