
import java.util.*;

/**
 * 
 */
public class FishingRod extends Tool {

    /**
     * Default constructor
     */
    public FishingRod(String name, int health, int breakingProbablity, AbilityChange abilityChange , int type) {
        super(name,4,health, breakingProbablity, abilityChange);
        newbuilders = new ArrayList<>();
        repairbuilders = new ArrayList<>();
        this.type = type;
        if (type == 1){

            newprice = 100;
            newbuilders.add(new Builder(15,Producer.wood(0),newprice));
            newbuilders.add(new Builder(2,Producer.thread(),newprice));

            repairprice = 10;
            repairbuilders.add(new Builder(5,Producer.wood(0),repairprice));
            repairbuilders.add(new Builder(1,Producer.thread(),repairprice));
        }
        if (type == 2){

            newprice = 300;
            newbuilders.add(new Builder(10,Producer.wood(1),newprice));
            newbuilders.add(new Builder(3,Producer.thread(),newprice));

            repairprice = 30;
            repairbuilders.add(new Builder(4,Producer.wood(1),repairprice));
            repairbuilders.add(new Builder(2,Producer.thread(),repairprice));
        }
        if (type == 3){

            newprice = 800;
            newbuilders.add(new Builder(6,Producer.wood(2),newprice));
            newbuilders.add(new Builder(4,Producer.thread(),newprice));

            repairprice = 80;
            repairbuilders.add(new Builder(3,Producer.wood(2),repairprice));
            repairbuilders.add(new Builder(1,Producer.thread(),repairprice));
        }
        if (type == 4){

            newprice = 2000;
            newbuilders.add(new Builder(3,Producer.wood(3),newprice));
            newbuilders.add(new Builder(1,Producer.thread(),newprice));

            repairprice = 200;
            repairbuilders.add(new Builder(2,Producer.wood(3),repairprice));
            repairbuilders.add(new Builder(1,Producer.thread(),repairprice));
        }
        setPrice(newprice);
    }

    public int getType() {
        return type;
    }
    public void fish(){
        health -= (5 - type)*5;
        abilityChange.affect();
    }

    @Override
    public String status() {
        String broken;
        if (health > 0)
            broken = " Not Broken!";
        else
            broken = "Broken!";
        if (type == 1)
            return "this is Small Fishing Rod \n chance of getting fish is : 40 percent \n It will cost 40 Energy Points after every use.\n"+"Health: "+ health + "\n"  + broken;
        else if (type == 2)
            return  "this is Old Fishing Rod \n chance of getting fish is : 60 percent \n It will cost 30 Energy Points after every use.\n" +"Health: "+ health + "\n"   + broken;
        else if (type == 3)
            return "this is Pine Fishing Rod \n chance of getting fish is : 80 percent \n It will cost 20 Energy Points after every use.\n"+"Health: "+ health + "\n"    + broken;
        else if (type == 4)
            return "this is Oak Fishing Rod \n chance of getting fish is : 100 percent \nIt will cost 10 Energy Points after every use.\n" +"Health: "+ health + "\n"  + broken;
        else
            return null;
    }
    @Override
    public ArrayList<String> madeof(){
        ArrayList<String > s = new ArrayList<>();
        if (type == 1)
            s.add("this is Small Fishing Rod and made of");
        if (type == 2)
            s.add("this is Old Fishing Rod and made of");
        if (type == 3)
            s.add("this is Pine Fishing Rod and made of");
        if (type == 4)
            s.add("this is Oak Fishing Rod and made of");
        for (Builder builder : newbuilders)
            s.add(builder.getItems().getName() + " x" + builder.getNum());
        s.add("price is: "+newbuilders.get(0).getPrice());
        return  s;
    }
}